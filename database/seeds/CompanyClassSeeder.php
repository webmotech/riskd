<?php

use Illuminate\Database\Seeder;

class CompanyClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company_class')->insert(['id'=>111,'company_group_id'=>11,'name'=>'Nursery Production (Under Cover)']);
        DB::table('company_class')->insert(['id'=>112,'company_group_id'=>11,'name'=>'Nursery Production (Outdoors)']);
        DB::table('company_class')->insert(['id'=>113,'company_group_id'=>11,'name'=>'Turf Growing']);
        DB::table('company_class')->insert(['id'=>114,'company_group_id'=>11,'name'=>'Floriculture Production (Under Cover)']);
        DB::table('company_class')->insert(['id'=>115,'company_group_id'=>11,'name'=>'Floriculture Production (Outdoors)']);

        DB::table('company_class')->insert(['id'=>121,'company_group_id'=>12,'name'=>'Mushroom Growing']);
        DB::table('company_class')->insert(['id'=>122,'company_group_id'=>12,'name'=>'Vegetable Growing (Under Cover)']);
        DB::table('company_class')->insert(['id'=>123,'company_group_id'=>12,'name'=>'Vegetable Growing (Outdoors)']);

        DB::table('company_class')->insert(['id'=>131,'company_group_id'=>13,'name'=>'Grape Growing']);
        DB::table('company_class')->insert(['id'=>132,'company_group_id'=>13,'name'=>'Kiwifruit Growing']);
        DB::table('company_class')->insert(['id'=>133,'company_group_id'=>13,'name'=>'Berry Fruit Growing']);
        DB::table('company_class')->insert(['id'=>134,'company_group_id'=>13,'name'=>'Apple and Pear Growing']);
        DB::table('company_class')->insert(['id'=>135,'company_group_id'=>13,'name'=>'Stone Fruit Growing']);
        DB::table('company_class')->insert(['id'=>136,'company_group_id'=>13,'name'=>'Citrus Fruit Growing']);
        DB::table('company_class')->insert(['id'=>137,'company_group_id'=>13,'name'=>'Olive Growing']);
        DB::table('company_class')->insert(['id'=>139,'company_group_id'=>13,'name'=>'Other Fruit and Tree Nut Growing']);

        DB::table('company_class')->insert(['id'=>141,'company_group_id'=>14,'name'=>'Sheep Farming (Specialised)']);
        DB::table('company_class')->insert(['id'=>142,'company_group_id'=>14,'name'=>'Beef Cattle Farming (Specialised)']);
        DB::table('company_class')->insert(['id'=>143,'company_group_id'=>14,'name'=>'Beef Cattle Feedlots (Specialised)']);
        DB::table('company_class')->insert(['id'=>144,'company_group_id'=>14,'name'=>'Sheep-Beef Cattle Farming']);
        DB::table('company_class')->insert(['id'=>145,'company_group_id'=>14,'name'=>'Grain-Sheep or Grain-Beef Cattle Farming']);
        DB::table('company_class')->insert(['id'=>146,'company_group_id'=>14,'name'=>'Rice Growing']);
        DB::table('company_class')->insert(['id'=>149,'company_group_id'=>14,'name'=>'Other Grain Growing']);

        DB::table('company_class')->insert(['id'=>151,'company_group_id'=>15,'name'=>'Sugar Cane Growing']);
        DB::table('company_class')->insert(['id'=>152,'company_group_id'=>15,'name'=>'Cotton Growing']);
        DB::table('company_class')->insert(['id'=>159,'company_group_id'=>15,'name'=>'Other Crop Growing n.e.c.']);

        DB::table('company_class')->insert(['id'=>160,'company_group_id'=>16,'name'=>'Dairy Cattle Farming']);

        DB::table('company_class')->insert(['id'=>171,'company_group_id'=>17,'name'=>'Poultry Farming (Meat)']);
        DB::table('company_class')->insert(['id'=>172,'company_group_id'=>17,'name'=>'Poultry Farming (Eggs)']);

        DB::table('company_class')->insert(['id'=>180,'company_group_id'=>18,'name'=>'Deer Farming']);

        DB::table('company_class')->insert(['id'=>191,'company_group_id'=>19,'name'=>'Horse Farming']);
        DB::table('company_class')->insert(['id'=>192,'company_group_id'=>19,'name'=>'Pig Farming']);
        DB::table('company_class')->insert(['id'=>193,'company_group_id'=>19,'name'=>'Beekeeping']);
        DB::table('company_class')->insert(['id'=>199,'company_group_id'=>19,'name'=>'Other Livestock Farming n.e.c.']);


        DB::table('company_class')->insert(['id'=>201,'company_group_id'=>20,'name'=>'Offshore Longline and Rack Aquaculture']);
        DB::table('company_class')->insert(['id'=>202,'company_group_id'=>20,'name'=>'Offshore Caged Aquaculture']);
        DB::table('company_class')->insert(['id'=>203,'company_group_id'=>20,'name'=>'Onshore Aquaculture']);


        DB::table('company_class')->insert(['id'=>301,'company_group_id'=>30,'name'=>'Forestry']);
        DB::table('company_class')->insert(['id'=>302,'company_group_id'=>30,'name'=>'Logging']);


        DB::table('company_class')->insert(['id'=>411,'company_group_id'=>41,'name'=>'Rock Lobster and Crab Potting']);
        DB::table('company_class')->insert(['id'=>412,'company_group_id'=>41,'name'=>'Prawn Fishing']);
        DB::table('company_class')->insert(['id'=>413,'company_group_id'=>41,'name'=>'Line Fishing']);
        DB::table('company_class')->insert(['id'=>414,'company_group_id'=>41,'name'=>'Fish Trawling, Seining and Netting']);
        DB::table('company_class')->insert(['id'=>419,'company_group_id'=>41,'name'=>'Other Fishing']);

        DB::table('company_class')->insert(['id'=>420,'company_group_id'=>42,'name'=>'Hunting and Trapping']);


        DB::table('company_class')->insert(['id'=>510,'company_group_id'=>51,'name'=>'Forestry Support Services']);

        DB::table('company_class')->insert(['id'=>521,'company_group_id'=>52,'name'=>'Cotton Ginning']);
        DB::table('company_class')->insert(['id'=>522,'company_group_id'=>52,'name'=>'Shearing Services']);
        DB::table('company_class')->insert(['id'=>529,'company_group_id'=>52,'name'=>'Other Agriculture and Fishing Support Services']);




        DB::table('company_class')->insert(['id'=>600,'company_group_id'=>60,'name'=>'Coal Mining']);


        DB::table('company_class')->insert(['id'=>700,'company_group_id'=>70,'name'=>'Oil and Gas Extraction']);


        DB::table('company_class')->insert(['id'=>801,'company_group_id'=>80,'name'=>'Iron Ore Mining']);
        DB::table('company_class')->insert(['id'=>802,'company_group_id'=>80,'name'=>'Bauxite Mining']);
        DB::table('company_class')->insert(['id'=>803,'company_group_id'=>80,'name'=>'Copper Ore Mining']);
        DB::table('company_class')->insert(['id'=>804,'company_group_id'=>80,'name'=>'Gold Ore Mining']);
        DB::table('company_class')->insert(['id'=>805,'company_group_id'=>80,'name'=>'Mineral Sand Mining']);
        DB::table('company_class')->insert(['id'=>806,'company_group_id'=>80,'name'=>'Nickel Ore Mining']);
        DB::table('company_class')->insert(['id'=>807,'company_group_id'=>80,'name'=>'Silver-Lead-Zinc Ore Mining']);
        DB::table('company_class')->insert(['id'=>809,'company_group_id'=>80,'name'=>'Other Metal Ore Mining']);


        DB::table('company_class')->insert(['id'=>911,'company_group_id'=>91,'name'=>'Gravel and Sand Quarrying']);
        DB::table('company_class')->insert(['id'=>919,'company_group_id'=>91,'name'=>'Other Construction Material Mining']);

        DB::table('company_class')->insert(['id'=>990,'company_group_id'=>99,'name'=>'Other Non-Metallic Mineral Mining and Quarrying']);


        DB::table('company_class')->insert(['id'=>1011,'company_group_id'=>101,'name'=>'Petroleum Exploration']);
        DB::table('company_class')->insert(['id'=>1012,'company_group_id'=>101,'name'=>'Mineral Exploration']);

        DB::table('company_class')->insert(['id'=>1090,'company_group_id'=>109,'name'=>'Other Mining Support Services']);




        DB::table('company_class')->insert(['id'=>1111,'company_group_id'=>111,'name'=>'Meat Processing']);
        DB::table('company_class')->insert(['id'=>1112,'company_group_id'=>111,'name'=>'Poultry Processing']);
        DB::table('company_class')->insert(['id'=>1113,'company_group_id'=>111,'name'=>'Cured Meat and Smallgoods Manufacturing']);

        DB::table('company_class')->insert(['id'=>1120,'company_group_id'=>112,'name'=>'Seafood Processing']);

        DB::table('company_class')->insert(['id'=>1131,'company_group_id'=>113,'name'=>'Milk and Cream Processing']);
        DB::table('company_class')->insert(['id'=>1132,'company_group_id'=>113,'name'=>'Ice Cream Manufacturing']);
        DB::table('company_class')->insert(['id'=>1133,'company_group_id'=>113,'name'=>'Cheese and Other Dairy Product Manufacturing']);

        DB::table('company_class')->insert(['id'=>1140,'company_group_id'=>114,'name'=>'Fruit and Vegetable Processing']);

        DB::table('company_class')->insert(['id'=>1150,'company_group_id'=>115,'name'=>'Oil and Fat Manufacturing']);

        DB::table('company_class')->insert(['id'=>1161,'company_group_id'=>116,'name'=>'Grain Mill Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>1162,'company_group_id'=>116,'name'=>'Cereal, Pasta and Baking Mix Manufacturing']);

        DB::table('company_class')->insert(['id'=>1171,'company_group_id'=>117,'name'=>'Bread Manufacturing (Factory based)']);
        DB::table('company_class')->insert(['id'=>1172,'company_group_id'=>117,'name'=>'Cake and Pastry Manufacturing (Factory based)']);
        DB::table('company_class')->insert(['id'=>1173,'company_group_id'=>117,'name'=>'Biscuit Manufacturing (Factory based)']);
        DB::table('company_class')->insert(['id'=>1174,'company_group_id'=>117,'name'=>'Bakery Product Manufacturing (Non-factory based)']);

        DB::table('company_class')->insert(['id'=>1181,'company_group_id'=>118,'name'=>'Sugar Manufacturing']);
        DB::table('company_class')->insert(['id'=>1182,'company_group_id'=>118,'name'=>'Confectionery Manufacturing']);

        DB::table('company_class')->insert(['id'=>1191,'company_group_id'=>119,'name'=>'Potato, Corn and Other Crisp Manufacturing']);
        DB::table('company_class')->insert(['id'=>1192,'company_group_id'=>119,'name'=>'Prepared Animal and Bird Feed Manufacturing']);
        DB::table('company_class')->insert(['id'=>1199,'company_group_id'=>119,'name'=>'Other Food Product Manufacturing n.e.c.']);


        DB::table('company_class')->insert(['id'=>1211,'company_group_id'=>121,'name'=>'Soft Drink, Cordial and Syrup Manufacturing']);
        DB::table('company_class')->insert(['id'=>1212,'company_group_id'=>121,'name'=>'Beer Manufacturing']);
        DB::table('company_class')->insert(['id'=>1213,'company_group_id'=>121,'name'=>'Spirit Manufacturing']);
        DB::table('company_class')->insert(['id'=>1214,'company_group_id'=>121,'name'=>'Wine and Other Alcoholic Beverage Manufacturing']);

        DB::table('company_class')->insert(['id'=>1220,'company_group_id'=>122,'name'=>'Cigarette and Tobacco Product Manufacturing']);


        DB::table('company_class')->insert(['id'=>1311,'company_group_id'=>131,'name'=>'Wool Scouring']);
        DB::table('company_class')->insert(['id'=>1312,'company_group_id'=>131,'name'=>'Natural Textile Manufacturing']);
        DB::table('company_class')->insert(['id'=>1313,'company_group_id'=>131,'name'=>'Synthetic Textile Manufacturing']);

        DB::table('company_class')->insert(['id'=>1320,'company_group_id'=>132,'name'=>'Leather Tanning, Fur Dressing and Leather Product Manufacturing']);

        DB::table('company_class')->insert(['id'=>1331,'company_group_id'=>133,'name'=>'Textile Floor Covering Manufacturing']);
        DB::table('company_class')->insert(['id'=>1332,'company_group_id'=>133,'name'=>'Rope, Cordage and Twine Manufacturing']);
        DB::table('company_class')->insert(['id'=>1333,'company_group_id'=>133,'name'=>'Cut and Sewn Textile Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>1334,'company_group_id'=>133,'name'=>'Textile Finishing and Other Textile Product Manufacturing']);

        DB::table('company_class')->insert(['id'=>1340,'company_group_id'=>134,'name'=>'Knitted Product Manufacturing']);

        DB::table('company_class')->insert(['id'=>1351,'company_group_id'=>135,'name'=>'Clothing Manufacturing']);
        DB::table('company_class')->insert(['id'=>1352,'company_group_id'=>135,'name'=>'Footwear Manufacturing']);


        DB::table('company_class')->insert(['id'=>1411,'company_group_id'=>141,'name'=>'Log Sawmilling']);
        DB::table('company_class')->insert(['id'=>1412,'company_group_id'=>141,'name'=>'Wood Chipping']);
        DB::table('company_class')->insert(['id'=>1413,'company_group_id'=>141,'name'=>'Timber Resawing and Dressing']);

        DB::table('company_class')->insert(['id'=>1491,'company_group_id'=>149,'name'=>'Prefabricated Wooden Building Manufacturing']);
        DB::table('company_class')->insert(['id'=>1492,'company_group_id'=>149,'name'=>'Wooden Structural Fitting and Component Manufacturing']);
        DB::table('company_class')->insert(['id'=>1493,'company_group_id'=>149,'name'=>'Veneer and Plywood Manufacturing']);
        DB::table('company_class')->insert(['id'=>1494,'company_group_id'=>149,'name'=>'Reconstituted Wood Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>1499,'company_group_id'=>149,'name'=>'Other Wood Product Manufacturing n.e.c.']);


        DB::table('company_class')->insert(['id'=>1510,'company_group_id'=>151,'name'=>'Pulp, Paper and Paperboard Manufacturing']);

        DB::table('company_class')->insert(['id'=>1521,'company_group_id'=>152,'name'=>'Corrugated Paperboard and Paperboard Container Manufacturing']);
        DB::table('company_class')->insert(['id'=>1522,'company_group_id'=>152,'name'=>'Paper Bag Manufacturing']);
        DB::table('company_class')->insert(['id'=>1523,'company_group_id'=>152,'name'=>'Paper Stationery Manufacturing']);
        DB::table('company_class')->insert(['id'=>1524,'company_group_id'=>152,'name'=>'Sanitary Paper Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>1529,'company_group_id'=>152,'name'=>'Other Converted Paper Product Manufacturing']);


        DB::table('company_class')->insert(['id'=>1611,'company_group_id'=>161,'name'=>'Printing']);
        DB::table('company_class')->insert(['id'=>1612,'company_group_id'=>161,'name'=>'Printing Support Services']);

        DB::table('company_class')->insert(['id'=>1620,'company_group_id'=>162,'name'=>'Reproduction of Recorded Media']);


        DB::table('company_class')->insert(['id'=>1701,'company_group_id'=>170,'name'=>'Petroleum Refining and Petroleum Fuel Manufacturing']);
        DB::table('company_class')->insert(['id'=>1709,'company_group_id'=>170,'name'=>'Other Petroleum and Coal Product Manufacturing']);


        DB::table('company_class')->insert(['id'=>1811,'company_group_id'=>181,'name'=>'Industrial Gas Manufacturing']);
        DB::table('company_class')->insert(['id'=>1812,'company_group_id'=>181,'name'=>'Basic Organic Chemical Manufacturing']);
        DB::table('company_class')->insert(['id'=>1813,'company_group_id'=>181,'name'=>'Basic Inorganic Chemical Manufacturing']);

        DB::table('company_class')->insert(['id'=>1821,'company_group_id'=>182,'name'=>'Synthetic Resin and Synthetic Rubber Manufacturing']);
        DB::table('company_class')->insert(['id'=>1829,'company_group_id'=>182,'name'=>'Other Basic Polymer Manufacturing']);

        DB::table('company_class')->insert(['id'=>1831,'company_group_id'=>183,'name'=>'Fertiliser Manufacturing']);
        DB::table('company_class')->insert(['id'=>1832,'company_group_id'=>183,'name'=>'Pesticide Manufacturing']);

        DB::table('company_class')->insert(['id'=>1841,'company_group_id'=>184,'name'=>'Human Pharmaceutical and Medicinal Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>1842,'company_group_id'=>184,'name'=>'Veterinary Pharmaceutical and Medicinal Product Manufacturing']);

        DB::table('company_class')->insert(['id'=>1851,'company_group_id'=>185,'name'=>'Cleaning Compound Manufacturing']);
        DB::table('company_class')->insert(['id'=>1852,'company_group_id'=>185,'name'=>'Cosmetic and Toiletry Preparation Manufacturing']);

        DB::table('company_class')->insert(['id'=>1891,'company_group_id'=>189,'name'=>'Photographic Chemical Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>1892,'company_group_id'=>189,'name'=>'Explosive Manufacturing']);
        DB::table('company_class')->insert(['id'=>1899,'company_group_id'=>189,'name'=>'Other Basic Chemical Product Manufacturing n.e.c.']);


        DB::table('company_class')->insert(['id'=>1911,'company_group_id'=>191,'name'=>'Polymer Film and Sheet Packaging Material Manufacturing']);
        DB::table('company_class')->insert(['id'=>1912,'company_group_id'=>191,'name'=>'Rigid and Semi-Rigid Polymer Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>1913,'company_group_id'=>191,'name'=>'Polymer Foam Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>1914,'company_group_id'=>191,'name'=>'Tyre Manufacturing']);
        DB::table('company_class')->insert(['id'=>1915,'company_group_id'=>191,'name'=>'Adhesive Manufacturing']);
        DB::table('company_class')->insert(['id'=>1916,'company_group_id'=>191,'name'=>'Paint and Coatings Manufacturing']);
        DB::table('company_class')->insert(['id'=>1919,'company_group_id'=>191,'name'=>'Other Polymer Product Manufacturing']);

        DB::table('company_class')->insert(['id'=>1920,'company_group_id'=>192,'name'=>'Natural Rubber Product Manufacturing']);


        DB::table('company_class')->insert(['id'=>2010,'company_group_id'=>201,'name'=>'Glass and Glass Product Manufacturing']);

        DB::table('company_class')->insert(['id'=>2021,'company_group_id'=>202,'name'=>'Clay Brick Manufacturing']);
        DB::table('company_class')->insert(['id'=>2029,'company_group_id'=>202,'name'=>'Other Ceramic Product Manufacturing']);

        DB::table('company_class')->insert(['id'=>2031,'company_group_id'=>203,'name'=>'Cement and Lime Manufacturing']);
        DB::table('company_class')->insert(['id'=>2032,'company_group_id'=>203,'name'=>'Plaster Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>2033,'company_group_id'=>203,'name'=>'Ready-Mixed Concrete Manufacturing']);
        DB::table('company_class')->insert(['id'=>2034,'company_group_id'=>203,'name'=>'Concrete Product Manufacturing']);

        DB::table('company_class')->insert(['id'=>2090,'company_group_id'=>209,'name'=>'Other Non-Metallic Mineral Product Manufacturing']);


        DB::table('company_class')->insert(['id'=>2110,'company_group_id'=>211,'name'=>'Iron Smelting and Steel Manufacturing']);

        DB::table('company_class')->insert(['id'=>2121,'company_group_id'=>212,'name'=>'Iron and Steel Casting']);
        DB::table('company_class')->insert(['id'=>2122,'company_group_id'=>212,'name'=>'Steel Pipe and Tube Manufacturing']);

        DB::table('company_class')->insert(['id'=>2131,'company_group_id'=>213,'name'=>'Alumina Production']);
        DB::table('company_class')->insert(['id'=>2132,'company_group_id'=>213,'name'=>'Aluminium Smelting']);
        DB::table('company_class')->insert(['id'=>2133,'company_group_id'=>213,'name'=>'Copper, Silver, Lead and Zinc Smelting and Refining']);
        DB::table('company_class')->insert(['id'=>2139,'company_group_id'=>213,'name'=>'Other Basic Non-Ferrous Metal Manufacturing']);

        DB::table('company_class')->insert(['id'=>2141,'company_group_id'=>214,'name'=>'Non-Ferrous Metal Casting']);
        DB::table('company_class')->insert(['id'=>2142,'company_group_id'=>214,'name'=>'Aluminium Rolling, Drawing, Extruding']);
        DB::table('company_class')->insert(['id'=>2149,'company_group_id'=>214,'name'=>'Other Basic Non-Ferrous Metal Product Manufacturing']);


        DB::table('company_class')->insert(['id'=>2210,'company_group_id'=>221,'name'=>'Iron and Steel Forging']);

        DB::table('company_class')->insert(['id'=>2221,'company_group_id'=>222,'name'=>'Structural Steel Fabricating']);
        DB::table('company_class')->insert(['id'=>2222,'company_group_id'=>222,'name'=>'Prefabricated Metal Building Manufacturing']);
        DB::table('company_class')->insert(['id'=>2223,'company_group_id'=>222,'name'=>'Architectural Aluminium Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>2224,'company_group_id'=>222,'name'=>'Metal Roof and Guttering Manufacturing (except Aluminium)']);
        DB::table('company_class')->insert(['id'=>2229,'company_group_id'=>222,'name'=>'Other Structural Metal Product Manufacturing']);

        DB::table('company_class')->insert(['id'=>2231,'company_group_id'=>223,'name'=>'Boiler, Tank and Other Heavy Gauge Metal Container Manufacturing']);
        DB::table('company_class')->insert(['id'=>2239,'company_group_id'=>223,'name'=>'Other Metal Container Manufacturing']);


        DB::table('company_class')->insert(['id'=>2240,'company_group_id'=>224,'name'=>'Sheet Metal Product Manufacturing (except Metal Structural and Container']);

        DB::table('company_class')->insert(['id'=>2291,'company_group_id'=>229,'name'=>'Spring and Wire Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>2292,'company_group_id'=>229,'name'=>'Nut, Bolt, Screw and Rivet Manufacturing']);
        DB::table('company_class')->insert(['id'=>2293,'company_group_id'=>229,'name'=>'Metal Coating and Finishing']);
        DB::table('company_class')->insert(['id'=>2299,'company_group_id'=>229,'name'=>'Other Fabricated Metal Product Manufacturing n.e.c.']);


        DB::table('company_class')->insert(['id'=>2311,'company_group_id'=>231,'name'=>'Motor Vehicle Manufacturing']);
        DB::table('company_class')->insert(['id'=>2312,'company_group_id'=>231,'name'=>'Motor Vehicle Body and Trailer Manufacturing']);
        DB::table('company_class')->insert(['id'=>2313,'company_group_id'=>231,'name'=>'Automotive Electrical Component Manufacturing']);
        DB::table('company_class')->insert(['id'=>2319,'company_group_id'=>231,'name'=>'Other Motor Vehicle Parts Manufacturing']);

        DB::table('company_class')->insert(['id'=>2391,'company_group_id'=>239,'name'=>'Shipbuilding and Repair Services']);
        DB::table('company_class')->insert(['id'=>2392,'company_group_id'=>239,'name'=>'Boatbuilding and Repair Services']);
        DB::table('company_class')->insert(['id'=>2393,'company_group_id'=>239,'name'=>'Railway Rolling Stock Manufacturing and Repair Services']);
        DB::table('company_class')->insert(['id'=>2394,'company_group_id'=>239,'name'=>'Aircraft Manufacturing and Repair Services']);
        DB::table('company_class')->insert(['id'=>2399,'company_group_id'=>239,'name'=>'Other Transport Equipment Manufacturing n.e.c.']);


        DB::table('company_class')->insert(['id'=>2411,'company_group_id'=>241,'name'=>'Photographic, Optical and Ophthalmic Equipment Manufacturing']);
        DB::table('company_class')->insert(['id'=>2412,'company_group_id'=>241,'name'=>'Medical and Surgical Equipment Manufacturing']);
        DB::table('company_class')->insert(['id'=>2419,'company_group_id'=>241,'name'=>'Other Professional and Scientific Equipment Manufacturing']);

        DB::table('company_class')->insert(['id'=>2421,'company_group_id'=>242,'name'=>'Computer and Electronic Office Equipment Manufacturing']);
        DB::table('company_class')->insert(['id'=>2422,'company_group_id'=>242,'name'=>'Communication Equipment Manufacturing']);
        DB::table('company_class')->insert(['id'=>2429,'company_group_id'=>242,'name'=>'Other Electronic Equipment Manufacturing']);

        DB::table('company_class')->insert(['id'=>2431,'company_group_id'=>243,'name'=>'Electric Cable and Wire Manufacturing']);
        DB::table('company_class')->insert(['id'=>2432,'company_group_id'=>243,'name'=>'Electric Lighting Equipment Manufacturing']);
        DB::table('company_class')->insert(['id'=>2439,'company_group_id'=>243,'name'=>'Other Electrical Equipment Manufacturing']);

        DB::table('company_class')->insert(['id'=>2441,'company_group_id'=>244,'name'=>'Whiteware Appliance Manufacturing']);
        DB::table('company_class')->insert(['id'=>2449,'company_group_id'=>244,'name'=>'Other Domestic Appliance Manufacturing']);

        DB::table('company_class')->insert(['id'=>2451,'company_group_id'=>245,'name'=>'Pump and Compressor Manufacturing']);
        DB::table('company_class')->insert(['id'=>2452,'company_group_id'=>245,'name'=>'Fixed Space Heating, Cooling and Ventilation Equipment Manufacturing']);

        DB::table('company_class')->insert(['id'=>2461,'company_group_id'=>246,'name'=>'Agricultural Machinery and Equipment Manufacturing']);
        DB::table('company_class')->insert(['id'=>2462,'company_group_id'=>246,'name'=>'Mining and Construction Machinery Manufacturing']);
        DB::table('company_class')->insert(['id'=>2463,'company_group_id'=>246,'name'=>'Machine Tool and Parts Manufacturing']);
        DB::table('company_class')->insert(['id'=>2469,'company_group_id'=>246,'name'=>'Other Specialised Machinery and Equipment Manufacturing']);

        DB::table('company_class')->insert(['id'=>2491,'company_group_id'=>249,'name'=>'Lifting and Material Handling Equipment Manufacturing']);
        DB::table('company_class')->insert(['id'=>2499,'company_group_id'=>249,'name'=>'Other Machinery and Equipment Manufacturing n.e.c.']);


        DB::table('company_class')->insert(['id'=>2511,'company_group_id'=>251,'name'=>'Wooden Furniture and Upholstered Seat Manufacturing']);
        DB::table('company_class')->insert(['id'=>2512,'company_group_id'=>251,'name'=>'Metal Furniture Manufacturing']);
        DB::table('company_class')->insert(['id'=>2513,'company_group_id'=>251,'name'=>'Mattress Manufacturing']);
        DB::table('company_class')->insert(['id'=>2519,'company_group_id'=>251,'name'=>'Other Furniture Manufacturing']);

        DB::table('company_class')->insert(['id'=>2591,'company_group_id'=>259,'name'=>'Jewellery and Silverware Manufacturing']);
        DB::table('company_class')->insert(['id'=>2592,'company_group_id'=>259,'name'=>'Toy, Sporting and Recreational Product Manufacturing']);
        DB::table('company_class')->insert(['id'=>2599,'company_group_id'=>259,'name'=>'Other Manufacturing n.e.c.']);




        DB::table('company_class')->insert(['id'=>2611,'company_group_id'=>261,'name'=>'Fossil Fuel Electricity Generation']);
        DB::table('company_class')->insert(['id'=>2612,'company_group_id'=>261,'name'=>'Hydro-Electricity Generation']);
        DB::table('company_class')->insert(['id'=>2619,'company_group_id'=>261,'name'=>'Other Electricity Generation']);

        DB::table('company_class')->insert(['id'=>2620,'company_group_id'=>262,'name'=>'Electricity Transmission']);

        DB::table('company_class')->insert(['id'=>2630,'company_group_id'=>263,'name'=>'Electricity Distribution']);

        DB::table('company_class')->insert(['id'=>2640,'company_group_id'=>264,'name'=>'On Selling Electricity and Electricity Market Operation']);


        DB::table('company_class')->insert(['id'=>2700,'company_group_id'=>270,'name'=>'Gas Supply']);


        DB::table('company_class')->insert(['id'=>2811,'company_group_id'=>281,'name'=>'Water Supply']);
        DB::table('company_class')->insert(['id'=>2812,'company_group_id'=>281,'name'=>'Sewerage and Drainage Services']);


        DB::table('company_class')->insert(['id'=>2911,'company_group_id'=>291,'name'=>'Solid Waste Collection Services']);
        DB::table('company_class')->insert(['id'=>2919,'company_group_id'=>291,'name'=>'Other Waste Collection Services']);

        DB::table('company_class')->insert(['id'=>2921,'company_group_id'=>292,'name'=>'Waste Treatment and Disposal Services']);
        DB::table('company_class')->insert(['id'=>2922,'company_group_id'=>292,'name'=>'Waste Remediation and Materials Recovery Services']);




        DB::table('company_class')->insert(['id'=>3011,'company_group_id'=>301,'name'=>'House Construction']);
        DB::table('company_class')->insert(['id'=>3019,'company_group_id'=>301,'name'=>'Other Residential Building Construction']);

        DB::table('company_class')->insert(['id'=>3020,'company_group_id'=>302,'name'=>'Non-Residential Building Construction']);


        DB::table('company_class')->insert(['id'=>3101,'company_group_id'=>310,'name'=>'Road and Bridge Construction']);
        DB::table('company_class')->insert(['id'=>3109,'company_group_id'=>310,'name'=>'Other Heavy and Civil Engineering Construction']);


        DB::table('company_class')->insert(['id'=>3211,'company_group_id'=>321,'name'=>'Land Development and Subdivision']);
        DB::table('company_class')->insert(['id'=>3212,'company_group_id'=>321,'name'=>'Site Preparation Services']);

        DB::table('company_class')->insert(['id'=>3221,'company_group_id'=>322,'name'=>'Concreting Services']);
        DB::table('company_class')->insert(['id'=>3222,'company_group_id'=>322,'name'=>'Bricklaying Services']);
        DB::table('company_class')->insert(['id'=>3223,'company_group_id'=>322,'name'=>'Roofing Services']);
        DB::table('company_class')->insert(['id'=>3224,'company_group_id'=>322,'name'=>'Structural Steel Erection Services']);

        DB::table('company_class')->insert(['id'=>3231,'company_group_id'=>323,'name'=>'Plumbing Services']);
        DB::table('company_class')->insert(['id'=>3232,'company_group_id'=>323,'name'=>'Electrical Services']);
        DB::table('company_class')->insert(['id'=>3233,'company_group_id'=>323,'name'=>'Air Conditioning and Heating Services']);
        DB::table('company_class')->insert(['id'=>3234,'company_group_id'=>323,'name'=>'Fire and Security Alarm Installation Services']);
        DB::table('company_class')->insert(['id'=>3239,'company_group_id'=>323,'name'=>'Other Building Installation Services']);

        DB::table('company_class')->insert(['id'=>3241,'company_group_id'=>324,'name'=>'Plastering and Ceiling Services']);
        DB::table('company_class')->insert(['id'=>3242,'company_group_id'=>324,'name'=>'Carpentry Services']);
        DB::table('company_class')->insert(['id'=>3243,'company_group_id'=>324,'name'=>'Tiling and Carpeting Services']);
        DB::table('company_class')->insert(['id'=>3244,'company_group_id'=>324,'name'=>'Painting and Decorating Services']);
        DB::table('company_class')->insert(['id'=>3245,'company_group_id'=>324,'name'=>'Glazing Services']);

        DB::table('company_class')->insert(['id'=>3291,'company_group_id'=>329,'name'=>'Landscape Construction Services']);
        DB::table('company_class')->insert(['id'=>3292,'company_group_id'=>329,'name'=>'Hire of Construction Machinery with Operator']);
        DB::table('company_class')->insert(['id'=>3299,'company_group_id'=>329,'name'=>'Other Construction Services n.e.c.']);




        DB::table('company_class')->insert(['id'=>3311,'company_group_id'=>331,'name'=>'Wool Wholesaling']);
        DB::table('company_class')->insert(['id'=>3312,'company_group_id'=>331,'name'=>'Cereal Grain Wholesaling']);
        DB::table('company_class')->insert(['id'=>3319,'company_group_id'=>331,'name'=>'Other Agricultural Product Wholesaling']);

        DB::table('company_class')->insert(['id'=>3321,'company_group_id'=>332,'name'=>'Petroleum Product Wholesaling']);
        DB::table('company_class')->insert(['id'=>3322,'company_group_id'=>332,'name'=>'Metal and Mineral Wholesaling']);
        DB::table('company_class')->insert(['id'=>3323,'company_group_id'=>332,'name'=>'Industrial and Agricultural Chemical Product Wholesaling']);

        DB::table('company_class')->insert(['id'=>3331,'company_group_id'=>333,'name'=>'Timber Wholesaling']);
        DB::table('company_class')->insert(['id'=>3332,'company_group_id'=>333,'name'=>'Plumbing Goods Wholesaling']);
        DB::table('company_class')->insert(['id'=>3339,'company_group_id'=>333,'name'=>'Other Hardware Goods Wholesaling']);


        DB::table('company_class')->insert(['id'=>3411,'company_group_id'=>341,'name'=>'Agricultural and Construction Machinery Wholesaling']);
        DB::table('company_class')->insert(['id'=>3419,'company_group_id'=>341,'name'=>'Other Specialised Industrial Machinery and Equipment Wholesaling']);

        DB::table('company_class')->insert(['id'=>3491,'company_group_id'=>349,'name'=>'Professional and Scientific Goods Wholesaling']);
        DB::table('company_class')->insert(['id'=>3492,'company_group_id'=>349,'name'=>'Computer and Computer Peripheral Wholesaling']);
        DB::table('company_class')->insert(['id'=>3493,'company_group_id'=>349,'name'=>'Telecommunication Goods Wholesaling']);
        DB::table('company_class')->insert(['id'=>3494,'company_group_id'=>349,'name'=>'Other Electrical and Electronic Goods Wholesaling']);
        DB::table('company_class')->insert(['id'=>3499,'company_group_id'=>349,'name'=>'Other Machinery and Equipment Wholesaling n.e.c.']);


        DB::table('company_class')->insert(['id'=>3501,'company_group_id'=>350,'name'=>'Car Wholesaling']);
        DB::table('company_class')->insert(['id'=>3502,'company_group_id'=>350,'name'=>'Commercial Vehicle Wholesaling']);
        DB::table('company_class')->insert(['id'=>3503,'company_group_id'=>350,'name'=>'Trailer and Other Motor Vehicle Wholesaling']);
        DB::table('company_class')->insert(['id'=>3504,'company_group_id'=>350,'name'=>'Motor Vehicle New Parts Wholesaling']);
        DB::table('company_class')->insert(['id'=>3505,'company_group_id'=>350,'name'=>'Motor Vehicle Dismantling and Used Parts Wholesaling']);


        DB::table('company_class')->insert(['id'=>3601,'company_group_id'=>360,'name'=>'General Line Grocery Wholesaling']);
        DB::table('company_class')->insert(['id'=>3602,'company_group_id'=>360,'name'=>'Meat, Poultry and Smallgoods Wholesaling']);
        DB::table('company_class')->insert(['id'=>3603,'company_group_id'=>360,'name'=>'Dairy Produce Wholesaling']);
        DB::table('company_class')->insert(['id'=>3604,'company_group_id'=>360,'name'=>'Fish and Seafood Wholesaling']);
        DB::table('company_class')->insert(['id'=>3605,'company_group_id'=>360,'name'=>'Fruit and Vegetable Wholesaling']);
        DB::table('company_class')->insert(['id'=>3606,'company_group_id'=>360,'name'=>'Liquor and Tobacco Product Wholesaling']);
        DB::table('company_class')->insert(['id'=>3609,'company_group_id'=>360,'name'=>'Other Grocery Wholesaling']);


        DB::table('company_class')->insert(['id'=>3711,'company_group_id'=>371,'name'=>'Textile Product Wholesaling']);
        DB::table('company_class')->insert(['id'=>3712,'company_group_id'=>371,'name'=>'Clothing and Footwear Wholesaling']);

        DB::table('company_class')->insert(['id'=>3720,'company_group_id'=>372,'name'=>'Pharmaceutical and Toiletry Goods Wholesaling']);

        DB::table('company_class')->insert(['id'=>3731,'company_group_id'=>373,'name'=>'Furniture and Floor Covering Wholesaling']);
        DB::table('company_class')->insert(['id'=>3732,'company_group_id'=>373,'name'=>'Jewellery and Watch Wholesaling']);
        DB::table('company_class')->insert(['id'=>3733,'company_group_id'=>373,'name'=>'Kitchen and Diningware Wholesaling']);
        DB::table('company_class')->insert(['id'=>3734,'company_group_id'=>373,'name'=>'Toy and Sporting Goods Wholesaling']);
        DB::table('company_class')->insert(['id'=>3735,'company_group_id'=>373,'name'=>'Book and Magazine Wholesaling']);
        DB::table('company_class')->insert(['id'=>3736,'company_group_id'=>373,'name'=>'Paper Product Wholesaling']);
        DB::table('company_class')->insert(['id'=>3739,'company_group_id'=>373,'name'=>'Other Goods Wholesaling n.e.c.']);


        DB::table('company_class')->insert(['id'=>3800,'company_group_id'=>380,'name'=>'Commission-Based Wholesaling']);




        DB::table('company_class')->insert(['id'=>3911,'company_group_id'=>391,'name'=>'Car Retailing']);
        DB::table('company_class')->insert(['id'=>3912,'company_group_id'=>391,'name'=>'Motor Cycle Retailing']);
        DB::table('company_class')->insert(['id'=>3913,'company_group_id'=>391,'name'=>'Trailer and Other Motor Vehicle Retailing']);

        DB::table('company_class')->insert(['id'=>3921,'company_group_id'=>392,'name'=>'Motor Vehicle Parts Retailing']);
        DB::table('company_class')->insert(['id'=>3922,'company_group_id'=>392,'name'=>'Tyre Retailing']);


        DB::table('company_class')->insert(['id'=>4000,'company_group_id'=>400,'name'=>'Fuel Retailing']);


        DB::table('company_class')->insert(['id'=>4110,'company_group_id'=>411,'name'=>'Supermarket and Grocery Stores']);

        DB::table('company_class')->insert(['id'=>4121,'company_group_id'=>412,'name'=>'Fresh Meat, Fish and Poultry Retailing']);
        DB::table('company_class')->insert(['id'=>4122,'company_group_id'=>412,'name'=>'Fruit and Vegetable Retailing']);
        DB::table('company_class')->insert(['id'=>4123,'company_group_id'=>412,'name'=>'Liquor Retailing']);
        DB::table('company_class')->insert(['id'=>4129,'company_group_id'=>412,'name'=>'Other Specialised Food Retailing']);


        DB::table('company_class')->insert(['id'=>4211,'company_group_id'=>421,'name'=>'Furniture Retailing']);
        DB::table('company_class')->insert(['id'=>4212,'company_group_id'=>421,'name'=>'Floor Coverings Retailing']);
        DB::table('company_class')->insert(['id'=>4213,'company_group_id'=>421,'name'=>'Houseware Retailing']);
        DB::table('company_class')->insert(['id'=>4214,'company_group_id'=>421,'name'=>'Manchester and Other Textile Goods Retailing']);

        DB::table('company_class')->insert(['id'=>4221,'company_group_id'=>422,'name'=>'Electrical, Electronic and Gas Appliance Retailing']);
        DB::table('company_class')->insert(['id'=>4222,'company_group_id'=>422,'name'=>'Computer and Computer Peripheral Retailing']);
        DB::table('company_class')->insert(['id'=>4229,'company_group_id'=>422,'name'=>'Other Electrical and Electronic Goods Retailing']);

        DB::table('company_class')->insert(['id'=>4231,'company_group_id'=>423,'name'=>'Hardware and Building Supplies Retailing']);
        DB::table('company_class')->insert(['id'=>4232,'company_group_id'=>423,'name'=>'Garden Supplies Retailing']);

        DB::table('company_class')->insert(['id'=>4241,'company_group_id'=>424,'name'=>'Sport and Camping Equipment Retailing']);
        DB::table('company_class')->insert(['id'=>4242,'company_group_id'=>424,'name'=>'Entertainment Media Retailing']);
        DB::table('company_class')->insert(['id'=>4243,'company_group_id'=>424,'name'=>'Toy and Game Retailing']);
        DB::table('company_class')->insert(['id'=>4244,'company_group_id'=>424,'name'=>'Newspaper and Book Retailing']);
        DB::table('company_class')->insert(['id'=>4245,'company_group_id'=>424,'name'=>'Marine Equipment Retailing']);

        DB::table('company_class')->insert(['id'=>4251,'company_group_id'=>425,'name'=>'Clothing Retailing']);
        DB::table('company_class')->insert(['id'=>4252,'company_group_id'=>425,'name'=>'Footwear Retailing']);
        DB::table('company_class')->insert(['id'=>4253,'company_group_id'=>425,'name'=>'Watch and Jewellery Retailing']);
        DB::table('company_class')->insert(['id'=>4259,'company_group_id'=>425,'name'=>'Other Personal Accessory Retailing']);

        DB::table('company_class')->insert(['id'=>4260,'company_group_id'=>426,'name'=>'Department Stores']);

        DB::table('company_class')->insert(['id'=>4271,'company_group_id'=>427,'name'=>'Pharmaceutical, Cosmetic and Toiletry Goods Retailing']);
        DB::table('company_class')->insert(['id'=>4272,'company_group_id'=>427,'name'=>'Stationery Goods Retailing']);
        DB::table('company_class')->insert(['id'=>4273,'company_group_id'=>427,'name'=>'Antique and Used Goods Retailing']);
        DB::table('company_class')->insert(['id'=>4274,'company_group_id'=>427,'name'=>'Flower Retailing']);
        DB::table('company_class')->insert(['id'=>4279,'company_group_id'=>427,'name'=>'Other Store-Based Retailing n.e.c.']);


        DB::table('company_class')->insert(['id'=>4310,'company_group_id'=>431,'name'=>'Non-Store Retailing']);






        DB::table('company_class')->insert(['id'=>4400,'company_group_id'=>440,'name'=>'Accommodation']);


        DB::table('company_class')->insert(['id'=>4511,'company_group_id'=>451,'name'=>'Cafes and Restaurants']);
        DB::table('company_class')->insert(['id'=>4512,'company_group_id'=>451,'name'=>'Takeaway Food Services']);
        DB::table('company_class')->insert(['id'=>4513,'company_group_id'=>451,'name'=>'Catering Services']);

        DB::table('company_class')->insert(['id'=>4520,'company_group_id'=>452,'name'=>'Pubs, Taverns and Bars']);

        DB::table('company_class')->insert(['id'=>4530,'company_group_id'=>453,'name'=>'Clubs (Hospitality)']);




        DB::table('company_class')->insert(['id'=>4610,'company_group_id'=>461,'name'=>'Road Freight Transport']);

        DB::table('company_class')->insert(['id'=>4621,'company_group_id'=>462,'name'=>'Interurban and Rural Bus Transport']);
        DB::table('company_class')->insert(['id'=>4622,'company_group_id'=>462,'name'=>'Urban Bus Transport (Including Tramway)']);
        DB::table('company_class')->insert(['id'=>4623,'company_group_id'=>462,'name'=>'Taxi and Other Road Transport']);


        DB::table('company_class')->insert(['id'=>4710,'company_group_id'=>471,'name'=>'Rail Freight Transport']);

        DB::table('company_class')->insert(['id'=>4720,'company_group_id'=>472,'name'=>'Rail Passenger Transport']);


        DB::table('company_class')->insert(['id'=>4810,'company_group_id'=>481,'name'=>'Water Freight Transport']);

        DB::table('company_class')->insert(['id'=>4820,'company_group_id'=>482,'name'=>'Water Passenger Transport']);


        DB::table('company_class')->insert(['id'=>4900,'company_group_id'=>490,'name'=>'Air and Space Transport']);


        DB::table('company_class')->insert(['id'=>5010,'company_group_id'=>501,'name'=>'Scenic and Sightseeing Transport']);

        DB::table('company_class')->insert(['id'=>5021,'company_group_id'=>502,'name'=>'Pipeline Transport']);
        DB::table('company_class')->insert(['id'=>5029,'company_group_id'=>502,'name'=>'Other Transport n.e.c.']);


        DB::table('company_class')->insert(['id'=>5101,'company_group_id'=>510,'name'=>'Postal Services']);
        DB::table('company_class')->insert(['id'=>5102,'company_group_id'=>510,'name'=>'Courier Pick-up and Delivery Services']);


        DB::table('company_class')->insert(['id'=>5211,'company_group_id'=>521,'name'=>'Stevedoring Services']);
        DB::table('company_class')->insert(['id'=>5212,'company_group_id'=>521,'name'=>'Port and Water Transport Terminal Operations']);
        DB::table('company_class')->insert(['id'=>5219,'company_group_id'=>521,'name'=>'Other Water Transport Support Services']);

        DB::table('company_class')->insert(['id'=>5220,'company_group_id'=>522,'name'=>'Airport Operations and Other Air Transport Support Services']);

        DB::table('company_class')->insert(['id'=>5291,'company_group_id'=>529,'name'=>'Customs Agency Services']);
        DB::table('company_class')->insert(['id'=>5292,'company_group_id'=>529,'name'=>'Freight Forwarding Services']);
        DB::table('company_class')->insert(['id'=>5299,'company_group_id'=>529,'name'=>'Other Transport Support Services n.e.c.']);


        DB::table('company_class')->insert(['id'=>5301,'company_group_id'=>530,'name'=>'Grain Storage Services']);
        DB::table('company_class')->insert(['id'=>5309,'company_group_id'=>530,'name'=>'Other Warehousing and Storage Services']);




        DB::table('company_class')->insert(['id'=>5411,'company_group_id'=>541,'name'=>'Newspaper Publishing']);
        DB::table('company_class')->insert(['id'=>5412,'company_group_id'=>541,'name'=>'Magazine and Other Periodical Publishing']);
        DB::table('company_class')->insert(['id'=>5413,'company_group_id'=>541,'name'=>'Book Publishing']);
        DB::table('company_class')->insert(['id'=>5414,'company_group_id'=>541,'name'=>'Directory and Mailing List Publishing']);
        DB::table('company_class')->insert(['id'=>5419,'company_group_id'=>541,'name'=>'Other Publishing (except Software, Music and Internet)']);

        DB::table('company_class')->insert(['id'=>5420,'company_group_id'=>542,'name'=>'Software Publishing']);


        DB::table('company_class')->insert(['id'=>5511,'company_group_id'=>551,'name'=>'Motion Picture and Video Production']);
        DB::table('company_class')->insert(['id'=>5512,'company_group_id'=>551,'name'=>'Motion Picture and Video Distribution']);
        DB::table('company_class')->insert(['id'=>5513,'company_group_id'=>551,'name'=>'Motion Picture Exhibition']);
        DB::table('company_class')->insert(['id'=>5514,'company_group_id'=>551,'name'=>'Post-production Services and Other Motion Picture and Video Activities']);

        DB::table('company_class')->insert(['id'=>5521,'company_group_id'=>552,'name'=>'Music Publishing']);
        DB::table('company_class')->insert(['id'=>5522,'company_group_id'=>552,'name'=>'Music and Other Sound Recording Activities']);


        DB::table('company_class')->insert(['id'=>5610,'company_group_id'=>561,'name'=>'Radio Broadcasting']);

        DB::table('company_class')->insert(['id'=>5621,'company_group_id'=>562,'name'=>'Free-to-Air Television Broadcasting']);
        DB::table('company_class')->insert(['id'=>5622,'company_group_id'=>562,'name'=>'Cable and Other Subscription Broadcasting']);


        DB::table('company_class')->insert(['id'=>5700,'company_group_id'=>570,'name'=>'Internet Publishing and Broadcasting']);


        DB::table('company_class')->insert(['id'=>5801,'company_group_id'=>580,'name'=>'Wired Telecommunications Network Operation']);
        DB::table('company_class')->insert(['id'=>5802,'company_group_id'=>580,'name'=>'Other Telecommunications Network Operation']);
        DB::table('company_class')->insert(['id'=>5809,'company_group_id'=>580,'name'=>'Other Telecommunications Services']);


        DB::table('company_class')->insert(['id'=>5910,'company_group_id'=>591,'name'=>'Internet Service Providers and Web Search Portals']);

        DB::table('company_class')->insert(['id'=>5921,'company_group_id'=>592,'name'=>'Data Processing and Web Hosting Services']);
        DB::table('company_class')->insert(['id'=>5922,'company_group_id'=>592,'name'=>'Electronic Information Storage Services']);


        DB::table('company_class')->insert(['id'=>6010,'company_group_id'=>601,'name'=>'Libraries and Archives']);

        DB::table('company_class')->insert(['id'=>6020,'company_group_id'=>602,'name'=>'Other Information Services']);




        DB::table('company_class')->insert(['id'=>6210,'company_group_id'=>621,'name'=>'Central Banking']);

        DB::table('company_class')->insert(['id'=>6221,'company_group_id'=>622,'name'=>'Banking']);
        DB::table('company_class')->insert(['id'=>6222,'company_group_id'=>622,'name'=>'Building Society Operation']);
        DB::table('company_class')->insert(['id'=>6223,'company_group_id'=>622,'name'=>'Credit Union Operation']);
        DB::table('company_class')->insert(['id'=>6229,'company_group_id'=>622,'name'=>'Other Depository Financial Intermediation']);

        DB::table('company_class')->insert(['id'=>6230,'company_group_id'=>623,'name'=>'Non-Depository Financing']);

        DB::table('company_class')->insert(['id'=>6240,'company_group_id'=>624,'name'=>'Financial Asset Investing']);


        DB::table('company_class')->insert(['id'=>6310,'company_group_id'=>631,'name'=>'Life Insurance']);

        DB::table('company_class')->insert(['id'=>6321,'company_group_id'=>632,'name'=>'Health Insurance']);
        DB::table('company_class')->insert(['id'=>6322,'company_group_id'=>632,'name'=>'General Insurance']);

        DB::table('company_class')->insert(['id'=>6330,'company_group_id'=>633,'name'=>'Superannuation Funds']);


        DB::table('company_class')->insert(['id'=>6411,'company_group_id'=>641,'name'=>'Financial Asset Broking Services']);
        DB::table('company_class')->insert(['id'=>6419,'company_group_id'=>641,'name'=>'Other Auxiliary Finance and Investment Services']);

        DB::table('company_class')->insert(['id'=>6420,'company_group_id'=>642,'name'=>'Auxiliary Insurance Services']);




        DB::table('company_class')->insert(['id'=>6611,'company_group_id'=>661,'name'=>'Passenger Car Rental and Hiring']);
        DB::table('company_class')->insert(['id'=>6619,'company_group_id'=>661,'name'=>'Other Motor Vehicle and Transport Equipment Rental and Hiring']);

        DB::table('company_class')->insert(['id'=>6620,'company_group_id'=>662,'name'=>'Farm Animal and Bloodstock Leasing']);

        DB::table('company_class')->insert(['id'=>6631,'company_group_id'=>663,'name'=>'Heavy Machinery and Scaffolding Rental and Hiring']);
        DB::table('company_class')->insert(['id'=>6632,'company_group_id'=>663,'name'=>'Video and Other Electronic Media Rental and Hiring']);
        DB::table('company_class')->insert(['id'=>6639,'company_group_id'=>663,'name'=>'Other Goods and Equipment Rental and Hiring n.e.c.']);

        DB::table('company_class')->insert(['id'=>6640,'company_group_id'=>664,'name'=>'Non-Financial Intangible Assets (Except Copyrights) Leasing']);


        DB::table('company_class')->insert(['id'=>6711,'company_group_id'=>671,'name'=>'Residential Property Operators']);
        DB::table('company_class')->insert(['id'=>6712,'company_group_id'=>671,'name'=>'Non-Residential Property Operators']);

        DB::table('company_class')->insert(['id'=>6720,'company_group_id'=>672,'name'=>'Real Estate Services']);





        DB::table('company_class')->insert(['id'=>6910,'company_group_id'=>691,'name'=>'Scientific Research Services']);

        DB::table('company_class')->insert(['id'=>6921,'company_group_id'=>692,'name'=>'Architectural Services']);
        DB::table('company_class')->insert(['id'=>6922,'company_group_id'=>692,'name'=>'Surveying and Mapping Services']);
        DB::table('company_class')->insert(['id'=>6923,'company_group_id'=>692,'name'=>'Engineering Design and Engineering Consulting Services']);
        DB::table('company_class')->insert(['id'=>6924,'company_group_id'=>692,'name'=>'Other Specialised Design Services']);
        DB::table('company_class')->insert(['id'=>6925,'company_group_id'=>692,'name'=>'Scientific Testing and Analysis Services']);

        DB::table('company_class')->insert(['id'=>6931,'company_group_id'=>693,'name'=>'Legal Services']);
        DB::table('company_class')->insert(['id'=>6932,'company_group_id'=>693,'name'=>'Accounting Services']);

        DB::table('company_class')->insert(['id'=>6940,'company_group_id'=>694,'name'=>'Advertising Services']);

        DB::table('company_class')->insert(['id'=>6950,'company_group_id'=>695,'name'=>'Market Research and Statistical Services']);


        DB::table('company_class')->insert(['id'=>6962,'company_group_id'=>696,'name'=>'Management Advice and Related Consulting Services']);

        DB::table('company_class')->insert(['id'=>6970,'company_group_id'=>697,'name'=>'Veterinary Services']);

        DB::table('company_class')->insert(['id'=>6991,'company_group_id'=>699,'name'=>'Professional Photographic Services']);
        DB::table('company_class')->insert(['id'=>6999,'company_group_id'=>699,'name'=>'Other Professional, Scientific and Technical Services n.e.c.']);


        DB::table('company_class')->insert(['id'=>7000,'company_group_id'=>700,'name'=>'Computer System Design and Related Services']);




        DB::table('company_class')->insert(['id'=>7211,'company_group_id'=>721,'name'=>'Employment Placement and Recruitment Services']);
        DB::table('company_class')->insert(['id'=>7212,'company_group_id'=>721,'name'=>'Labour Supply Services']);

        DB::table('company_class')->insert(['id'=>7220,'company_group_id'=>722,'name'=>'Travel Agency and Tour Arrangement Services']);

        DB::table('company_class')->insert(['id'=>7291,'company_group_id'=>729,'name'=>'Office Administrative Services']);
        DB::table('company_class')->insert(['id'=>7292,'company_group_id'=>729,'name'=>'Document Preparation Services']);
        DB::table('company_class')->insert(['id'=>7293,'company_group_id'=>729,'name'=>'Credit Reporting and Debt Collection Services']);
        DB::table('company_class')->insert(['id'=>7294,'company_group_id'=>729,'name'=>'Call Centre Operation']);
        DB::table('company_class')->insert(['id'=>7299,'company_group_id'=>729,'name'=>'Other Administrative Services n.e.c.']);


        DB::table('company_class')->insert(['id'=>7311,'company_group_id'=>731,'name'=>'Building and Other Industrial Cleaning Services']);
        DB::table('company_class')->insert(['id'=>7312,'company_group_id'=>731,'name'=>'Building Pest Control Services']);
        DB::table('company_class')->insert(['id'=>7313,'company_group_id'=>731,'name'=>'Gardening Services']);

        DB::table('company_class')->insert(['id'=>7320,'company_group_id'=>732,'name'=>'Packaging Services']);




        DB::table('company_class')->insert(['id'=>7510,'company_group_id'=>751,'name'=>'Central Government Administration']);

        DB::table('company_class')->insert(['id'=>7520,'company_group_id'=>752,'name'=>'State Government Administration']);

        DB::table('company_class')->insert(['id'=>7530,'company_group_id'=>753,'name'=>'Local Government Administration']);

        DB::table('company_class')->insert(['id'=>7540,'company_group_id'=>754,'name'=>'Justice']);

        DB::table('company_class')->insert(['id'=>7551,'company_group_id'=>755,'name'=>'Domestic Government Representation']);
        DB::table('company_class')->insert(['id'=>7552,'company_group_id'=>755,'name'=>'Foreign Government Representation']);


        DB::table('company_class')->insert(['id'=>7600,'company_group_id'=>760,'name'=>'Defence']);


        DB::table('company_class')->insert(['id'=>7711,'company_group_id'=>771,'name'=>'Police Services']);
        DB::table('company_class')->insert(['id'=>7712,'company_group_id'=>771,'name'=>'Investigation and Security Services']);
        DB::table('company_class')->insert(['id'=>7713,'company_group_id'=>771,'name'=>'Fire Protection and Other Emergency Services']);
        DB::table('company_class')->insert(['id'=>7714,'company_group_id'=>771,'name'=>'Correctional and Detention Services']);
        DB::table('company_class')->insert(['id'=>7719,'company_group_id'=>771,'name'=>'Other Public Order and Safety Services']);

        DB::table('company_class')->insert(['id'=>7720,'company_group_id'=>772,'name'=>'Regulatory Services']);




        DB::table('company_class')->insert(['id'=>8010,'company_group_id'=>801,'name'=>'Preschool Education']);

        DB::table('company_class')->insert(['id'=>8021,'company_group_id'=>802,'name'=>'Primary Education']);
        DB::table('company_class')->insert(['id'=>8022,'company_group_id'=>802,'name'=>'Secondary Education']);
        DB::table('company_class')->insert(['id'=>8023,'company_group_id'=>802,'name'=>'Combined Primary and Secondary Education']);
        DB::table('company_class')->insert(['id'=>8024,'company_group_id'=>802,'name'=>'Special School Education']);


        DB::table('company_class')->insert(['id'=>8101,'company_group_id'=>810,'name'=>'Technical and Vocational Education and Training']);
        DB::table('company_class')->insert(['id'=>8102,'company_group_id'=>810,'name'=>'Higher Education']);


        DB::table('company_class')->insert(['id'=>8211,'company_group_id'=>821,'name'=>'Sports and Physical Recreation Instruction']);
        DB::table('company_class')->insert(['id'=>8212,'company_group_id'=>821,'name'=>'Arts Education']);
        DB::table('company_class')->insert(['id'=>8219,'company_group_id'=>821,'name'=>'Adult, Community and Other Education n.e.c.']);

        DB::table('company_class')->insert(['id'=>8220,'company_group_id'=>822,'name'=>'Educational Support Services']);




        DB::table('company_class')->insert(['id'=>8401,'company_group_id'=>840,'name'=>'Hospitals (Except Psychiatric Hospitals)']);
        DB::table('company_class')->insert(['id'=>8402,'company_group_id'=>840,'name'=>'Psychiatric Hospitals']);


        DB::table('company_class')->insert(['id'=>8511,'company_group_id'=>851,'name'=>'General Practice Medical Services']);
        DB::table('company_class')->insert(['id'=>8512,'company_group_id'=>851,'name'=>'Specialist Medical Services']);

        DB::table('company_class')->insert(['id'=>8520,'company_group_id'=>852,'name'=>'Pathology and Diagnostic Imaging Services']);

        DB::table('company_class')->insert(['id'=>8531,'company_group_id'=>853,'name'=>'Dental Services']);
        DB::table('company_class')->insert(['id'=>8532,'company_group_id'=>853,'name'=>'Optometry and Optical Dispensing']);
        DB::table('company_class')->insert(['id'=>8533,'company_group_id'=>853,'name'=>'Physiotherapy Services']);
        DB::table('company_class')->insert(['id'=>8534,'company_group_id'=>853,'name'=>'Chiropractic and Osteopathic Services']);
        DB::table('company_class')->insert(['id'=>8539,'company_group_id'=>853,'name'=>'Other Allied Health Services']);

        DB::table('company_class')->insert(['id'=>8591,'company_group_id'=>859,'name'=>'Ambulance Services']);
        DB::table('company_class')->insert(['id'=>8599,'company_group_id'=>859,'name'=>'Other Health Care Services n.e.c.']);


        DB::table('company_class')->insert(['id'=>8601,'company_group_id'=>860,'name'=>'Aged Care Residential Services']);
        DB::table('company_class')->insert(['id'=>8609,'company_group_id'=>860,'name'=>'Other Residential Care Services']);


        DB::table('company_class')->insert(['id'=>8710,'company_group_id'=>871,'name'=>'Child Care Services']);

        DB::table('company_class')->insert(['id'=>8790,'company_group_id'=>879,'name'=>'Other Social Assistance Services']);




        DB::table('company_class')->insert(['id'=>8910,'company_group_id'=>891,'name'=>'Museum Operation']);

        DB::table('company_class')->insert(['id'=>8921,'company_group_id'=>892,'name'=>'Zoological and Botanical Gardens Operation']);
        DB::table('company_class')->insert(['id'=>8922,'company_group_id'=>892,'name'=>'Nature Reserves and Conservation Parks Operation']);


        DB::table('company_class')->insert(['id'=>9001,'company_group_id'=>900,'name'=>'Performing Arts Operation']);
        DB::table('company_class')->insert(['id'=>9002,'company_group_id'=>900,'name'=>'Creative Artists, Musicians, Writers and Performers']);
        DB::table('company_class')->insert(['id'=>9003,'company_group_id'=>900,'name'=>'Performing Arts Venue Operation']);


        DB::table('company_class')->insert(['id'=>9111,'company_group_id'=>911,'name'=>'Health and Fitness Centres and Gymnasia Operation']);
        DB::table('company_class')->insert(['id'=>9112,'company_group_id'=>911,'name'=>'Sports and Physical Recreation Clubs and Sports Professionals']);
        DB::table('company_class')->insert(['id'=>9113,'company_group_id'=>911,'name'=>'Sports and Physical Recreation Venues, Grounds and Facilities Operation']);
        DB::table('company_class')->insert(['id'=>9114,'company_group_id'=>911,'name'=>'Sports and Physical Recreation Administrative Service']);

        DB::table('company_class')->insert(['id'=>9121,'company_group_id'=>912,'name'=>'Horse and Dog Racing Administration and Track Operation']);
        DB::table('company_class')->insert(['id'=>9129,'company_group_id'=>912,'name'=>'Other Horse and Dog Racing Activities']);

        DB::table('company_class')->insert(['id'=>9131,'company_group_id'=>913,'name'=>'Amusement Parks and Centres Operation']);
        DB::table('company_class')->insert(['id'=>9139,'company_group_id'=>913,'name'=>'Amusement and Other Recreational Activities n.e.c.']);


        DB::table('company_class')->insert(['id'=>9201,'company_group_id'=>920,'name'=>'Casino Operation']);
        DB::table('company_class')->insert(['id'=>9202,'company_group_id'=>920,'name'=>'Lottery Operation']);
        DB::table('company_class')->insert(['id'=>9209,'company_group_id'=>920,'name'=>'Other Gambling Activities']);




        DB::table('company_class')->insert(['id'=>9411,'company_group_id'=>941,'name'=>'Automotive Electrical Services']);
        DB::table('company_class')->insert(['id'=>9412,'company_group_id'=>941,'name'=>'Automotive Body, Paint and Interior Repair']);
        DB::table('company_class')->insert(['id'=>9419,'company_group_id'=>941,'name'=>'Other Automotive Repair and Maintenance']);

        DB::table('company_class')->insert(['id'=>9421,'company_group_id'=>942,'name'=>'Domestic Appliance Repair and Maintenance']);
        DB::table('company_class')->insert(['id'=>9422,'company_group_id'=>942,'name'=>'Electronic (except Domestic Appliance) and Precision Equipment Repair']);
        DB::table('company_class')->insert(['id'=>9429,'company_group_id'=>942,'name'=>'Other Machinery and Equipment Repair and Maintenance']);

        DB::table('company_class')->insert(['id'=>9491,'company_group_id'=>949,'name'=>'Clothing and Footwear Repair']);
        DB::table('company_class')->insert(['id'=>9499,'company_group_id'=>949,'name'=>'Other Repair and Maintenance n.e.c.']);


        DB::table('company_class')->insert(['id'=>9511,'company_group_id'=>951,'name'=>'Hairdressing and Beauty Services']);
        DB::table('company_class')->insert(['id'=>9512,'company_group_id'=>951,'name'=>'Diet and Weight Reduction Centre Operation']);

        DB::table('company_class')->insert(['id'=>9520,'company_group_id'=>952,'name'=>'Funeral, Crematorium and Cemetery Services']);

        DB::table('company_class')->insert(['id'=>9531,'company_group_id'=>953,'name'=>'Laundry and Dry-Cleaning Services']);
        DB::table('company_class')->insert(['id'=>9532,'company_group_id'=>953,'name'=>'Photographic Film Processing']);
        DB::table('company_class')->insert(['id'=>9533,'company_group_id'=>953,'name'=>'Parking Services']);
        DB::table('company_class')->insert(['id'=>9534,'company_group_id'=>953,'name'=>'Brothel Keeping and Prostitution Services']);
        DB::table('company_class')->insert(['id'=>9539,'company_group_id'=>953,'name'=>'Other Personal Services n.e.c.']);

        DB::table('company_class')->insert(['id'=>9540,'company_group_id'=>954,'name'=>'Religious Services']);
        DB::table('company_class')->insert(['id'=>9551,'company_group_id'=>955,'name'=>'Business and Professional Association Services']);
        DB::table('company_class')->insert(['id'=>9552,'company_group_id'=>955,'name'=>'Labour Association Services']);
        DB::table('company_class')->insert(['id'=>9559,'company_group_id'=>955,'name'=>'Other Interest Group Services n.e.c.']);
        DB::table('company_class')->insert(['id'=>9601,'company_group_id'=>960,'name'=>'Private Households Employing Staff']);
        DB::table('company_class')->insert(['id'=>9602,'company_group_id'=>960,'name'=>'Undifferentiated Goods-Producing Activities of Private Households for Own Use']);
        DB::table('company_class')->insert(['id'=>9603,'company_group_id'=>960,'name'=>'Undifferentiated Service-Producing Activities of Private Households for Own Use']);

    }
}
