<?php

use Illuminate\Database\Seeder;

class EntityTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('entity_type')->insert(['name'=>'Aggregated']);
        DB::table('entity_type')->insert(['name'=>'Association']);
        DB::table('entity_type')->insert(['name'=>'Bank']);
        DB::table('entity_type')->insert(['name'=>'Financial Institution']);
        DB::table('entity_type')->insert(['name'=>'Government']);
        DB::table('entity_type')->insert(['name'=>'Partnership-Individual']);
        DB::table('entity_type')->insert(['name'=>'Partnership-Corporation']);
        DB::table('entity_type')->insert(['name'=>'Private Company']);
        DB::table('entity_type')->insert(['name'=>'Public Company-Listed']);
        DB::table('entity_type')->insert(['name'=>'Public Company-Unlisted']);
        DB::table('entity_type')->insert(['name'=>'Sole Trader']);
        DB::table('entity_type')->insert(['name'=>'Discretionary Trust']);
        DB::table('entity_type')->insert(['name'=>'Unit Trust']);
    }
}
