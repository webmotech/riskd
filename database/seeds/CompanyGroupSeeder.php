<?php

use Illuminate\Database\Seeder;

class CompanyGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company_group')->insert(['id'=>11,'sub_divisions_id'=>1,'name'=>'Nursery and Floriculture Production']);
        DB::table('company_group')->insert(['id'=>12,'sub_divisions_id'=>1,'name'=>'Mushroom and Vegetable Growing']);
        DB::table('company_group')->insert(['id'=>13,'sub_divisions_id'=>1,'name'=>'Fruit and Tree Nut Growing']);
        DB::table('company_group')->insert(['id'=>14,'sub_divisions_id'=>1,'name'=>'Sheep, Beef Cattle and Grain Farming']);
        DB::table('company_group')->insert(['id'=>15,'sub_divisions_id'=>1,'name'=>'Other Crop Growing']);
        DB::table('company_group')->insert(['id'=>16,'sub_divisions_id'=>1,'name'=>'Dairy Cattle Farming']);
        DB::table('company_group')->insert(['id'=>17,'sub_divisions_id'=>1,'name'=>'Poultry Farming']);
        DB::table('company_group')->insert(['id'=>18,'sub_divisions_id'=>1,'name'=>'Deer Farming']);
        DB::table('company_group')->insert(['id'=>19,'sub_divisions_id'=>1,'name'=>'Other Livestock Farming']);
        DB::table('company_group')->insert(['id'=>20,'sub_divisions_id'=>2,'name'=>'Aquaculture']);
        DB::table('company_group')->insert(['id'=>30,'sub_divisions_id'=>3,'name'=>'Forestry and Logging']);
        DB::table('company_group')->insert(['id'=>41,'sub_divisions_id'=>4,'name'=>'Fishing']);
        DB::table('company_group')->insert(['id'=>42,'sub_divisions_id'=>4,'name'=>'Hunting and Trapping']);
        DB::table('company_group')->insert(['id'=>51,'sub_divisions_id'=>5,'name'=>'Forestry Support Services']);
        DB::table('company_group')->insert(['id'=>52,'sub_divisions_id'=>5,'name'=>'Agriculture and Fishing Support Services']);
        DB::table('company_group')->insert(['id'=>60,'sub_divisions_id'=>6,'name'=>'Coal Mining']);
        DB::table('company_group')->insert(['id'=>70,'sub_divisions_id'=>7,'name'=>'Oil and Gas Extraction']);
        DB::table('company_group')->insert(['id'=>80,'sub_divisions_id'=>8,'name'=>'Metal Ore Mining']);
        DB::table('company_group')->insert(['id'=>91,'sub_divisions_id'=>9,'name'=>'Construction Material Mining']);
        DB::table('company_group')->insert(['id'=>99,'sub_divisions_id'=>9,'name'=>'Other Non-Metallic Mineral Mining and Quarrying']);
        DB::table('company_group')->insert(['id'=>101,'sub_divisions_id'=>10,'name'=>'Exploration']);
        DB::table('company_group')->insert(['id'=>109,'sub_divisions_id'=>10,'name'=>'Other Mining Support Services']);
        DB::table('company_group')->insert(['id'=>111,'sub_divisions_id'=>11,'name'=>'Meat and Meat Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>112,'sub_divisions_id'=>11,'name'=>'Seafood Processing']);
        DB::table('company_group')->insert(['id'=>113,'sub_divisions_id'=>11,'name'=>'Dairy Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>114,'sub_divisions_id'=>11,'name'=>'Fruit and Vegetable Processing']);
        DB::table('company_group')->insert(['id'=>115,'sub_divisions_id'=>11,'name'=>'Oil and Fat Manufacturing']);
        DB::table('company_group')->insert(['id'=>116,'sub_divisions_id'=>11,'name'=>'Grain Mill and Cereal Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>117,'sub_divisions_id'=>11,'name'=>'Bakery Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>118,'sub_divisions_id'=>11,'name'=>'Sugar and Confectionery Manufacturing']);
        DB::table('company_group')->insert(['id'=>119,'sub_divisions_id'=>11,'name'=>'Other Food Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>121,'sub_divisions_id'=>12,'name'=>'Beverage Manufacturing']);
        DB::table('company_group')->insert(['id'=>122,'sub_divisions_id'=>12,'name'=>'Cigarette and Tobacco Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>131,'sub_divisions_id'=>13,'name'=>'Textile Manufacturing']);
        DB::table('company_group')->insert(['id'=>132,'sub_divisions_id'=>13,'name'=>'Leather Tanning, Fur Dressing and Leather Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>133,'sub_divisions_id'=>13,'name'=>'Textile Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>134,'sub_divisions_id'=>13,'name'=>'Knitted Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>135,'sub_divisions_id'=>13,'name'=>'Clothing and Footwear Manufacturing']);
        DB::table('company_group')->insert(['id'=>141,'sub_divisions_id'=>14,'name'=>'Log Sawmilling and Timber Dressing']);
        DB::table('company_group')->insert(['id'=>149,'sub_divisions_id'=>14,'name'=>'Other Wood Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>151,'sub_divisions_id'=>15,'name'=>'Pulp, Paper and Paperboard Manufacturing']);
        DB::table('company_group')->insert(['id'=>152,'sub_divisions_id'=>15,'name'=>'Converted Paper Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>161,'sub_divisions_id'=>16,'name'=>'Printing and Printing Support Services']);
        DB::table('company_group')->insert(['id'=>162,'sub_divisions_id'=>16,'name'=>'Reproduction of Recorded Media']);
        DB::table('company_group')->insert(['id'=>170,'sub_divisions_id'=>17,'name'=>'Petroleum and Coal Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>181,'sub_divisions_id'=>18,'name'=>'Basic Chemical Manufacturing']);
        DB::table('company_group')->insert(['id'=>182,'sub_divisions_id'=>18,'name'=>'Basic Polymer Manufacturing']);
        DB::table('company_group')->insert(['id'=>183,'sub_divisions_id'=>18,'name'=>'Fertiliser and Pesticide Manufacturing']);
        DB::table('company_group')->insert(['id'=>184,'sub_divisions_id'=>18,'name'=>'Pharmaceutical and Medicinal Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>185,'sub_divisions_id'=>18,'name'=>'Cleaning Compound and Toiletry Preparation Manufacturing']);
        DB::table('company_group')->insert(['id'=>189,'sub_divisions_id'=>18,'name'=>'Other Basic Chemical Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>191,'sub_divisions_id'=>19,'name'=>'Polymer Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>192,'sub_divisions_id'=>19,'name'=>'Natural Rubber Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>201,'sub_divisions_id'=>20,'name'=>'Glass and Glass Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>202,'sub_divisions_id'=>20,'name'=>'Ceramic Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>203,'sub_divisions_id'=>20,'name'=>'Cement, Lime, Plaster and Concrete Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>209,'sub_divisions_id'=>20,'name'=>'Other Non-Metallic Mineral Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>211,'sub_divisions_id'=>21,'name'=>'Basic Ferrous Metal Manufacturing']);
        DB::table('company_group')->insert(['id'=>212,'sub_divisions_id'=>21,'name'=>'Basic Ferrous Metal Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>213,'sub_divisions_id'=>21,'name'=>'Basic Non-Ferrous Metal Manufacturing']);
        DB::table('company_group')->insert(['id'=>214,'sub_divisions_id'=>21,'name'=>'Basic Non-Ferrous Metal Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>221,'sub_divisions_id'=>22,'name'=>'Iron and Steel Forging']);
        DB::table('company_group')->insert(['id'=>222,'sub_divisions_id'=>22,'name'=>'Structural Metal Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>223,'sub_divisions_id'=>22,'name'=>'Metal Container Manufacturing']);
        DB::table('company_group')->insert(['id'=>224,'sub_divisions_id'=>22,'name'=>'Sheet Metal Product Manufacturing (except Metal Structural and Container Products)']);
        DB::table('company_group')->insert(['id'=>229,'sub_divisions_id'=>22,'name'=>'Other Fabricated Metal Product Manufacturing']);
        DB::table('company_group')->insert(['id'=>231,'sub_divisions_id'=>23,'name'=>'Motor Vehicle and Motor Vehicle Part Manufacturing']);
        DB::table('company_group')->insert(['id'=>239,'sub_divisions_id'=>23,'name'=>'Other Transport Equipment Manufacturing']);
        DB::table('company_group')->insert(['id'=>241,'sub_divisions_id'=>24,'name'=>'Professional and Scientific Equipment Manufacturing']);
        DB::table('company_group')->insert(['id'=>242,'sub_divisions_id'=>24,'name'=>'Computer and Electronic Equipment Manufacturing']);
        DB::table('company_group')->insert(['id'=>243,'sub_divisions_id'=>24,'name'=>'Electrical Equipment Manufacturing']);
        DB::table('company_group')->insert(['id'=>244,'sub_divisions_id'=>24,'name'=>'Domestic Appliance Manufacturing']);
        DB::table('company_group')->insert(['id'=>245,'sub_divisions_id'=>24,'name'=>'Pump, Compressor, Heating and Ventilation Equipment Manufacturing']);
        DB::table('company_group')->insert(['id'=>246,'sub_divisions_id'=>24,'name'=>'Specialised Machinery and Equipment Manufacturing']);
        DB::table('company_group')->insert(['id'=>249,'sub_divisions_id'=>24,'name'=>'Other Machinery and Equipment Manufacturing']);
        DB::table('company_group')->insert(['id'=>251,'sub_divisions_id'=>25,'name'=>'Furniture Manufacturing']);
        DB::table('company_group')->insert(['id'=>259,'sub_divisions_id'=>25,'name'=>'Other Manufacturing']);
        DB::table('company_group')->insert(['id'=>261,'sub_divisions_id'=>26,'name'=>'Electricity Generation']);
        DB::table('company_group')->insert(['id'=>262,'sub_divisions_id'=>26,'name'=>'Electricity Transmission']);
        DB::table('company_group')->insert(['id'=>263,'sub_divisions_id'=>26,'name'=>'Electricity Distribution']);
        DB::table('company_group')->insert(['id'=>264,'sub_divisions_id'=>26,'name'=>'On Selling Electricity and Electricity Market Operation']);
        DB::table('company_group')->insert(['id'=>270,'sub_divisions_id'=>27,'name'=>'Gas Supply']);
        DB::table('company_group')->insert(['id'=>281,'sub_divisions_id'=>28,'name'=>'Water Supply, Sewerage and Drainage Services']);
        DB::table('company_group')->insert(['id'=>291,'sub_divisions_id'=>29,'name'=>'Waste Collection Services']);
        DB::table('company_group')->insert(['id'=>292,'sub_divisions_id'=>29,'name'=>'Waste Treatment, Disposal and Remediation Services']);
        DB::table('company_group')->insert(['id'=>301,'sub_divisions_id'=>30,'name'=>'Residential Building Construction']);
        DB::table('company_group')->insert(['id'=>302,'sub_divisions_id'=>30,'name'=>'Non-Residential Building Construction']);
        DB::table('company_group')->insert(['id'=>310,'sub_divisions_id'=>31,'name'=>'Heavy and Civil Engineering Construction']);
        DB::table('company_group')->insert(['id'=>321,'sub_divisions_id'=>32,'name'=>'Land Development and Site Preparation Services']);
        DB::table('company_group')->insert(['id'=>322,'sub_divisions_id'=>32,'name'=>'Building Structure Services']);
        DB::table('company_group')->insert(['id'=>323,'sub_divisions_id'=>32,'name'=>'Building Installation Services']);
        DB::table('company_group')->insert(['id'=>324,'sub_divisions_id'=>32,'name'=>'Building Completion Services']);
        DB::table('company_group')->insert(['id'=>329,'sub_divisions_id'=>32,'name'=>'Other Construction Services']);
        DB::table('company_group')->insert(['id'=>331,'sub_divisions_id'=>33,'name'=>'Agricultural Product Wholesaling']);
        DB::table('company_group')->insert(['id'=>332,'sub_divisions_id'=>33,'name'=>'Mineral, Metal and Chemical Wholesaling']);
        DB::table('company_group')->insert(['id'=>333,'sub_divisions_id'=>33,'name'=>'Timber and Hardware Goods Wholesaling']);
        DB::table('company_group')->insert(['id'=>341,'sub_divisions_id'=>34,'name'=>'Specialised Industrial Machinery and Equipment Wholesaling']);
        DB::table('company_group')->insert(['id'=>349,'sub_divisions_id'=>34,'name'=>'Other Machinery and Equipment Wholesaling']);
        DB::table('company_group')->insert(['id'=>350,'sub_divisions_id'=>35,'name'=>'Motor Vehicle and Motor Vehicle Parts Wholesaling']);
        DB::table('company_group')->insert(['id'=>360,'sub_divisions_id'=>36,'name'=>'Grocery, Liquor and Tobacco Product Wholesaling']);
        DB::table('company_group')->insert(['id'=>371,'sub_divisions_id'=>37,'name'=>'Textile, Clothing and Footwear Wholesaling']);
        DB::table('company_group')->insert(['id'=>372,'sub_divisions_id'=>37,'name'=>'Pharmaceutical and Toiletry Goods Wholesaling']);
        DB::table('company_group')->insert(['id'=>373,'sub_divisions_id'=>37,'name'=>'Furniture, Floor Covering and Other Goods Wholesaling']);
        DB::table('company_group')->insert(['id'=>380,'sub_divisions_id'=>38,'name'=>'Commission-Based Wholesaling']);
        DB::table('company_group')->insert(['id'=>391,'sub_divisions_id'=>39,'name'=>'Motor Vehicle Retailing']);
        DB::table('company_group')->insert(['id'=>392,'sub_divisions_id'=>39,'name'=>'Motor Vehicle Parts and Tyre Retailing']);
        DB::table('company_group')->insert(['id'=>400,'sub_divisions_id'=>40,'name'=>'Fuel Retailing']);
        DB::table('company_group')->insert(['id'=>411,'sub_divisions_id'=>41,'name'=>'Supermarket and Grocery Stores']);
        DB::table('company_group')->insert(['id'=>412,'sub_divisions_id'=>41,'name'=>'Specialised Food Retailing']);
        DB::table('company_group')->insert(['id'=>421,'sub_divisions_id'=>42,'name'=>'Furniture, Floor Coverings, Houseware and Textile Goods Retailing']);
        DB::table('company_group')->insert(['id'=>422,'sub_divisions_id'=>42,'name'=>'Electrical and Electronic Goods Retailing']);
        DB::table('company_group')->insert(['id'=>423,'sub_divisions_id'=>42,'name'=>'Hardware, Building and Garden Supplies Retailing']);
        DB::table('company_group')->insert(['id'=>424,'sub_divisions_id'=>42,'name'=>'Recreational Goods Retailing']);
        DB::table('company_group')->insert(['id'=>425,'sub_divisions_id'=>42,'name'=>'Clothing, Footwear and Personal Accessory Retailing']);
        DB::table('company_group')->insert(['id'=>426,'sub_divisions_id'=>42,'name'=>'Department Stores']);
        DB::table('company_group')->insert(['id'=>427,'sub_divisions_id'=>42,'name'=>'Pharmaceutical and Other Store-Based Retailing']);

        DB::table('company_group')->insert(['id'=>431,'sub_divisions_id'=>43,'name'=>'Non-Store Retailing']);
        DB::table('company_group')->insert(['id'=>432,'sub_divisions_id'=>43,'name'=>'Retail Commission-Based Buying and/or Selling']);



        DB::table('company_group')->insert(['id'=>440,'sub_divisions_id'=>44,'name'=>'Accommodation']);

        DB::table('company_group')->insert(['id'=>451,'sub_divisions_id'=>45,'name'=>'Cafes, Restaurants and Takeaway Food Services']);
        DB::table('company_group')->insert(['id'=>452,'sub_divisions_id'=>45,'name'=>'Pubs, Taverns and Bars']);
        DB::table('company_group')->insert(['id'=>453,'sub_divisions_id'=>45,'name'=>'Clubs (Hospitality)']);



        DB::table('company_group')->insert(['id'=>461,'sub_divisions_id'=>46,'name'=>'Road Freight Transport']);
        DB::table('company_group')->insert(['id'=>462,'sub_divisions_id'=>46,'name'=>'Road Passenger Transport']);

        DB::table('company_group')->insert(['id'=>471,'sub_divisions_id'=>47,'name'=>'Rail Freight Transport']);
        DB::table('company_group')->insert(['id'=>472,'sub_divisions_id'=>47,'name'=>'Rail Passenger Transport']);

        DB::table('company_group')->insert(['id'=>481,'sub_divisions_id'=>48,'name'=>'Water Freight Transport']);
        DB::table('company_group')->insert(['id'=>482,'sub_divisions_id'=>48,'name'=>'Water Passenger Transport']);

        DB::table('company_group')->insert(['id'=>490,'sub_divisions_id'=>49,'name'=>'Air and Space Transport']);

        DB::table('company_group')->insert(['id'=>501,'sub_divisions_id'=>50,'name'=>'Scenic and Sightseeing Transport']);
        DB::table('company_group')->insert(['id'=>502,'sub_divisions_id'=>50,'name'=>'Pipeline and Other Transport']);

        DB::table('company_group')->insert(['id'=>510,'sub_divisions_id'=>51,'name'=>'Postal and Courier Pick-up and Delivery Services']);

        DB::table('company_group')->insert(['id'=>521,'sub_divisions_id'=>52,'name'=>'Water Transport Support Services']);
        DB::table('company_group')->insert(['id'=>522,'sub_divisions_id'=>52,'name'=>'Airport Operations and Other Air Transport Support Services']);
        DB::table('company_group')->insert(['id'=>529,'sub_divisions_id'=>52,'name'=>'Other Transport Support Services']);

        DB::table('company_group')->insert(['id'=>530,'sub_divisions_id'=>53,'name'=>'Warehousing and Storage Services']);



        DB::table('company_group')->insert(['id'=>541,'sub_divisions_id'=>54,'name'=>'Newspaper, Periodical, Book and Directory Publishing']);
        DB::table('company_group')->insert(['id'=>542,'sub_divisions_id'=>54,'name'=>'Software Publishing']);

        DB::table('company_group')->insert(['id'=>551,'sub_divisions_id'=>55,'name'=>'Motion Picture and Video Activities']);
        DB::table('company_group')->insert(['id'=>552,'sub_divisions_id'=>55,'name'=>'Sound Recording and Music Publishing']);

        DB::table('company_group')->insert(['id'=>561,'sub_divisions_id'=>56,'name'=>'Radio Broadcasting']);
        DB::table('company_group')->insert(['id'=>562,'sub_divisions_id'=>56,'name'=>'Television Broadcasting']);

        DB::table('company_group')->insert(['id'=>570,'sub_divisions_id'=>57,'name'=>'Internet Publishing and Broadcasting']);

        DB::table('company_group')->insert(['id'=>580,'sub_divisions_id'=>58,'name'=>'Telecommunications Services']);

        DB::table('company_group')->insert(['id'=>591,'sub_divisions_id'=>59,'name'=>'Internet Service Providers and Web Search Portals']);
        DB::table('company_group')->insert(['id'=>592,'sub_divisions_id'=>59,'name'=>'Data Processing, Web Hosting and Electronic Information Storage Services']);

        DB::table('company_group')->insert(['id'=>601,'sub_divisions_id'=>60,'name'=>'Libraries and Archives']);
        DB::table('company_group')->insert(['id'=>602,'sub_divisions_id'=>60,'name'=>'Other Information Services']);



        DB::table('company_group')->insert(['id'=>621,'sub_divisions_id'=>62,'name'=>'Central Banking']);
        DB::table('company_group')->insert(['id'=>622,'sub_divisions_id'=>62,'name'=>'Depository Financial Intermediation']);
        DB::table('company_group')->insert(['id'=>623,'sub_divisions_id'=>62,'name'=>'Non-Depository Financing']);
        DB::table('company_group')->insert(['id'=>624,'sub_divisions_id'=>62,'name'=>'Financial Asset Investing']);

        DB::table('company_group')->insert(['id'=>631,'sub_divisions_id'=>63,'name'=>'Life Insurance']);
        DB::table('company_group')->insert(['id'=>632,'sub_divisions_id'=>63,'name'=>'Health and General Insurance']);
        DB::table('company_group')->insert(['id'=>633,'sub_divisions_id'=>63,'name'=>'Superannuation Funds']);

        DB::table('company_group')->insert(['id'=>641,'sub_divisions_id'=>64,'name'=>'Auxiliary Finance and Investment Services']);
        DB::table('company_group')->insert(['id'=>642,'sub_divisions_id'=>64,'name'=>'Auxiliary Insurance Services']);



        DB::table('company_group')->insert(['id'=>661,'sub_divisions_id'=>66,'name'=>'Motor Vehicle and Transport Equipment Rental and Hiring']);
        DB::table('company_group')->insert(['id'=>662,'sub_divisions_id'=>66,'name'=>'Farm Animal and Bloodstock Leasing']);
        DB::table('company_group')->insert(['id'=>663,'sub_divisions_id'=>66,'name'=>'Other Goods and Equipment Rental and Hiring']);
        DB::table('company_group')->insert(['id'=>664,'sub_divisions_id'=>66,'name'=>'Non-Financial Intangible Assets (Except Copyrights) Leasing']);

        DB::table('company_group')->insert(['id'=>671,'sub_divisions_id'=>67,'name'=>'Property Operators']);
        DB::table('company_group')->insert(['id'=>672,'sub_divisions_id'=>67,'name'=>'Real Estate Services']);




        DB::table('company_group')->insert(['id'=>691,'sub_divisions_id'=>69,'name'=>'Scientific Research Services']);
        DB::table('company_group')->insert(['id'=>692,'sub_divisions_id'=>69,'name'=>'Architectural, Engineering and Technical Services']);
        DB::table('company_group')->insert(['id'=>693,'sub_divisions_id'=>69,'name'=>'Legal and Accounting Services']);
        DB::table('company_group')->insert(['id'=>694,'sub_divisions_id'=>69,'name'=>'Advertising Services']);
        DB::table('company_group')->insert(['id'=>695,'sub_divisions_id'=>69,'name'=>'Market Research and Statistical Services']);
        DB::table('company_group')->insert(['id'=>696,'sub_divisions_id'=>69,'name'=>'Management and Related Consulting Services']);
        DB::table('company_group')->insert(['id'=>697,'sub_divisions_id'=>69,'name'=>'Veterinary Services']);
        DB::table('company_group')->insert(['id'=>699,'sub_divisions_id'=>69,'name'=>'Other Professional, Scientific and Technical Services']);

        DB::table('company_group')->insert(['id'=>700,'sub_divisions_id'=>70,'name'=>'Computer System Design and Related Services']);



        DB::table('company_group')->insert(['id'=>721,'sub_divisions_id'=>72,'name'=>'Employment Services']);
        DB::table('company_group')->insert(['id'=>722,'sub_divisions_id'=>72,'name'=>'Travel Agency and Tour Arrangement Services']);
        DB::table('company_group')->insert(['id'=>729,'sub_divisions_id'=>72,'name'=>'Other Administrative Services']);

        DB::table('company_group')->insert(['id'=>731,'sub_divisions_id'=>73,'name'=>'Building Cleaning, Pest Control and Gardening Services']);
        DB::table('company_group')->insert(['id'=>732,'sub_divisions_id'=>73,'name'=>'Packaging Services']);



        DB::table('company_group')->insert(['id'=>751,'sub_divisions_id'=>75,'name'=>'Central Government Administration']);
        DB::table('company_group')->insert(['id'=>752,'sub_divisions_id'=>75,'name'=>'State Government Administration']);
        DB::table('company_group')->insert(['id'=>753,'sub_divisions_id'=>75,'name'=>'Local Government Administration']);
        DB::table('company_group')->insert(['id'=>754,'sub_divisions_id'=>75,'name'=>'Justice']);
        DB::table('company_group')->insert(['id'=>755,'sub_divisions_id'=>75,'name'=>'Government Representation']);

        DB::table('company_group')->insert(['id'=>760,'sub_divisions_id'=>76,'name'=>'Defence']);

        DB::table('company_group')->insert(['id'=>771,'sub_divisions_id'=>77,'name'=>'Public Order and Safety Services']);
        DB::table('company_group')->insert(['id'=>772,'sub_divisions_id'=>77,'name'=>'Regulatory Services']);



        DB::table('company_group')->insert(['id'=>801,'sub_divisions_id'=>80,'name'=>'Preschool Education']);
        DB::table('company_group')->insert(['id'=>802,'sub_divisions_id'=>80,'name'=>'School Education']);

        DB::table('company_group')->insert(['id'=>810,'sub_divisions_id'=>81,'name'=>'Tertiary Education']);

        DB::table('company_group')->insert(['id'=>821,'sub_divisions_id'=>82,'name'=>'Adult, Community and Other Education']);
        DB::table('company_group')->insert(['id'=>822,'sub_divisions_id'=>82,'name'=>'Educational Support Services']);



        DB::table('company_group')->insert(['id'=>840,'sub_divisions_id'=>84,'name'=>'Hospitals']);

        DB::table('company_group')->insert(['id'=>851,'sub_divisions_id'=>85,'name'=>'Medical Services']);
        DB::table('company_group')->insert(['id'=>852,'sub_divisions_id'=>85,'name'=>'Pathology and Diagnostic Imaging Services']);
        DB::table('company_group')->insert(['id'=>853,'sub_divisions_id'=>85,'name'=>'Allied Health Services']);
        DB::table('company_group')->insert(['id'=>859,'sub_divisions_id'=>85,'name'=>'Other Health Care Services']);

        DB::table('company_group')->insert(['id'=>860,'sub_divisions_id'=>86,'name'=>'Residential Care Services']);

        DB::table('company_group')->insert(['id'=>871,'sub_divisions_id'=>87,'name'=>'Child Care Services']);
        DB::table('company_group')->insert(['id'=>879,'sub_divisions_id'=>87,'name'=>'Other Social Assistance Services']);



        DB::table('company_group')->insert(['id'=>891,'sub_divisions_id'=>89,'name'=>'Museum Operation']);
        DB::table('company_group')->insert(['id'=>892,'sub_divisions_id'=>89,'name'=>'Parks and Gardens Operations']);

        DB::table('company_group')->insert(['id'=>900,'sub_divisions_id'=>90,'name'=>'Creative and Performing Arts Activities']);

        DB::table('company_group')->insert(['id'=>911,'sub_divisions_id'=>91,'name'=>'Sports and Physical Recreation Activities']);
        DB::table('company_group')->insert(['id'=>912,'sub_divisions_id'=>91,'name'=>'Horse and Dog Racing Activities']);
        DB::table('company_group')->insert(['id'=>913,'sub_divisions_id'=>91,'name'=>'Amusement and Other Recreation Activities']);

        DB::table('company_group')->insert(['id'=>920,'sub_divisions_id'=>92,'name'=>'Gambling Activities']);



        DB::table('company_group')->insert(['id'=>941,'sub_divisions_id'=>94,'name'=>'Automotive Repair and Maintenance']);
        DB::table('company_group')->insert(['id'=>942,'sub_divisions_id'=>94,'name'=>'Machinery and Equipment Repair and Maintenance']);
        DB::table('company_group')->insert(['id'=>949,'sub_divisions_id'=>94,'name'=>'Other Repair and Maintenance']);

        DB::table('company_group')->insert(['id'=>951,'sub_divisions_id'=>95,'name'=>'Personal Care Services']);
        DB::table('company_group')->insert(['id'=>952,'sub_divisions_id'=>95,'name'=>'Funeral, Crematorium and Cemetery Services']);
        DB::table('company_group')->insert(['id'=>953,'sub_divisions_id'=>95,'name'=>'Other Personal Services']);
        DB::table('company_group')->insert(['id'=>954,'sub_divisions_id'=>95,'name'=>'Religious Services']);
        DB::table('company_group')->insert(['id'=>955,'sub_divisions_id'=>95,'name'=>'Civic, Professional and Other Interest Group Services']);


        DB::table('company_group')->insert(['id'=>960,'sub_divisions_id'=>96,'name'=>'Private Households Employing Staff and Undifferentiated Goods- and Service-Producing Activities of Households for Own Use']);

    }
}
//
