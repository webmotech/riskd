<?php

use Illuminate\Database\Seeder;

class ConfidentialitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('confidentiality')->insert(['name'=>'Public']);
        DB::table('confidentiality')->insert(['name'=>'Asic']);
        DB::table('confidentiality')->insert(['name'=>'Published']);
        DB::table('confidentiality')->insert(['name'=>'Confidential']);
    }
}
