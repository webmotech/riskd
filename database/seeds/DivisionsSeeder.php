<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DivisionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('divisions')->insert(['id'=>'1','name'=>'Agriculture, Forestry and Fishing']);
        DB::table('divisions')->insert(['id'=>'2','name'=>'Mining']);
        DB::table('divisions')->insert(['id'=>'3','name'=>'Manufacturing']);
        DB::table('divisions')->insert(['id'=>'4','name'=>'Electricity, Gas, Water and Waste Services']);
        DB::table('divisions')->insert(['id'=>'5','name'=>'Construction']);
        DB::table('divisions')->insert(['id'=>'6','name'=>'Wholesale Trade']);
        DB::table('divisions')->insert(['id'=>'7','name'=>'Retail Trade']);
        DB::table('divisions')->insert(['id'=>'8','name'=>'Accommodation and Food Services']);
        DB::table('divisions')->insert(['id'=>'9','name'=>'Transport, Postal and Warehousing']);
        DB::table('divisions')->insert(['id'=>'10','name'=>'Information Media and Telecommunications']);
        DB::table('divisions')->insert(['id'=>'11','name'=>'Financial and Insurance Services']);
        DB::table('divisions')->insert(['id'=>'12','name'=>'Rental, Hiring and Real Estate Services']);
        DB::table('divisions')->insert(['id'=>'13','name'=>'Professional, Scientific and Technical Services']);
        DB::table('divisions')->insert(['id'=>'14','name'=>'Administrative and Support Services']);
        DB::table('divisions')->insert(['id'=>'15','name'=>'Public Administration and Safety']);
        DB::table('divisions')->insert(['id'=>'16','name'=>'Education and Training']);
        DB::table('divisions')->insert(['id'=>'17','name'=>'Health Care and Social Assistance']);
        DB::table('divisions')->insert(['id'=>'18','name'=>'Arts and Recreation Services']);
        DB::table('divisions')->insert(['id'=>'19','name'=>'Other Services']);

    }
}
