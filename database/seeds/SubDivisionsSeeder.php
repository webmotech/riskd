<?php

use Illuminate\Database\Seeder;

class SubDivisionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_divisions')->insert(['id'=>'01','divisions_id'=>1,'name'=>'Agriculture']);
        DB::table('sub_divisions')->insert(['id'=>'02','divisions_id'=>1,'name'=>'Aquaculture']);
        DB::table('sub_divisions')->insert(['id'=>'03','divisions_id'=>1,'name'=>'Forestry and Logging']);
        DB::table('sub_divisions')->insert(['id'=>'04','divisions_id'=>1,'name'=>'Fishing, Hunting and Trapping']);
        DB::table('sub_divisions')->insert(['id'=>'05','divisions_id'=>1,'name'=>'Agriculture, Forestry and Fishing Support Services']);
        DB::table('sub_divisions')->insert(['id'=>'06','divisions_id'=>2,'name'=>'Coal Mining']);
        DB::table('sub_divisions')->insert(['id'=>'07','divisions_id'=>2,'name'=>'Oil and Gas Extraction']);
        DB::table('sub_divisions')->insert(['id'=>'08','divisions_id'=>2,'name'=>'Metal Ore Mining']);
        DB::table('sub_divisions')->insert(['id'=>'09','divisions_id'=>2,'name'=>'Non-Metallic Mineral Mining and Quarrying']);
        DB::table('sub_divisions')->insert(['id'=>'10','divisions_id'=>2,'name'=>'Exploration and Other Mining Support Services']);
        DB::table('sub_divisions')->insert(['id'=>'11','divisions_id'=>3,'name'=>'Food Product Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'12','divisions_id'=>3,'name'=>'Beverage and Tobacco Product Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'13','divisions_id'=>3,'name'=>'Textile, Leather, Clothing and Footwear Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'14','divisions_id'=>3,'name'=>'Wood Product Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'15','divisions_id'=>3,'name'=>'Pulp, Paper and Converted Paper Product Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'16','divisions_id'=>3,'name'=>'Printing (including the Reproduction of Recorded Media)']);
        DB::table('sub_divisions')->insert(['id'=>'17','divisions_id'=>3,'name'=>'Petroleum and Coal Product Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'18','divisions_id'=>3,'name'=>'Basic Chemical and Chemical Product Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'19','divisions_id'=>3,'name'=>'Polymer Product and Rubber Product Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'20','divisions_id'=>3,'name'=>'Non-Metallic Mineral Product Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'21','divisions_id'=>3,'name'=>'Primary Metal and Metal Product Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'22','divisions_id'=>3,'name'=>'Fabricated Metal Product Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'23','divisions_id'=>3,'name'=>'Transport Equipment Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'24','divisions_id'=>3,'name'=>'Machinery and Equipment Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'25','divisions_id'=>3,'name'=>'Furniture and Other Manufacturing']);
        DB::table('sub_divisions')->insert(['id'=>'26','divisions_id'=>4,'name'=>'Electricity Supply']);
        DB::table('sub_divisions')->insert(['id'=>'27','divisions_id'=>4,'name'=>'Gas Supply']);
        DB::table('sub_divisions')->insert(['id'=>'28','divisions_id'=>4,'name'=>'Water Supply, Sewerage and Drainage Services']);
        DB::table('sub_divisions')->insert(['id'=>'29','divisions_id'=>4,'name'=>'Waste Collection, Treatment and Disposal Services']);
        DB::table('sub_divisions')->insert(['id'=>'30','divisions_id'=>5,'name'=>'Building Construction']);
        DB::table('sub_divisions')->insert(['id'=>'31','divisions_id'=>5,'name'=>'Heavy and Civil Engineering Construction']);
        DB::table('sub_divisions')->insert(['id'=>'32','divisions_id'=>5,'name'=>'Construction Services']);
        DB::table('sub_divisions')->insert(['id'=>'33','divisions_id'=>6,'name'=>'Basic Material Wholesaling']);
        DB::table('sub_divisions')->insert(['id'=>'34','divisions_id'=>6,'name'=>'Machinery and Equipment Wholesaling']);
        DB::table('sub_divisions')->insert(['id'=>'35','divisions_id'=>6,'name'=>'Motor Vehicle and Motor Vehicle Parts Wholesaling']);
        DB::table('sub_divisions')->insert(['id'=>'36','divisions_id'=>6,'name'=>'Grocery, Liquor and Tobacco Product Wholesaling']);
        DB::table('sub_divisions')->insert(['id'=>'37','divisions_id'=>6,'name'=>'Other Goods Wholesaling']);
        DB::table('sub_divisions')->insert(['id'=>'38','divisions_id'=>6,'name'=>'Commission-Based Wholesaling']);
        DB::table('sub_divisions')->insert(['id'=>'39','divisions_id'=>7,'name'=>'Motor Vehicle and Motor Vehicle Parts Retailing']);
        DB::table('sub_divisions')->insert(['id'=>'40','divisions_id'=>7,'name'=>'Fuel Retailing']);
        DB::table('sub_divisions')->insert(['id'=>'41','divisions_id'=>7,'name'=>'Food Retailing']);
        DB::table('sub_divisions')->insert(['id'=>'42','divisions_id'=>7,'name'=>'Other Store-Based Retailing']);
        DB::table('sub_divisions')->insert(['id'=>'43','divisions_id'=>7,'name'=>'Non-Store Retailing and Retail Commission-Based Buying and/or Selling']);
        DB::table('sub_divisions')->insert(['id'=>'44','divisions_id'=>8,'name'=>'Accommodation']);
        DB::table('sub_divisions')->insert(['id'=>'45','divisions_id'=>8,'name'=>'Food and Beverage Services']);
        DB::table('sub_divisions')->insert(['id'=>'46','divisions_id'=>9,'name'=>'Road Transport']);
        DB::table('sub_divisions')->insert(['id'=>'47','divisions_id'=>9,'name'=>'Rail Transport']);
        DB::table('sub_divisions')->insert(['id'=>'48','divisions_id'=>9,'name'=>'Water Transport']);
        DB::table('sub_divisions')->insert(['id'=>'49','divisions_id'=>9,'name'=>'Air and Space Transport']);
        DB::table('sub_divisions')->insert(['id'=>'50','divisions_id'=>9,'name'=>'Other Transport']);
        DB::table('sub_divisions')->insert(['id'=>'51','divisions_id'=>9,'name'=>'Postal and Courier Pick-up and Delivery Services']);
        DB::table('sub_divisions')->insert(['id'=>'52','divisions_id'=>9,'name'=>'Transport Support Services']);
        DB::table('sub_divisions')->insert(['id'=>'53','divisions_id'=>9,'name'=>'Warehousing and Storage Services']);
        DB::table('sub_divisions')->insert(['id'=>'54','divisions_id'=>10,'name'=>'Publishing (except Internet and Music Publishing)']);
        DB::table('sub_divisions')->insert(['id'=>'55','divisions_id'=>10,'name'=>'Motion Picture and Sound Recording Activities']);
        DB::table('sub_divisions')->insert(['id'=>'56','divisions_id'=>10,'name'=>'Broadcasting (except Internet)']);
        DB::table('sub_divisions')->insert(['id'=>'57','divisions_id'=>10,'name'=>'Internet Publishing and Broadcasting']);
        DB::table('sub_divisions')->insert(['id'=>'58','divisions_id'=>10,'name'=>'Telecommunications Services']);
        DB::table('sub_divisions')->insert(['id'=>'59','divisions_id'=>10,'name'=>'Internet Service Providers, Web Search Portals and Data Processing Services']);
        DB::table('sub_divisions')->insert(['id'=>'60','divisions_id'=>10,'name'=>'Library and Other Information Services']);
        DB::table('sub_divisions')->insert(['id'=>'62','divisions_id'=>11,'name'=>'Finance']);
        DB::table('sub_divisions')->insert(['id'=>'63','divisions_id'=>11,'name'=>'Insurance and Superannuation Funds']);
        DB::table('sub_divisions')->insert(['id'=>'64','divisions_id'=>11,'name'=>'Auxiliary Finance and Insurance Services']);
        DB::table('sub_divisions')->insert(['id'=>'66','divisions_id'=>12,'name'=>'Rental and Hiring Services (except Real Estate)']);
        DB::table('sub_divisions')->insert(['id'=>'67','divisions_id'=>12,'name'=>'Property Operators and Real Estate Services']);
        DB::table('sub_divisions')->insert(['id'=>'69','divisions_id'=>13,'name'=>'Professional, Scientific and Technical Services (Except Computer System Design and Related Services)']);
        DB::table('sub_divisions')->insert(['id'=>'70','divisions_id'=>13,'name'=>'Computer System Design and Related Services']);
        DB::table('sub_divisions')->insert(['id'=>'72','divisions_id'=>14,'name'=>'Administrative Services']);
        DB::table('sub_divisions')->insert(['id'=>'73','divisions_id'=>14,'name'=>'Building Cleaning, Pest Control and Other Support Services']);
        DB::table('sub_divisions')->insert(['id'=>'75','divisions_id'=>15,'name'=>'Public Administration']);
        DB::table('sub_divisions')->insert(['id'=>'76','divisions_id'=>15,'name'=>'Defence']);
        DB::table('sub_divisions')->insert(['id'=>'77','divisions_id'=>15,'name'=>'Public Order, Safety and Regulatory Services']);
        DB::table('sub_divisions')->insert(['id'=>'80','divisions_id'=>16,'name'=>'Preschool and School Education']);
        DB::table('sub_divisions')->insert(['id'=>'81','divisions_id'=>16,'name'=>'Tertiary Education']);
        DB::table('sub_divisions')->insert(['id'=>'82','divisions_id'=>16,'name'=>'Adult, Community and Other Education']);
        DB::table('sub_divisions')->insert(['id'=>'84','divisions_id'=>17,'name'=>'Hospitals']);
        DB::table('sub_divisions')->insert(['id'=>'85','divisions_id'=>17,'name'=>'Medical and Other Health Care Services']);
        DB::table('sub_divisions')->insert(['id'=>'86','divisions_id'=>17,'name'=>'Residential Care Services']);
        DB::table('sub_divisions')->insert(['id'=>'87','divisions_id'=>17,'name'=>'Social Assistance Services']);
        DB::table('sub_divisions')->insert(['id'=>'89','divisions_id'=>18,'name'=>'Heritage Activities']);
        DB::table('sub_divisions')->insert(['id'=>'90','divisions_id'=>18,'name'=>'Creative and Performing Arts Activities']);
        DB::table('sub_divisions')->insert(['id'=>'91','divisions_id'=>18,'name'=>'Sports and Recreation Activities']);
        DB::table('sub_divisions')->insert(['id'=>'92','divisions_id'=>18,'name'=>'Gambling Activities']);
        DB::table('sub_divisions')->insert(['id'=>'94','divisions_id'=>19,'name'=>'Repair and Maintenance']);
        DB::table('sub_divisions')->insert(['id'=>'95','divisions_id'=>19,'name'=>'Personal and Other Services']);
        DB::table('sub_divisions')->insert(['id'=>'96','divisions_id'=>19,'name'=>'Private Households Employing Staff and Undifferentiated Goods- and Service-Producing Activities of Households for Own Use']);

    }
}
