<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DesignationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('designations')->insert(['name'=>'Junier Analyst']);
        DB::table('designations')->insert(['name'=>'Senier Analyst']);
    }
}
