<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fname'=>'Admin',
            'lname'=>'User',
            'email'=>'admin@gmail.com',
            'user_role_id'=>1,
            'designation_id'=>0,
            'address'=>'test',
            'telephone'=>'123456789',
            'username'=>'admin',
            'backup_password'=>bcrypt('admin'),
            'password'=>bcrypt('Abcd!234'),
        ]);
    }
}
