<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserRoleSeeder::class);
         $this->call(DesignationSeeder::class);
         $this->call(DivisionsSeeder::class);
         $this->call(SubDivisionsSeeder::class);
         $this->call(CompanyGroupSeeder::class);
         $this->call(CompanyClassSeeder::class);
         $this->call(EntityTypeSeeder::class);
         $this->call(ConfidentialitySeeder::class);
         $this->call(CountrySeeder::class);
         $this->call(UsersSeeder::class);
    }
}
