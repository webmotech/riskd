<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert(['role_code'=>'ADMIN','name'=>'Administrator','template'=>'admin.layout.menu.admin-menu']);
        DB::table('user_roles')->insert(['role_code'=>'MANAGER','name'=>'Manager','template'=>'admin.layout.menu.manager-menu']);
        DB::table('user_roles')->insert(['role_code'=>'JUNIOUR_ANALYST','name'=>'Juniour Analyst','template'=>'admin.layout.menu.juniour-analyst-menu']);
        DB::table('user_roles')->insert(['role_code'=>'SENIOR_ANALYST','name'=>'Senior Analyst','template'=>'admin.layout.menu.senior-analyst-menu']);
    }
}
