<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddDataToReportStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('report_status')->insert(['name'=>'Pending For Approval', 'value'=>0]);
        DB::table('report_status')->insert(['name'=>'Approved', 'value'=>1]);
        DB::table('report_status')->insert(['name'=>'Rejected', 'value'=>2]);
        DB::table('report_status')->insert(['name'=>'Editable', 'value'=>3]);
        DB::table('report_status')->insert(['name'=>'Generated', 'value'=>4]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_status', function (Blueprint $table) {
            //
        });
    }
}
