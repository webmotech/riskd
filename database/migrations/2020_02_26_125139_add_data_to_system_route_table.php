<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddDataToSystemRouteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::table('system_route')->insert(['route_name'=>'generate-report']);
       DB::table('system_route')->insert(['route_name'=>'approve-report']);
       DB::table('system_route')->insert(['route_name'=>'users.create']);
       DB::table('system_route')->insert(['route_name'=>'users.store']);
       DB::table('system_route')->insert(['route_name'=>'users.destroy']);
       DB::table('system_route')->insert(['route_name'=>'users.edit']);
       DB::table('system_route')->insert(['route_name'=>'users.update']);
       DB::table('system_route')->insert(['route_name'=>'company.edit']);
       DB::table('system_route')->insert(['route_name'=>'company.update']);
       DB::table('system_route')->insert(['route_name'=>'view-report']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('system_route', function (Blueprint $table) {
            //
        });
    }
}
