<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('report_id');
            $table->string('rounding');
            $table->string('base_currency');
            $table->string('quality');
            $table->integer('report_period_months');
            $table->string('scope');
            $table->integer('confidentiality_id');
            $table->integer('financial_year');
            $table->string('financial_month');
            $table->double('sales',10,2);
            $table->double('cost_of_sales',10,4);
            $table->double('gross_profit',10,4);
            $table->double('other_income',10,4);
            $table->double('depreciation',10,4);
            $table->double('amortisation',10,4);
            $table->double('impairment',10,4);
            $table->double('interest_expenses_gross',10,4);
            $table->double('operating_lease_expenses',10,4);
            $table->double('finance_lease_hire_expenses_charges',10,4);
            $table->double('non_recurring_gain_loses',10,4);
            $table->double('other_gain_loses',10,4);
            $table->double('other_expenses',10,4);
            $table->double('ebit',10,4);
            $table->double('ebitda',10,4);
            $table->double('normalized_ebitda',10,4);
            $table->double('profit_before_tax',10,4);
            $table->double('profit_before_tax_after_abnormals',10,4);
            $table->double('tax_benefit_expenses',10,4);
            $table->double('profit_after_tax',10,4);
            $table->double('distribution_ordividends',10,4);
            $table->double('other_post_tax_items_gains_losses',10,4);
            $table->double('profit_after_tax_distribution',10,4);
            $table->double('cash',10,4);
            $table->double('trade_debtors',10,4);
            $table->double('total_inventories',10,4);
            $table->double('loan_to_related_parties_1',10,4);
            $table->double('other_current_assets',10,4);
            $table->double('total_current_assets',10,4);
            $table->double('fixed_assets',10,4);
            $table->double('net_intangibles',10,4);
            $table->double('loan_to_related_parties_2',10,4);
            $table->double('other_non_current_assets',10,4);
            $table->double('total_non_current_assets',10,4);
            $table->double('total_assets',10,4);
            $table->double('trade_creditors',10,4);
            $table->double('interest_bearing_debt_1',10,4);
            $table->double('loan_from_related_parties_1',10,4);
            $table->double('other_current_liabilities',10,4);
            $table->double('total_current_liabilities',10,4);
            $table->double('interest_bearing_debt_2',10,4);
            $table->double('loan_from_related_parties_2',10,4);
            $table->double('other_non_current_liabilities',10,4);
            $table->double('total_non_current_liabilities',10,4);
            $table->double('total_liabilities',10,4);
            $table->double('share_capital',10,4);
            $table->double('prefence_shares',10,4);
            $table->double('threasury_shares',10,4);
            $table->double('equity_ownerships',10,4);
            $table->double('total_reserves',10,4);
            $table->double('retained_earning',10,4);
            $table->double('minorty_interest',10,4);
            $table->double('total_equity',10,4);
            $table->double('balance',10,4);
            $table->double('operating_cash_flow',10,4);
            $table->double('contingent_liabilities',10,4);
            $table->double('other_commitmentes',10,4);
            $table->double('operating_lease_outstanding',10,4);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports_details');
    }
}
