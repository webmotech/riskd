<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyAttributeInKeyRatioCalculationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('key_ratio_calculation', function (Blueprint $table) {
            $table->decimal('gross_profit_margin',20,4)->nullable()->change();
            $table->decimal('ebitda',20,4)->nullable()->change();
            $table->decimal('normalised_ebitda',20,4)->nullable()->change();
            $table->decimal('ebit',20,4)->nullable()->change();
            $table->decimal('net_profit_margin',20,4)->nullable()->change();
            $table->decimal('profitability',20,4)->nullable()->change();
            $table->decimal('return_on_investment',20,4)->nullable()->change();
            $table->decimal('return_on_assets',20,4)->nullable()->change();
            $table->decimal('return_on_equity',20,4)->nullable()->change();
            $table->decimal('working_capital',20,4)->nullable()->change();
            $table->decimal('working_capital_to_sales',20,4)->nullable()->change();
            $table->decimal('cash_flow_coverage',20,4)->nullable()->change();
            $table->decimal('cash_ratio',20,4)->nullable()->change();
            $table->decimal('current_ratio',20,4)->nullable()->change();
            $table->decimal('quick_ratio',20,4)->nullable()->change();
            $table->decimal('capital_adequacy',20,4)->nullable()->change();
            $table->decimal('net_tangible_worth',20,4)->nullable()->change();
            $table->decimal('net_asset_backing',20,4)->nullable()->change();
            $table->decimal('gearing',20,4)->nullable()->change();
            $table->decimal('debt_to_equity',20,4)->nullable()->change();
            $table->decimal('interest_coverage',20,4)->nullable()->change();
            $table->decimal('repayment_capability',20,4)->nullable()->change();
            $table->decimal('financial_leverage',20,4)->nullable()->change();
            $table->decimal('short_ratio',20,4)->nullable()->change();
            $table->decimal('operating_leverage',20,4)->nullable()->change();
            $table->decimal('creditor_exposure',20,4)->nullable()->change();
            $table->decimal('creditor_days',20,4)->nullable()->change();
            $table->decimal('inventory_days',20,4)->nullable()->change();
            $table->decimal('debtor_days',20,4)->nullable()->change();
            $table->decimal('cash_conversion_cycle',20,4)->nullable()->change();
            $table->decimal('sales_annualised',20,4)->nullable()->change();
            $table->decimal('activity',20,4)->nullable()->change();
            $table->decimal('sales_growth',20,4)->nullable()->change();
            $table->decimal('related_party_loans_receivable',20,4)->nullable()->change();
            $table->decimal('related_party_loans_payable',20,4)->nullable()->change();
            $table->decimal('related_party_loans_dependency',20,4)->nullable()->change();
            $table->decimal('quick_asset_composition',20,4)->nullable()->change();
            $table->decimal('current_asset_composition',20,4)->nullable()->change();
            $table->decimal('current_liability_composition',20,4)->nullable()->change();
            $table->decimal('zscore_risk_measure',20,4)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('key_ratio_calculation', function (Blueprint $table) {
            //
        });
    }
}
