<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditorwatchApiDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditorwatch_api_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('report_id')->nullable();
            $table->string('main_name')->nullable();
            $table->string('abn')->nullable();
            $table->string('entity_status')->nullable();
            $table->string('entity_status_effective_from')->nullable();
            $table->string('entity_type')->nullable();
            $table->string('gst')->nullable();
            $table->string('locality')->nullable();
            $table->string('record_last_updated')->nullable();
            $table->string('name')->nullable();
            $table->string('acn')->nullable();
            $table->string('type')->nullable();
            $table->string('staus')->nullable();
            $table->string('registration_date')->nullable();
            $table->string('review_date')->nullable();
            $table->string('class')->nullable();
            $table->string('subclass')->nullable();
            $table->string('asic_locality')->nullable();
            $table->string('current_credit_score')->nullable();
            $table->string('history_credit_score')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditorwatch_api_data');
    }
}
