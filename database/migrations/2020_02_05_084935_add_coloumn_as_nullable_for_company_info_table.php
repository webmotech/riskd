<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColoumnAsNullableForCompanyInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_info', function (Blueprint $table) {
            $table->string('company_token');
            $table->string('abn')->nullable()->change();
            $table->string('acn')->nullable()->change();
            $table->string('rbn')->nullable()->change();
            $table->string('equity')->nullable()->change();
            $table->integer('confidentialiity_id')->nullable()->change();
            $table->string('address_unit_number')->nullable()->change();
            $table->string('address_street_number')->nullable()->change();
            $table->string('address_street_name')->nullable()->change();
            $table->string('address_street_suburb')->nullable()->change();
            $table->string('address_suburb')->nullable()->change();
            $table->string('address_state')->nullable()->change();
            $table->string('address_postal_code')->nullable()->change();
            $table->string('contact_telephone_type')->nullable()->change();
            $table->string('contact_telephone_number')->nullable()->change();
            $table->string('business_telephone_type')->nullable()->change();
            $table->string('business_telephone_number')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
