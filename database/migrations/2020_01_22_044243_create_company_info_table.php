<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('entity_type_id');
            $table->string('abn');
            $table->string('acn');
            $table->string('rbn');
            $table->string('equity');
            $table->date('established_date');
            $table->integer('confidentialiity_id');
            $table->string('portfolio_analysis_status');
            $table->string('address_unit_number');
            $table->string('address_street_number');
            $table->string('address_street_name');
            $table->string('address_street_suburb');
            $table->string('address_suburb');
            $table->string('address_state');
            $table->string('country_code');
            $table->string('address_postal_code');
            $table->string('contact_telephone_type');
            $table->string('contact_telephone_number');
            $table->string('business_telephone_type');
            $table->string('business_telephone_number');
            $table->integer('anzic_classification_division_id');
            $table->integer('anzic_classification_sub_division_id');
            $table->integer('anzic_classification_group_id');
            $table->integer('anzic_classification_class_id');
            $table->integer('primary_classification_division_id');
            $table->integer('primary_classification_sub_division_id');
            $table->integer('primary_classification_group_id');
            $table->integer('primary_classification_class_id');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_info');
    }
}
