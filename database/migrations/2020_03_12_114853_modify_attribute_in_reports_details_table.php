<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyAttributeInReportsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_details', function (Blueprint $table) {
            $table->decimal('sales',20,4)->nullable()->change();
            $table->decimal('cost_of_sales',20,4)->nullable()->change();
            $table->decimal('gross_profit',20,4)->nullable()->change();
            $table->decimal('other_income',20,4)->nullable()->change();
            $table->decimal('depreciation',20,4)->nullable()->change();
            $table->decimal('amortisation',20,4)->nullable()->change();
            $table->decimal('impairment',10,4)->nullable()->change();
            $table->decimal('interest_expenses_gross',20,4)->nullable()->change();
            $table->decimal('operating_lease_expenses',20,4)->nullable()->change();
            $table->decimal('finance_lease_hire_expenses_charges',20,4)->nullable()->change();
            $table->decimal('non_recurring_gain_loses',20,4)->nullable()->change();
            $table->decimal('other_gain_loses',20,4)->nullable()->change();
            $table->decimal('other_expenses',20,4)->nullable()->change();
            $table->decimal('ebit',20,4)->nullable()->change();
            $table->decimal('ebitda',20,4)->nullable()->change();
            $table->decimal('normalized_ebitda',20,4)->nullable()->change();
            $table->decimal('profit_before_tax',20,4)->nullable()->change();
            $table->decimal('profit_before_tax_after_abnormals',20,4)->nullable()->change();
            $table->decimal('tax_benefit_expenses',20,4)->nullable()->change();
            $table->decimal('profit_after_tax',20,4)->nullable()->change();
            $table->decimal('distribution_ordividends',20,4)->nullable()->change();
            $table->decimal('other_post_tax_items_gains_losses',20,4)->nullable()->change();
            $table->decimal('profit_after_tax_distribution',20,4)->nullable()->change();
            $table->decimal('cash',20,4)->nullable()->change();
            $table->decimal('trade_debtors',20,4)->nullable()->change();
            $table->decimal('total_inventories',20,4)->nullable()->change();
            $table->decimal('loan_to_related_parties_1',20,4)->nullable()->change();
            $table->decimal('other_current_assets',20,4)->nullable()->change();
            $table->decimal('total_current_assets',20,4)->nullable()->change();
            $table->decimal('fixed_assets',20,4)->nullable()->change();
            $table->decimal('net_intangibles',20,4)->nullable()->change();
            $table->decimal('loan_to_related_parties_2',20,4)->nullable()->change();
            $table->decimal('other_non_current_assets',20,4)->nullable()->change();
            $table->decimal('total_non_current_assets',20,4)->nullable()->change();
            $table->decimal('total_assets',20,4)->nullable()->change();
            $table->decimal('trade_creditors',20,4)->nullable()->change();
            $table->decimal('interest_bearing_debt_1',20,4)->nullable()->change();
            $table->decimal('loan_from_related_parties_1',20,4)->nullable()->change();
            $table->decimal('other_current_liabilities',20,4)->nullable()->change();
            $table->decimal('total_current_liabilities',20,4)->nullable()->change();
            $table->decimal('interest_bearing_debt_2',20,4)->nullable()->change();
            $table->decimal('loan_from_related_parties_2',20,4)->nullable()->change();
            $table->decimal('other_non_current_liabilities',20,4)->nullable()->change();
            $table->decimal('total_non_current_liabilities',20,4)->nullable()->change();
            $table->decimal('total_liabilities',20,4)->nullable()->change();
            $table->decimal('share_capital',20,4)->nullable()->change();
            $table->decimal('prefence_shares',20,4)->nullable()->change();
            $table->decimal('threasury_shares',20,4)->nullable()->change();
            $table->decimal('equity_ownerships',20,4)->nullable()->change();
            $table->decimal('total_reserves',20,4)->nullable()->change();
            $table->decimal('retained_earning',20,4)->nullable()->change();
            $table->decimal('minorty_interest',20,4)->nullable()->change();
            $table->decimal('total_equity',20,4)->nullable()->change();
            $table->decimal('balance',20,4)->nullable()->change();
            $table->decimal('operating_cash_flow',20,4)->nullable()->change();
            $table->decimal('contingent_liabilities',20,4)->nullable()->change();
            $table->decimal('other_commitmentes',20,4)->nullable()->change();
            $table->decimal('operating_lease_outstanding',20,4)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_details', function (Blueprint $table) {
            //
        });
    }
}
