<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddDataToUserRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //ADMIN
        //MANAGER
        //JUNIOUR_ANALYST
        //SENIOR_ANALYST
        DB::table('user_routes')->insert(['route_id'=>3,'user_role_code'=>'MANAGER']);
        DB::table('user_routes')->insert(['route_id'=>3,'user_role_code'=>'ADMIN']);
        DB::table('user_routes')->insert(['route_id'=>4,'user_role_code'=>'MANAGER']);
        DB::table('user_routes')->insert(['route_id'=>4,'user_role_code'=>'ADMIN']);
        DB::table('user_routes')->insert(['route_id'=>6,'user_role_code'=>'MANAGER']);
        DB::table('user_routes')->insert(['route_id'=>6,'user_role_code'=>'ADMIN']);
        DB::table('user_routes')->insert(['route_id'=>7,'user_role_code'=>'MANAGER']);
        DB::table('user_routes')->insert(['route_id'=>7,'user_role_code'=>'ADMIN']);
        DB::table('user_routes')->insert(['route_id'=>5,'user_role_code'=>'ADMIN']);

        DB::table('user_routes')->insert(['route_id'=>8,'user_role_code'=>'MANAGER']);
        DB::table('user_routes')->insert(['route_id'=>8,'user_role_code'=>'ADMIN']);
        DB::table('user_routes')->insert(['route_id'=>9,'user_role_code'=>'MANAGER']);
        DB::table('user_routes')->insert(['route_id'=>9,'user_role_code'=>'ADMIN']);

        DB::table('user_routes')->insert(['route_id'=>1,'user_role_code'=>'MANAGER']);
        DB::table('user_routes')->insert(['route_id'=>1,'user_role_code'=>'ADMIN']);
        DB::table('user_routes')->insert(['route_id'=>10,'user_role_code'=>'MANAGER']);
        DB::table('user_routes')->insert(['route_id'=>10,'user_role_code'=>'ADMIN']);

        DB::table('user_routes')->insert(['route_id'=>2,'user_role_code'=>'ADMIN']);
        DB::table('user_routes')->insert(['route_id'=>2,'user_role_code'=>'MANAGER']);
        DB::table('user_routes')->insert(['route_id'=>2,'user_role_code'=>'SENIOR_ANALYST']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_routes', function (Blueprint $table) {
            //
        });
    }
}
