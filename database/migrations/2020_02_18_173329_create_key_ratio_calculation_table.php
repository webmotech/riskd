<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeyRatioCalculationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('key_ratio_calculation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('report_details_id');
            $table->double('gross_profit_margin',4);
            $table->double('ebitda',4);
            $table->double('normalised_ebitda',4);
            $table->double('ebit',4);
            $table->double('net_profit_margin',4);
            $table->double('profitability',4);
            $table->double('return_on_investment',4);
            $table->double('return_on_assets',4);
            $table->double('return_on_equity',4);
            $table->double('working_capital',4);
            $table->double('working_capital_to_sales',4);
            $table->double('cash_flow_coverage',4);
            $table->double('cash_ratio',4);
            $table->double('current_ratio',4);
            $table->double('quick_ratio',4);
            $table->double('capital_adequacy',4);
            $table->double('net_tangible_worth',4);
            $table->double('net_asset_backing',4);
            $table->double('gearing',4);
            $table->double('debt_to_equity',4);
            $table->double('interest_coverage',4);
            $table->double('repayment_capability',4);
            $table->double('financial_leverage',4);
            $table->double('short_ratio',4);
            $table->double('operating_leverage',4);
            $table->double('creditor_exposure',4);
            $table->double('creditor_days',4);
            $table->double('inventory_days',4);
            $table->double('debtor_days',4);
            $table->double('cash_conversion_cycle',4);
            $table->double('sales_annualised',4);
            $table->double('activity',4);
            $table->double('sales_growth',4);
            $table->double('related_party_loans_receivable',4);
            $table->double('related_party_loans_payable',4);
            $table->double('related_party_loans_dependency',4);
            $table->double('quick_asset_composition',4);
            $table->double('current_asset_composition',4);
            $table->double('current_liability_composition',4);
            $table->double('zscore_risk_measure',4);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('key_ratio_calculation');
    }
}
