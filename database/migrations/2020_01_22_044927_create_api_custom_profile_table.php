<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiCustomProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_custom_profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('report_id');
            $table->string('main_name');
            $table->string('abn');
            $table->string('entity_status');
            $table->dateTime('entity_status_effective_from');
            $table->string('entity_type');
            $table->date('gst');
            $table->string('locality');
            $table->dateTime('record_last_updated');
            $table->string('name');
            $table->string('acn');
            $table->string('type');
            $table->string('status');
            $table->date('registered_date');
            $table->date('review_date');
            $table->string('class');
            $table->string('subclass');
            $table->string('asic_locallity');
            $table->double('current_credit_score');
            $table->double('history_credit_score');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_custom_profile');
    }
}
