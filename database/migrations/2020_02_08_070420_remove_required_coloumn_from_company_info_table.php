<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveRequiredColoumnFromCompanyInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_info', function (Blueprint $table) {
            $table->integer('anzic_classification_division_id')->nullable()->change();
            $table->integer('anzic_classification_sub_division_id')->nullable()->change();
            $table->integer('anzic_classification_group_id')->nullable()->change();
            $table->integer('anzic_classification_class_id')->nullable()->change();
            $table->integer('primary_classification_division_id')->nullable()->change();
            $table->integer('primary_classification_sub_division_id')->nullable()->change();
            $table->integer('primary_classification_group_id')->nullable()->change();
            $table->integer('primary_classification_class_id')->nullable()->change();
            $table->string('portfolio_analysis_status')->nullable()->change();
            $table->string('country_code')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_info', function (Blueprint $table) {
            //
        });
    }
}
