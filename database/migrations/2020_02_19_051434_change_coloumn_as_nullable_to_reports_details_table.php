<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColoumnAsNullableToReportsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_details', function (Blueprint $table) {
            $table->string('rounding')->nullable()->change();
            $table->string('base_currency')->nullable()->change();
            $table->string('quality')->nullable()->change();
            $table->integer('report_period_months')->nullable()->change();
            $table->string('scope')->nullable()->change();
            $table->integer('confidentiality_id')->nullable()->change();
            $table->integer('financial_year')->nullable()->change();
            $table->string('financial_month')->nullable()->change();
            $table->decimal('sales',10,2)->nullable()->change();
            $table->decimal('cost_of_sales',10,4)->nullable()->change();
            $table->decimal('gross_profit',10,4)->nullable()->change();
            $table->decimal('other_income',10,4)->nullable()->change();
            $table->decimal('depreciation',10,4)->nullable()->change();
            $table->decimal('amortisation',10,4)->nullable()->change();
            $table->decimal('impairment',10,4)->nullable()->change();
            $table->decimal('interest_expenses_gross',10,4)->nullable()->change();
            $table->decimal('operating_lease_expenses',10,4)->nullable()->change();
            $table->decimal('finance_lease_hire_expenses_charges',10,4)->nullable()->change();
            $table->decimal('non_recurring_gain_loses',10,4)->nullable()->change();
            $table->decimal('other_gain_loses',10,4)->nullable()->change();
            $table->decimal('other_expenses',10,4)->nullable()->change();
            $table->decimal('ebit',10,4)->nullable()->change();
            $table->decimal('ebitda',10,4)->nullable()->change();
            $table->decimal('normalized_ebitda',10,4)->nullable()->change();
            $table->decimal('profit_before_tax',10,4)->nullable()->change();
            $table->decimal('profit_before_tax_after_abnormals',10,4)->nullable()->change();
            $table->decimal('tax_benefit_expenses',10,4)->nullable()->change();
            $table->decimal('profit_after_tax',10,4)->nullable()->change();
            $table->decimal('distribution_ordividends',10,4)->nullable()->change();
            $table->decimal('other_post_tax_items_gains_losses',10,4)->nullable()->change();
            $table->decimal('profit_after_tax_distribution',10,4)->nullable()->change();
            $table->decimal('cash',10,4)->nullable()->change();
            $table->decimal('trade_debtors',10,4)->nullable()->change();
            $table->decimal('total_inventories',10,4)->nullable()->change();
            $table->decimal('loan_to_related_parties_1',10,4)->nullable()->change();
            $table->decimal('other_current_assets',10,4)->nullable()->change();
            $table->decimal('total_current_assets',10,4)->nullable()->change();
            $table->decimal('fixed_assets',10,4)->nullable()->change();
            $table->decimal('net_intangibles',10,4)->nullable()->change();
            $table->decimal('loan_to_related_parties_2',10,4)->nullable()->change();
            $table->decimal('other_non_current_assets',10,4)->nullable()->change();
            $table->decimal('total_non_current_assets',10,4)->nullable()->change();
            $table->decimal('total_assets',10,4)->nullable()->change();
            $table->decimal('trade_creditors',10,4)->nullable()->change();
            $table->decimal('interest_bearing_debt_1',10,4)->nullable()->change();
            $table->decimal('loan_from_related_parties_1',10,4)->nullable()->change();
            $table->decimal('other_current_liabilities',10,4)->nullable()->change();
            $table->decimal('total_current_liabilities',10,4)->nullable()->change();
            $table->decimal('interest_bearing_debt_2',10,4)->nullable()->change();
            $table->decimal('loan_from_related_parties_2',10,4)->nullable()->change();
            $table->decimal('other_non_current_liabilities',10,4)->nullable()->change();
            $table->decimal('total_non_current_liabilities',10,4)->nullable()->change();
            $table->decimal('total_liabilities',10,4)->nullable()->change();
            $table->decimal('share_capital',10,4)->nullable()->change();
            $table->decimal('prefence_shares',10,4)->nullable()->change();
            $table->decimal('threasury_shares',10,4)->nullable()->change();
            $table->decimal('equity_ownerships',10,4)->nullable()->change();
            $table->decimal('total_reserves',10,4)->nullable()->change();
            $table->decimal('retained_earning',10,4)->nullable()->change();
            $table->decimal('minorty_interest',10,4)->nullable()->change();
            $table->decimal('total_equity',10,4)->nullable()->change();
            $table->decimal('balance',10,4)->nullable()->change();
            $table->decimal('operating_cash_flow',10,4)->nullable()->change();
            $table->decimal('contingent_liabilities',10,4)->nullable()->change();
            $table->decimal('other_commitmentes',10,4)->nullable()->change();
            $table->decimal('operating_lease_outstanding',10,4)->nullable()->change();
            $table->integer('created_by')->nullable()->change();
            $table->integer('updated_by')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_details', function (Blueprint $table) {
            //
        });
    }
}
