<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColoumnAsNullableToKeyRatioCalculationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('key_ratio_calculation', function (Blueprint $table) {
            $table->decimal('gross_profit_margin',10,4)->nullable()->change();
            $table->decimal('ebitda',10,4)->nullable()->change();
            $table->decimal('normalised_ebitda',10,4)->nullable()->change();
            $table->decimal('ebit',10,4)->nullable()->change();
            $table->decimal('net_profit_margin',10,4)->nullable()->change();
            $table->decimal('profitability',10,4)->nullable()->change();
            $table->decimal('return_on_investment',10,4)->nullable()->change();
            $table->decimal('return_on_assets',10,4)->nullable()->change();
            $table->decimal('return_on_equity',10,4)->nullable()->change();
            $table->decimal('working_capital',10,4)->nullable()->change();
            $table->decimal('working_capital_to_sales',10,4)->nullable()->change();
            $table->decimal('cash_flow_coverage',10,4)->nullable()->change();
            $table->decimal('cash_ratio',10,4)->nullable()->change();
            $table->decimal('current_ratio',10,4)->nullable()->change();
            $table->decimal('quick_ratio',10,4)->nullable()->change();
            $table->decimal('capital_adequacy',10,4)->nullable()->change();
            $table->decimal('net_tangible_worth',10,4)->nullable()->change();
            $table->decimal('net_asset_backing',10,4)->nullable()->change();
            $table->decimal('gearing',10,4)->nullable()->change();
            $table->decimal('debt_to_equity',10,4)->nullable()->change();
            $table->decimal('interest_coverage',10,4)->nullable()->change();
            $table->decimal('repayment_capability',10,4)->nullable()->change();
            $table->decimal('financial_leverage',10,4)->nullable()->change();
            $table->decimal('short_ratio',10,4)->nullable()->change();
            $table->decimal('operating_leverage',10,4)->nullable()->change();
            $table->decimal('creditor_exposure',10,4)->nullable()->change();
            $table->decimal('creditor_days',10,4)->nullable()->change();
            $table->decimal('inventory_days',10,4)->nullable()->change();
            $table->decimal('debtor_days',10,4)->nullable()->change();
            $table->decimal('cash_conversion_cycle',10,4)->nullable()->change();
            $table->decimal('sales_annualised',10,4)->nullable()->change();
            $table->decimal('activity',10,4)->nullable()->change();
            $table->decimal('sales_growth',10,4)->nullable()->change();
            $table->decimal('related_party_loans_receivable',10,4)->nullable()->change();
            $table->decimal('related_party_loans_payable',10,4)->nullable()->change();
            $table->decimal('related_party_loans_dependency',10,4)->nullable()->change();
            $table->decimal('quick_asset_composition',10,4)->nullable()->change();
            $table->decimal('current_asset_composition',10,4)->nullable()->change();
            $table->decimal('current_liability_composition',10,4)->nullable()->change();
            $table->decimal('zscore_risk_measure',10,4)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('key_ratio_calculation', function (Blueprint $table) {
            //
        });
    }
}
