<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDataInConfidentialityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('confidentiality')->truncate();
        DB::table('confidentiality')->insert(['name'=>'Publish']);
        DB::table('confidentiality')->insert(['name'=>'ASIC Published']);
        DB::table('confidentiality')->insert(['name'=>'Confidential']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('confidentiality', function (Blueprint $table) {
            //
        });
    }
}
