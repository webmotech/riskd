<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
header("Access-Control-Allow-Origin: *");
//Route::get('/', function () {
//    return view('welcome');
//});
Route::group(['middleware' => ['first.login','auth','user.manage']], function () {

Route::resource('admin/dashboard','DashboardController');

Route::resource('admin/users','UsersController');

Route::resource('user-roles','UserRolesController');
Route::resource('admin/settings','ApplicationSettingsController');

Route::resource('designations','DesignationController');

Route::resource('admin/company','CompanyController');
Route::get('company/get-all-companies','CompanyController@getAllCompanies');

Route::resource('divisions','DivisionsController');

Route::resource('sub-divisions','SubDivisionsController');
Route::get('get-sub-divisions-by-division/{id}','SubDivisionsController@getSubDivisionsByDivision');

Route::resource('company-group','CompanyGroupController');
Route::get('get-company-group-by-sub-divisions/{id}','CompanyGroupController@getGroupBySubDivision');

Route::resource('company-class','CompanyClassController');
Route::get('get-class-by-group/{id}','CompanyClassController@getClassByGroup');

//Reports Controller
Route::resource('admin/reports','ReportsController');
Route::get('admin/reports/list/pending','ReportsController@pendingReports');
Route::get('admin/reports/list/pending-for-approval','ReportsController@pendingForApproval');
Route::get('admin/reports/list/approved-report','ReportsController@approvedReports');
Route::get('admin/reports/list/reject','ReportsController@reject');
Route::group(['middleware'=>['report.auth']],function(){
    Route::get('admin/reports/view/report/{token}','ReportsController@viewReport')->name('view-report');
});


Route::get('admin/reports/generate-report/{reporttoken}','ReportsController@generateReport')->name('generate-report');
Route::get('admin/reports/list/get-by-status','ReportsController@getReportsByStatus');
Route::get('admin/report/cancel-report/{id}','ReportsController@cancelReport')->name('cancel-report');
Route::get('admin/report/approve-report/{id}','ReportsController@approveReport')->name('approve-report');
Route::get('admin/report/reject-report/{id}','ReportsController@rejectReport')->name('reject-report');
Route::post('admin/report/clone-report','ReportsController@cloneReport')->name('clone-report');

//Chart data
Route::get('admin/chart/credit-score-history/{token}', 'ChartDataController@getCreditScoreHistory');
Route::get('admin/chart/revenue-data/{token}', 'ChartDataController@getRevenueData');
Route::get('admin/chart/gpnp-margin-data/{token}', 'ChartDataController@getGpNpMarginData');
Route::get('admin/chart/grossnet-profit-data/{token}', 'ChartDataController@getGrossNetProfitData');
Route::get('admin/chart/ebitda-data/{token}', 'ChartDataController@getEbitdaData');
Route::get('admin/chart/ratio-data/{token}', 'ChartDataController@getRatioData');
Route::get('admin/chart/working-capital-data/{token}', 'ChartDataController@getWorkingCapitalData');
Route::get('admin/chart/working-equity-data/{token}', 'ChartDataController@getWorkingEquityData');
Route::get('admin/chart/interest-cover-data/{token}', 'ChartDataController@getInterestCoverData');
Route::get('admin/chart/save-chart-images', 'ChartDataController@saveChartImage');

Route::get('users/get-all-users','UsersController@getAllUsers');
    Route::post('admin/authenticate/change-password/{id}','AuthController@changePassword')->name('password.change');
});

Route::post('admin/authenticate','AuthController@authenticate');
Route::post('admin/report/auth','AuthController@reportAuthenticate');
Route::get('admin/authenticate/reset-password','AuthController@resetPassword');
Route::post('admin/authenticate/update-password','AuthController@updatePassword');
Route::post('admin/user/reset-user-password','AuthController@resetUserPassword');

Route::get('admin/logout','AuthController@logout');
Route::get('/','AuthController@login');
Route::get('login','AuthController@login')->name('login');
Route::get('auth/report','AuthController@reportAuth')->name('auth.report');

//Report Configuration
Route::get('config/get-base-currency','ReportsConfigController@getBaseCurrency');
Route::get('config/get-rounding','ReportsConfigController@getRounding');
Route::get('config/get-quality','ReportsConfigController@getQuality');
Route::get('config/get-scope','ReportsConfigController@getScope');
Route::get('config/get-months','ReportsConfigController@getMonths');
Route::get('config/get-confidentiality','ReportsConfigController@getConfidentiality');

Route::get('test','ReportsController@test');

Route::get('admin/reports/print/{token}','ReportsController@printReport');
Route::get('admin/unauthenticate','UserRoleManagementController@unauthneticate');
Route::get('admin/report-error','ReportsController@pdfError');
Route::get('system/email/test','ReportsController@testMail');
