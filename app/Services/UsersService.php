<?php


namespace App\Services;


use App\Http\Resources\UserResource;
use App\Repository\Contract\UsersRepositoryInterface;
use App\Services\Contract\ServiceInterface;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersService implements ServiceInterface
{

    /**
     * @var APIResponseService
     */
    protected $apiResponseService;

    /**
     * @var UsersRepositoryInterface
     */
    protected $user;

    /**
     * UsersService constructor.
     * @param APIResponseService $apiResponseService
     * @param UsersRepositoryInterface $user
     */
    function __construct(
        APIResponseService $apiResponseService,
        UsersRepositoryInterface $user
    ) {
        $this->apiResponseService = $apiResponseService;
        $this->user = $user;
    }

    /**
     * Create New User
     * @param $data
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function create($data)
    {
        try {

            $checkExist = $this->user->checkUserByUsername($data['txtusernames']);
            if ($checkExist > 0) {
                return  $this->apiResponseService->failed('', 200, '', 'This username Already exist');
            }

            $checkExistEmail = $this->user->checkByEmail($data['email']);
            if ($checkExistEmail > 0) {
                return  $this->apiResponseService->failed('', 200, '', 'This Email Already exist');
            }

            $array = [
                'password' => bcrypt($data['txtpasswords']),
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'email' => $data['email'],
                'user_role_id' => $data['user_role'],
                'backup_password' => bcrypt($data['txtusernames']),
                // 'designation_id' => $data['designation'],
                'address' => $data['address'],
                'telephone' => $data['telephone'],
                'username' => $data['txtusernames'],

            ];

            $user = $this->user->create($array);
            return $this->apiResponseService->success(200, $user);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return $this->apiResponseService->failed($e, 500);
        }
    }

    /**
     * Update User's Data
     * @param $data
     * @param $id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function update($data, $id)
    {
        try {

            $checkExist = $this->user->checkUserByUsername($data['editusername'], $id);
            if ($checkExist > 0) {
                return  $this->apiResponseService->failed(null, 200, '', 'This username Already exist');
            }

            $checkExistEmail = $this->user->checkByEmail($data['editemail'], $id);
            if ($checkExistEmail > 0) {
                return  $this->apiResponseService->failed(null, 200, '', 'This Email Already exist');
            }

            $array = [
                'fname' => $data['editfname'],
                'lname' => $data['editlname'],
                'email' => $data['editemail'],
                'user_role_id' => $data['edituser_role'],
                //                'designation_id' => $data['editdesignation'],
                'address' => $data['editaddress'],
                'telephone' => $data['edittelephone'],
                'username' => $data['editusername'],
            ];

            $user = $this->user->update($array, $id);
            return $this->apiResponseService->success(200, $user);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return $this->apiResponseService->failed($e, 500);
        }
    }

    public function fetch($id)
    {
        // TODO: Implement fetch() method.
    }

    public function fetchAll()
    {
        // TODO: Implement fetchAll() method.
    }

    /**
     * Deleted USer by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function delete($id)
    {
        try {
            $this->user->delete($id);
            return $this->apiResponseService->success(200, [], 'User has been deleted');
        } catch (\Exception $e) {
            return $this->apiResponseService->failed($e, 500);
        }
    }

    /**
     * Get All Registered Users
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getAllUsers()
    {
        try {

            return UserResource::collection($this->user->getAll());
        } catch (\Exception $e) {
        }
    }

    /**
     * Update Loggedin User's Password
     * @param $data
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAuthPassword($data)
    {

        try {
            //            LoggedIn User
            $authUser = Auth::user();

            $this->user->updateAuthPassword($data, $authUser->id);
            return redirect()->to('/');
        } catch (\Exception $e) {
        }
    }


    /**
     * Update Users Password
     * @param $data
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword($data, $id)
    {
        try {
            $user = $this->user->changePassword($data, $id);

            return $this->apiResponseService->success(200, [], 'Password has been updated');
        } catch (\Exception $e) {
            return $this->apiResponseService->failed($e, 500);
        }
    }

    public function resetUserPassword($data)
    {

        try {

            $authUser = User::find(Auth::id());


            if (!Hash::check($data['current_password'], $authUser->password)) {

                return $this->apiResponseService->failed('', 200, '', 'Invalid current password');
            }

            $authUser->password = bcrypt($data['new_password']);
            $authUser->save();

            return $this->apiResponseService->success(200, [], 'Password has been reset');
        } catch (\Exception $e) {
            dd($e->getMessage());
            return $this->apiResponseService->failed($e, 500);
        }
    }
}
