<?php


namespace App\Services;


class APIResponseService
{

    /**
     * Success Response
     * @param $code
     * @param $msg
     * @param $data
     */
    public function success($code, $data = [], $msg = 'Completed', $error = [])
    {
        $response = [
            'success' => true,
            'http_status' => $code,
            'http_status_message' => $this->httpStatus($code),
            'data' => $data,
            'msg' => $msg,
            'error' => $error
        ];

        return response()->json($response, $code);
    }

    /**
     * Failed Response Method
     * @param $code
     * @param string $system_error
     * @param string $msg
     * @param array $error
     * @return \Illuminate\Http\JsonResponse
     */
    public function failed($exception, $code, $system_error = 'Error occured', $msg = 'failed', $error = [])
    {

        dd($exception->getMessage());
        $response = [
            'success' => false,
            'http_status' => $code,
            'http_status_message' => $this->httpStatus($code),
            'data' => [],
            'msg' => $msg,
            'error' => $error
        ];

        return response()->json($response, $code);
    }

    /**
     * @param $code
     * @return mixed
     */
    public function httpStatus($code)
    {
        $httpStatus = config('httpStatus');
        return $httpStatus[$code];
    }
}
