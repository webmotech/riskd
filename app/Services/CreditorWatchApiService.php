<?php


namespace App\Services;


use App\ApplicationSettings;
use League\Flysystem\Config;

class CreditorWatchApiService
{

    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $apiUrl;

    protected $apiUsername;

    protected $apiPassword;

    protected $apiResponseService;


    function __construct(APIResponseService $APIResponseService)
    {
        $settings = ApplicationSettings::where('code','CREDTIWATCH')->first()->value;
        $this->apiUrl = config('creditorwatch.api_url');
        $this->apiUsername = $settings['username'];
        $this->apiPassword = $settings['password'];
        $this->apiResponseService = $APIResponseService;
    }

    /**
     * @return bool
     */
    public function getAuthToken()
    {
        $authUrl = $this->apiUrl.'/login';

        $data = array(
            'username' => $this->apiUsername,
            'password' => $this->apiPassword
        );

        $payload = json_encode($data);


        // Prepare new cURL resource
        $ch = curl_init($authUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));

        // Submit the POST request
        $result = curl_exec($ch);
        $responseData = json_decode($result);
        // Close cURL session handle
        curl_close($ch);

        if(isset($responseData->token)){
            $response = [
                'success' => true,
                'data' => $responseData->token,
                'code' => 200,
                'msg' => 'Success',
            ];
            return $response;
        }else{
            $response = [
                'success' => false,
                'data' => [],
                'code' => 401,
                'msg' => $responseData->message,
            ];
            return $response;
        }

    }

    /**
     * @param $token
     * @param $abn
     * @param $acn
     * @return array
     */
    public function getCustomProfileData($token,$abn,$acn)
    {
        if(($abn==null) || empty($abn)){
            $url = $this->apiUrl.'/custom-profile?acn='.$acn;
        }else{

            $url = $this->apiUrl.'/custom-profile?abn='.$abn;
        }

        $agents = array(
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
            'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.9) Gecko/20100508 SeaMonkey/2.0.4',
            'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
            'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; da-dk) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1'

        );
        // Prepare new cURL resource
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_USERAGENT,$agents[array_rand($agents)] );

        // Set HTTP Header for GET request
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$token.''
        ));

        // Submit the POST request
        $result = curl_exec($ch);
        $data = json_decode($result,true);

        // Close cURL session handle
        curl_close($ch);


        if(isset($data['errors']))
        {
            $response = [
                'errors'=> $data['errors'],
                'abrData'=> [],
                'asicData'=> []
            ];
            return $response;
        }
        $response = [
            'abrData'=>$data['abrData'],
            'asicData'=> $data['asicData']
        ];

        return $response;

    }

    public function getCreditScoreData($authToken,$abn,$acn)
    {

        if(($abn==null) || empty($abn)){
            $url = $this->apiUrl.'/credit-score?acn='.$acn;
        }else{

            $url = $this->apiUrl.'/credit-score?abn='.$abn;
        }



        // Prepare new cURL resource
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        // Set HTTP Header for GET request
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer '.$authToken.''
        ));

        // Submit the POST request
        $result = curl_exec($ch);
        $data = json_decode($result,true);

        // Close cURL session handle
        curl_close($ch);

        $response = $data['creditScore']['scores'];

        return $response;
    }
}
