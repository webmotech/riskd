<?php


namespace App\Services\Contract;


interface ServiceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function create($data);

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function update($data,$id);

    /**
     * @param $id
     * @return mixed
     */
    public function fetch($id);

    /**
     * @return mixed
     */
    public function fetchAll();

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

}
