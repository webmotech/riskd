<?php


namespace App\Services;


use App\Mail\ReportNotification;
use App\Mail\TestMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EmailService
{

    /**
     * When Generate The Report Will send this email to User
     * @param $reportToken
     */
    public function sendReportNotification($reportToken)
    {
        $reportUrl = url('admin/reports/view/report/').'/'.$reportToken.'?usrtoken='.md5(Auth::id());
        Mail::to(Auth::user()->email)
            ->send(new ReportNotification($reportUrl));

    }

    /**
     * When Generate The Report Will send this email to User
     * @param $reportToken
     */
    public function testEmail()
    {

        try{
            Mail::to('shanila.j@jinasenainfotech.com')
                ->cc('shanilaonline@gmail.com')
                ->send(new TestMail());
            return response()->json(['success'=>true,'Email has been Sent']);
        }catch (\Exception $e){
            dd($e->getMessage());
        }

    }
}
