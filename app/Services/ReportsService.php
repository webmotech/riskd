<?php


namespace App\Services;


use App\Http\Resources\ReportDetailsResource;
use App\Http\Resources\ReportsResource;
use App\ReportExecutiveSummery;
use App\Repository\ApiCustomProfileRepository;
use App\Repository\Contract\ApiCustomProfileRepositoryInterface;
use App\Repository\Contract\CompanyRepositoryInterface;
use App\Repository\Contract\KeyRatioCalculationRepositoryInterface;
use App\Repository\Contract\ReportDetailsRepositoryInterface;
use App\Repository\Contract\ReportRepositoryInterface;
use App\Services\Contract\ServiceInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Khill\Lavacharts\Lavacharts;
use Lava;

class ReportsService implements ServiceInterface
{

    /**
     * @var APIResponseService
     */
    protected $apiResponseService;
    /**
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;
    /**
     * @var KeyRatioCalculationService
     */
    protected $keyRatioService;
    /**
     * @var ReportRepositoryInterface
     */
    protected $reportRepository;
    /**
     * @var ReportDetailsRepositoryInterface
     */
    protected $reportDetailsRepository;
    /**
     * @var KeyRatioCalculationRepositoryInterface
     */
    protected $keyRatioRepository;
    /**
     * @var CreditorWatchApiService
     */
    protected $creditorwatchApiService;
    /**
     * @var ApiCustomProfileRepositoryInterface
     */
    protected $apiCustomProfileRepository;
    /**
     * @var EmailService
     */
    protected $emailService;

    /**
     * ReportsService constructor.
     * @param APIResponseService $apiResponseService
     * @param CompanyRepositoryInterface $companyRepository
     * @param KeyRatioCalculationService $keyRatioCalculationService
     * @param ReportRepositoryInterface $reportRepository
     * @param ReportDetailsRepositoryInterface $reportDetailsRepository
     * @param KeyRatioCalculationRepositoryInterface $keyRatioRepository
     * @param CreditorWatchApiService $creditorwatchApiService
     * @param ApiCustomProfileRepositoryInterface $apiCustomProfileRepository
     * @param EmailService $emailService
     */
    function __construct(
        APIResponseService $apiResponseService,
        CompanyRepositoryInterface $companyRepository,
        KeyRatioCalculationService $keyRatioCalculationService,
        ReportRepositoryInterface $reportRepository,
        ReportDetailsRepositoryInterface $reportDetailsRepository,
        KeyRatioCalculationRepositoryInterface $keyRatioRepository,
        CreditorWatchApiService $creditorwatchApiService,
        ApiCustomProfileRepositoryInterface $apiCustomProfileRepository,
        EmailService $emailService
    ) {
        $this->apiResponseService = $apiResponseService;
        $this->companyRepository = $companyRepository;
        $this->keyRatioService = $keyRatioCalculationService;
        $this->reportRepository = $reportRepository;
        $this->reportDetailsRepository = $reportDetailsRepository;
        $this->keyRatioRepository = $keyRatioRepository;
        $this->creditorwatchApiService = $creditorwatchApiService;
        $this->apiCustomProfileRepository = $apiCustomProfileRepository;
        $this->emailService = $emailService;
    }


    public function create($data)
    {
        try {


            DB::beginTransaction();
            $company = $this->companyRepository->get($data['hdncompanyToken']);

            $reportArray = [
                'report_token' => md5(date('Ymdhis')),
                'company_id' => $company->id,
                'abn' => $company->abn,
                'acn' => $company->acn,
                'report_type' => $data['report_type'],
                'report_status' => 0,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
            ];

            $dataArray = $data['data'];

            //            Create Report
            $report = $this->reportRepository->create($reportArray);
            $reportId = $report->id;

            if ((isset($data['exective_summery'])) && ($data['report_type'] == 1)) {
                $report->executiveSummery()->createMany($data['exective_summery']);
            }


            //            Make Report Details and Key Ratio Array
            foreach ($dataArray as $details) {
                $reportDetailsArray = $this->makeReportDetailsArray($details, $reportId);


                $reportDetails = $reportDetailsArray['report_details'];
                $keyRatio = $reportDetailsArray['key_ratio'];

                //                Create Report Details Record
                $reportDetailsTable = $this->reportDetailsRepository->create($reportDetails);
                //                Create Key Ratio Record
                $reportDetailsTable->keyRatioCalculation()->create($keyRatio);
            }

            //Calculate Sales Growth
            $this->updateSalesGrowth($reportId);

            DB::commit();
            return $this->apiResponseService->success(201, 'Report has been created');
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage(), $e->getFile(), $e->getLine());
            return $this->apiResponseService->failed($e, 500, 'Error Occured');
        }
    }


    public function updateSalesGrowth($reportId)
    {
        $reportDetails = $this->reportDetailsRepository->getReportDetailsByReportId($reportId);

        $yearArray = [];
        foreach ($reportDetails as $yearData) {
            array_push($yearArray, ['key_ratio_id' => $yearData['keyRatioCalculation']['id'], 'year' => $yearData['financial_year'], 'salesAnnualized' => $yearData['keyRatioCalculation']['sales_annualised']]);
        }

        for ($index = 0; $index < count($yearArray); $index++) {

            if ($index == 0) {
                $data = ['sales_growth' => null];
                $id = $yearArray[$index]['key_ratio_id'];
                //                Update Sales Growth
                $this->keyRatioRepository->update($data, $id);
            } else {
                $previousYearAnnualizedSales = $yearArray[$index - 1]['salesAnnualized'];

                $currentYearAnnualizedSales = $yearArray[$index]['salesAnnualized'];
                $salesGrowth = ($previousYearAnnualizedSales != 0) ? (($currentYearAnnualizedSales - $previousYearAnnualizedSales) / $previousYearAnnualizedSales) * 100 : 0;
                $data = ['sales_growth' => $salesGrowth];
                $id = $yearArray[$index]['key_ratio_id'];
                //                Update Sales Growth
                $this->keyRatioRepository->update($data, $id);
            }
        }
    }

    public function makeReportDetailsArray($row, $reportId)
    {



        $array = [
            'rounding' => $row['rounding'],
            'report_id' => $reportId,
            'base_currency' => $row['base_currency'],
            'quality' => $row['quality'],
            'report_period_months' => $row['reporting_period'],
            'scope' => $row['scope'],
            'confidentiality_id' => $row['confidentiality_record'],
            'financial_year' => $row['financial_year'],
            'financial_month' => $row['month'],
            'sales' => $row['sales'],
            'cost_of_sales' => $row['cost_of_sales'],
            'gross_profit' => $row['gross_profit'],
            'other_income' => $row['other_income'],
            'depreciation' => $row['depreciation'],
            'amortisation' => $row['amortisation'],
            'impairment' => $row['impairment'],
            'interest_expenses_gross' => $row['interest_expense_gross'],
            'operating_lease_expenses' => $row['operating_lease_expense'],
            'finance_lease_hire_expenses_charges' => $row['finance_lease_hire_purchase_charges'],
            'non_recurring_gain_loses' => $row['non_recurring_gains_losses'],
            'other_gain_loses' => $row['other_gains_losses'],
            'other_expenses' => $row['other_expenses'],
            'ebit' => $row['ebit'],
            'ebitda' => $row['ebitda'],
            'normalized_ebitda' => $row['normalized_ebitda'],
            'profit_before_tax' => $row['profit_before_tax'],
            'profit_before_tax_after_abnormals' => $row['profit_before_tax_after_abnormals'],
            'tax_benefit_expenses' => $row['tax_benefit_expense'],
            'profit_after_tax' => $row['profit_after_tax'],
            'distribution_ordividends' => $row['distribution_or_dividends'],
            'other_post_tax_items_gains_losses' => $row['other_post_tax_items_gains_losses'],
            'profit_after_tax_distribution' => $row['profit_after_tax_distributions'],
            'cash' => $row['cash'],
            'trade_debtors' => $row['trade_debtors'],
            'total_inventories' => $row['total_inventories'],
            'loan_to_related_parties_1' => $row['loans_to_related_parties'],
            'other_current_assets' => $row['other_current_assets'],
            'total_current_assets' => $row['total_current_assets'],
            'fixed_assets' => $row['fixed_assets'],
            'net_intangibles' => $row['net_intangibles'],
            'loan_to_related_parties_2' => $row['loan_to_related_parties2'],
            'other_non_current_assets' => $row['other_non_current_assets'],
            'total_non_current_assets' => $row['total_non_current_assets'],
            'total_assets' => $row['total_assets'],
            'trade_creditors' => $row['total_creditors'],
            'interest_bearing_debt_1' => $row['interes_bearing_debt'],
            'loan_from_related_parties_1' => $row['loan_from_related_parties'],
            'other_current_liabilities' => $row['other_current_liabilities'],
            'total_current_liabilities' => $row['total_current_liabilities'],
            'interest_bearing_debt_2' => $row['interest_bearing_debt2'],
            'loan_from_related_parties_2' => $row['loan_from_related_parties_2'],
            'other_non_current_liabilities' => $row['other_non_current_liabilities'],
            'total_non_current_liabilities' => $row['total_non_current_liabilities'],
            'total_liabilities' => $row['total_liabilities'],
            'share_capital' => $row['share_capital'],
            'prefence_shares' => $row['prefence_shares'],
            'threasury_shares' => $row['treasury_shares'],
            'equity_ownerships' => $row['equity_ownerships'],
            'total_reserves' => $row['total_reserves'],
            'retained_earning' => $row['retained_earnings'],
            'minorty_interest' => $row['minorty_interest'],
            'total_equity' => $row['total_equity'],
            'balance' => $row['balance'],
            'operating_cash_flow' => $row['operating_cash_flow'],
            'contingent_liabilities' => $row['contingent_liabilities'],
            'other_commitmentes' => $row['other_commiments'],
            'operating_lease_outstanding' => $row['operating_lease_outstandings'],
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
        ];

        $data['report_details'] = $array;
        //            create KeyRation Array
        $data['key_ratio'] = $this->makeKeyRatioCalculationArray($array);

        return $data;
    }

    public function makeKeyRatioCalculationArray($data)
    {

        //        Annualized Profit after Tax and Dividends
        $annualizedProfitAfterTaxAndDividends = $this->keyRatioService->getAnnualizedProfitAfterTaxAndDividends($data['profit_after_tax_distribution'], $data['report_period_months']);
        //        Annualized Profit before Interest and tax
        $annualizedProfitBeforeInterestAndTax = $this->keyRatioService->getAnnualizedProfitBeforeInterestAndTax($data['ebit'], $data['report_period_months']);
        //        Annualized Profit after tax
        $annualizedProfitAfterTax = $this->keyRatioService->getAnnualizedProfitAfterTax($data['profit_after_tax'], $data['report_period_months']);
        //        Working Capital
        $workingCapital = $this->keyRatioService->getWorkingCapital($data['total_current_assets'], $data['total_current_liabilities']);
        //        Annualized Sales
        $annualizedSales = $this->keyRatioService->getAnnualizedSales($data['sales'], $data['report_period_months']);
        //        Annualized operating Cash Flow
        $annualizedOperatingCashFlow = $this->keyRatioService->getAnnualizedOperatingCashFlow($data['operating_cash_flow'], $data['report_period_months']);
        //        Net tangible Worth
        $netTangibleworth = $this->keyRatioService->getNetTangibleWorth($data['total_assets'], $data['total_liabilities'], $data['net_intangibles']);
        $annualizedProfitBeforeTax = $this->keyRatioService->getAnnualizedProfitBeforeTax($data['profit_before_tax_after_abnormals'], $data['report_period_months']);
        $annualizedEbitda = $this->keyRatioService->getAnnualizedEbitda($data['ebitda'], $data['report_period_months']);
        $annualizedCostofGoodsSold = $this->keyRatioService->getAnnualizedCostOfGoodsSold($data['cost_of_sales'], $data['report_period_months']);
        $creditorDays = $this->keyRatioService->getCreditorDays($data['trade_creditors'], $annualizedCostofGoodsSold);
        $inventoryDays = $this->keyRatioService->getInventoryDays($data['total_inventories'], $annualizedCostofGoodsSold);
        $debtorDays = $this->keyRatioService->getDebtorDays($data['trade_debtors'], $annualizedSales);


        $keyRatio = [
            'gross_profit_margin' => $this->keyRatioService->getGrossProfitMargin($data['gross_profit'], $data['sales']),
            'ebitda' => $this->keyRatioService->getEbitda($data['ebitda']),
            'normalised_ebitda' => $this->keyRatioService->getNormalizedEbitda($data['normalized_ebitda']),
            'ebit' => $this->keyRatioService->getEbit($data['ebit']),
            'net_profit_margin' => $this->keyRatioService->getNetProfitMargin($data['profit_before_tax_after_abnormals'], $data['sales']),
            'profitability' => $this->keyRatioService->getProfitability($annualizedProfitAfterTaxAndDividends, $data['total_assets']),
            'return_on_investment' => $this->keyRatioService->getReturnOnInvestment($data['retained_earning'], $data['total_assets']),
            'return_on_assets' => $this->keyRatioService->getReturnOnAssets($annualizedProfitBeforeInterestAndTax, $data['total_assets']),
            'return_on_equity' => $this->keyRatioService->getReturnOnEquity($annualizedProfitAfterTax, $data['total_equity']),
            'working_capital' => $this->keyRatioService->getWorkingCapital($data['total_current_assets'], $data['total_current_liabilities']),
            'working_capital_to_sales' => $this->keyRatioService->getWorkingCapitalToSales($workingCapital, $annualizedSales),
            'cash_flow_coverage' => $this->keyRatioService->getCashFlowCoverage($annualizedOperatingCashFlow, $data['total_current_liabilities']),
            'cash_ratio' => $this->keyRatioService->getCashRatio($data['cash'], $data['total_current_liabilities']),
            'current_ratio' => $this->keyRatioService->getCurrentRatio($data['total_current_assets'], $data['total_current_liabilities']),
            'quick_ratio' => $this->keyRatioService->getQuickRatio($data['total_current_assets'], $data['total_inventories'], $data['total_current_liabilities']),
            'capital_adequacy' => $this->keyRatioService->getCapitalAdequacy($data['total_assets'], $data['total_liabilities'], $data['net_intangibles'], $data['loan_to_related_parties_1'], $data['loan_to_related_parties_2'], $annualizedSales),
            'net_tangible_worth' => $this->keyRatioService->getNetTangibleWorth($data['total_assets'], $data['total_liabilities'], $data['net_intangibles']),
            'net_asset_backing' => $this->keyRatioService->getNetAssetbanking($netTangibleworth, $annualizedSales),
            'gearing' => $this->keyRatioService->getGearing($data['total_liabilities'], $data['total_assets']),
            'debt_to_equity' => $this->keyRatioService->getDebtToEquity($data['interest_bearing_debt_1'], $data['loan_from_related_parties_1'], $data['interest_bearing_debt_2'], $data['loan_from_related_parties_2'], $data['total_equity']),
            'interest_coverage' => $this->keyRatioService->getInterestCoverage($data['ebit'], $data['interest_expenses_gross']),
            'repayment_capability' => $this->keyRatioService->getRepaymentCapability($annualizedProfitBeforeTax, $data['total_liabilities']),
            'financial_leverage' => $this->keyRatioService->getFinancialLeverage($data['interest_bearing_debt_1'], $data['loan_from_related_parties_1'], $data['interest_bearing_debt_2'], $data['loan_from_related_parties_2'], $annualizedEbitda),
            'short_ratio' => $this->keyRatioService->getShortRatio($data['interest_bearing_debt_1'], $data['loan_from_related_parties_1'], $data['interest_bearing_debt_2'], $data['loan_from_related_parties_2']),
            'creditor_exposure' => $this->keyRatioService->getCreditorExposure($data['trade_creditors'], $data['total_assets']),
            'creditor_days' => $this->keyRatioService->getCreditorDays($data['trade_creditors'], $annualizedCostofGoodsSold),
            'inventory_days' => $this->keyRatioService->getInventoryDays($data['total_inventories'], $annualizedCostofGoodsSold),
            'debtor_days' => $this->keyRatioService->getDebtorDays($data['trade_debtors'], $annualizedSales),
            'cash_conversion_cycle' => $this->keyRatioService->getCashConversionCycle($debtorDays, $inventoryDays, $creditorDays),
            'sales_annualised' => $annualizedSales,
            'activity' => $this->keyRatioService->getActivity($annualizedSales, $data['total_assets']),
            'related_party_loans_receivable' => $this->keyRatioService->getRelatedPartyLoansReceivable($data['loan_to_related_parties_1'], $data['loan_to_related_parties_2'], $data['total_assets']),
            'related_party_loans_payable' => $this->keyRatioService->getRelatedPartyLoansPayable($data['loan_from_related_parties_1'], $data['loan_from_related_parties_2'], $data['total_liabilities']),
            'related_party_loans_dependency' => $this->keyRatioService->getRelatedPartyLoansDependency($data['loan_from_related_parties_1'], $data['loan_from_related_parties_2'], $workingCapital),
            'zscore_risk_measure' => 0,
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
        ];
        return $keyRatio;
    }



    public function update($data, $id)
    {
        try {
            DB::beginTransaction();

            $reportArray = [
                'report_type' => $data['report_type'],
                'report_status' => 0,
                'updated_by' => Auth::id(),
            ];

            if(!isset($data['data'])){
                return $this->apiResponseService->failed('',200,'Minimum one year required','Minimum one year required');
            }

            $dataArray = $data['data'];
            $report = $this->reportRepository->getReportDataByToken($id);
            $reportId = $report->id;

            if ((isset($data['exective_summery'])) && ($data['report_type'] == 1)) {
                ReportExecutiveSummery::where('report_id', $reportId)->delete();
                $report->executiveSummery()->createMany($data['exective_summery']);
            }

            //            Update Report
            $this->reportRepository->update($reportArray, $id);

            //           Get Report Details  List by Report ID
            $reportDetails = $this->reportDetailsRepository->getReportDetailsByReportId($reportId);

            $reportDetailsIdList = $reportDetails->pluck('id');

            //           Delete Report Details
            $this->reportDetailsRepository->deleteByReportId($reportId);

            //            Delete Key Ratio by Report Details ID
            $this->keyRatioRepository->deleteByReportDetailsId($reportDetailsIdList);

            //            Make Report Details and Key Ratio Array
            foreach ($dataArray as $details) {
                $reportDetailsArray = $this->makeReportDetailsArray($details, $reportId);


                $reportDetails = $reportDetailsArray['report_details'];
                $keyRatio = $reportDetailsArray['key_ratio'];

                //                Create Report Details Record
                $reportDetailsTable = $this->reportDetailsRepository->create($reportDetails);
                //                Create Key Ratio Record
                $reportDetailsTable->keyRatioCalculation()->create($keyRatio);
            }

            //Calculate Sales Growth
            $this->updateSalesGrowth($reportId);


            DB::commit();
            return $this->apiResponseService->success(200, [], 'Report has been Updated');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponseService->failed($e, 500);
        }
    }

    public function fetch($id)
    {
        // TODO: Implement fetch() method.
    }

    public function fetchAll()
    {
        // TODO: Implement fetchAll() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }


    /**
     * Get Reports Data By Status
     * @param $data
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getReportsByStatus($data)
    {
        $whereArray = $data['status'];


        return ReportsResource::collection($this->reportRepository->getReportsByStatus($whereArray));
    }

    /**
     * Cancel Report By User
     * @param $reportToken
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelReport($reportToken)
    {
        try {
            //            Cancel status
            $status = 3;
            $this->reportRepository->changeReportStatus($reportToken, $status);
            return $this->apiResponseService->success(200, 'Report Has been Cancelled');
        } catch (\Exception $e) {
            return $this->apiResponseService->failed($e, 500);
        }
    }

    public function getReportDataByToken($id)
    {
        try {
            $report = $this->reportRepository->getReportDataByToken($id);

            $data['report'] = $report;
            $data['details'] =  $report->reportDetails;
            return $data;
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Approve Report By User
     * @param $reportToken
     * @return \Illuminate\Http\JsonResponse
     */
    public function approveReport($reportToken)
    {
        try {
            //            Approve status
            $status = 1;
            $data = [
                'report_status' => $status,
                'approved_or_reject_by' => Auth::id(),
                'approved_rejected_date' => date('Y-m-d H:i:s')
            ];
            $this->reportRepository->update($data, $reportToken);
            return $this->apiResponseService->success(200, 'Report Has been Approved');
        } catch (\Exception $e) {
            return $this->apiResponseService->failed($e, 500);
        }
    }

    /**
     * Reject Report By User
     * @param $reportToken
     * @return \Illuminate\Http\JsonResponse
     */
    public function rejectReport($reportToken)
    {
        try {
            //            Reject status
            $status = 2;
            $data = [
                'report_status' => $status,
                'approved_or_reject_by' => Auth::id(),
                'approved_rejected_date' => date('Y-m-d H:i:s')
            ];
            $this->reportRepository->update($data, $reportToken);
            return $this->apiResponseService->success(200, 'Report Has been Rejected');
        } catch (\Exception $e) {
            return $this->apiResponseService->failed($e, 500);
        }
    }


    public function generateReport($reportToken)
    {
        try {
            DB::beginTransaction();
            $reportUrl = url('admin/reports/view/report/') . '/' . $reportToken;

            $reports = $this->getReportDataByToken($reportToken);
            $reportId = $reports['report']['id'];
            //            Check If Allready generated the Report
            if ($reports['report']['report_status'] == 4) {
                return $this->apiResponseService->success(200, ['report_url' => $reportUrl], 'Report has been generated');
            }
            //            Authenticate with CreditorWatch
            $authRequest = $this->creditorwatchApiService->getAuthToken();
            if ($authRequest['success'] == false) {
                return $this->apiResponseService->failed('', 200, $authRequest['msg'], $authRequest['msg']);
            }
            $authToken = $authRequest['data'];

            $companyId = $reports['report']['company_id'];
            $company = $this->companyRepository->getCompanyById($companyId);

            $customProfileData = $this->creditorwatchApiService->getCustomProfileData($authToken, $company->abn, $company->acn);

            if(isset($customProfileData['errors'])){
                return $this->apiResponseService->failed('', 200, $customProfileData['errors'][0]['detail'], $customProfileData['errors'][0]['detail']);
            }

            $creditScoreData = $this->creditorwatchApiService->getCreditScoreData($authToken, $company->abn, $company->acn);

            $abrData = $customProfileData['abrData'];
            $asicData = $customProfileData['asicData'];


            $address = $asicData['organisation']['address'];

            $apiDataArray = [
                'report_id' => $reportId,
                'main_name' => $abrData['name'],
                'abn' => $abrData['abn'],
                'entity_status' => $abrData['status'],
                'entity_status_effective_from' => date('Y-m-d', strtotime($abrData['gst'][0]['effectiveFrom'])),
                'entity_type' => $abrData['description'],
                'gst' =>  date('Y-m-d', strtotime($abrData['gst'][0]['effectiveFrom'])),
                'locality' => $abrData['addresses'][0]['postcode'] . ' ' . $abrData['addresses'][0]['stateCode'],
                'record_last_updated' => date('Y-m-d', strtotime($abrData['updatedDate'])),
                'name' => $asicData['organisation']['name'],
                'acn' => $abrData['acn'],
                'type' => $asicData['organisation']['type'],
                'status' => $asicData['organisation']['status'],
                'registered_date' => date('Y-m-d', strtotime($asicData['organisation']['registrationDate'])),
                'review_date' => date('Y-m-d', strtotime($asicData['organisation']['reviewDate'])),
                'class' => $asicData['organisation']['class'],
                'subclass' => $asicData['organisation']['subclass'],
                'asic_locallity' => $address['suburb'] . ' ' . $address['state'] . ' ' . $address['postcode'],
                'current_credit_score' => $creditScoreData['currentCreditScore'],
                'history_credit_score'  => 0,
                'created_by'  => Auth::id(),
                'updated_by'  => Auth::id(),
            ];


            $creditScoreHistory = $creditScoreData['creditScoreHistory'];

            //            Create Custom Profile Data Record
            $customProfileDtaTable = $this->apiCustomProfileRepository->create($apiDataArray);

            $creditScoreHistoryArray = [];
            foreach ($creditScoreHistory as $item) {

                $item['date'] = date('Y-m-d', strtotime($item['date']));
                array_push($creditScoreHistoryArray, $item);
            }

            //            Create CreditWatch History Record
            $customProfileDtaTable->apiCreditScoreHistory()->createMany($creditScoreHistoryArray);
            $reportData = [
                'report_status' => 4,
                'generated_at' => date('Y-m-d H:i:s')
            ];

            $this->reportRepository->update($reportData, $reportToken);
           $this->emailService->sendReportNotification($reportToken);
            DB::commit();
            return $this->apiResponseService->success(200, ['report_url' => $reportUrl], 'Report has been generated');
        } catch (\Exception $e) {

            DB::rollBack();

            return $this->apiResponseService->failed($e, 500, 'Error Occured', 'Error Occured');
        }
    }

    /**
     * Get Generated Report Data
     * @param $token
     */
    public function getGeneratedReportData($token)
    {
        $report = $this->reportRepository->getReportDataByToken($token);
        if (is_null($report)) {
            return false;
        }
        $response['report'] = $report;
        $response['current_credit_percentage'] = $this->getCurrentCreditPercentage($report->customProfileData['current_credit_score']);
        $response['apidata'] = $report->customProfileData;
        $response['report_details'] = $this->reportDetailsRepository->getReportDetailsByReportId($report->id);

        return $response;
    }


    public function getCurrentCreditPercentage($currentCreditScore)
    {

        if ($currentCreditScore >= 2 && $currentCreditScore <= 149) {
            return '71.81';
        } else if ($currentCreditScore >= 150 && $currentCreditScore <= 299) {
            return '5.38';
        } else if ($currentCreditScore >= 300 && $currentCreditScore <= 449) {
            return '5.16';
        } else if ($currentCreditScore >= 450 && $currentCreditScore <= 599) {
            return '2.55';
        } else if ($currentCreditScore >= 600 && $currentCreditScore <= 749) {
            return '1.20';
        } else if ($currentCreditScore >= 750 && $currentCreditScore <= 850) {
            return '0.89';
        } else {
            return 0;
        }
    }

    /**
     * Get Previous Year Report Data By Company Token
     * @param $companyToken
     */
    public function getPreviousYearReportDatabyCompany($companyToken)
    {
        $report = $this->reportRepository->getPreviousYearReportDatabyCompany($companyToken);
        if (is_null($report)) {
            return [];
        }

        $responseData = [];
        foreach($report as $reportData){
           foreach($reportData['reportDetails'] as $reportDetails){
               array_push($responseData,$reportDetails);
           }
        }




        $report['reportDetails'] = $this->sortPreviousYearReportData($responseData);

        return $report;
    }

    /**
     * Sort Previous Year Data
     *
     * @param [array] $responseData
     * @return void
     */
    public function sortPreviousYearReportData($responseData)
    {
        $sortedArray = array();

        foreach ($responseData as $data) {
            $sortedArray[] = $data['financial_year'];
        }

        array_multisort($sortedArray, SORT_ASC, $responseData);

        return $this->getlastYearsFromReport($responseData);

    }

    /**
     * Get Last years From Report
     *
     * @param array $data
     * @return void
     */
    public function getlastYearsFromReport($data)
    {
            $arrayLength = count($data);

            if($arrayLength<=3){
                return $data;
            }

            $removeCount = $arrayLength - 3;

            return array_slice($data,$removeCount);
    }

    //    public function generatePdf()
    //    {
    //
    //
    //        try{
    //            $population = Lava::DataTable();
    //
    //            $population->addDateColumn('Year')
    //                ->addNumberColumn('Number of People')
    //                ->addRow(['2006', 623452])
    //                ->addRow(['2007', 685034])
    //                ->addRow(['2008', 716845])
    //                ->addRow(['2009', 757254])
    //                ->addRow(['2010', 778034])
    //                ->addRow(['2011', 792353])
    //                ->addRow(['2012', 839657])
    //                ->addRow(['2013', 842367])
    //                ->addRow(['2014', 873490]);
    //
    //            Lava::AreaChart('Population', $population, [
    //                'title' => 'Population Growth',
    //                'legend' => [
    //                    'position' => 'in'
    //                ]
    //            ]);
    //
    //
    //
    //         //   return view('admin.reports.pdf.report');
    //
    //
    //            $view = view('admin.reports.pdf.report');
    //            $pdf = App::make('dompdf.wrapper');
    //            $pdf->loadHTML($view);
    //            return $pdf->stream();
    //            $reportPath ='assets/reports/'.date('Ymd').'/'.date('YmdHis').'.pdf';
    //            Storage::disk('local')->put($reportPath, $pdf->output());
    //
    //            dd($reportPath);
    //            return $reportPath;
    //        }catch (\Exception $e){
    //            dd($e->getMessage(),$e->getLine(),$e->getFile());
    //        }
    //    }

}
