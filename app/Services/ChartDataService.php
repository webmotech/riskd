<?php


namespace App\Services;


use App\ApiCreditScoreHistory;
use App\Reports;
use Illuminate\Support\Facades\Storage;

class ChartDataService
{


    public function saveChartImage($data)
    {
        $reports = Reports::where('report_token', $data['report_token'])->first();
        Reports::where('report_token', $data['report_token'])->update(['chart_count' => $reports->chart_count + 1]);
        $url = $data['img'];
        $contents = file_get_contents($url);
        $reportPath = 'assets/reports/' . $data['report_token'] . '/' . $data['type'] . '.png';
        Storage::disk('local')->put($reportPath, $contents);
    }


    /**
     * Get Credit Score History data for Chart
     * @param $reportToken
     */
    public function getCreditScoreHistory($reportToken)
    {
        $report = Reports::where('report_token', $reportToken)->first();
        $customProfileId = $report->customProfileData->id;
        $creditScore = ApiCreditScoreHistory::where('api_custom_profile_id', $customProfileId)->get();
        $scoreArray = [];
        foreach ($creditScore as $key => $scoreData) {

            $scoreArray['date'][$key] = date('M Y', strtotime($scoreData->date));
            $scoreArray['value'][$key] = floatval($scoreData->score);
        }
        $scoreArray['date'] = array_reverse($scoreArray['date']);
        $scoreArray['value'] = array_reverse($scoreArray['value']);
        return response()->json($scoreArray);
    }

    public function sortDataByYear($chartData){

        $sortedArray = array();

        foreach ($chartData as $data) {
            $sortedArray[] = $data['financial_year'];
        }

        array_multisort($sortedArray, SORT_ASC, $chartData);

        return $chartData;
    }

    public function getRevenueData($reportToken)
    {
        $report = Reports::where('report_token', $reportToken)->first();

        $dataArray['year'] = [];
        $dataArray['value'] = [];
        $reportData = $this->sortDataByYear($report->reportDetails->toArray());

        foreach ($reportData as $key => $data) {

            array_push($dataArray['year'], $data['financial_year']);
            array_push($dataArray['value'], round(floatval($data['sales']), 2));
        }
        return response()->json($dataArray);
    }

    public function getGpNpMarginData($reportToken)
    {
        $report = Reports::where('report_token', $reportToken)
        ->with('reportDetails.keyRatioCalculation')
        ->first();

        $dataArray['year'] = [];
        $dataArray['gp'] = [];
        $dataArray['np'] = [];
        $reportData = $this->sortDataByYear($report->reportDetails->toArray());
        foreach ($reportData as $key => $data) {

            $keyRatio = $data['key_ratio_calculation'];

            array_push($dataArray['year'], $data['financial_year']);
            array_push($dataArray['gp'], round(floatval($keyRatio['gross_profit_margin']), 2));
            array_push($dataArray['np'], round(floatval($keyRatio['net_profit_margin']), 2));
        }
        return response()->json($dataArray);
    }

    public function getGrossNetProfitData($reportToken)
    {
        $report = Reports::where('report_token', $reportToken)->first();
        $dataArray['year'] = [];
        $dataArray['gp'] = [];
        $dataArray['np'] = [];
        $reportData = $this->sortDataByYear($report->reportDetails->toArray());
        foreach ($reportData as $key => $data) {

            array_push($dataArray['year'], $data['financial_year']);
            array_push($dataArray['gp'], floatval($data['gross_profit']));
            array_push($dataArray['np'], floatval($data['profit_before_tax']));
        }
        return response()->json($dataArray);
    }

    public function getEbitdaData($reportToken)
    {
        $report = Reports::where('report_token', $reportToken)->first();
        $dataArray['year'] = [];
        $dataArray['ebit'] = [];
        $dataArray['normalized_ebit'] = [];

        $reportData = $this->sortDataByYear($report->reportDetails->toArray());

        foreach ($reportData as $key => $data) {

            array_push($dataArray['year'], $data['financial_year']);
            array_push($dataArray['ebit'], floatval($data['ebit']));
            array_push($dataArray['normalized_ebit'], floatval($data['normalized_ebitda']));
        }
        return response()->json($dataArray);
    }

    public function getRatioData($reportToken)
    {
        $report = Reports::where('report_token', $reportToken)
        ->with('reportDetails.keyRatioCalculation')
        ->first();

        $dataArray['year'] = [];
        $dataArray['current_ratio'] = [];
        $dataArray['quick_ratio'] = [];
        $dataArray['cash_ratio'] = [];


        $reportData = $this->sortDataByYear($report->reportDetails->toArray());

        foreach ($reportData as $key => $data) {
            $keyRatio = $data['key_ratio_calculation'];

            array_push($dataArray['year'], $data['financial_year']);
            array_push($dataArray['current_ratio'], round(floatval($keyRatio['current_ratio']), 2));
            array_push($dataArray['quick_ratio'], round(floatval($keyRatio['quick_ratio']), 2));
            array_push($dataArray['cash_ratio'], round(floatval($keyRatio['cash_ratio']), 2));
        }
        return response()->json($dataArray);
    }

    public function getWorkingCapitalData($reportToken)
    {
        $report = Reports::where('report_token', $reportToken)
        ->with('reportDetails.keyRatioCalculation')
        ->first();

        $dataArray['year'] = [];
        $dataArray['current_assets'] = [];
        $dataArray['current_liabilities'] = [];
        $dataArray['working_capital'] = [];

        $reportData = $this->sortDataByYear($report->reportDetails->toArray());

        foreach ($reportData as $key => $data) {
            $keyRatio = $data['key_ratio_calculation'];

            array_push($dataArray['year'], $data['financial_year']);
            array_push($dataArray['current_assets'], floatval($data['total_current_assets']));
            array_push($dataArray['current_liabilities'], floatval($data['total_current_liabilities']));
            array_push($dataArray['working_capital'], floatval($keyRatio['working_capital']));
        }

        return response()->json($dataArray);
    }

    public function getWorkingEquityData($reportToken)
    {
        $report = Reports::where('report_token', $reportToken)->first();
        $dataArray['year'] = [];
        $dataArray['total_assets'] = [];
        $dataArray['total_liabilities'] = [];
        $dataArray['equity'] = [];

        $reportData = $this->sortDataByYear($report->reportDetails->toArray());

        foreach ($reportData as $key => $data) {

            $totalDebt = $data['interest_bearing_debt_1'] + $data['interest_bearing_debt_2'] + $data['loan_from_related_parties_1'] + $data['loan_from_related_parties_2'];
            array_push($dataArray['year'], $data['financial_year']);
            array_push($dataArray['total_assets'], round(floatval($data['total_assets']), 2));
            array_push($dataArray['total_liabilities'], round(floatval($totalDebt), 2));
            array_push($dataArray['equity'], round(floatval($data['total_equity']), 2));
        }
        return response()->json($dataArray);
    }

    public function getInterestCoverData($reportToken)
    {
        $report = Reports::where('report_token', $reportToken)
        ->with('reportDetails.keyRatioCalculation')
        ->first();

        $dataArray['year'] = [];
        $dataArray['debt'] = [];
        $dataArray['interest'] = [];

        $reportData = $this->sortDataByYear($report->reportDetails->toArray());

        foreach ($reportData as $key => $data) {
            $keyRatio = $data['key_ratio_calculation'];

            array_push($dataArray['year'], $data['financial_year']);
            array_push($dataArray['interest'], round(floatval($keyRatio['interest_coverage']), 2));
            array_push($dataArray['debt'], round(floatval($keyRatio['debt_to_equity']), 2));
        }

        return response()->json($dataArray);
    }
}
