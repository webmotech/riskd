<?php


namespace App\Services;


use App\ReportExecutiveSummery;
use App\Repository\Contract\ReportDetailsRepositoryInterface;
use App\Repository\Contract\ReportRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportCloneService
{

    /**
     * @var ReportsService
     */
    protected $reportService;
    /**
     * @var ReportRepositoryInterface
     */
    protected $reportRepository;

    protected $reportDetailsRepository;

    protected $apiResponseService;

    /**
     * ReportCloneService constructor.
     * @param ReportsService $reportsService
     * @param ReportRepositoryInterface $reportRepository
     */
    function __construct(
        ReportsService $reportsService,
        ReportRepositoryInterface $reportRepository,
        ReportDetailsRepositoryInterface $reportDetailsRepository,
        APIResponseService $apiResponseService
    )
    {
        $this->reportService = $reportsService;
        $this->reportRepository = $reportRepository;
        $this->reportDetailsRepository = $reportDetailsRepository;
        $this->apiResponseService = $apiResponseService;
    }

    /**
     * Clone a New Report Using Existing Report
     * @param $data
     */
    public function cloneReport($data)
    {

        try{
            DB::beginTransaction();
            $reportType = $data['type'];
            $reportToken = $data['rptoken'];

//            Get Report Data by Report Token
            $currentReport = $this->reportService->getReportDataByToken($reportToken);
            $report = $currentReport['report'];

            $reportToken = md5(date('YmdHis'));
//            http://riskd.test/admin/reports/e801a41d865b3944deadba161aaf2cce/edit
            $reportUrl = url('admin/reports').'/'.$reportToken.'/edit';

            $newReportArray = [
                'report_token' => $reportToken,
                'abn' => $report->company->abn,
                'acn' => $report->company->acn,
                'company_id' => $report->company_id,
                'report_type' => $reportType,
                'report_status' => 3,
                'clone_report_id' => $report->id,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
                'chart_count' => 0,
            ];
            $newReport = $this->reportRepository->create($newReportArray);
            $newReportId = $newReport->id;

            if($reportType==1 && ($report->executiveSummery->count()!=0)){
                $summery = $report->executiveSummery[0]->summery;
                ReportExecutiveSummery::create(['report_id'=>$newReportId,'summery'=>$summery]);

            }
            $reportDetails = $currentReport['details'];

            foreach ($reportDetails as $details){
                $detailsArray = $this->makeReportDetailsArray($details,$newReportId);

                $reportDetailArray = $detailsArray['report_details'];
                $keyRatioArray = $detailsArray['keyratio'];

//                Create Report Details Record
                $reportDetailsTable = $this->reportDetailsRepository->create($reportDetailArray);
//                Create Key Ratio Record
                $reportDetailsTable->keyRatioCalculation()->create($keyRatioArray);
            }

            DB::commit();

            return $this->apiResponseService->success(201,['report_token'=>$reportToken, 'report_url'=>$reportUrl]);
        }catch (\Exception $e){
            dd($e->getMessage());
            DB::rollBack();

        }
    }

    public function makeReportDetailsArray($row, $reportId)
    {
        $keyRatioCalculation = $row->keyRatioCalculation;

//        New Report Details Array
        $data['report_details'] = [
            'report_id' => $reportId,
            'rounding' => $row['rounding'],
            'base_currency' => $row['base_currency'],
            'quality' => $row['quality'],
            'report_period_months' => $row['report_period_months'],
            'scope' => $row['scope'],
            'confidentiality_id' => $row['confidentiality_id'],
            'financial_year' => $row['financial_year'],
            'financial_month' => $row['financial_month'],
            'sales' => $row['sales'],
            'cost_of_sales' => $row['cost_of_sales'],
            'gross_profit' => $row['gross_profit'],
            'other_income' => $row['other_income'],
            'depreciation' => $row['depreciation'],
            'amortisation' => $row['amortisation'],
            'impairment' => $row['impairment'],
            'interest_expenses_gross' => $row['interest_expenses_gross'],
            'operating_lease_expenses' => $row['operating_lease_expenses'],
            'finance_lease_hire_expenses_charges' => $row['finance_lease_hire_expenses_charges'],
            'non_recurring_gain_loses' => $row['non_recurring_gain_loses'],
            'other_gain_loses' => $row['other_gain_loses'],
            'other_expenses' => $row['other_expenses'],
            'ebit' => $row['ebit'],
            'ebitda' => $row['ebitda'],
            'normalized_ebitda' => $row['normalized_ebitda'],
            'profit_before_tax' => $row['profit_before_tax'],
            'profit_before_tax_after_abnormals' => $row['profit_before_tax_after_abnormals'],
            'tax_benefit_expenses' => $row['tax_benefit_expenses'],
            'profit_after_tax' => $row['profit_after_tax'],
            'distribution_ordividends' => $row['distribution_ordividends'],
            'other_post_tax_items_gains_losses' => $row['other_post_tax_items_gains_losses'],
            'profit_after_tax_distribution' => $row['profit_after_tax_distribution'],
            'cash' => $row['cash'],
            'trade_debtors' => $row['trade_debtors'],
            'total_inventories' => $row['total_inventories'],
            'loan_to_related_parties_1' => $row['loan_to_related_parties_1'],
            'other_current_assets' => $row['other_current_assets'],
            'total_current_assets' => $row['total_current_assets'],
            'fixed_assets' => $row['fixed_assets'],
            'net_intangibles' => $row['net_intangibles'],
            'loan_to_related_parties_2' => $row['loan_to_related_parties_2'],
            'other_non_current_assets' => $row['other_non_current_assets'],
            'total_non_current_assets' => $row['total_non_current_assets'],
            'total_assets' => $row['total_assets'],
            'trade_creditors' => $row['trade_creditors'],
            'interest_bearing_debt_1' => $row['interest_bearing_debt_1'],
            'loan_from_related_parties_1' => $row['loan_from_related_parties_1'],
            'other_current_liabilities' => $row['other_current_liabilities'],
            'total_current_liabilities' => $row['total_current_liabilities'],
            'interest_bearing_debt_2' => $row['interest_bearing_debt_2'],
            'loan_from_related_parties_2' => $row['loan_from_related_parties_2'],
            'other_non_current_liabilities' => $row['other_non_current_liabilities'],
            'total_non_current_liabilities' => $row['total_non_current_liabilities'],
            'total_liabilities' => $row['total_liabilities'],
            'share_capital' => $row['share_capital'],
            'prefence_shares' => $row['prefence_shares'],
            'threasury_shares' => $row['threasury_shares'],
            'equity_ownerships' => $row['equity_ownerships'],
            'total_reserves' => $row['total_reserves'],
            'retained_earning' => $row['retained_earning'],
            'minorty_interest' => $row['minorty_interest'],
            'total_equity' => $row['total_equity'],
            'balance' => $row['balance'],
            'operating_cash_flow' => $row['operating_cash_flow'],
            'contingent_liabilities' => $row['contingent_liabilities'],
            'other_commitmentes' => $row['other_commitmentes'],
            'operating_lease_outstanding' => $row['operating_lease_outstanding'],

        ];

        $data['keyratio'] = $this->makeKeyRatioArray($keyRatioCalculation);

        return $data;
    }

    public function makeKeyRatioArray($keyRatio)
    {
        $ratioArray = [
            'gross_profit_margin' =>$keyRatio['gross_profit_margin'],
            'ebitda' =>$keyRatio['ebitda'],
            'normalised_ebitda' =>$keyRatio['normalised_ebitda'],
            'ebit' =>$keyRatio['ebit'],
            'net_profit_margin' =>$keyRatio['net_profit_margin'],
            'profitability' =>$keyRatio['profitability'],
            'return_on_investment' =>$keyRatio['return_on_investment'],
            'return_on_assets' =>$keyRatio['return_on_assets'],
            'return_on_equity' =>$keyRatio['return_on_equity'],
            'working_capital' =>$keyRatio['working_capital'],
            'working_capital_to_sales' =>$keyRatio['working_capital_to_sales'],
            'cash_flow_coverage' =>$keyRatio['cash_flow_coverage'],
            'cash_ratio' =>$keyRatio['cash_ratio'],
            'current_ratio' =>$keyRatio['current_ratio'],
            'quick_ratio' =>$keyRatio['quick_ratio'],
            'capital_adequacy' =>$keyRatio['capital_adequacy'],
            'net_tangible_worth' =>$keyRatio['net_tangible_worth'],
            'net_asset_backing' =>$keyRatio['net_asset_backing'],
            'gearing' =>$keyRatio['gearing'],
            'debt_to_equity' =>$keyRatio['debt_to_equity'],
            'interest_coverage' =>$keyRatio['interest_coverage'],
            'repayment_capability' =>$keyRatio['repayment_capability'],
            'financial_leverage' =>$keyRatio['financial_leverage'],
            'short_ratio' =>$keyRatio['short_ratio'],
            'operating_leverage' =>$keyRatio['operating_leverage'],
            'creditor_exposure' =>$keyRatio['creditor_exposure'],
            'creditor_days' =>$keyRatio['creditor_days'],
            'inventory_days' =>$keyRatio['inventory_days'],
            'debtor_days' =>$keyRatio['debtor_days'],
            'cash_conversion_cycle' =>$keyRatio['cash_conversion_cycle'],
            'sales_annualised' =>$keyRatio['sales_annualised'],
            'activity' =>$keyRatio['activity'],
            'sales_growth' =>$keyRatio['sales_growth'],
            'related_party_loans_receivable' =>$keyRatio['related_party_loans_receivable'],
            'related_party_loans_payable' =>$keyRatio['related_party_loans_payable'],
            'related_party_loans_dependency' =>$keyRatio['related_party_loans_dependency'],
            'quick_asset_composition' =>$keyRatio['quick_asset_composition'],
            'current_asset_composition' =>$keyRatio['current_asset_composition'],
            'current_liability_composition' =>$keyRatio['current_liability_composition'],
            'zscore_risk_measure' =>$keyRatio['zscore_risk_measure'],
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),

        ];

        return $ratioArray;
    }
}
