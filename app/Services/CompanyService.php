<?php


namespace App\Services;


use App\Repository\Contract\CompanyRepositoryInterface;
use App\Services\Contract\ServiceInterface;
use App\Http\Resources\CompanyResource;
use Illuminate\Support\Facades\Auth;

class CompanyService implements ServiceInterface
{

    /**
     * @var APIResponseService
     */
    protected $apiResponseService;

    /**
     * @var CompanyRepositoryInterface
     */
    protected $companyRepository;

    /**
     * CompanyService constructor.
     * @param APIResponseService $apiResponseService
     * @param CompanyRepositoryInterface $companyRepository
     */
    function __construct(
        APIResponseService $apiResponseService,
        CompanyRepositoryInterface $companyRepository
)
    {
        $this->apiResponseService = $apiResponseService;
        $this->companyRepository = $companyRepository;
    }

    /**
     * Create New Company
     * @param $data
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function create($data)
    {
       try{

           $array = [
               'company_token' => md5(strtotime(date('Y-m-d H:i:s'))),
               'name' => $data['entity_name'],
               'entity_type_id' => $data['ddlentitytype'],
               'abn' => $data['abn'],
               'acn' => $data['acn'],
               'rbn' => $data['rbn'],
               'equity' => $data['equity'],
               'established_date' => $data['date_established'],
               'confidentialiity_id' => $data['confidentiality'],
               'portfolio_analysis_status' => $data['portfolio_status'],
               'address_unit_number' => $data['unit_number'],
               'address_street_number' => $data['street_number'],
               'address_street_name' => $data['street_name'],
               'address_suburb' => $data['suburb'],
               'address_state' => $data['state'],
               'address_postal_code' => $data['postal_code'],
               'country_code' => $data['ddlcountry'],
               'contact_telephone_type' => $data['contact_type'],
               'contact_telephone_number' => $data['contact_type_number'],
               'business_telephone_type' => $data['business_type'],
               'business_telephone_number' => $data['business_type_number'],
               'anzic_classification_division_id' => (isset($data['anzic_division']))?$data['anzic_division'] : null,
               'anzic_classification_sub_division_id' => (isset($data['anzic_subdivision'])) ? $data['anzic_subdivision'] : null,
               'anzic_classification_group_id' => (isset($data['anzic_group'])) ? $data['anzic_group'] : null,
               'anzic_classification_class_id' => (isset($data['anzic_class'])) ? $data['anzic_class'] : null,
               'primary_classification_division_id' => (isset($data['primary_division'])) ? $data['primary_division'] :null,
               'primary_classification_sub_division_id' => (isset($data['primary_subdivision'])) ? $data['primary_subdivision'] : null,
               'primary_classification_group_id' => (isset($data['primary_group'])) ? $data['primary_group'] : null,
               'primary_classification_class_id' => (isset($data['primary_class'])) ? $data['primary_class'] : null,
               'created_by' => Auth::id(),
               'updated_by' => Auth::id(),
           ];


           $company = $this->companyRepository->create($array);
           return $this->apiResponseService->success(201,$company,'Company has been created');
       }catch (\Exception $e){

            return $this->apiResponseService->failed($e,500);
       }
    }

    /**
     * Update Existing Company
     * @param $data
     * @param $id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function update($data, $id)
    {
        try{

            $array = [
                'name' => $data['entity_name'],
                'entity_type_id' => $data['ddlentitytype'],
                'abn' => $data['abn'],
                'acn' => $data['acn'],
                'rbn' => $data['rbn'],
                'equity' => $data['equity'],
                'established_date' => $data['date_established'],
                'confidentialiity_id' => $data['confidentiality'],
                'portfolio_analysis_status' => $data['portfolio_status'],
                'address_unit_number' => $data['unit_number'],
                'address_street_number' => $data['street_number'],
                'address_street_name' => $data['street_name'],
                'address_suburb' => $data['suburb'],
                'address_state' => $data['state'],
                'address_postal_code' => $data['postal_code'],
                'country_code' => $data['ddlcountry'],
                'contact_telephone_type' => $data['contact_type'],
                'contact_telephone_number' => $data['contact_type_number'],
                'business_telephone_type' => $data['business_type'],
                'business_telephone_number' => $data['business_type_number'],
                'anzic_classification_division_id' => $data['anzic_division'],
                'anzic_classification_sub_division_id' => $data['anzic_subdivision'],
                'anzic_classification_group_id' => $data['anzic_group'],
                'anzic_classification_class_id' => $data['anzic_class'],
                'primary_classification_division_id' => $data['primary_division'],
                'primary_classification_sub_division_id' => $data['primary_subdivision'],
                'primary_classification_group_id' => $data['primary_group'],
                'primary_classification_class_id' => $data['primary_class'],
                'updated_by' => Auth::id(),
            ];


            $company = $this->companyRepository->update($array,$id);
            return $this->apiResponseService->success(200,$company,'Company has been Updated');
        }catch (\Exception $e){

            return $this->apiResponseService->failed($e,500);
        }
    }

    /**
     * Get Company By ID
     * @param $id
     * @return mixed
     */
    public function fetch($id)
    {
        return $this->companyRepository->get($id);
    }

    public function fetchAll()
    {
        // TODO: Implement fetchAll() method.
    }

    /**
     * Delete Existing company
     * @param $id
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function delete($id)
    {
       try{
          $this->companyRepository->delete($id);
          return $this->apiResponseService->success(200,[],'Company has been deleted');
       }catch (\Exception $e){
           return $this->apiResponseService->failed($e,500,[],'Company has not been deleted');
       }
    }

    /**
     * Get All Registered Company
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getAllCompanies()
    {
        return CompanyResource::collection($this->companyRepository->getAllCompanies());
    }

}
