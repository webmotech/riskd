<?php


namespace App\Services;


class KeyRatioCalculationService
{

    /**
     * Calculate Gross Profit Margin
     * @param $grossProfit
     * @param $sales
     * @return float|int
     */
    public function getGrossProfitMargin($grossProfit,$sales)
    {
        return (floatval($sales)!=0) ? (($grossProfit/$sales)*100) : 0;

    }

    /**
     * Calculate EBITDA
     * @param $ebitda
     * @return mixed
     */
    public function getEbitda($ebitda)
    {
        return $ebitda;
    }

    /**
     * Calculate EBIT
     * @param $ebit
     * @return mixed
     */
    public function getEbit($ebit)
    {
        return $ebit;
    }

    /**
     * Calculate Normalized Ebitda
     * @param $normalizedEbitda
     */
    public function getNormalizedEbitda($normalizedEbitda):float
    {
        return $normalizedEbitda;
    }

    /**
     * Calculate Net Profit Margin
     * @param $profitBeforeTaxAfterAbnormal
     * @param $sales
     * @return float|int
     */
    public function getNetProfitMargin($profitBeforeTaxAfterAbnormal,$sales)
    {
        return (floatval($sales)!=0) ? (($profitBeforeTaxAfterAbnormal/$sales)*100) : 0;
    }

    /**
     * Calculate Profitability
     * @param $anualizedProfitAfterTaxAndDividends
     * @param $totalAssets
     * @return float|int
     */
    public function getProfitability($anualizedProfitAfterTaxAndDividends,$totalAssets)
    {
        return (floatval($totalAssets)!=0) ? (($anualizedProfitAfterTaxAndDividends/$totalAssets)*100) : 0;
    }

    /**
     * Calculate Return On Investment
     * @param $retainedEarning
     * @param $totalAssets
     * @return float|int
     */
    public function getReturnOnInvestment($retainedEarning,$totalAssets)
    {
        return (floatval($totalAssets)!=0) ? (($retainedEarning/$totalAssets)*100) : 0;
    }

    /**
     * Calculate Return on Assets
     * @param $annualizedProfitBeforeInterestAndTax
     * @param $totalAssets
     * @return float|int
     */
    public function getReturnOnAssets($annualizedProfitBeforeInterestAndTax,$totalAssets)
    {
        return (floatval($totalAssets)!=0) ? (($annualizedProfitBeforeInterestAndTax/$totalAssets)*100) : 0;
    }

    /**
     * Calculate Return on Equity
     * @param $annualizedProfitAfterTax
     * @param $totalEquity
     * @return float|int
     */
    public function getReturnOnEquity($annualizedProfitAfterTax, $totalEquity)
    {
        return (floatval($totalEquity)!=0) ? (($annualizedProfitAfterTax/$totalEquity)*100) : 0;
    }

    /**
     * Calculate Working Capital
     * @param $totalCurrentAssets
     * @param $totalCurrentLiabilities
     * @return mixed
     */
    public function getWorkingCapital($totalCurrentAssets, $totalCurrentLiabilities)
    {
        return $totalCurrentAssets - $totalCurrentLiabilities;
    }

    /**
     * Calculate Working capital To Sales
     * @param $workingCapital
     * @param $annualizedSales
     * @return float|int
     */
    public function getWorkingCapitalToSales($workingCapital, $annualizedSales)
    {
        return (floatval($annualizedSales)!=0) ? (($workingCapital/$annualizedSales)*100) : 0;
    }

    /**
     * Calculate Cash Flow Coverage
     * @param $annualizedOperatingCashFlow
     * @param $totalCurrentLiabilities
     * @return float|int
     */
    public function getCashFlowCoverage($annualizedOperatingCashFlow, $totalCurrentLiabilities)
    {
        return (floatval($totalCurrentLiabilities)!=0) ? (($annualizedOperatingCashFlow/$totalCurrentLiabilities)*100) : 0;
    }

    /**
     * Calculate Cash Ratio
     * @param $cash
     * @param $totalCurrentLiabilities
     * @return float|int
     */
    public function getCashRatio($cash, $totalCurrentLiabilities)
    {
        return (floatval($totalCurrentLiabilities)!=0) ? ($cash / $totalCurrentLiabilities) : 0;
    }

    /**
     * Calculate Current Ratio
     * @param $totalCurrentAssets
     * @param $totalCurrentLiabilities
     * @return float|int
     */
    public function getCurrentRatio($totalCurrentAssets, $totalCurrentLiabilities)
    {
        return (floatval($totalCurrentLiabilities)!=0) ? ($totalCurrentAssets / $totalCurrentLiabilities) : 0;
    }

    /**
     * Calculate Quick Ratio
     * @param $totalCurrentAssets
     * @param $totalInventories
     * @param $totalCurrentLiabilities
     * @return float|int
     */
    public function getQuickRatio($totalCurrentAssets, $totalInventories, $totalCurrentLiabilities)
    {
        return (floatval($totalCurrentLiabilities)!=0) ? (($totalCurrentAssets - $totalInventories)/$totalCurrentLiabilities) : 0;
    }

    /**
     * Calculate Capital Adequacy
     * @param $totalAssets
     * @param $totalLiabilities
     * @param $netIntangibles
     * @param $loanToRelatedParties1
     * @param $loanToRelatedParties2
     * @param $annualizedSales
     * @return float|int
     */
    public function getCapitalAdequacy($totalAssets, $totalLiabilities, $netIntangibles, $loanToRelatedParties1,$loanToRelatedParties2, $annualizedSales)
    {
        return (floatval($annualizedSales)!=0) ? (($totalAssets-$totalLiabilities-$netIntangibles-$loanToRelatedParties1-$loanToRelatedParties2)/$annualizedSales) : 0;
    }

    /**
     * Calculate Net Tangible Worth
     * @param $totalAssets
     * @param $totalLiabilities
     * @param $netIntangibles
     * @return mixed
     */
    public function getNetTangibleWorth($totalAssets, $totalLiabilities, $netIntangibles)
    {

        return $totalAssets - $totalLiabilities - $netIntangibles;

    }

    /**
     * Calculate Net Asset Backing
     * @param $netTangibleWorth
     * @param $annualizedSales
     * @return float|int
     */
    public function getNetAssetbanking($netTangibleWorth, $annualizedSales)
    {
        return (floatval($annualizedSales)!=0) ? (($netTangibleWorth/$annualizedSales)*100) : 0;
    }

    /**
     * Calculate Gearing
     * @param $totalLiabilities
     * @param $totalAssets
     * @return float|int
     */
    public function getGearing($totalLiabilities, $totalAssets)
    {
        return (floatval($totalAssets)!=0) ? (($totalLiabilities/$totalAssets)*100) : 0;
    }

    /**
     * Calculate Debt To Equity
     * @param $interestBearingDebt1
     * @param $loanFromRelatedParties1
     * @param $interestBearingDebt2
     * @param $loanFromRelatedParties2
     * @param $totalEquity
     * @return float|int
     */
    public function getDebtToEquity($interestBearingDebt1, $loanFromRelatedParties1, $interestBearingDebt2, $loanFromRelatedParties2, $totalEquity)
    {
        return (floatval($totalEquity)!=0) ? (($interestBearingDebt1 + $loanFromRelatedParties1 + $interestBearingDebt2 + $loanFromRelatedParties2)/$totalEquity) : 0;

    }

    /**
     * Calculate Interest Coverage
     * @param $ebit
     * @param $interestExpense
     * @return float|int
     */
    public function getInterestCoverage($ebit, $interestExpense)
    {
        return (floatval($interestExpense)!=0) ? ($ebit/$interestExpense) : 0;
    }

    /**
     * Calculate Repayment  Capability
     * @param $annualizedProfitBeforeTax
     * @param $totalLiabilities
     * @return float|int
     */
    public function getRepaymentCapability($annualizedProfitBeforeTax, $totalLiabilities)
    {
        return (floatval($totalLiabilities)!=0) ? (($annualizedProfitBeforeTax/$totalLiabilities)*100) : 0;
    }

    /**
     * CalculateFinancialLeverage
     * @param $interestbearingDebt1
     * @param $loanFromRelatedParties1
     * @param $interestBearingDebt2
     * @param $loanFromRelatedParties2
     * @param $annualizedEbitda
     * @return float|int
     */
    public function getFinancialLeverage($interestbearingDebt1, $loanFromRelatedParties1, $interestBearingDebt2, $loanFromRelatedParties2, $annualizedEbitda)
    {
        return (floatval($annualizedEbitda)!=0) ? (($interestbearingDebt1+$loanFromRelatedParties1+$interestBearingDebt2+$loanFromRelatedParties2)/$annualizedEbitda) : 0;

    }

    /**
     * Calculate Short Ratio
     * @param $interestBearingDebt1
     * @param $loanFromRelatedParties1
     * @param $interestBearingDebt2
     * @param $loanFromRelatedParties2
     * @return float|int
     */
    public function getShortRatio($interestBearingDebt1, $loanFromRelatedParties1, $interestBearingDebt2, $loanFromRelatedParties2)
    {
        $param1 = $interestBearingDebt1+$loanFromRelatedParties1;
        $param2 =  $interestBearingDebt1+$loanFromRelatedParties1+$interestBearingDebt2+$loanFromRelatedParties2;

        return (floatval($param2)!=0) ? (($param1/$param2)*100) : 0;

    }

    /**
     * Calculate Operating Leverage
     * @param $changeInEbit
     * @param $changeInSales
     * @return float|int
     */
    public function getOperatingLeverage($changeInEbit,$changeInSales)
    {
        return (floatval($changeInSales)!=0) ? ($changeInEbit/$changeInSales) : 0;
    }

    /**
     * Calculate CreditorExposure
     * @param $tradeCreditors
     * @param $totalAssets
     * @return float|int
     */
    public function getCreditorExposure($tradeCreditors,$totalAssets)
    {
        return (floatval($totalAssets)!=0) ? (($tradeCreditors/$totalAssets)*100) : 0;
    }

    /**
     * Calculate Creditor Days
     * @param $tradeCreditors
     * @param $annualizedCostOfGoodsSold
     * @return float|int
     */
    public function getCreditorDays($tradeCreditors, $annualizedCostOfGoodsSold)
    {
        return (floatval($annualizedCostOfGoodsSold)!=0) ? (($tradeCreditors/$annualizedCostOfGoodsSold)*365) : 0;
    }

    /**
     * Calculate Inventory Days
     * @param $totalinventories
     * @param $annualizedCostOfGoodsSold
     * @return float|int
     */
    public function getInventoryDays($totalinventories, $annualizedCostOfGoodsSold)
    {
        return (floatval($annualizedCostOfGoodsSold)!=0) ? (($totalinventories/$annualizedCostOfGoodsSold)*365) : 0;
    }

    /**
     * Calculate Debtor Days
     * @param $tradeDebtors
     * @param $annualizedSales
     * @return float|int
     */
    public function getDebtorDays($tradeDebtors,$annualizedSales)
    {
        return (floatval($annualizedSales)!=0) ? (($tradeDebtors/$annualizedSales)*365) : 0;
    }

    /**
     * Calculate CashConversionCycle
     * @param $debtorDays
     * @param $inventoryDays
     * @param $creditorDays
     * @return mixed
     */
    public function getCashConversionCycle($debtorDays, $inventoryDays, $creditorDays)
    {
        return ($debtorDays+$inventoryDays) - $creditorDays;
    }

    /**
     * Calculate Annualized Sales
     * @param $sales
     * @param $reportingPeriodMonth
     * @return float|int
     */
    public function getAnnualizedSales($sales, $reportingPeriodMonth)
    {
        return ($sales/12) * $reportingPeriodMonth;
    }

    /**
     * Calculate Activity
     * @param $annualizedSales
     * @param $totalAssets
     * @return float|int
     */
    public function getActivity($annualizedSales,$totalAssets)
    {
        return (floatval($totalAssets)!=0) ? ($annualizedSales/$totalAssets) : 0;
    }

    /**
     * Calculate SalesGrowth
     * @param $currentYearAnnualizedSales
     * @param $previousYearAnnualizedSales
     * @return float|int
     */
    public function getSalesGrowth($currentYearAnnualizedSales, $previousYearAnnualizedSales)
    {
        return (floatval($previousYearAnnualizedSales)!=0) ? (($currentYearAnnualizedSales - $previousYearAnnualizedSales)/$previousYearAnnualizedSales)*100 : 0;
    }

    /**
     * Calculate Related Party Loan Receivable
     * @param $loansToRelatedParties1
     * @param $loansToRelatedParties2
     * @param $totalAssets
     * @return float|int
     */
    public function getRelatedPartyLoansReceivable($loansToRelatedParties1, $loansToRelatedParties2,$totalAssets)
    {
        return (floatval($totalAssets)!=0) ? (($loansToRelatedParties1+$loansToRelatedParties2)/$totalAssets)*100 : 0;
    }

    /**
     * Calculcate get Related Party Loans Payable
     * @param $loanFromRelatedParties1
     * @param $loanFromRelatedparties2
     * @param $totalLiabilities
     * @return float|int
     */
    public function getRelatedPartyLoansPayable($loanFromRelatedParties1,$loanFromRelatedparties2, $totalLiabilities)
    {
        return (floatval($totalLiabilities)!=0) ? (($loanFromRelatedParties1+$loanFromRelatedparties2)/$totalLiabilities)*100 : 0;
    }

    /**
     * Calculate Related Party LoansDependency
     * @param $loanFromRelatedParties1
     * @param $loanFromRelatedparties2
     * @param $workingCapital
     * @return float|int
     */
    public function getRelatedPartyLoansDependency($loanFromRelatedParties1,$loanFromRelatedparties2, $workingCapital)
    {
        return (floatval($workingCapital)!=0) ? (($loanFromRelatedParties1+$loanFromRelatedparties2)/$workingCapital)*100 : 0;
    }

    /**
     * Calculate Annualized Profit After Tax and Dividends
     * @param $profitAfterTaxAndDistribution
     * @param $reportPeriodMonths
     * @return float|int
     */
    public function getAnnualizedProfitAfterTaxAndDividends($profitAfterTaxAndDistribution,$reportPeriodMonths)
    {
        return ($profitAfterTaxAndDistribution/12) * $reportPeriodMonths;
    }

    /**
     * Calculate Annualized Profit before interest and tax
     * @param $ebit
     * @param $reportPeriodMonths
     * @return float|int
     */
    public function getAnnualizedProfitBeforeInterestAndTax($ebit,$reportPeriodMonths)
    {
        return ($ebit/12) * $reportPeriodMonths;
    }

    /**
     * @param $profitAfterTax
     * @param $reportPeriodMonths
     * @return float|int
     */
    public function getAnnualizedProfitAfterTax($profitAfterTax, $reportPeriodMonths)
    {
        return ($profitAfterTax/12) * $reportPeriodMonths;
    }

    /**
     * @param $netOperatingCashFlow
     * @param $reportPeriodMonths
     * @return float|int
     */
    public function getAnnualizedOperatingCashFlow($netOperatingCashFlow, $reportPeriodMonths)
    {
        return ($netOperatingCashFlow/12) * $reportPeriodMonths;
    }

    /**
     * @param $profitBeforeTaxAfterAbnormal
     * @param $reportingPeriodMonths
     * @return float|int
     */
    public function getAnnualizedProfitBeforeTax($profitBeforeTaxAfterAbnormal,$reportingPeriodMonths)
    {
        return ($profitBeforeTaxAfterAbnormal/12) * $reportingPeriodMonths;
    }

    /**
     * Calculate Annualized Ebitda
     * @param $ebitda
     * @param $reportingPeriodMonths
     * @return float|int
     */
    public function getAnnualizedEbitda($ebitda, $reportingPeriodMonths)
    {
        return ($ebitda/12) * $reportingPeriodMonths;
    }

    /**
     * Calculate Annualized Cost of Goods Sold
     * @param $costOfSales
     * @param $reportingPeriodMonths
     * @return float|int
     */
    public function getAnnualizedCostOfGoodsSold($costOfSales, $reportingPeriodMonths)
    {
        return ($costOfSales/12) * $reportingPeriodMonths;
    }








}
