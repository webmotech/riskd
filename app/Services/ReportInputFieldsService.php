<?php


namespace App\Services;


class ReportInputFieldsService
{

    public function getReportInputFields()
    {
            $data['fields'] = config('reports_input_field.input_fields');
            $data['no_of_years'] = 0;

            return $data;
    }


}
