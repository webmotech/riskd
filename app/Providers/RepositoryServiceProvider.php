<?php

namespace App\Providers;

use App\Repository\ApiCustomProfileRepository;
use App\Repository\CompanyRepository;
use App\Repository\Contract\ApiCustomProfileRepositoryInterface;
use App\Repository\Contract\CompanyRepositoryInterface;
use App\Repository\Contract\KeyRatioCalculationRepositoryInterface;
use App\Repository\Contract\ReportDetailsRepositoryInterface;
use App\Repository\Contract\ReportRepositoryInterface;
use App\Repository\Contract\UsersRepositoryInterface;
use App\Repository\KeyRatioCalculationRepository;
use App\Repository\ReportDetailsRepository;
use App\Repository\ReportRepository;
use App\Repository\UsersRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(UsersRepositoryInterface::class,UsersRepository::class);
        $this->app->bind(CompanyRepositoryInterface::class,CompanyRepository::class);
        $this->app->bind(ReportRepositoryInterface::class,ReportRepository::class);
        $this->app->bind(ReportDetailsRepositoryInterface::class,ReportDetailsRepository::class);
        $this->app->bind(KeyRatioCalculationRepositoryInterface::class,KeyRatioCalculationRepository::class);
        $this->app->bind(ApiCustomProfileRepositoryInterface::class,ApiCustomProfileRepository::class);
    }
}
