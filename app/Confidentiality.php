<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confidentiality extends Model
{
    protected $table = 'confidentiality';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
