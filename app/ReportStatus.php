<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportStatus extends Model
{
    protected $table = 'report_status';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
