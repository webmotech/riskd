<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportDetails extends Model
{
    protected $table = 'reports_details';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function keyRatioCalculation()
    {
        return $this->hasOne(KeyRatioCalculations::class,'report_details_id','id');
    }

    public function confidentiality(){
        return $this->belongsTo(Confidentiality::class,'confidentiality_id','id');
    }
}
