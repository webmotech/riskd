<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiCustomProfile extends Model
{
    protected $table = 'api_custom_profile';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function apiCreditScoreHistory()
    {
        return $this->hasMany(ApiCreditScoreHistory::class,'api_custom_profile_id','id');
    }
}
