<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportExecutiveSummery extends Model
{
    protected $table = 'report_executive_summery';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
