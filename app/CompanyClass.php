<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyClass extends Model
{
    protected $table = 'company_class';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
