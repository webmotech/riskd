<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'report_token' => $this->report_token,
            'company' => $this->company->name,
            'abn' => $this->abn,
            'acn' => $this->acn,
            'report_type' =>config('reports_config.report_type')[$this->report_type],
            'report_status' => $this->reportStatus->name,
            'status' => $this->report_status,
            'created_by' => $this->createdBy->fname.' '.$this->createdBy->lname,
            'updated_by' => $this->updatedBy->fname.' '.$this->updatedBy->lname,
            'approve_reject_by' =>($this->approveRejectBy)? $this->approveRejectBy->fname.' '.$this->approveRejectBy->lname:'',
            'created_at' => date('Y-m-d H:i:s',strtotime($this->created_at)),
            'updated_at' => date('Y-m-d H:i:s',strtotime($this->updated_at)),
            'approve_reject_at' => date('Y-m-d H:i:s',strtotime($this->approved_rejected_date)),

        ];
    }
}
