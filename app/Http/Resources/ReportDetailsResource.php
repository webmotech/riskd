<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'report_id' => $this->report_id,
            'rounding' => $this->rounding,
            'base_currency' => $this->base_currency,
            'quality' => $this->quality,
            'report_period_months' => $this->report_period_months,
            'scope' => $this->scope,
            'confidentiality_id' => $this->confidentiality_id,
            'financial_year' => $this->financial_year,
            'financial_month' => $this->financial_month,
            'sales' => $this->sales,
            'cost_of_sales' => $this->cost_of_sales,
            'gross_profit' => $this->gross_profit,
            'other_income' => $this->other_income,
            'depreciation' => $this->depreciation,
            'amortisation' => $this->amortisation,
            'impairment' => $this->impairment,
            'interest_expenses_gross' => $this->interest_expenses_gross,
            'operating_lease_expenses' => $this->operating_lease_expenses,
            'finance_lease_hire_expenses_charges' => $this->finance_lease_hire_expenses_charges,
            'non_recurring_gain_loses' => $this->non_recurring_gain_loses,
            'other_gain_loses' => $this->other_gain_loses,
            'other_expenses' => $this->other_expenses,
            'ebit' => $this->ebit,
            'ebitda' => $this->ebitda,
            'normalized_ebitda' => $this->normalized_ebitda,
            'profit_before_tax' => $this->profit_before_tax,
            'profit_before_tax_after_abnormals' => $this->profit_before_tax_after_abnormals,
            'tax_benefit_expenses' => $this->tax_benefit_expenses,
            'profit_after_tax' => $this->profit_after_tax,
            'distribution_ordividends' => $this->distribution_ordividends,
            'other_post_tax_items_gains_losses' => $this->other_post_tax_items_gains_losses,
            'profit_after_tax_distribution' => $this->profit_after_tax_distribution,
            'cash' => $this->cash,
            'trade_debtors' => $this->trade_debtors,
            'total_inventories' => $this->total_inventories,
            'loan_to_related_parties_1' => $this->loan_to_related_parties_1,
            'other_current_assets' => $this->other_current_assets,
            'total_current_assets' => $this->total_current_assets,
            'fixed_assets' => $this->fixed_assets,
            'net_intangibles' => $this->net_intangibles,
            'loan_to_related_parties_2' => $this->loan_to_related_parties_2,
            'other_non_current_assets' => $this->other_non_current_assets,
            'total_non_current_assets' => $this->total_non_current_assets,
            'total_assets' => $this->total_assets,
            'trade_creditors' => $this->trade_creditors,
            'interest_bearing_debt_1' => $this->interest_bearing_debt_1,
            'loan_from_related_parties_1' => $this->loan_from_related_parties_1,
            'other_current_liabilities' => $this->other_current_liabilities,
            'total_current_liabilities' => $this->total_current_liabilities,
            'interest_bearing_debt_2' => $this->interest_bearing_debt_2,
            'loan_from_related_parties_2' => $this->loan_from_related_parties_2,
            'other_non_current_liabilities' => $this->other_non_current_liabilities,
            'total_non_current_liabilities' => $this->total_non_current_liabilities,
            'total_liabilities' => $this->total_liabilities,
            'share_capital' => $this->share_capital,
            'prefence_shares' => $this->prefence_shares,
            'threasury_shares' => $this->threasury_shares,
            'equity_ownerships' => $this->equity_ownerships,
            'total_reserves' => $this->total_reserves,
            'retained_earning' => $this->retained_earning,
            'minorty_interest' => $this->minorty_interest,
            'total_equity' => $this->total_equity,
            'balance' => $this->balance,
            'operating_cash_flow' => $this->operating_cash_flow,
            'contingent_liabilities' => $this->contingent_liabilities,
            'other_commitmentes' => $this->other_commitmentes,
            'operating_lease_outstanding' => $this->operating_lease_outstanding,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
