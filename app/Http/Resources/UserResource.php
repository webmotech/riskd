<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'id' => $this->id,
            'fname' => $this->fname,
            'lname' => $this->lname,
            'email' => $this->email,
            'designation_id' => $this->designation_id,
            'user_role_id' => $this->user_role_id,
            'designation' => ($this->designation==null) ? '' : $this->designation->name,
            'user_role' => $this->userRole->name,
            'telephone' => $this->telephone,
            'address' => $this->address,
            'username' => $this->username,

        ];
    }
}
