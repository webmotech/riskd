<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckFirstLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//       dd($request->route()->action['as']);
        if(Auth::user()->is_reset_password==0){
            return redirect()->to('admin/authenticate/reset-password');
        }


        return $next($request);
    }
}
