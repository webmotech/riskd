<?php

namespace App\Http\Middleware;

use App\SystemRoute;
use App\UserRoutes;
use Closure;
use Illuminate\Support\Facades\Auth;

class UserRoleManage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $redirectUrl = url('admin/unauthenticate');
//        Ajax Request
        if($request->expectsJson()){

            if(isset($request->route()->action['as'])){

                $routeName = $request->route()->action['as'];

                $permission = $this->checkUserPermission($routeName);
                if($permission){
                    return $next($request);
                }else{
                    $response = [
                        'success' => false,
                        'http_status'=> 200,
                        'http_status_message'=> 'failed',
                        'data' => [],
                        'msg' => 'Permission denied. Please contact your system administrator.',

                    ];
                    return response()->json($response);
                }
            }
        }else{
            if(isset($request->route()->action['as'])){

                $routeName = $request->route()->action['as'];
                $permission = $this->checkUserPermission($routeName);
                if($permission){
                    return $next($request);
                }else{
                    return redirect()->to($redirectUrl)->send();
                }
            }
        }

        return $next($request);
    }

    public function checkUserPermission($routeName){

        $loginUserRole = Auth::user()->userRole->role_code;

        $route = SystemRoute::where('route_name',$routeName)->first();
        if(is_null($route)){
            return true;
        }else{
            $routeId = $route->id;
            $userRoutes = UserRoutes::where('route_id',$routeId)
                ->where('user_role_code',$loginUserRole)->count();
            if($userRoutes==0){
                return false;
            }else{
                return true;
            }
        }

    }
}
