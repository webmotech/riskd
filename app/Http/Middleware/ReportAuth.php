<?php

namespace App\Http\Middleware;

use Closure;

class ReportAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        if(isset($request['usrtoken'])){
            $data['report_token'] = last(explode('/',$request->url()));
            $data['usrtoken'] = $request['token'];

            return redirect()->route('auth.report',$data);
        }


        return $next($request);
    }
}
