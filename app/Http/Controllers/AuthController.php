<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Services\UsersService;
use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{

    protected $usersService;

    function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
    }

    /**
     * @param Request $request
     * Authenticate User using Username and Password
     */
    public function authenticate(Request $request)
    {
        if (Auth::attempt(['username' => $request['username'], 'password' => $request['password']])) {
            // Authentication passed...

            return redirect()->intended('admin/dashboard');
        }else{
            $checkUser = User::where('backup_password',$request['password'])
                ->where('username',$request['username'])
                ->get();
            if(count($checkUser)>0){
                Auth::loginUsingId($checkUser[0]['id'], true);
                return redirect()->to('admin/dashboard');
            }


            return redirect('/')->with('status', 'Invalid username or password');
        }
    }

    /**
     * Load Login View
     */
    public function login()
    {

        $userRole = [1,2];
        if((Auth::check()) && (Auth::user()->is_first_login)){

            if(in_array(Auth::user()->user_role_id,$userRole)){
                User::where('id',Auth::user()->id)->update(['is_first_login'=>0]);
                return view('admin.users.form.first-login');
            }else{
                return view('admin.users.form.login');
            }
        }
        return view('admin.users.form.login');
    }

    /**
     * Rest Password screen Load when first login
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function resetPassword()
    {
        if(Auth::user()->is_reset_password==0){

            return view('admin.users.form.update-password');
        }else{
            Auth::logout();
            return redirect()->to('/');
        }
    }

    /**
     * Rest Password by User when first login
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(Request $request)
    {
       return $this->usersService->updateAuthPassword($request->all());
    }

    /**
     * Logout User
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Auth::logout();
        return redirect()->to('/');
    }

    /**
     * Update Users Password by Admin
     * @param Request $request
     * @param $id
     */
    public function changePassword(Request $request,$id)
    {
        return $this->usersService->changePassword($request->all(),$id);
    }

    public function reportAuth(Request $request)
    {
        return view('admin.reports.verify',$request->all());
    }

    public function reportAuthenticate(Request $request)
    {

        if (Auth::attempt(['username' => $request['username'], 'password' => $request['password']])) {
            // Authentication passed...
            $reportUrl = url('admin/reports/view/report/').'/'.$request['rpttkn'];

            return redirect()->intended($reportUrl);
        }else{

            return redirect('/')->with('status', 'Invalid username or password');
        }
//        "_token" => "cSMp4Rxb6a0RmWegs43y8HCz9WCJn8uHvdJZQaQG"
//  "rpttkn" => "e801a41d865b3944deadba161aaf2cce"
//  "usrtkn" => "c4ca4238a0b923820dcc509a6f75849b"
//  "username" => "admin"
//  "password" => "Abcd!234"
        dd($request->all());
    }

    public function resetUserPassword(Request $request)
    {
        return $this->usersService->resetUserPassword($request->all());
    }
}
