<?php

namespace App\Http\Controllers;

use App\Services\APIResponseService;
use Illuminate\Support\Facades\Auth;
use App\ApplicationSettings;
use Illuminate\Http\Request;

class ApplicationSettingsController extends Controller
{

    protected $apiResponseService;

    function __construct(APIResponseService $apiResponseService)
    {
        $this->apiResponseService = $apiResponseService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = ApplicationSettings::where('code','CREDTIWATCH')->first();
        if(!is_null($settings)){
            $array = $settings->toArray();
        }else{
            $array = [];
        }

        return view('admin.settings.index',['data' => $array]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try{
            $data = [
                'code' => 'CREDTIWATCH',
                'value' => ['username'=>$request['api_username'],'password'=>$request['api_password']],
                'created_by'=> Auth::id(),
                'updated_by'=> Auth::id(),
            ];
            $checkExist = ApplicationSettings::where('code','CREDTIWATCH')->count();
            if($checkExist>0){
                $data = [
                    'value' => ['username'=>$request['api_username'],'password'=>$request['api_password']],
                    'updated_by'=> Auth::id(),
                ];
                ApplicationSettings::where('code','CREDTIWATCH')->update($data);
            }else{
                $data = [
                    'code' => 'CREDTIWATCH',
                    'value' => ['username'=>$request['api_username'],'password'=>$request['api_password']],
                    'created_by'=> Auth::id(),
                    'updated_by'=> Auth::id(),
                ];
                ApplicationSettings::create($data);

            }
            return $this->apiResponseService->success(200,'Application Settings Has been saved');
       }catch (\Exception $e){
            return  $this->apiResponseService->failed(500);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
