<?php

namespace App\Http\Controllers;

use App\Services\ChartDataService;
use Illuminate\Http\Request;

class ChartDataController extends Controller
{

    protected $chartDataService;

    function __construct(ChartDataService $chartDataService)
    {
        $this->chartDataService = $chartDataService;
    }

    public function saveChartImage(Request $request)
    {
        return $this->chartDataService->saveChartImage($request->all());
    }

    /**
     * Get Credit Score History Data
     * @param $reportToken
     */
    public function getCreditScoreHistory($reportToken)
    {
        return $this->chartDataService->getCreditScoreHistory($reportToken);
    }

    public function getRevenueData($reportToken)
    {
        return $this->chartDataService->getRevenueData($reportToken);
    }

    public function getGpNpMarginData($reportToken)
    {
        return $this->chartDataService->getGpNpMarginData($reportToken);
    }

    public function getGrossNetProfitData($reportToken)
    {
        return $this->chartDataService->getGrossNetProfitData($reportToken);
    }

    public function getEbitdaData($reportToken)
    {
        return $this->chartDataService->getEbitdaData($reportToken);
    }

    public function getRatioData($reportToken)
    {
        return $this->chartDataService->getRatioData($reportToken);
    }

    public function getWorkingCapitalData($reportToken)
    {
        return $this->chartDataService->getWorkingCapitalData($reportToken);
    }

    public function getWorkingEquityData($reportToken)
    {
        return $this->chartDataService->getWorkingEquityData($reportToken);
    }

    public function getInterestCoverData($reportToken)
    {
        return $this->chartDataService->getInterestCoverData($reportToken);
    }
}
