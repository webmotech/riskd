<?php

namespace App\Http\Controllers;

use App\Services\CompanyService;
use Illuminate\Http\Request;
use App\Confidentiality;
use App\CompanyClass;
use App\CompanyGroup;
use App\SubDivitions;
use App\EntityType;
use App\Divisions;
use App\Country;

class CompanyController extends Controller
{

    protected $companyService;

    function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.company.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'country'=>Country::all(),
            'entity_type'=>EntityType::all(),
            'division' => Divisions::all(),
            'confidentiality' => Confidentiality::all()
        ];

        return view('admin.company.form.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->companyService->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->companyService->fetch($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = $this->companyService->fetch($id);
        if(is_null($company)){
            abort(404);
        }

        $data = [
            'company' => $company,
            'country' => Country::all(),
            'entity_type' => EntityType::all(),
            'sub_division' => SubDivitions::all(),
            'group' => CompanyGroup::all(),
            'class' => CompanyClass::all(),
            'sub_division' => SubDivitions::all(),
            'confidentiality' => Confidentiality::all(),
            'division' => Divisions::all()
        ];
        return view('admin.company.form.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return  $this->companyService->update($request->all(),$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->companyService->delete($id);
    }

    public function getAllCompanies()
    {
        return $this->companyService->getAllCompanies();
    }
}
