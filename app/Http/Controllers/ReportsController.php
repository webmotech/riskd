<?php

namespace App\Http\Controllers;

use App\Confidentiality;
use App\Services\CompanyService;
use App\Services\EmailService;
use App\Services\ReportCloneService;
use App\Services\ReportInputFieldsService;
use App\Services\ReportsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use PDF;


class ReportsController extends Controller
{
    /**
     * @var ReportInputFieldsService
     */
    protected $reportsService;

    protected $companyService;

    protected $reportCloneService;

    protected $emailService;


    function __construct(
        ReportsService $reportsService,
        CompanyService $companyService,
        ReportCloneService $reportCloneService,
        EmailService $emailService
    )
    {
        $this->reportsService = $reportsService;
        $this->companyService = $companyService;
        $this->reportCloneService = $reportCloneService;
        $this->emailService = $emailService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('admin.reports.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(isset($_COOKIE['cptoken'])){

            $companyToken = $_COOKIE['cptoken'] ;
        }
        $reportData = $this->reportsService->getPreviousYearReportDatabyCompany($companyToken);

        $data['no_of_years']= (!empty($reportData))?count($reportData['reportDetails']):0;
        $data['previous'] = (!empty($reportData))?$reportData['reportDetails']:[];
        $data['report'] = (0<count($reportData['reportDetails']))?$reportData[0] : [];

        return view('admin.reports.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return  $this->reportsService->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['no_of_years']=0;
        $data['previous'] = [];
        $data['confidentiality'] = Confidentiality::all();
        $data['company'] = $this->companyService->getAllCompanies();
        //Get Report Data By report token
        $data['report_data'] = $this->reportsService->getReportDataByToken($id);

        $reportData = $this->reportsService->getPreviousYearReportDatabyCompany($data['report_data']['report']->company->company_token);

        $data['no_of_years']= (!empty($reportData))?count($reportData['reportDetails']):0;
        $data['previous'] = (!empty($reportData))?$reportData['reportDetails']:[];
        $data['report'] = (!empty($reportData[0]))?$reportData[0]:[];

        if(is_null($data['report_data'])){
            abort(404);
        }

        return view('admin.reports.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return  $this->reportsService->update($request->all(),$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Load Pending Report List
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pendingReports()
    {
        return view('admin.reports.pending-reports');
    }

    /**
     * Rejected Report List
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reject(){

        return view('admin.reports.reject-reports');
    }

    /**
     * Get Report List By Status
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getReportsByStatus(Request $request)
    {
        return $this->reportsService->getReportsByStatus($request->all());
    }


    public function cancelReport($reportToken)
    {
        return $this->reportsService->cancelReport($reportToken);
    }

    public function approveReport($reportToken)
    {
        return $this->reportsService->approveReport($reportToken);
    }

    public function rejectReport($reportToken)
    {
        return $this->reportsService->rejectReport($reportToken);
    }

    public function pendingForApproval()
    {
        return view('admin.reports.pending-for-approval');
    }

    public function approvedReports()
    {
        return view('admin.reports.approved-reports');
    }

    /**
     * Clone New Report using Existing one
     * @param Request $request
     */
    public function cloneReport(Request $request)
    {
        return $this->reportCloneService->cloneReport($request->all());
    }

    public function generateReport($reportToken)
    {
//        $population = Lava::DataTable();
//
//        $population->addDateColumn('Year')
//            ->addNumberColumn('Number of People')
//            ->addRow(['2006', 623452])
//            ->addRow(['2007', 685034])
//            ->addRow(['2008', 716845])
//            ->addRow(['2009', 757254])
//            ->addRow(['2010', 778034])
//            ->addRow(['2011', 792353])
//            ->addRow(['2012', 839657])
//            ->addRow(['2013', 842367])
//            ->addRow(['2014', 873490]);
//
//        Lava::AreaChart('Population', $population, [
//            'title' => 'Population Growth',
//            'legend' => [
//                'position' => 'in'
//            ]
//        ]);


         // return view('admin.reports.pdf.report');



        return $this->reportsService->generateReport($reportToken);
    }

    /**
     * Load generated report as Web view
     * @param $token
     */
    public function viewReport($token)
    {
//        Get Generated Report Data
        $reports = $this->reportsService->getGeneratedReportData($token);

        if(($reports['report']['report_status']!=4) || ($reports==false) ){
            abort(404);
        }
        return view('admin.reports.report-view',$reports);
    }

    public function printReport($token)
    {


        try{


//        //        Get Generated Report Data
        $reports = $this->reportsService->getGeneratedReportData($token);

        if(($reports['report']['report_status']!=4) || ($reports==false) ){
            abort(404);
        }


        $pdf = PDF::loadView('admin.reports.pdf.report',$reports,[],[

            'default_font' => 'Times New Roman'
        ]);


        return $pdf->stream($reports['report']['report_token'].'.pdf');
        }catch(\Exception $e){
            return redirect()->to('admin/report-error')->send();
        }



    }

    public function pdfError()
    {
        return view('admin.report-error');
    }

    public function test(Request $request){

        $url = $request['img'];
        $contents = file_get_contents($url);
        $name =
        Storage::put(date('Ymdhis').'.png', $contents);
    }

    public function testMail(){
        try{

            return $this->emailService->testEmail();

        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }
}
