<?php

namespace App\Http\Controllers;

use App\Confidentiality;
use Illuminate\Http\Request;

class ReportsConfigController extends Controller
{

    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $reportConfig;

    /**
     * ReportsConfigController constructor.
     */
    function __construct()
    {
      $this->reportConfig = config('reports_config');

    }

    /**
     * Get Base Currency List
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBaseCurrency()
    {
        return response()->json($this->reportConfig['base_currency']);
    }

    /**
     * Get Rounding List
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRounding()
    {
        return response()->json($this->reportConfig['rounding']);
    }

    /**
     * Get Quality List
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQuality()
    {
        return response()->json($this->reportConfig['quality']);
    }

    /**
     * Get Scope List
     * @return \Illuminate\Http\JsonResponse
     */
    public function getScope()
    {
        return response()->json($this->reportConfig['scope']);
    }

    /**
     * Get Months List
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMonths()
    {
        return response()->json($this->reportConfig['months']);
    }

    public function getConfidentiality()
    {
        return response()->json(Confidentiality::all());
    }
}
