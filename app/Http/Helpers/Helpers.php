<?php


namespace App\Http\Helpers;


class Helpers
{

    /**
     * Check Value Negative or Positive
     * @param $value
     */
    public static function checkValue($value)
    {
        if ($value < 0) {
            echo '<span class="text-danger"><i class="fa fa-caret-down"></i>' . number_format($value) . '</span>';
        } else if ($value > 0) {
            echo '<span class="text-success"><i class="fa fa-caret-up"></i>' . number_format($value) . '</span>';
        } else {
            echo '<span class="text-success">' . number_format($value) . '</span>';
        }
    }


    public static function checkPdfValue($value)
    {
        if ($value < 0) {
            echo '<span class="text-danger"><i class="fa fa-caret-down"> </i><img style="display:inline" src="/assets/img/down.png"/>&nbsp;' . number_format($value) . '</span>';
        } else if ($value > 0) {
            echo '<span class="text-success"><img src="/assets/img/up.png"/>&nbsp;' . number_format($value) . '</span>';
        } else {
            echo '<span class="text-success">' . number_format($value) . '</span>';
        }
    }


    /**
     * Check Key Ratio Value Negative or Positive
     * @param $value
     */
    public static function checkRatioValue($value, $status = true)
    {
        if ($status == true) {

            if ($value < 0) {
                echo '<span class="text-danger"><i class="fa fa-caret-down"></i>' . number_format($value, 2) . '</span>';
            } else if ($value > 0) {
                echo '<span class="text-success"><i class="fa fa-caret-up"></i>' . number_format($value, 2) . '</span>';
            } else {
                echo '<span class="text-success">' . number_format($value, 2) . '</span>';
            }
        } else {

            if ($value < 0) {
                echo '<span class="text-danger"><i class="fa fa-caret-down"></i>' . number_format($value) . '</span>';
            } else if ($value > 0) {
                echo '<span class="text-success"><i class="fa fa-caret-up"></i>' . number_format($value) . '</span>';
            } else {
                echo '<span class="text-success">' . number_format($value) . '</span>';
            }
        }
    }


    public static function checkPdfRatioValue($value, $status = true)
    {

        if ($status == true) {
            if ($value < 0) {
                echo '<span class="text-danger"><i class="fa fa-caret-down"> </i><img style="display:inline" src="/assets/img/down.png"/>&nbsp;' . number_format($value, 2) . '</span>';
            } else if ($value > 0) {
                echo '<span class="text-success"><img src="/assets/img/up.png"/>&nbsp;' . number_format($value, 2) . '</span>';
            } else {
                echo '<span class="text-success">' . number_format($value, 2) . '</span>';
            }
        } else {
            if ($value < 0) {
                echo '<span class="text-danger"><i class="fa fa-caret-down"> </i><img style="display:inline" src="/assets/img/down.png"/>&nbsp;' . number_format($value) . '</span>';
            } else if ($value > 0) {
                echo '<span class="text-success"><img src="/assets/img/up.png"/>&nbsp;' . number_format($value) . '</span>';
            } else {
                echo '<span class="text-success">' . number_format($value) . '</span>';
            }
        }
    }

    public static function getGrossProfitMargin($value)
    {
        if (($value <= 10) && ($value > 0)) {
            return 'The Gross Profit Margin is marginal in the most recent year of assessment. This measure is used to show how much profit is left after the cost of goods sold is accounted for but before deducting other expenses. A high number indicates strong profitability and a very low or a negative number indicates possible problems with the cost structure.';
        } elseif ($value < 0) {
            return 'The Gross Profit Margin is negative in the most recent year of assessment. This measure is used to show how much profit is left after the cost of goods sold is accounted for but before deducting other expenses. A high number indicates strong profitability and a very low or a negative number indicates possible problems with the cost structure.';
        } else {
            return 'The Gross Profit Margin is satisfactory in the most recent year of assessment. This measure is used to show how much profit is left after the cost of goods sold is accounted for but before deducting other expenses. A high number indicates strong profitability and a very low or a negative number indicates possible problems with the cost structure.';
        }
    }

    public static function getNetProfitMargin($value)
    {

        if (($value <= 5) && ($value > 0)) {
            return 'The Net Profit Margin is marginal in the most recent year of assessment. This measure illustrates the profits left after all ordinary expenses have been deducted from revenue earned. Higher the number more profitable the entity is.  A very low or a negative number indicates lack of control over expenses.';
        } elseif ($value < 0) {

            return 'The Net Profit Margin is negative in the most recent year of assessment. This measure illustrates the profits left after all ordinary expenses have been deducted from revenue earned. Higher the number more profitable the entity is.  A very low or a negative number indicates lack of control over expenses.';
        } else {
            return 'The Net Profit Margin is satisfactory in the most recent year of assessment. This measure illustrates the profits left after all ordinary expenses have been deducted from revenue earned. Higher the number more profitable the entity is.  A very low or a negative number indicates lack of control over expenses.';
        }
    }

    public static function getReturnOnAssets($data)
    {
        if ($data < 5) {
            return "Return on Assets is unsatisfactory in the most recent year of assessment. This measure illustrates how well the resources of an entity are used to generate income. Typically, a high number demonstrates that the management uses resources efficiently.";
        } elseif (($data >= 5) && ($data < 10)) {
            return "Return on Assets is marginal in the most recent year of assessment. This measure illustrates how well the resources of an entity are used to generate income. Typically, a high number demonstrates that the management uses resources efficiently.";
        } else {
            return "Return on Assets is satisfactory in the most recent year of assessment. This measure illustrates how well the resources of an entity are used to generate income. Typically, a high number demonstrates that the management uses resources efficiently.";
        }
    }

    public static function getCurrentRatio($data)
    {
        if ($data < 1.2) {
            return "Current Ratio is weak in the most recent year of assessment. This ratio demonstrates the entity’s ability to meet short-term obligations using the current assets available at disposal. If the Current Ratio is less than 1.30X then the entity may have difficulty meeting regular bills on time. A higher ratio is typically better than a lower one.";
        } elseif ($data < 1.8) {
            return "Current Ratio is marginal in the most recent year of assessment. This ratio demonstrates the entity’s ability to meet short-term obligations using the current assets available at disposal. If the Current Ratio is less than 1.30X then the entity may have difficulty meeting regular bills on time. A higher ratio is typically better than a lower one.";
        } else {
            return "Current Ratio is  satisfactory in the most recent year of assessment. This ratio demonstrates the entity’s ability to meet short-term obligations using the current assets available at disposal. If the Current Ratio is less than 1.30X then the entity may have difficulty meeting regular bills on time. A higher ratio is typically better than a lower one.";
        }
    }

    public static function getQuickRatio($data)
    {
        if ($data < 1) {
            return "Quick Ratio is  weak in the most recent year of assessment. Similar to the Current Ratio this measure indicates the entity’s ability to meet short-term obligations. However instead of considering all current assets, this ratio uses only near cash assets to see how short-term obligations are met.  Generally, a Quick Ratio more than 1.00X is considered satisfactory and risk increases as the ratio reduces.";
        } elseif ($data < 1.2) {
            return "Quick Ratio is marginal in the most recent year of assessment. Similar to the Current Ratio this measure indicates the entity’s ability to meet short-term obligations. However instead of considering all current assets, this ratio uses only near cash assets to see how short-term obligations are met.  Generally, a Quick Ratio more than 1.00X is considered satisfactory and risk increases as the ratio reduces.";
        } else {
            return "Quick Ratio is satisfactory in the most recent year of assessment. Similar to the Current Ratio this measure indicates the entity’s ability to meet short-term obligations. However instead of considering all current assets, this ratio uses only near cash assets to see how short-term obligations are met.  Generally, a Quick Ratio more than 1.00X is considered satisfactory and risk increases as the ratio reduces.";
        }
    }

    public static function getGearing($data)
    {
        if ($data > 50) {
            return "The Gearing level is high in the most recent year of assessment. This measure indicates the level of the entity’s operations funded by external funders compared to shareholders. While gearing levels need to be interpreted in comparison to industry, a level over 50% is considered as high.";
        } elseif ($data > 25) {
            return "The Gearing level is moderate in the most recent year of assessment. This measure indicates the level of the entity’s operations funded by external funders compared to shareholders. While gearing levels need to be interpreted in comparison to industry, a level over 50% is considered as high.";
        } else {
            return "The Gearing level is satisfactory in the most recent year of assessment. This measure indicates the level of the entity’s operations funded by external funders compared to shareholders. While gearing levels need to be interpreted in comparison to industry, a level over 50% is considered as high.";
        }
    }

    public static function getInterestCoverage($data)
    {
        if ($data <= 2) {
            return "Interest Cover is  unsatisfactory in the most recent year of assessment. This measure illustrates how many times the current interest payment is covered by the earnings of an organisation. The Interest Cover shows the entity’s ability to make interest payments on time. Higher the number the better.";
        } elseif ($data <= 4) {
            return "Interest Cover is moderate in the most recent year of assessment. This measure illustrates how many times the current interest payment is covered by the earnings of an organisation. The Interest Cover shows the entity’s ability to make interest payments on time. Higher the number the better.";
        } else {
            return "Interest Cover is considered satisfactory in the most recent year of assessment. This measure illustrates how many times the current interest payment is covered by the earnings of an organisation. The Interest Cover shows the entity’s ability to make interest payments on time. Higher the number the better.";
        }
    }

    public static function formatACN($acn)
    {
        $numberFormat = number_format($acn);
        $acnArray = explode(',', $numberFormat);
        $acnArray[0] = str_pad($acnArray[0], 3, 0, STR_PAD_LEFT);
        $finalAcn = implode(' ', $acnArray);
        return $finalAcn;
    }
}
