<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoutes extends Model
{
    protected $table = 'user_routes';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
