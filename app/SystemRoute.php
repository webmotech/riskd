<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemRoute extends Model
{
    protected $table = 'system_route';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function userRoutes()
    {
        return $this->hasMany(UserRoutes::class,'route_id','id');
    }
}
