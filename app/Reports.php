<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
    protected $table = 'reports';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function reportDetails()
    {
        return $this->hasMany(ReportDetails::class,'report_id','id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class,'company_id','id')->withTrashed();
    }

    public function reportStatus()
    {
        return $this->belongsTo(ReportStatus::class,'report_status','value');
    }

    public function customProfileData()
    {
        return $this->hasOne(ApiCustomProfile::class,'report_id','id');
    }

    public function executiveSummery()
    {
        return $this->hasMany(ReportExecutiveSummery::class,'report_id','id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class,'created_by','id')->withTrashed();
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class,'updated_by','id')->withTrashed();
    }

    public function approveRejectBy()
    {
        return $this->belongsTo(User::class,'approved_or_reject_by','id')->withTrashed();
    }
}
