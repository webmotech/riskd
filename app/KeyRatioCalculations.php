<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeyRatioCalculations extends Model
{
    protected $table = 'key_ratio_calculation';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
