<?php


namespace App\Repository;


use App\Repository\Contract\UsersRepositoryInterface;
use App\User;

class UsersRepository implements UsersRepositoryInterface
{


    public function create($data)
    {
        return User::create($data);
    }

    public function update($data,$id)
    {

       $user = User::find($id);
       $user->fname = $data['fname'];
       $user->lname = $data['lname'];
       $user->email = $data['email'];
       $user->user_role_id = $data['user_role_id'];
       $user->designation_id = (isset($data['designation_id']))?$data['designation_id']:null;
       $user->address = $data['address'];
       $user->telephone = $data['telephone'];
       $user->username = $data['username'];
       $user->save();
       return $user;

    }

    public function delete($id)
    {
        return User::where('id',$id)->delete();
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        return User::whereNotin('user_role_id',[1])->get();
    }

    public function updateAuthPassword($data,$authUserId)
    {
        $user = User::find($authUserId);
        $user->password = bcrypt($data['new_password']);
        $user->is_reset_password = 1;
        $user->save();
        return $user;
    }

    public function changePassword($data,$id)
    {

        $user = User::find($id);
        $user->password = bcrypt($data['new_password']);
        $user->save();
        return $user;
    }

    public function checkUserByUsername($username,$id=null)
    {
        if(!is_null($id)){
            return User::where('username',$username)
                ->whereNotin('id',[$id])
                ->count();
        }
        return User::where('username',$username)->count();
    }

    public function checkByEmail($email,$id=null)
    {
        if(!is_null($id)){
            return User::where('email',$email)
                ->whereNotin('id',[$id])
                ->count();
        }
        return User::where('email',$email)->count();
    }

}
