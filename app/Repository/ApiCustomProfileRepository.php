<?php


namespace App\Repository;


use App\ApiCustomProfile;
use App\Repository\Contract\ApiCustomProfileRepositoryInterface;

class ApiCustomProfileRepository implements ApiCustomProfileRepositoryInterface
{
    public function create($data)
    {
       return ApiCustomProfile::create($data);
    }

    public function update($data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

}
