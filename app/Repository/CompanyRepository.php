<?php


namespace App\Repository;


use App\Repository\Contract\CompanyRepositoryInterface;
use App\Company;

class CompanyRepository implements CompanyRepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return Company::create($data);
    }

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function update($data, $id)
    {
       return Company::where('company_token',$id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return Company::where('company_token',$id)->delete();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return Company::where('company_token',$id)->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCompanyById($id)
    {
        return Company::where('id',$id)->first();
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getAllCompanies()
    {
        return Company::all();
    }

}
