<?php


namespace App\Repository;


use App\ReportDetails;
use App\Repository\Contract\ReportDetailsRepositoryInterface;

class ReportDetailsRepository implements ReportDetailsRepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
       return ReportDetails::create($data);
    }

    public function update($data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getReportDetailsByReportId($reportId)
    {
        return ReportDetails::where('report_id',$reportId)
        ->with('keyRatioCalculation')
        ->orderBy('financial_year','ASC')
        ->get();
    }

    public function deleteByReportId($reportId)
    {
       return ReportDetails::where('report_id',$reportId)->delete();
    }


}
