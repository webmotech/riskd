<?php


namespace App\Repository;


use App\Company;
use App\Reports;
use App\Repository\Contract\ReportRepositoryInterface;

class ReportRepository implements ReportRepositoryInterface
{
    public function create($data)
    {
        return Reports::create($data);
    }

    public function update($data, $id)
    {
        return Reports::where('report_token', $id)->update($data);
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function getReportsByStatus($where)
    {
        return Reports::whereIn('report_status', $where)
            ->with(['createdBy', 'updatedBy', 'approveRejectBy'])
            ->get();
    }

    /**
     * @param $reportToken
     * @param $status
     * @return mixed
     */
    public function changeReportStatus($reportToken, $status)
    {
        return Reports::where('report_token', $reportToken)->update(['report_status' => $status]);
    }


    public function getReportDataByToken($id)
    {

        return Reports::where('report_token', $id)->first();
    }

    public function getPreviousYearReportDatabyCompany($companyToken)
    {

        $company = Company::where('company_token', $companyToken)->withTrashed()

            ->first();

        $report = Reports::where('company_id', $company->id)
            ->where('report_status', 4)
            ->orderBy('generated_at', 'DESC')
            ->get();


        return $report;
    }
}
