<?php


namespace App\Repository\Contract;


interface ReportDetailsRepositoryInterface
{
    /**
     * Define create method
     * @return mixed
     */
    public function create($data);

    /**
     * Define update method
     * @return mixed
     */
    public function update($data,$id);

    /**
     * Define delete method
     * @return mixed
     */
    public function delete($id);

    /**
     * Define Get Data by ID method
     * @param $id
     * @return mixed
     */
    public function get($id);

    /**
     * Define Fetch All data method
     * @return mixed
     */
    public function getAll();
}
