<?php


namespace App\Repository;


use App\KeyRatioCalculations;
use App\Repository\Contract\KeyRatioCalculationRepositoryInterface;

class KeyRatioCalculationRepository implements KeyRatioCalculationRepositoryInterface
{
    public function create($data)
    {
        // TODO: Implement create() method.
    }

    public function update($data, $id)
    {
        return KeyRatioCalculations::where('id',$id)->update($data);
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    /**
     * Delete Key Ratio By report Details ID
     * @param $reportDetailsIdList
     */
    public function deleteByReportDetailsId($reportDetailsIdList)
    {
        return KeyRatioCalculations::whereIn('report_details_id',$reportDetailsIdList)->delete();
    }

}
