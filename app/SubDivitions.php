<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubDivitions extends Model
{
    protected $table = 'sub_divisions';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
