<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designations extends Model
{
    protected $table = 'designations';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
