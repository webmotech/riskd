<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisions extends Model
{
    protected $table = 'divisions';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
