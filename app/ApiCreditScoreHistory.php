<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiCreditScoreHistory extends Model
{

    protected $table = 'api_credit_score_history';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
