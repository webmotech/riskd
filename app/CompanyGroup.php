<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyGroup extends Model
{
    protected $table = 'company_group';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
