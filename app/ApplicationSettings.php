<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationSettings extends Model
{
    protected $table = 'application_settings';

    protected $primaryKey = 'id';

    protected $casts = [
        'value' => 'array'
    ];

    protected $guarded = [];
}
