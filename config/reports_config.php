<?php
return [
    //Report Type Configuration
    'report_type' => [
        1 => 'Financial Risk Assessment Report with Analysis',
        2 => 'Financial Risk Assessment Report'
    ],

    // Rounding Configuration
    'rounding' => [
        'N' => 'None',
        'TH' => 'Thousands',
        'MI' => 'Millions'
    ],

    // Base Currency Configuration
    'base_currency' => [
        'AUD' => 'AUD',
        'CAD' => 'CAD',
        'CNY' => 'CNY',
        'EUR' => 'EUR',
        'GBP' => 'GBP',
        'LKR' => 'LKR',
        'NZD' => 'NZD',
        'USD' => 'USD',
    ],

    //Quality Configuration
    'quality' => [
        'Management' => 'Management',
        'Autdited' => 'Audited',
        'Statutory' => 'Statutory',
        'Forcast' => 'Forecast',
        'Anualised' => 'Annualised',

    ],

    //Scope Configuration
    'scope' => [
        'ASIC' => 'ASIC',
        'Consolidated' => 'Consolidated',
        'Parents' => 'Parent',
        'Aggregated' => 'Aggregated'
    ],

    'months' => [
        'January' => 'January',
        'February' => 'February',
        'March' => 'March',
        'April' => 'April',
        'May' => 'May',
        'June' => 'June',
        'July' => 'July',
        'August' => 'August',
        'September' => 'September',
        'October' => 'October',
        'November' => 'November',
        'December' => 'December',
    ],

];
