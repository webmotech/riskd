<?php $__env->startSection('custom-css'); ?>
    <style type="text/css">
        .footer {
            position: absolute;
            /* bottom: 0px; */
            top:95%;
        }
        body{
            font-family: "Calibri" !important;
        }

        .text-danger{
            color: #dc3545;
        }
        .alert-danger {
            color: #721c24;
            background-color: #f8d7da;
        }
        .bg-primary {
            background-color: #007bff !important;
        }
        .text-success {
            color: #28a745!important;
        }
        tr td{
            padding: 10px;
            text-align: left;
        }

        .text-warning {
            color: #fb6340 !important;
        }

        /*page[size="A4"] {*/
        /*    width: 95vw !important;*/
        /*    min-height: 99vh;*/
        /*}*/

        .report-type{
            margin-top:250px;
        }

        .add-page{
            page-break-after: always;
            margin-top: 10px;
        }
        .page-no{
            position: relative;
            /* bottom: 0px; */
            top:90%;
        }
        page{
            margin-top:200px;
        }
        .table {
            border-collapse: collapse !important;
        }
        .table td,
        .table th {
            background-color: #fff !important;
        }

        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }
        .col {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -ms-flex-positive: 1;
            flex-grow: 1;
            max-width: 100%;
        }


        .col-md-1 {
            -ms-flex: 0 0 8.333333%;
            flex: 0 0 8.333333%;
            max-width: 8.333333%;
        }
        .col-md-2 {
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
        }

        p td{
            text-align: justify;
        }
        .col-md-3 {
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }
        .col-md-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }
        .col-md-5 {
            -ms-flex: 0 0 41.666667%;
            flex: 0 0 41.666667%;
            max-width: 41.666667%;
        }
        .col-md-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
        .col-md-7 {
            -ms-flex: 0 0 58.333333%;
            flex: 0 0 58.333333%;
            max-width: 58.333333%;
        }
        .col-md-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }
        .col-md-9 {
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }
        .col-md-10 {
            -ms-flex: 0 0 83.333333%;
            flex: 0 0 83.333333%;
            max-width: 83.333333%;
        }
        .col-md-11 {
            -ms-flex: 0 0 91.666667%;
            flex: 0 0 91.666667%;
            max-width: 91.666667%;
        }
        .col-md-12 {
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .tbl-style tr:nth-child(even){
            background-color: #f2f2f2;

        }
        .tbl-style tr th {
            border: 0.5px solid #949494;
        }
        .tbl-style td{
            border: 0.5px solid #949494;
        }

        .tbl-special tr th {
            border: 0.5px solid #949494;
        }
        .tbl-special td{
            border: 0.5px solid #949494;

        }



        .container {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: 0;
            margin-left: 0;
        }

        /* @page  {
            margin: 56px;
            header: page-header;
            margin-header: 5mm;
            margin-footer: 5mm;
        } */

        @page  {
    footer: html_otherpages;
    margin: 56px;
    header: page-header;
    margin-header: 5mm;
    margin-footer: 5mm;
        }

    @page  :first {
        footer: html_firstpage;
        margin: 56px;
        header: page-header;
        margin-header: 5mm;
        margin-footer: 5mm;
    }
    .summernote .note-editable {
  p {
    font-size: 18px!important;
    font-weight: normal;
  }






    </style>
    <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <htmlpageheader name="page-header">

    </htmlpageheader>

    <htmlpagefooter name="firstpage" style="display:none">
        <p style="text-align: right;font-size:12px;"></p>
    </htmlpagefooter>

    <htmlpagefooter name="otherpages" style="display:none">
        <p style="text-align: right;font-size:12px;">Page {PAGENO}</p>
    </htmlpagefooter>





    <div class="container p-3">

        <page>
            <p class="text-left"><img src="<?php echo e(asset('assets/img/logo-report.png')); ?>" alt="" style="width:500px;margin-top:100px"></p>
            <h2 class="report-type"><?php echo e(config('reports_config.report_type')[$report->report_type]); ?></h2>
            <div style="margin-top: 60%" class="footer">

                <p style="font-size:10pt" class="text-left">Report Ref: <b><?php echo e($report->id); ?></b></p>
                <p style="font-size:10pt" class="text-left mt-5">Report Date: <b><?php echo e($report->generated_at); ?></b>
                <p style="font-size:10pt" class="">Phone : 1300 50 13 12 | Email : admin@creditorwatch.com.au</p>

                </p>
            </div>

        </page>
        <div class="add-page"></div>
        <page>
            <div class="row">
                <div class="col-md-12">


                    <h1 style="font-size:18pt" class="text-danger">Organisation Summary</h1>
                    <h3 style="margin-top:36px" class="text-danger">ABR Summary</h3>


                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="tbl-style" style="width:100%;font-size:12px; border-collapse: collapse !important;" >
                        <tr>
                            <th style="width:250px;text-align: left; padding-left:8px;">Main Name</th>
                            <td style="font-family: Calibri"><?php echo e($apidata['main_name']); ?></td>
                        </tr>
                        
                        <tr>
                            <th style="width:250px;text-align: left; padding-left:8px;">ABN</th>
                            <td><?php echo e(str_replace(',',' ',number_format($apidata['abn']))); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left; padding-left:8px;">Entity Status</th>
                            <td><?php echo e($apidata['entity_status']); ?></td>
                        </tr>
                        
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">ABN is Current</th>
                            <td>Yes</td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left; padding-left:8px;">Entity Type</th>
                            <td><?php echo e($apidata['entity_type']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left; padding-left:8px;">GST</th>
                            <td><?php echo e($apidata['gst']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left; padding-left:8px;">Locality</th>
                            <td><?php echo e($apidata['locality']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left; padding-left:8px;">Record Last Updated</th>
                            <td><?php echo e($apidata['record_last_updated']); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">



                    <h3 style="margin-top:50px;" class="text-danger">ASIC Summary</h3>


                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table border="1" class="tbl-style" style="width:100%;font-size:12px; border-collapse: collapse !important;">
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">Name</th>
                            <td><?php echo e($apidata['name']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">ACN</th>
                            <td><?php echo e(Helpers::formatAcn($apidata['acn'])); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">Type</th>
                            <td><?php echo e($apidata['type']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">Status</th>
                            <td><?php echo e($apidata['status']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">Controlling Jurisdiction</th>
                            <td>ASIC</td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">Registration Date</th>
                            <td><?php echo e($apidata['registered_date']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">Review Date</th>
                            <td><?php echo e($apidata['review_date']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">Class</th>
                            <td><?php echo e($apidata['class']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">Sub Class</th>
                            <td><?php echo e($apidata['subclass']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">Locality</th>
                            <td><?php echo e($apidata['asic_locallity']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:250px;text-align: left;padding-left:8px;">Next Review Date</th>
                            <td><?php echo e($apidata['review_date']); ?></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="add-page"></div>
        </page>

        <page>
            <div class="row">
                <div class="col-md-12">
                    <h2 style="font-size:14pt;" class="text-danger">Non – Financial Based Credit Score</h2>
                    <p style="text-align: justify;font-size: 12px" class="mt-2">The score is a statistically based score indicating an entity's credit worthiness. The score
                        ultimately ranks entities based on their riskiness and is designed to assist you in making more
                        informed and consistent credit decisions.</p>
                    <p style="text-align: justify;font-size: 12px">The score is based between 0 and 850 index points with a higher score considered lower risk while
                        lower scores are deemed to be riskier entities. It should be used in partnership with your
                        internal credit procedures and policies.
                    </p>
                    <p style="text-align: justify;font-size: 12px">The Entity has acceptable creditworthiness. Extend terms within consideration. The entity has a <?php echo e($current_credit_percentage); ?>% chance of failure within the next 12 months.

                </div>
            </div>
            <div style="margin-top:40px;" class="row">
                <div class="container">
                    <div class="col-md-12">

                        <?php
                            $creditScorePercentage = number_format(($apidata['current_credit_score']/850)*100,2);

                        ?>
                        <div class="progress" style=" height:50px; background:linear-gradient(30deg, rgba(255,0,31,1) 0%, rgba(255,226,0,1) 49%, rgba(18,255,0,1) 100%);">
                            <div class="" role="progressbar" style=" height: 50px; text-align:right;width: <?php echo e($creditScorePercentage); ?>%;  background:rgba(0,0,0,0.2);font-size:30px" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                <?php echo e($apidata['current_credit_score']); ?>

                            </div>
                        </div>
                        <table>
                            <tr>
                                <td style="width: 96%; padding: 0">0</td>
                                <td align="right" style="text-align:right; padding: 0">850</td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style="width: 87%; padding: 0" class="text-danger">Higher Risk</td>
                                <td align="right" style="text-align:right; padding: 0"><p>&nbsp;Lower Risk</p></td>
                            </tr>
                        </table>

                    </div>

                </div>

                <div class="col-md-12">
                    <h2 style="font-size:14pt" class="mt-1">Historical Credit Scores</h2>

                    <div class="row" style="width: 100%" id="credit-chart">
                        <img style="width: 100%;" src="<?php echo e(asset('assets/reports')); ?>/<?php echo e($report->report_token); ?>/<?php echo e('credit_score.png'); ?>"/>
                    </div>

                </div>

            </div>
            <div class="add-page"></div>
            <div class="row">
                <div class="col-md-12">
                    <h2 style="font-size:14pt" class="text-danger">Recommendations</h2>
                    <table class="tbl-style" style="width:100%; font-size: 12px; border-collapse: collapse !important;">
                        <thead class="thead-light">
                        <tr>
                            <th width="20%">Range Risk</th>
                            <th width="20%">level</th>
                            <th width="60%">Recommendation</th>
                        </tr>
                        </thead>
                        <tbody style="text-align: justify;">
                        <tr>
                            <td>0</td>
                            <td>Critical</td>
                            <td>ACN deregistered or ABN cancelled.</td>
                        </tr>
                        <tr>
                            <td>1-125</td>
                            <td>Critical</td>
                            <td>Entity has a critical status and significant adverse information present.
                                Trading eligibility must be considered.</td>
                        </tr>
                        <tr>
                            <td>126-250</td>
                            <td>Very High</td>
                            <td>Entity has multiple pieces of adverse information present. COD trading highly
                                recommended.</td>
                        </tr>
                        <tr>
                            <td>251-450</td>
                            <td>High</td>
                            <td>Entity has a below average creditworthiness score and some adverse information
                                may be present. Trade with caution, monitor closely and consider your payment
                                terms.</td>
                        </tr>
                        <tr>
                            <td>451-550</td>
                            <td>Moderate</td>
                            <td>Entity has moderate creditworthiness with or without adverse information.
                                Monitor ongoing payment behaviour.</td>
                        </tr>
                        <tr>
                            <td>551-850</td>
                            <td>Low</td>
                            <td>Entity has acceptable creditworthiness. Extend terms within consideration.</td>
                        </tr>
                        </tbody>
                    </table>

                    <p style="text-align: justify;font-size: 12px">Please note that the score and recommendation should be used in partnership with your
                        company's internal credit procedures and policies. The score should not be used as the sole reason in making a decision about the entity.</p>

                </div>
            </div>
            <div class="add-page"></div>
            <div class="row">
                <div class="col-md-12">
                    <h2 style="font-size:14pt; margin-bottom:0px;" class="mt-3 text-danger">Financial Summary </h2>
                    <table class="tbl-special" style="margin-top:50px; width:100%;font-size: 12px;border-collapse: collapse !important;">
                        <tbody>

                        <tr class="alert alert-danger">

                            <td style="width: 30%; text-align:left;">Financial Year</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <td colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><?php echo e($details['financial_year']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">

                            <td width='300px' style="text-align:left">Rounding</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td width="" colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><?php echo e(config('reports_config.rounding')[$details['rounding']]); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="text-align:left">Base Currency</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td width="" colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><?php echo e($details['base_currency']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="text-align:left">Quality</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td width="" colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><?php echo e(config('reports_config.quality')[$details['quality']]); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="text-align:left">Reporting Period - Months</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td width="" colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><?php echo e($details['report_period_months']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="text-align:left">Month</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td width="" colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><?php echo e($details['financial_month']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr style="border: #949494 solid 0.5px;">
                            <td style="border: none;text-align: left; padding-top:15px; padding-bottom:15px;"><h4>Income Statement</h4></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>
                                <?php if($key>0): ?>
                                    <td style="border: none"></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Sales</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['sales'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['sales']-$report_details[$key-1]['sales'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Gross Profit</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['gross_profit'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['gross_profit']-$report_details[$key-1]['gross_profit'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Other Income</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['other_income'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['other_income']-$report_details[$key-1]['other_income'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">EBIT</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['ebit'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['ebit']-$report_details[$key-1]['ebit'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">EBITDA</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['ebitda'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['ebitda']-$report_details[$key-1]['ebitda'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Profit Before Tax</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['profit_before_tax'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['profit_before_tax']-$report_details[$key-1]['profit_before_tax'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Profit After Tax</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['profit_after_tax'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['profit_after_tax']-$report_details[$key-1]['profit_after_tax'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>

                        <tr style="border: #949494 solid 0.5px;">
                            <td style="border: none;text-align: left; padding-top:15px; padding-bottom:15px;"><h4>Balance Sheet</h4></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>
                                <?php if($key>0): ?>
                                    <td style="border: none"></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>

                        <tr class="">
                            <td width='300px' style="text-align:left">Total Current Assets</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_current_assets'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['total_current_assets']-$report_details[$key-1]['total_current_assets'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Total Non-Current Assets</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_non_current_assets'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['total_non_current_assets']-$report_details[$key-1]['total_non_current_assets'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Total Assets</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_assets'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['total_assets']-$report_details[$key-1]['total_assets'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Total Current Liabilities</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_current_liabilities'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['total_current_liabilities']-$report_details[$key-1]['total_current_liabilities'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Total Non-Current Liabilities</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_non_current_liabilities'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['total_non_current_liabilities']-$report_details[$key-1]['total_non_current_liabilities'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Total Liabilities</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_liabilities'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['total_liabilities']-$report_details[$key-1]['total_liabilities'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Share Capital</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['share_capital'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['share_capital']-$report_details[$key-1]['share_capital'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left">Total Equity</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <td style="text-align:right"><?php echo e(number_format($details['total_equity'])); ?></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><?php echo e(Helpers::checkPdfValue($details['total_equity']-$report_details[$key-1]['total_equity'])); ?></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
            <div class="add-page"></div>
            <div class="row">
                <?php
                    $currentKeyRatio = last($report_details->toArray())['key_ratio_calculation'];

                ?>

                <?php if($report->report_type==1): ?>
                    <div class="col-md-12">
                        <h3 class="text-danger">Financial Analysis</h3>
                        <div style="font-size:14px; text-align:justify;">
                        <?php $__currentLoopData = $report->executiveSummery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $summery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo $summery['summery']; ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div>
                    </div>
                <?php elseif($report->report_type==2): ?>
                    <div class="col-md-12">
                        <h3 class="text-danger">Key Performance Indicator Analysis</h3>
                        <table class="tbl-special" style="margin-top:50px;font-size: 12px;width:100%;border-collapse: collapse !important;">
                            <tbody style="text-align: justify;">
                            <tr>
                                <td style="width: 30%; font-size:14px;"><b> Gross Profit Margin</b></td>
                                <td style="font-size:14px;text-align: justify; padding-bottom:20px; line-height:18px;"><?php echo e(Helpers::getGrossProfitMargin($currentKeyRatio['gross_profit_margin'])); ?></td>
                            </tr>
                            <tr>
                                <td style="font-size:14px;"><b> Net Profit Margin </b></td>
                                <td style="font-size:14px;text-align: justify;line-height:18px;padding-bottom:20px;"><?php echo e(Helpers::getNetProfitMargin($currentKeyRatio['net_profit_margin'])); ?></td>
                            </tr>

                            <tr>
                                <td style="font-size:14px;"><b> Return on Assets</b></td>
                                <td style="font-size:14px;text-align: justify;line-height:18px;padding-bottom:20px;"><?php echo e(Helpers::getReturnOnAssets($currentKeyRatio['return_on_assets'])); ?></td>
                            </tr>
                            <tr>
                                <td style="font-size:14px;"><b> Current Ratio</b> </td>
                                <td style="font-size:14px;text-align: justify;line-height:18px;padding-bottom:20px;"><?php echo e(Helpers::getCurrentRatio($currentKeyRatio['current_ratio'])); ?></td>
                            </tr>
                            <tr>
                                <td style="font-size:14px;"><b> Quick Ratio</b> </td>
                                <td style="font-size:14px;text-align: justify;line-height:18px;padding-bottom:20px;"><?php echo e(Helpers::getQuickRatio($currentKeyRatio['quick_ratio'])); ?></td>
                            </tr>

                            <tr>
                                <td style="font-size:14px;"><b> Gearing</b></td>
                                <td style="font-size:14px;text-align: justify;line-height:18px;padding-bottom:20px;"><?php echo e(Helpers::getGearing($currentKeyRatio['gearing'])); ?></td>
                            </tr>
                            <tr>
                                <td style="font-size:14px;"><b> Interest Coverage </b></td>
                                <td style="font-size:14px;text-align: justify;line-height:18px;padding-bottom:20px;"><?php echo e(Helpers::getInterestCoverage($currentKeyRatio['interest_coverage'])); ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>

            </div>
            <div class="add-page"></div>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-danger">Financial Trend Graphs</h3>
                </div>
            </div>

            <div class="row">
                <table>
                    <tr>
                        <td><img style="width:100%;" src="<?php echo e(asset('assets/reports')); ?>/<?php echo e($report->report_token); ?>/<?php echo e('revenue.png'); ?>"/></td>
                        <td> <img style="width:100%;" src="<?php echo e(asset('assets/reports')); ?>/<?php echo e($report->report_token); ?>/<?php echo e('gpnp_margin.png'); ?>"/></td>
                    </tr>
                    <tr>
                        <td><img style="width:100%;" src="<?php echo e(asset('assets/reports')); ?>/<?php echo e($report->report_token); ?>/<?php echo e('gross_net_profit.png'); ?>"/></td>
                        <td> <img style="width:100%;" src="<?php echo e(asset('assets/reports')); ?>/<?php echo e($report->report_token); ?>/<?php echo e('ebit.png'); ?>"/></td>
                    </tr>
                    <tr>
                        <td><img style="width:100%;" src="<?php echo e(asset('assets/reports')); ?>/<?php echo e($report->report_token); ?>/<?php echo e('ratio.png'); ?>"/></td>
                        <td> <img style="width:100%;" src="<?php echo e(asset('assets/reports')); ?>/<?php echo e($report->report_token); ?>/<?php echo e('capital.png'); ?>"/></td>
                    </tr>
                    <tr>
                        <td><img style="width:100%;" src="<?php echo e(asset('assets/reports')); ?>/<?php echo e($report->report_token); ?>/<?php echo e('equity.png'); ?>"/></td>
                        <td> <img style="width:100%;" src="<?php echo e(asset('assets/reports')); ?>/<?php echo e($report->report_token); ?>/<?php echo e('interest_cover.png'); ?>"/></td>
                    </tr>
                </table>

            </div>


        </page>
        <div class="add-page"></div>
        <page>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-danger">Financial Performance</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="tbl-special" style="margin-top:50px;font-size: 12px;width:100%; border-collapse: collapse !important;">
                        <tbody>

                        <tr class="alert alert-danger">

                            <td style=" font-size: 12px; text-align:left; padding:7px;">Financial Year</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <td style="text-align:right; padding:7px;min-width:287px;"><?php echo e($details['financial_year']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">

                            <td width='300px' style="text-align:left; padding:7px">Rounding</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(config('reports_config.rounding')[$details['rounding']]); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="text-align:left; padding:7px">Base Currency</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e($details['base_currency']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="text-align:left; padding:7px">Quality</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(config('reports_config.quality')[$details['quality']]); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="text-align:left; padding:7px">Reporting Period - Months</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e($details['report_period_months']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="text-align:left; padding:7px">Confidenality Record </td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e($details->confidentiality['name']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="text-align:left; padding:7px">Month</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e($details['financial_month']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr>
                        <tr style="border: #949494 solid 0.5px;">
                            <td style="border: none;text-align: left; padding-top:15px; padding-bottom:15px;"><h4>Income Statement</h4></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:6px">Sales</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(number_format($details['sales'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Cost of Sales</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(number_format($details['cost_of_sales'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Gross Profit</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(number_format($details['gross_profit'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Other Income</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(number_format($details['other_income'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Depreciation</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(number_format($details['depreciation'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Amortisation</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(number_format($details['amortisation'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Impairment</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(number_format($details['impairment'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Interest Expense</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(number_format($details['interest_expenses_gross'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Non - Recurring Gains/ (Losses)</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(number_format($details['non_recurring_gain_loses'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Other Gains / (Losses)</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e(number_format($details['other_gain_loses'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Other Expenses</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><strong><?php echo e(number_format($details['other_expenses'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">EBIT</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><strong><?php echo e(number_format($details['ebit'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">EBITDA</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><strong><?php echo e(number_format($details['ebitda'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Normalised EBITDA</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><strong><?php echo e(number_format($details['normalized_ebitda'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Profit Before Tax</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><strong><?php echo e(number_format($details['profit_before_tax'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Profit Before Tax (After Abnormals)</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><strong><?php echo e(number_format($details['profit_before_tax_after_abnormals'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left; padding:7px">Tax Benefit/ (Expense)</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><strong><?php echo e(number_format($details['tax_benefit_expenses'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:7px">Profit After Tax</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['profit_after_tax'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:7px">Distribution or Dividends</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['distribution_ordividends'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:7px">Other Post Tax Items - Gains/ (Losses)</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['other_post_tax_items_gains_losses'])); ?></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:7px">Profit After Tax & Distribution</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['profit_after_tax_distribution'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </page>
        <div class="add-page">

        </div>
        <page>
            <div class="row">
                <div class="col-md-12">
                    <table class="tbl-special" style="width:100%;font-size: 12px;border-collapse: collapse !important;">
                        <tbody>

                        <tr class="alert alert-danger">

                            <td style="text-align:left;padding: 6px; min-width:287px;">Financial Year</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <td style="text-align:right;"><?php echo e($details['financial_year']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">

                            <td width='300px' style="padding: 6px;text-align:left">Rounding</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(config('reports_config.rounding')[$details['rounding']]); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="padding: 6px;text-align:left">Base Currency</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e($details['base_currency']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="padding: 6px;text-align:left">Quality</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(config('reports_config.quality')[$details['quality']]); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="padding: 6px;text-align:left">Reporting Period - Months</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e($details['report_period_months']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style="text-align:left; padding:7px">Confidenality Record </td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right; padding:7px"><?php echo e($details->confidentiality['name']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <td width='300px' style=" padding: 6px;text-align:left">Month</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e($details['financial_month']); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr style="border: #949494 solid 0.5px;">
                            <td style="border: none; padding:6px; text-align: left; padding-top:15px; padding-bottom:15px;"><h4>Balance Sheet</h4></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr style="border: #949494 solid 0.5px;">
                            <td style="border: none; padding:6px; text-align: left;padding-top:10px; padding-bottom:10px;"><p class="text-warning">Assets</p></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>

                        <tr style="width: 100%;">
                            <td  style="padding:6px;text-align:left ">Cash</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['cash'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Trade Debtors</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['trade_debtors'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Total Inventories</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_inventories'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Loans to Related Parties</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['loan_to_related_parties_1'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Other Current Assets</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['other_current_assets'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Total Current Assets</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_current_assets'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Fixed Assets</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['fixed_assets'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Net Intangibles</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['net_intangibles'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Loans to Related Parties</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['loan_to_related_parties_2'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Other Non-Current Assets</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['other_non_current_assets'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Total Non-Current Assets</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_non_current_assets'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:6px;text-align:left">Total Assets</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_assets'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr style="border: #949494 solid 0.5px;">
                            <td style="border: none; padding:6px; text-align: left; padding-top:10px; padding-bottom:10px;"><p class="text-warning">Liabilities</p></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>

                        <tr class="">
                            <td width='300px' style="padding:5px;text-align:left">Trade Creditors</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['trade_creditors'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:5px;text-align:left">Interest Bearing Debt</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['interest_bearing_debt_1'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:5px;text-align:left">Loan from Related Parties</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['loan_from_related_parties_1'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:5px;text-align:left">Other Current Liabilities</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['other_current_liabilities'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="padding:5px;text-align:left">Total Current Liabilities</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><?php echo e(number_format($details['total_current_liabilities'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>



                        </tbody>
                    </table>
                </div>
            </div>
        </page>
        <div class="add-page"></div>
        <page>

                        <div class="row">
                            <div class="col-md-12">
                                <table class="tbl-special" style="width:100%;font-size: 12px;border-collapse: collapse !important;">
                                    <tbody>
                                        <tr class="">
                                            <td width='300px' style="padding:5px;text-align:left;min-width:287px;">Interest Bearing Debt</td>
                                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td style="text-align:right"><?php echo e(number_format($details['interest_bearing_debt_2'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tr>

                                        <tr class="">
                                            <td width='300px' style="text-align:left;padding:5px;">Loan from Related Parties</td>
                                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td style="text-align:right"><?php echo e(number_format($details['loan_from_related_parties_2'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tr>
                                        <tr class="">
                                            <td width='300px' style="text-align:left;padding:5px;">Other Non-Current Liabilities</td>
                                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td style="text-align:right"><?php echo e(number_format($details['other_non_current_liabilities'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:5px;">Total Non-Current Liabilities</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;"><?php echo e(number_format($details['total_non_current_liabilities'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:5px;">Total Liabilities</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;"><?php echo e(number_format($details['total_liabilities'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr style="border: #949494 solid 0.5px;">
                            <td style="border: none; padding:6px; text-align: left;"><p class="text-warning">Equity</p></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>


                        <tr class="">
                            <td width='300px' style="text-align:left;padding:5px;">Share Capital</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;"><?php echo e(number_format($details['share_capital'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:5px;">Preference Shares</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;"><?php echo e(number_format($details['prefence_shares'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:5px;">Treasury Shares</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;"><?php echo e(number_format($details['threasury_shares'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:5px;">Equity Ownerships</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;"><?php echo e(number_format($details['equity_ownerships'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:5px;">Total Reserves</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;"><?php echo e(number_format($details['total_reserves'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:5px;">Retained Earnings</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;"><?php echo e(number_format($details['retained_earning'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:5px;">Minority Interest</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;"><?php echo e(number_format($details['minorty_interest'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <td width='300px' style="text-align:left;padding:5px;">Total Equity</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;"><?php echo e(number_format($details['total_equity'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        </tbody>

                    </table>

                </div>
            </div>
        </page>


        <div class="add-page"></div>
        <page>
            <div class="row">
                <?php


                    ?>
                <div class="col-md-12">
                    <h3 class="text-danger">Key Ratios</h3>
                    <table class="tbl-special" style=" margin-top:30px;width: 100%; font-size:12px;border-collapse: collapse !important;">
                        <tr>
                            <th style="width:27%"></th>
                            <th style="width:fit-content">Denomination</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $colspanvalue=2; ?>
                            <?php else: ?>
                            <?php $colspanvalue=1; ?>

                            <?php endif; ?>
                                <th colspan="<?php echo e($colspanvalue); ?>"><?php echo e($details['financial_year']); ?></th>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <th style="">FY<?php echo e($report_details[$lastIndex]['financial_year']); ?> vs FY<?php echo e($report_details[$lastIndex-1]['financial_year']); ?></th>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="border-left:#949494 solid 0.5px; border-right: none; padding:7px; text-align: left;"><p class="text-warning"><strong>Profitability</strong></p></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <td style="border: none"> </td>
                            <td style="border-right: #949494 solid 0.5px;border-left: none"> </td>

                        </tr>
                        <tr>
                            <td style="padding:7px">Gross Profit Margin</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $colspanvalue=2; ?>
                            <?php else: ?>
                            <?php $colspanvalue=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                        <td colspan="<?php echo e($colspanvalue); ?>" style="padding:7px; text-align:center;"><?php echo e(number_format($keyRatio['gross_profit_margin'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['gross_profit_margin']-$report_details[$lastIndex-1]['keyRatioCalculation']['gross_profit_margin'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">EBIDTA</td>
                            <td style="padding:7px;text-align:center;">Thousands</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $colspanvalue=2; ?>
                            <?php else: ?>
                            <?php $colspanvalue=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                        <td colspan="<?php echo e($colspanvalue); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['ebitda'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['ebitda']-$report_details[$lastIndex-1]['keyRatioCalculation']['ebitda'],false)); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Net Profit Margin</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $colspanvalue=2; ?>
                            <?php else: ?>
                            <?php $colspanvalue=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($colspanvalue); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['net_profit_margin'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['net_profit_margin']-$report_details[$lastIndex-1]['keyRatioCalculation']['net_profit_margin'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Return on Assets</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $colspanvalue=2; ?>
                            <?php else: ?>
                            <?php $colspanvalue=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($colspanvalue); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['return_on_assets'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['return_on_assets']-$report_details[$lastIndex-1]['keyRatioCalculation']['return_on_assets'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Return on Equity</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $colspanvalue=2; ?>
                            <?php else: ?>
                            <?php $colspanvalue=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                        <td colspan="<?php echo e($colspanvalue); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['return_on_equity'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['return_on_equity']-$report_details[$lastIndex-1]['keyRatioCalculation']['return_on_equity'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="border-left:#949494 solid 0.5px; border-right: none; padding:7px; text-align: left;"><p class="text-warning"><strong>Financial Leverage</strong></p></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <td style="border: none"> </td>
                            <td style="border-right: #949494 solid 0.5px;border-left: none"> </td>


                        </tr>
                        <tr>
                            <td style="padding:7px">Net Tangible Worth</td>
                            <td style="padding:7px;text-align:center;">Thousands</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $colspanvalue=2; ?>
                            <?php else: ?>
                            <?php $colspanvalue=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($colspanvalue); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['net_tangible_worth'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['net_tangible_worth']-$report_details[$lastIndex-1]['keyRatioCalculation']['net_tangible_worth'],false)); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Gearing</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['gearing'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['gearing']-$report_details[$lastIndex-1]['keyRatioCalculation']['gearing'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Debt to Equity</td>
                            <td style="padding:7px;text-align:center;">X</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                        <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['debt_to_equity'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['debt_to_equity']-$report_details[$lastIndex-1]['keyRatioCalculation']['debt_to_equity'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Interest Coverage</td>
                            <td style="padding:7px;text-align:center;">X</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['interest_coverage'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['interest_coverage']-$report_details[$lastIndex-1]['keyRatioCalculation']['interest_coverage'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Repayment Capability</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                        <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['repayment_capability'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['repayment_capability']-$report_details[$lastIndex-1]['keyRatioCalculation']['repayment_capability'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="border-left:#949494 solid 0.5px; border-right: none; padding:7px; text-align: left;"><p class="text-warning"><strong>Liquidity</strong></p></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <td style="border: none"> </td>
                            <td style="border-right: #949494 solid 0.5px;border-left: none"> </td>


                        </tr>
                        <tr>
                            <td style="padding:7px">Working Capital</td>
                            <td style="padding:7px;text-align:center;">Thousands</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['working_capital'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7p;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['working_capital']-$report_details[$lastIndex-1]['keyRatioCalculation']['working_capital'],false)); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Working Capital to Sales</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['working_capital_to_sales'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['working_capital_to_sales']-$report_details[$lastIndex-1]['keyRatioCalculation']['working_capital_to_sales'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Current Ratio</td>
                            <td style="padding:7px;text-align:center;">X</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['current_ratio'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['current_ratio']-$report_details[$lastIndex-1]['keyRatioCalculation']['current_ratio'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Quick Ratio</td>
                            <td style="padding:7px;text-align:center;">X</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['quick_ratio'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['quick_ratio']-$report_details[$lastIndex-1]['keyRatioCalculation']['quick_ratio'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="border-left:#949494 solid 0.5px; border-right: none; padding:6px; text-align: left;"><p class="text-warning"><strong>Asset Turnover</strong></p></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <td style="border: none"> </td>
                            <td style="border-right: #949494 solid 0.5px;border-left: none"> </td>


                        </tr>
                        <tr>
                            <td style="padding:7px">Creditor Days</td>
                            <td style="padding:7px;text-align:center;">Days</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['creditor_days'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['creditor_days']-$report_details[$lastIndex-1]['keyRatioCalculation']['creditor_days'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Inventory Days</td>
                            <td style="padding:7px;text-align:center;">Days</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['inventory_days'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['inventory_days']-$report_details[$lastIndex-1]['keyRatioCalculation']['inventory_days'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Debtor Days</td>
                            <td style="padding:7px;text-align:center;">Days</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['debtor_days'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['debtor_days']-$report_details[$lastIndex-1]['keyRatioCalculation']['debtor_days'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Cash Conversion Cycle</td>
                            <td style="padding:7px;text-align:center;">Days</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                        <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['cash_conversion_cycle'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['cash_conversion_cycle']-$report_details[$lastIndex-1]['keyRatioCalculation']['cash_conversion_cycle'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="border-left:#949494 solid 0.5px; border-right: none; padding:7px; text-align: left;"><p class="text-warning"><strong>Other Ratios</strong></p></td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="border: none"> </td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <td style="border: none"> </td>
                            <td style="border-right: #949494 solid 0.5px;border-left: none"> </td>


                        </tr>
                        <tr>
                            <td style="padding:7px">Sales (Annualised)</td>
                            <td style="padding:7px;text-align:center;">Thousands</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['sales_annualised'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['sales_annualised']-$report_details[$lastIndex-1]['keyRatioCalculation']['sales_annualised'],false)); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Sales Growth</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>
                                <?php if($key==0): ?>
                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;">N/A</td>
                                <?php else: ?>
                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['sales_growth'],2)); ?></td>
                                <?php endif; ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['sales_growth']-$report_details[$lastIndex-1]['keyRatioCalculation']['sales_growth'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Related Party Loans Receivable</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['related_party_loans_receivable'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['related_party_loans_receivable']-$report_details[$lastIndex-1]['keyRatioCalculation']['related_party_loans_receivable'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Related Party Loans Payable</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['related_party_loans_payable'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['related_party_loans_payable']-$report_details[$lastIndex-1]['keyRatioCalculation']['related_party_loans_payable'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td style="padding:7px">Related Party Loans Dependency</td>
                            <td style="padding:7px;text-align:center;">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())==1): ?>
                            <?php $cols=2; ?>
                            <?php else: ?>
                            <?php $cols=1; ?>

                            <?php endif; ?>
                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td colspan="<?php echo e($cols); ?>" style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['related_party_loans_dependency'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td style="padding:7px;text-align:center;"><?php echo e(Helpers::checkPdfRatioValue($report_details[$lastIndex]['keyRatioCalculation']['related_party_loans_dependency']-$report_details[$lastIndex-1]['keyRatioCalculation']['related_party_loans_dependency'])); ?></td>
                            <?php endif; ?>
                        </tr>
                    </table>
                </div>
            </div>
        </page>
        <div class="add-page"></div>
        <page>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-danger">Glossary of Financial Calculations</h3>
                    <table class="tbl-special" style="width:100%;font-size:12px;border-collapse: collapse !important;">
                        <tr class="text-danger">
                            <td class="text-danger" style="padding: 5px;padding-top:9px;padding-bottom:9px;" widtd="37%">Profitability Indicators</td>
                            <td class="text-danger" style="padding: 5px;padding-top:9px;padding-bottom:9px;">Calculation</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Gross Profit Margin</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Gross Profit / Sales shown as a percentage</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">EBITDA</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Earnings Before Interest Depreciation Tax and Amortisation</td>
                        </tr>
                        <!-- <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Normalised EBITDA</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Earnings Before Interest Depreciation Tax Amortisation & Extraordinary Items</td>
                        </tr> -->
                        <!-- <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">EBIT</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Earnings Before Interest and Tax</td>
                        </tr> -->
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Net Profit Margin</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Net Profit Before Tax / Sales shown as a percentage</td>
                        </tr>
                        <!-- <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Profitability</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Annualised Profit after Tax and Dividends / Total Assets shown as a percentage</td>
                        </tr> -->
                        <!-- <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Reinvestment</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Retained Earnings / Total Assets shown as a percentage</td>
                        </tr> -->
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Return on Assets</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Annualised Profit before Interest and Tax / Total Assets shown as a percentage</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Return on Equity</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Annualised Profit after Tax / Shareholders Equity shown as a percentage</td>
                        </tr>
                        <tr class="text-danger">
                            <td class="text-danger" style="padding: 5px;padding-top:9px;padding-bottom:9px;">Financial Leverage</td>
                            <td class="text-danger" style="padding: 5px;padding-top:9px;padding-bottom:9px;"></td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Net Tangible Worth</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Total Net Assets - Intangibles</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Gearing</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Total Liabilities / Total Assets shown as a percentage</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Debt to Equity</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Total Debt / Shareholders Equity</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Interest Coverage</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Profit before Tax and Interest Expense / Interest Expense</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Repayment Capability</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Annualised Profit before Tax / Total Liabilities shown as a percentage</td>
                        </tr>

                        <tr class="text-danger">
                            <td class="text-danger" style="padding: 5px;padding-top:9px;padding-bottom:9px;">Liquidity</td>
                            <td class="text-danger" style="padding: 5px;padding-top:9px;padding-bottom:9px;"></td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Working Capital</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Current Assets - Current Liabilities</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Working Capital to Sales</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Working Capital / Annualised Sales shown as a percentage</td>
                        </tr>
                        <!-- <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Cash Flow Coverage</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Annualised Operating Cash Flow / Current Liabilities shown as a percentage</td>
                        </tr>   -->
                        <!-- <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Cash Ratio</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Cash/Current Liabilities shown as a percentage</td>
                        </tr>     -->
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Current Ratio</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Current Assets / Current Liabilities</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Quick Ratio</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">(Current Assets - Inventories) / Current Liabilities</td>
                        </tr>

                        <tr class="text-danger">
                            <td class="text-danger" style="padding: 5px;padding-top:9px;padding-bottom:9px;">Asset Turnover</td>
                            <td class="text-danger" style="padding: 5px;padding-top:9px;padding-bottom:9px;"></td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Creditor Days</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">(Trade Creditors / Annualised Cost of Goods Sold) x 365 days</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Inventory Days</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">(Inventories / Annualised Cost of Goods Sold) x 365 Days</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Debtor Days</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">(Trade Debtors / Annualised Sales) x 365 days</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Cash Conversion Cycle</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Debtor Days + Inventory Days - Creditor Days</td>
                        </tr>
                        <tr class="text-danger">
                            <td class="text-danger" style="padding: 5px;padding-top:9px;padding-bottom:9px;">Other Indicators</td>
                            <td class="text-danger" style="padding: 5px;padding-top:9px;padding-bottom:9px;"></td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Sales (Annualised)</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Annualised Sales Revenue</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Sales Growth</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Percentage Change in Sales</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Related Party Loans Receivable</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Current Plus Non-Current Loans Owing by Related Parties as a % of Total Assets</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Related Party Loans Payable</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Current Plus Non-Current Loans Owing to Related Parties as a % of Total Liabilities</td>
                        </tr>
                        <tr>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Related Party Loans Dependency</td>
                            <td style="padding: 5px;padding-top:9px;padding-bottom:9px;">Related Party Loans Payable / Working Capital shown as a percentage</td>
                        </tr>

                    </table>
                </div>
            </div>
        </page>
        <div class="add-page"></div>
        <page>
            <h3 class="text-danger">Disclaimer</h3>
            <p style="text-align: justify;font-size:12px;">
                CreditorWatch is committed to ensuring that the information provided is accurate and comprehensive however due to data being received from sources not controlled by CreditorWatch we cannot guarantee that it is complete, verified or free of errors. The information should therefore be used in conjunction with your own investigations and you should not rely solely on this information when making credit or financial decisions. To the extent permitted by law, CreditorWatch will not be held responsible for any errors or omissions therein concerning the information sourced and published in its publications, websites, API or emails.
            </p>
        </page>

    </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.print-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/reports/pdf/report.blade.php ENDPATH**/ ?>