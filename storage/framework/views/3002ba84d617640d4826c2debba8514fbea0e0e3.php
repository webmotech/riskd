<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-12">
                            <h1 class=" text-center">Welcome to Risk D</h1>
                            <p class="text-center alert alert-success">Version 1.1</p>
                            <div id="visitfromworld" style="width:100%; height:350px"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>

    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/dashboard/index.blade.php ENDPATH**/ ?>