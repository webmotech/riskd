<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- <html> -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        body {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            margin: 0 !important;
            width: 100% !important;
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
            font-family: sans-serif;
        }

    </style>
</head>

<body>
<!--[if mso]>
<center>
    <table><tr><td width="750">
<![endif]-->
<div style="max-width:700px;margin:0px auto;background-color: #f7f7f7;">
    <div class="mHeader" style="padding:5px;text-align: center;border-bottom:1px solid #e0e0e0;">
        <img style="margin:20px auto; max-width:300px;"  class="mLogo" src="https://thumbs2.imgbox.com/7f/ae/hFnlc9vi_t.png" alt="Risk d" title="Risk d" border="0">
    </div>
    <div class="mBody" style="min-height: 250px;padding: 10px 20px;font-size: 14px;">


        <div #invoice id="invoice-template" style="font-size: 13px;font-family: sans-serif;padding: 15px 20px;border: 2px solid #e0e0e0;border-style: dashed;margin: 15px auto;">
            <div style="text-align:left;float:left;display:inline-block;">
                <div style="color:#8b0510;font-size: 25px;font-weight: 700;">Report Generation Notification </div>
            </div>



            <div style="margin-top: 60px;">



                <table style="width:100%">

                    <tr>
                        <td style="width:300px;padding-top: 5px;">
                            <p>Hi <?php echo e(Auth::user()->fname); ?>,<br><br> Your report generation request has been processed. you can view the report which you have generated using below url.
                                <br>
Report Url  - <a href="<?php echo e($url); ?>">View Report</a>
                            </p>
                            <p>RiskD</p>
                            <p>PLEASE NOTE: This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. E-mail transmission cannot be guaranteed to be secure or error-free as information could be intercepted, corrupted, lost, destroyed, arrive late or incomplete, or contain viruses. The sender therefore does not accept liability for any errors or omissions in the contents of this message, which arise as a result of e-mail transmission. </p>
                        </td>

                    </tr>



                </table>




            </div>

        </div>

    </div>
    <div class="mFooter" style="border-top:1px solid #e0e0e0;color:#616161;font-size:12px;">

        <div style="width:100%;text-align:center;opacity:0.5;padding:10px;">Powered by Risk D</div>
    </div>
</div>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->
</body>

</html>
<?php /**PATH D:\xampp_new\htdocs\riskd\resources\views/admin/email/report-notification.blade.php ENDPATH**/ ?>