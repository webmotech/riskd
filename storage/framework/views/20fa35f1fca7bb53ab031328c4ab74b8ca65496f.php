<div class="modal fade" id="report-generate-modal" role="dialog">

    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                <h5 class="modal-title text-center">Generating Report.....</h5>
            </div>
            <div id="demo-modal">
                <p class="text-center">
                    <img id="loader-img" style="height:50%" src="<?php echo e(asset('assets/img/loader.gif')); ?>"/>

                </p>

                <div id="result" class="col-md-12">

                    <p class="text-center warning-msg text-danger">Report being generated. Please do not close the browser.</p>


                </div>
                <div id="result-error" class="col-md-12">

                    <div class="">
                        <p id="error-msg" style="display: none" class="alert alert-danger text-center" ></p>
                        <p id="success-msg" style="display: none" class="alert alert-success text-center" ></p>
                      <p id="viewbtn" class="text-center"> </p>


                    </div>


                </div>
            </div>
            <div class="modal-footer">
               <button style="display:none" id="close-btn" type="button" class="btn btn-default"
                        data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/reports/component/report-generating.blade.php ENDPATH**/ ?>