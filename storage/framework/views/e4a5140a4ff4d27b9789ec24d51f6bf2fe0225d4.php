<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Risk-D</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/bootstrap/dist/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/fontawesome-free/css/all.min.cs')); ?>s">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/icon-kit/dist/css/iconkit.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/ionicons/dist/css/ionicons.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/perfect-scrollbar/css/perfect-scrollbar.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/jvectormap/jquery-jvectormap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/weather-icons/css/weather-icons.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/c3/c3.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/owl.carousel/dist/assets/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/owl.carousel/dist/assets/owl.theme.default.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/dist/css/theme.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/font-awesome-animation.min.css')); ?>">

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/af-2.3.4/b-1.6.1/datatables.min.css"/>
    <script src="<?php echo e(asset('theme/src/js/vendor/modernizr-2.8.3.min.js')); ?>"></script>



    <link rel="stylesheet" href="<?php echo e(asset('assets/css/animate.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/jquery-confirm.min.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/parsley.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(asset('assets/select2/css/select2.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(asset('assets/select2/css/select2-bootstrap4.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(asset('assets/zebra/css/bootstrap/zebra_datepicker.css')); ?>"/>
    <style type="text/css">
        .sel2 .parsley-errors-list.filled {
            margin-top: 42px;
            margin-bottom: -56px;
        }

        .sel2 .parsley-errors-list:not(.filled) {
            display: none;

        }

        .sel2 .parsley-errors-list.filled + span.select2 {
            /*margin-bottom: 30px;*/
        }

        .sel2 .parsley-errors-list.filled + span.select2 span.select2-selection--single {
            background: #FAEDEC !important;
            border: 1px solid #E85445;
        }
        .parsley-errors-list li{
            line-height: 15px;
        }
    </style>

    <?php echo $__env->yieldContent('custom-css'); ?>
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="wrapper">
    <header class="header-top" header-theme="dark">
        <div class="container-fluid">
            <div class="d-flex justify-content-between">
                <div class="top-menu d-flex align-items-center">
                    <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>

                    <button type="button" id="navbar-fullscreen" class="nav-link"><i class="ik ik-maximize"></i></button>
                </div>
                <div class="top-menu d-flex align-items-center">

                    <div class="dropdown">
                        <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="avatar" src="<?php echo e(asset('assets/img/user.png')); ?>" alt=""></a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">


                            <a class="dropdown-item" href="<?php echo e(url('admin/logout')); ?>"><i class="ik ik-power dropdown-icon"></i> Logout</a>
                            <a class="dropdown-item" onclick="loadResetPasswordModal()" href="#"><i class="ik ik-user dropdown-icon"></i> Reset Password</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>

    <div class="page-wrap">
        <div class="app-sidebar colored">
            <div class="sidebar-header">
                <a class="header-brand" href="#">
                    <div class="logo-img pt-1 ml-4">
                        <img style="max-width: 100px;" src="<?php echo e(asset('assets/img/logo.png')); ?>" class="img-responsive mt-1"/>
                    </div>

                </a>
                <button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
                <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
            </div>

            <div class="sidebar-content">
                <div class="nav-container">
                    <nav id="main-menu-navigation" class="navigation-main">
                        <div class="nav-lavel">Hi <?php echo e(Auth::user()->fname); ?>,</div>
                        <?php echo $__env->make(Auth::user()->userRole->template, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



                    </nav>
                </div>
            </div>
        </div>
        <div class="main-content">
            <div class="container-fluid">

               <?php echo $__env->yieldContent('content'); ?>



            </div>
        </div>



        <div class="chat-panel" hidden>
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <a href="javascript:void(0);"><i class="ik ik-message-square text-success"></i></a>
                    <span class="user-name">John Doe</span>
                    <button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="card-body">
                    <div class="widget-chat-activity flex-1">
                        <div class="messages">
                            <div class="message media reply">
                                <figure class="user--online">
                                    <a href="#">

                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>Epic Cheeseburgers come in all kind of styles.</p>
                                </div>
                            </div>
                            <div class="message media">
                                <figure class="user--online">
                                    <a href="#">
                                        <img src="" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>Cheeseburgers make your knees weak.</p>
                                </div>
                            </div>
                            <div class="message media reply">
                                <figure class="user--offline">
                                    <a href="#">
                                        <img src="" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>Cheeseburgers will never let you down.</p>
                                    <p>They'll also never run around or desert you.</p>
                                </div>
                            </div>
                            <div class="message media">
                                <figure class="user--online">
                                    <a href="#">
                                        <img src="" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>A great cheeseburger is a gastronomical event.</p>
                                </div>
                            </div>
                            <div class="message media reply">
                                <figure class="user--busy">
                                    <a href="#">
                                        <img src="" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>There's a cheesy incarnation waiting for you no matter what you palete preferences are.</p>
                                </div>
                            </div>
                            <div class="message media">
                                <figure class="user--online">
                                    <a href="#">
                                        <img src="" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>If you are a vegan, we are sorry for you loss.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="javascript:void(0)" class="card-footer" method="post">
                    <div class="d-flex justify-content-end">
                        <textarea class="border-0 flex-1" rows="1" placeholder="Type your message here"></textarea>
                        <button class="btn btn-icon" type="submit"><i class="ik ik-arrow-right text-success"></i></button>
                    </div>
                </form>
            </div>
        </div>

        <footer class="footer">
            <div class="w-100 clearfix">
                <span class="text-center text-sm-left d-md-inline-block">Copyright © 2020 Risk D.</span>

            </div>
        </footer>

    </div>
</div>

<?php echo $__env->make('admin.reports.component.company-select-modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.reports.component.clone-modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.reports.component.prosess-clone-modal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('admin.users.form.reset-user-password', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script src="<?php echo e(asset('theme/plugins/popper.js/dist/umd/popper.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/screenfull/dist/screenfull.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/datatables.net-responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')); ?>"></script>

<script src="<?php echo e(asset('theme/plugins/moment/moment.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/d3/dist/d3.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/c3/c3.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/js/tables.js')); ?>"></script>

<script src="<?php echo e(asset('theme/js/charts.js')); ?>"></script>
<script src="<?php echo e(asset('theme/dist/js/theme.min.js')); ?>"></script>

<script src="<?php echo e(asset('assets/js/bootstrap-notify.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/notification.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/jquery-confirm.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/bootstrap-datepicker.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/parsley.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/select2/js/select2.js')); ?>"></script>
<script src="<?php echo e(asset('assets/zebra/zebra_datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/common.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/jquery.cookie.js')); ?>"></script>
<script src="<?php echo e(asset('assets/modules/js/reports/clone-report.js')); ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>



<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/af-2.3.4/b-1.6.1/datatables.min.js"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script type="text/javascript">
    var base_url = '<?php echo e(url('')); ?>';
    var userRole = '<?php echo e(Auth::user()->user_role_id); ?>';

    function loadResetPasswordModal()
    {
        $('#reset-user-password').modal('toggle');
    }

    $('#frmresetuserpassword').submit(function(e){
        if ($(this).parsley().isValid() ) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: base_url + '/admin/user/reset-user-password',
                data: $('#frmresetuserpassword').serialize(),
                success: function (res) {
                    if (res.success) {
                        $('#reset-user-password').modal('toggle');
                        successNotify(res.msg);

                    } else {
                        failedNotify(res.msg);
                    }
                }
            });
        }
    })

</script>
<?php echo $__env->yieldContent('custom-js'); ?>
</body>
</html>
<?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/layout/master.blade.php ENDPATH**/ ?>