<div class="modal fade" id="process-clone-modal" role="dialog">

    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">×</button> -->
                <h5 class="modal-title text-center">Clonning Report.....</h5>
            </div>
            <div id="demo-modal">
                <div id="result" class="col-md-12">

                    <p class="text-center warning-msg text-danger">Processing. Do not close the browser</p>


                </div>
                <p class="text-center">
                    <img id="loader-img" style="height:50%" src="<?php echo e(asset('assets/img/process.gif')); ?>"/>

                </p>


                <div id="result-error" class="col-md-12">

                    <div class="">
                        <p id="clone-error-msg" style="display: none" class="alert alert-danger text-center" ></p>
                        <p id="clone-success-msg" style="display: none" class="alert alert-success text-center" ></p>
                      <p id="clone-viewbtn" class="text-center"> </p>


                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default"
                        data-dismiss="modal">Close</button> -->
            </div>
        </div>
    </div>
</div>
<?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/reports/component/prosess-clone-modal.blade.php ENDPATH**/ ?>