<?php $__env->startSection('custom-css'); ?>
    <style type="text/css">
        .previous .table td, tr .table th{
            padding: 0.7em !important;
            width: 100%;
        }
        #input-area select {
            height: 33px;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class=" bg-primary text-light p-1">
                    <h1 class="text-center"> New Report Creation</h1>

                </div>
                <form id="frmreportEdit" data-parsley-validate="">
                    <?php echo e(csrf_field()); ?>

                <input type="hidden" name="hdncompanyToken" value="<?php echo e($report_data['report']->company->company_token); ?>" id="hdncompanyToken"/>
                    <input type="hidden" name="hdnreportToken" value="<?php echo e($report_data['report']['report_token']); ?>" id="hdnreportToken"/>
                    <div class="card-body">
                        <p class="text-right"><span id="validate-year" class="alert alert-danger" style="display: none">You Can add Maximum 3 years</span> &nbsp;<button type="button" onclick="createYear()" class="btn btn-success"><i class="fa fa-plus"></i>Add Year</button></p>
                        <div class="row">
                            <div class="col-md-5 p-0"></div>
                            <div class="col-md-7 p-0">
                                <div class=" form-group">
                                    <select required="" name="report_type" id="report_type" class="form-control">
                                        <option value="">Select Report Type</option>
                                        <?php $__currentLoopData = config('reports_config.report_type'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$reportType): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($report_data['report']['report_type']==$key): ?>
                                                <option selected value="<?php echo e($key); ?>"><?php echo e($reportType); ?></option>
                                                <?php else: ?>
                                            <option value="<?php echo e($key); ?>"><?php echo e($reportType); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7 table-responsive text-nowrap col-sm-7 col-lg-7 previous">

                                <table class="table table-bordered">
                                    <tr class="p-0 border-0">
                                        <td class="p-1 text-light border-0" colspan="<?php echo e($no_of_years+1); ?>">
                                            <strong>&nbsp;</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">ABN</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e($report['abn']); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">ACN</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e($report['acn']); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Company</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e($report->company->name); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Rounding</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(config('reports_config.rounding')[$previousData['rounding']]); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Base Currency</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e($previousData['base_currency']); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Quality</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(config('reports_config.quality')[$previousData['quality']]); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Reporting Period (months)</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e($previousData['report_period_months']); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Scope</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e($previousData['scope']); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Confidentiality Record</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e($previousData->confidentiality['name']); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Financial Year</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e($previousData['financial_year']); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Month</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e($previousData['financial_month']); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                            <strong>Income Statement</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Sales</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['sales'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Cost of sales</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['cost_of_sales'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Gross profit</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['gross_profit'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other income</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['other_income'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Depreciation</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['depreciation'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Amortisation</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['amortisation'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Impairment</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['impairment'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Interest expense gross</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['interest_expenses_gross'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Operating lease expense</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['operating_lease_expenses'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Finance lease hire purchase charges</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['finance_lease_hire_expenses_charges'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Non-recurring gains (losses)</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['non_recurring_gain_loses'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other gains (losses)</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['other_gain_loses'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other expenses</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['other_expenses'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">EBIT</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['ebit'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">EBITDA</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['ebitda'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Normalised EBITDA</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['normalized_ebitda'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Profit before tax</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['profit_before_tax'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Profit before tax after abnormals</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['profit_before_tax_after_abnormals'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Tax benefit (expense)</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['tax_benefit_expenses'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Profit after tax</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['profit_after_tax'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Distribution or dividends</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['distribution_ordividends'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other post tax items - gains/ (losses)</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['other_post_tax_items_gains_losses'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Profit after tax distribution</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['profit_after_tax_distribution'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                            <strong>Balance Sheet Assets</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                            Assets
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Cash</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['cash'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Trade debtors</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['trade_debtors'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total inventories</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['total_inventories'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Loans to related parties</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['loan_to_related_parties_1'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other current assets</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['other_current_assets'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total current assets</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['total_current_assets'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Fixed assets</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['fixed_assets'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Net intangibles</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['net_intangibles'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Loan to related parties</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['loan_to_related_parties_2'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other non-current assets</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['other_non_current_assets'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total non-curent assets</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['total_non_current_assets'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total assets</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['total_assets'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                            <strong>Liabilities</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Trade creditors</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['trade_creditors'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Interest bearing debt</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['interest_bearing_debt_1'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Loan from related parties</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['loan_from_related_parties_1'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other current liabilities</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['other_current_liabilities'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total current liabilities</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['total_current_liabilities'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Interest bearing debt</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['interest_bearing_debt_2'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Loans from related parties</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['loan_from_related_parties_2'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other non-current liabilities</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['other_non_current_liabilities'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total non-current liabilities</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['total_non_current_liabilities'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total liabilities</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['total_liabilities'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                            <strong>Equity</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Share capital</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['share_capital'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Preference shares</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['prefence_shares'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Treasury shares</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['threasury_shares'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Equity ownerships</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['equity_ownerships'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total reserves</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['total_reserves'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Retained earnings</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['retained_earning'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Minorty interest</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['minorty_interest'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total equity</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['total_equity'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Balance</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['balance'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>

                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                            <strong>Additional Information</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Operating cash flow</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['operating_cash_flow'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Contingent liabilities</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['contingent_liabilities'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other commitments</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['other_commitmentes'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Operating lease outstandings</td>
                                        <?php if($no_of_years > 0): ?>
                                            <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <td><?php echo e(number_format($previousData['operating_lease_outstanding'])); ?></td>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tr>

                                </table>

                            </div>

                            <div class="col-md-5 col-sm-5 col-lg-5 pl-1">


                                <div class="row" id="input-area">
                                    <?php
                                        $tabIndex = 1;
                                        $subIndex = 0.1;
                                    ?>

                                    <?php $__currentLoopData = $report_data['details']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col p-0" id="coloumn_<?php echo e($key); ?>">
                                        <table class="table table-bordered">
                                            <tr>
                                        <td class="p-0" style="padding: 0.2em !important;"><button onclick="removeMe(<?php echo e($key); ?>)" type="button" class="btn btn-danger btn-block"><i class="fa fa-trash"></i></i>Remove</button> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e($report_data['report']['abn']); ?>" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" readonly class="form-control" name="data[<?php echo e($key); ?>][abn]" id="abn_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e($report_data['report']['acn']); ?>"  tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" readonly class="form-control" name="data[<?php echo e($key); ?>][acn]" id="acn_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>

                                                <td class="p-0"><select required="" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" readonly class="form-control" name="data[<?php echo e($key); ?>][company]" id="company_<?php echo e($key); ?>">
                                                        <?php $__currentLoopData = $company; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $companyData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($report_data['report']->company->id==$companyData['id']): ?>
                                                                <option selected value="<?php echo e($companyData['id']); ?>"><?php echo e($companyData['name']); ?></option>
                                                                <?php else: ?>
                                                            <option value="<?php echo e($companyData['id']); ?>"><?php echo e($companyData['name']); ?></option>
                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select required="" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][rounding]" id="rounding_<?php echo e($key); ?>">
                                                        <?php $__currentLoopData = config('reports_config.rounding'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $roundingKey => $rounding): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($details['rounding']==$roundingKey): ?>
                                                                <option selected value="<?php echo e($roundingKey); ?>"><?php echo e($rounding); ?></option>
                                                                <?php else: ?>
                                                            <option value="<?php echo e($roundingKey); ?>"><?php echo e($rounding); ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][base_currency]" id="base_currency_<?php echo e($key); ?>">
                                                        <?php $__currentLoopData = config('reports_config.base_currency'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $currencyKey => $currency): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($details['base_currency']==$currencyKey): ?>
                                                                <option selected value="<?php echo e($currencyKey); ?>"><?php echo e($currency); ?></option>
                                                                <?php else: ?>
                                                            <option value="<?php echo e($currencyKey); ?>"><?php echo e($currency); ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][quality]" id="quality_<?php echo e($key); ?>">
                                                        <?php $__currentLoopData = config('reports_config.quality'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $qualityKey => $quality): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($details['quality']==$qualityKey): ?>
                                                                <option selected value="<?php echo e($qualityKey); ?>"><?php echo e($quality); ?></option>
                                                                <?php else: ?>
                                                            <option value="<?php echo e($qualityKey); ?>"><?php echo e($quality); ?></option>
                                                            <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select required="" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][reporting_period]" id="reporting_period_<?php echo e($key); ?>">

                                                        <?php for($reportingPeriodIndex = 1; $reportingPeriodIndex<=24; $reportingPeriodIndex++): ?>
                                                            <?php if($details['report_period_months']==$reportingPeriodIndex): ?>
                                                                <option selected value="<?php echo e($reportingPeriodIndex); ?>"><?php echo e($reportingPeriodIndex); ?></option>
                                                            <?php else: ?>
                                                                <option value="<?php echo e($reportingPeriodIndex); ?>"><?php echo e($reportingPeriodIndex); ?></option>
                                                            <?php endif; ?>
                                                        <?php endfor; ?>
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][scope]" id="scope_<?php echo e($key); ?>">
                                                        <?php $__currentLoopData = config('reports_config.scope'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $scopeKey => $scope): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($details['scope']==$scopeKey): ?>
                                                                <option selected value="<?php echo e($scopeKey); ?>"><?php echo e($scope); ?></option>
                                                                <?php else: ?>
                                                            <option value="<?php echo e($scopeKey); ?>"><?php echo e($scope); ?></option>
                                                            <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][confidentiality_record]" id="confidentiality_record_<?php echo e($key); ?>">

                                                        <?php $__currentLoopData = $confidentiality; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $confident): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($details['confidentiality_id']==$confident['id']): ?>
                                                                <option selected value="<?php echo e($confident['id']); ?>"><?php echo e($confident['name']); ?></option>
                                                            <?php else: ?>
                                                            <option value="<?php echo e($confident['id']); ?>"><?php echo e($confident['name']); ?></option>
                                                            <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select required=""  tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][financial_year]" id="financial_year_<?php echo e($key); ?>">
                                                        <?php for($yearIndex = 2000; $yearIndex<=2050; $yearIndex++): ?>
                                                            <?php if($details['financial_year']==$yearIndex): ?>
                                                                <option selected value="<?php echo e($yearIndex); ?>">FY<?php echo e($yearIndex); ?></option>
                                                                <?php else: ?>
                                                            <option value="<?php echo e($yearIndex); ?>">FY<?php echo e($yearIndex); ?></option>
                                                            <?php endif; ?>
                                                            <?php endfor; ?>
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select required="" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][month]" id="month_<?php echo e($key); ?>">
                                                        <?php $__currentLoopData = config('reports_config.months'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $monthKey => $month): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if($details['financial_month']==$monthKey): ?>
                                                                <option selected value="<?php echo e($monthKey); ?>"><?php echo e($month); ?></option>
                                                            <?php else: ?>
                                                            <option value="<?php echo e($monthKey); ?>"><?php echo e($month); ?></option>
                                                            <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Income Statement</td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" onchange="calculateGrossProfit(<?php echo e($key); ?>)" class="form-control" value="<?php echo e(round($details['sales'])); ?>" name="data[<?php echo e($key); ?>][sales]" id="sales_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['cost_of_sales'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" value="0" onchange="calculateGrossProfit(<?php echo e($key); ?>)" class="form-control" name="data[<?php echo e($key); ?>][cost_of_sales]" id="cost_of_sales_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['gross_profit'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" readonly class="form-control" name="data[<?php echo e($key); ?>][gross_profit]" onblur="blurGrossProfit(<?php echo e($key); ?>)" id="gross_profit_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['other_income'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][other_income]" onchange="changeOtherIncome(<?php echo e($key); ?>)" id="other_income_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['depreciation'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][depreciation]" onchange="changeDepreciation(<?php echo e($key); ?>)" id="depreciation_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['amortisation'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][amortisation]" onchange="changeAmortisation(<?php echo e($key); ?>)" id="amortisation_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['impairment'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][impairment]" onchange="changeImpairment(<?php echo e($key); ?>)" id="impairment_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['interest_expenses_gross'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" onchange="changeInterestExpense(<?php echo e($key); ?>)" name="data[<?php echo e($key); ?>][interest_expense_gross]" id="interest_expense_gross_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['operating_lease_expenses'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][operating_lease_expense]" onchange="changeOperatingLeaseExpense(<?php echo e($key); ?>)" id="operating_lease_expense_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['finance_lease_hire_expenses_charges'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][finance_lease_hire_purchase_charges]" onchange="changeFinanceLeaseHirePurchaseCharges(<?php echo e($key); ?>)" id="finance_lease_hire_purchase_charges_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['non_recurring_gain_loses'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][non_recurring_gains_losses]" onchange="changeNonRecurrentGainLosses(<?php echo e($key); ?>)" id="non_recurring_gains_losses_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['other_gain_loses'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][other_gains_losses]" onchange="changeOtherGainLosses(<?php echo e($key); ?>)" id="other_gains_losses_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['other_expenses'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][other_expenses]" onchange="changeOtherExpense(<?php echo e($key); ?>)" id="other_expenses_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['ebit'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" readonly class="form-control" name="data[<?php echo e($key); ?>][ebit]" id="ebit_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['ebitda'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" readonly class="form-control" name="data[<?php echo e($key); ?>][ebitda]" id="ebitda_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['normalized_ebitda'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" readonly class="form-control" name="data[<?php echo e($key); ?>][normalized_ebitda]" id="normalized_ebitda_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['profit_before_tax'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" readonly class="form-control" name="data[<?php echo e($key); ?>][profit_before_tax]" id="profit_before_tax_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['profit_before_tax_after_abnormals'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" readonly class="form-control" name="data[<?php echo e($key); ?>][profit_before_tax_after_abnormals]" onblur="blurProfitBeforeTaxAfterAbnormal(<?php echo e($key); ?>)" id="profit_before_tax_after_abnormals_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['tax_benefit_expenses'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][tax_benefit_expense]" onchange="changeTaxBenefitExpense(<?php echo e($key); ?>)"  id="tax_benefit_expense_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['profit_after_tax'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][profit_after_tax]" readonly onblur="changeProfitAfterTax(<?php echo e($key); ?>)" id="profit_after_tax_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['distribution_ordividends'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][distribution_or_dividends]" onchange="changeDistributionOrDividends(<?php echo e($key); ?>)" id="distribution_or_dividends_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['other_post_tax_items_gains_losses'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][other_post_tax_items_gains_losses]" onchange="changeOtherPostTaxItemsGainsLosses(<?php echo e($key); ?>)" id="other_post_tax_items_gains_losses_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['profit_after_tax_distribution'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][profit_after_tax_distributions]" readonly id="profit_after_tax_distributions_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Balance Sheet Assets</td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Assets</td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['cash'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][cash]" onchange="changeCash(<?php echo e($key); ?>)" id="cash_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['trade_debtors'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][trade_debtors]" onchange="changeTradeDebtors(<?php echo e($key); ?>)" id="trade_debtors_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['total_inventories'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][total_inventories]" onchange="changetotalInventories(<?php echo e($key); ?>)" id="total_inventories_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['loan_to_related_parties_1'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][loans_to_related_parties]" onchange="changeLoanToRelatedParties1(<?php echo e($key); ?>)" id="loans_to_related_parties_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['other_current_assets'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][other_current_assets]" onchange="changeOtherCurrentAssets(<?php echo e($key); ?>)" id="other_current_assets_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['total_current_assets'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][total_current_assets]" readonly onblur="blurTotalCurrentAssets(<?php echo e($key); ?>)" id="total_current_assets_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['fixed_assets'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][fixed_assets]" onchange="changeFixedAssets(<?php echo e($key); ?>)" id="fixed_assets_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['net_intangibles'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][net_intangibles]" onchange="changeNetIntangibles(<?php echo e($key); ?>)" id="net_intangibles_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['loan_to_related_parties_2'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][loan_to_related_parties2]" onchange="changeLoanToRelatedParties2(<?php echo e($key); ?>)" id="loan_to_related_parties2_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['other_non_current_assets'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][other_non_current_assets]" onchange="changeOtherNonCurrentAssets(<?php echo e($key); ?>)" id="other_non_current_assets_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['total_non_current_assets'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][total_non_current_assets]" readonly onblur="blurTotalNonCurrentAssets(<?php echo e($key); ?>)" id="total_non_current_assets_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['total_assets'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][total_assets]" readonly onblur="blurTotalAssets(<?php echo e($key); ?>)" id="total_assets_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Liabilities</td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['trade_creditors'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][total_creditors]" onchange="changeTradeCreditors(<?php echo e($key); ?>)" id="total_creditors_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['interest_bearing_debt_1'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][interes_bearing_debt]" onchange="changeInterestBearingDebt1(<?php echo e($key); ?>)" id="interes_bearing_debt_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['loan_from_related_parties_1'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][loan_from_related_parties]" onchange="changeLoanFromRelatedParties1(<?php echo e($key); ?>)" id="loan_from_related_parties_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['other_current_liabilities'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][other_current_liabilities]" onchange="changeOtherCurrentLiabilities(<?php echo e($key); ?>)" id="other_current_liabilities_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['total_current_liabilities'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][total_current_liabilities]" readonly onblur="blurTotalCurrentLiabilities(<?php echo e($key); ?>)" id="total_current_liabilities_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['interest_bearing_debt_2'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][interest_bearing_debt2]" onchange="changeInterestBearingDebt2(<?php echo e($key); ?>)" id="interest_bearing_debt2_<?php echo e($key); ?>"/> </td>
                                            </tr>

                                            <td class="p-0"><input type="text" value="<?php echo e(round($details['loan_from_related_parties_2'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][loan_from_related_parties_2]" onchange="changeLoanFromRelatedParties2(<?php echo e($key); ?>)" id="loan_from_related_parties_2_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['other_non_current_liabilities'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][other_non_current_liabilities]" onchange="changeOtherNonCurrentLiabilities(<?php echo e($key); ?>)" id="other_non_current_liabilities_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['total_non_current_liabilities'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][total_non_current_liabilities]" readonly onblur="blurTotalNonCurrentLiabilities(<?php echo e($key); ?>)" id="total_non_current_liabilities_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['total_liabilities'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][total_liabilities]" readonly onblur="blurTotalLiabilities(<?php echo e($key); ?>)" id="total_liabilities_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Equity</td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['share_capital'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][share_capital]" onchange="changeShareCapital(<?php echo e($key); ?>)" id="share_capital_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['prefence_shares'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][prefence_shares]" onchange="changePrefenceShares(<?php echo e($key); ?>)" id="prefence_shares_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['threasury_shares'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][treasury_shares]" onchange="changeTreasuryShares(<?php echo e($key); ?>)" id="treasury_shares_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['equity_ownerships'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][equity_ownerships]" readonly onblur="blurEquityOwnerships(<?php echo e($key); ?>)" id="equity_ownerships_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['total_reserves'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][total_reserves]" onchange="changeTotalReserves(<?php echo e($key); ?>)" id="total_reserves_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['retained_earning'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][retained_earnings]" onchange="changeRetainedEarning(<?php echo e($key); ?>)" id="retained_earnings_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['minorty_interest'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][minorty_interest]" onchange="changeMinortyInterest(<?php echo e($key); ?>)" id="minorty_interest_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['total_equity'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][total_equity]" readonly onblur="blurTotalEquity(<?php echo e($key); ?>)" id="total_equity_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['balance'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][balance]" readonly id="balance_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Additional Information</td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['operating_cash_flow'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][operating_cash_flow]" id="operating_cash_flow_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['contingent_liabilities'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][contingent_liabilities]" id="contingent_liabilities_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['other_commitmentes'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][other_commiments]" id="other_commiments_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="<?php echo e(round($details['operating_lease_outstanding'])); ?>" data-parsley-type="number" tabindex="<?php echo e(($tabIndex++)+$subIndex); ?>" class="form-control" name="data[<?php echo e($key); ?>][operating_lease_outstandings]" id="operating_lease_outstandings_<?php echo e($key); ?>"/> </td>
                                            </tr>
                                        </table>
                                    </div>

                                        <?php
                                            $tabIndex = 1;
                                            $subIndex += 0.1;
                                        ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <input type="hidden" id="nextcoloumn" value="<?php echo e($key); ?>"/>
                                <input type="hidden" id="hdnyears" value="<?php echo e($subIndex); ?>"/>


                                </div>


                            </div>

                        </div>

                        <div class="row">
                            <hr>
                            <div class="col-md-12" id="summery-area">
                                



                            <?php $__currentLoopData = $report_data['report']->executiveSummery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$summery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="form-group">
                                        <label>Financial Analysis</label>
                                        <textarea name="exective_summery[<?php echo e($key); ?>][summery]" required id="executive_summery<?php echo e($key); ?>" class="form-control"><?php echo e($summery->summery); ?></textarea>
                                       </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <input type="hidden" id="summeryindex" value="<?php echo e($key); ?>"/>
                                <input type="hidden" id="summeryCount" value="<?php echo e(count($report_data['report']->executiveSummery)); ?>"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-right">
                                    <a href="<?php echo e(url()->previous()); ?>" class="btn btn-warning">Back</a>
                                    <?php if($report_data['report']['report_status']==0): ?>
                                        <button type="button" id="savebtn" class="btn btn-success">Save</button>
                                        <?php else: ?>
                                         <button type="button" id="savebtn" class="btn btn-success">Save and Send for Approval</button>
                                        <?php endif; ?>

                                </p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
    <script type="text/javascript" src="<?php echo e(asset('assets/modules/js/reports/edit-report.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/modules/js/reports/report-calculation.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/reports/edit.blade.php ENDPATH**/ ?>