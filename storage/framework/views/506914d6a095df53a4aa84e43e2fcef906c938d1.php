<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="p-1">
                    <h1 class="text-center"> Pending Approval</h1>

                </div>

                <div class="card-body">


                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="">
                                        <div class="col-lg-12 col-md-12">
                                            <h3 class="card-title">Pending Approval</h3>
                                            <p class="text-right">


                                            </p>
                                            <table id="pending-report-table" class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Company</th>
                                                    <th>Report Type</th>
                                                    <th>Status</th>
                                                    <th>Created by</th>
                                                    <th>Created at</th>
                                                    <th>Action</th>


                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Company</th>
                                                    <th>Report Type</th>
                                                    <th>Status</th>
                                                    <th>Created by</th>
                                                    <th>Created at</th>
                                                    <th>Action</th>


                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>



                    </div>

                </div>

            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#pending-report-table').DataTable({

            ajax: {
                "url": "/admin/reports/list/get-by-status",
                "async": true,
                "data": {status: [0]},
            },
            "datatype": "json",
            "order": [[ 0, "desc" ]],
            "columns": [
                { "data": "id" },
                { "data": "company" },
                { "data": "report_type" },
                { "data": "report_status" },
                { "data": "created_by" },
                { "data": "created_at" },
                {
                    data:null,
                    render: function( data, type, full, meta) {
                        return '<a href="" class="editor_edit btn btn-primary"><i class="fa fa-edit"></i>Edit</a>' +
                            '<a href="" class="editor_approve btn btn-success"><i class="fa fa-check"></i>Approve</a>'+
                            '<a href="" class="editor_reject btn btn-danger"><i class="fa fa-times"></i>Reject</a>'

                        }
                }

            ],

        });
    });

    // Edit Report
    $('#pending-report-table').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#pending-report-table').DataTable().row($(this).parents('tr')).data();
        location.href = base_url+'/admin/reports/'+data.report_token+'/edit';
    });


    // Report Rejection
    $('#pending-report-table').on('click', 'a.editor_reject', function (e) {
        e.preventDefault();
        var data = $('#pending-report-table').DataTable().row($(this).parents('tr')).data();
        $.confirm({
            title: 'Reject!',
            content: 'Are you sure?',
            buttons: {
                Confirm: function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type:'GET',
                        url: base_url+'/admin/report/reject-report/'+data.report_token,
                        success:function(res){
                            if(res.success){
                                successNotify(res.msg);

                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }else{
                                failedNotify(res.msg);

                            }
                        }
                    });
                },
                cancel: function () {

                }

            }
        });
    } );

    // Approve Report
    $('#pending-report-table').on('click', 'a.editor_approve', function (e) {
        e.preventDefault();
        var data = $('#pending-report-table').DataTable().row($(this).parents('tr')).data();

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type:'GET',
                        url: base_url+'/admin/report/approve-report/'+data.report_token,
                        success:function(res){
                            if(res.success){
                                successNotify(res.msg);

                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }else{
                                failedNotify(res.msg);

                            }
                        }
                    });

    } );
</script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/reports/pending-for-approval.blade.php ENDPATH**/ ?>