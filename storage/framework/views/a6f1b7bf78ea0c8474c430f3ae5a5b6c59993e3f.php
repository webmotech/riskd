<?php $__env->startSection('custom-css'); ?>
    <style type="text/css">
        .print-btn{
            position: fixed;
            right: 10px;
            bottom: 50%;
            z-index: 1000;
        }
        p td{
            text-align: justify;
        }
    </style>
    <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="container p-3">
<div class="print-btn">
    <a target="_blank" class="btn btn-outline-primary btn-lg" href="<?php echo e(url('admin/reports/print')); ?>/<?php echo e($report->report_token); ?>"><i class="fa fa-print"></i>Print Report</a>
</div>
        <page>
            <p class="text-left"><img src="<?php echo e(asset('assets/img/logo-report.png')); ?>" alt="" style="width:600px;margin-top:100px"></p>
            <h3 class="report-type"><?php echo e(config('reports_config.report_type')[$report->report_type]); ?></h3>
            <div class="footer">

                <p class="text-left">Report Ref: <b><?php echo e($report->id); ?></b></p>
                <p class="text-left mt-5">Report Date: <b><?php echo e($report->generated_at); ?></b>
                <p class="">Phone : 1300 50 13 12 | Email : admin@creditorwatch.com.au</p>

                </p>
            </div>
            <div class="page-no"><p class="text-right">01</p></div>
        </page>
<div class="add-page"></div>
        <page>
            <div class="row">
                <div class="col-md-12">


                    <h1 class="text-danger">Organisation Summary</h1>
                    <h5 class="text-danger">ABR Summary</h5>


                </div>
            </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <tr>
                                <th style="width:350px;">Main Name</th>
                                <td><?php echo e($apidata['main_name']); ?></td>
                            </tr>
                            
                            <tr>
                                <th style="width:350px;">ABN</th>
                                <td><?php echo e(str_replace(',',' ',number_format($apidata['abn']))); ?></td>
                            </tr>
                            <tr>
                                <th style="width:350px;">Entity Status</th>
                                <td><?php echo e($apidata['entity_status']); ?></td>
                            </tr>
                            
                            <tr>
                                <th style="width:350px;">ABN is Current</th>
                                <td>Yes</td>
                            </tr>
                            <tr>
                                <th style="width:350px;">Entity Type</th>
                                <td><?php echo e($apidata['entity_type']); ?></td>
                            </tr>
                            <tr>
                                <th style="width:350px;">GST</th>
                                <td><?php echo e($apidata['gst']); ?></td>
                            </tr>
                            <tr>
                                <th style="width:350px;">Locality</th>
                                <td><?php echo e($apidata['locality']); ?></td>
                            </tr>
                            <tr>
                                <th style="width:350px;">Record Last Updated</th>
                                <td><?php echo e($apidata['record_last_updated']); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            <div class="row">
                <div class="col-md-12">



                    <h5 class="text-danger">ASIC Summary</h5>


                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <tr>
                            <th style="width:350px;">Name</th>
                            <td><?php echo e($apidata['name']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:350px;">ACN</th>
                            <td><?php echo e(Helpers::formatAcn($apidata['acn'])); ?></td>
                        </tr>
                        <tr>
                            <th style="width:350px;">Type</th>
                            <td><?php echo e($apidata['type']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:350px;">Status</th>
                            <td><?php echo e($apidata['status']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:350px;">Controlling Jurisdiction</th>
                            <td>ASIC</td>
                        </tr>
                        <tr>
                            <th style="width:350px;">Registration Date</th>
                            <td><?php echo e($apidata['registered_date']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:350px;">Review Date</th>
                            <td><?php echo e($apidata['review_date']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:350px;">Class</th>
                            <td><?php echo e($apidata['class']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:350px;">Sub Class</th>
                            <td><?php echo e($apidata['subclass']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:350px;">Locality</th>
                            <td><?php echo e($apidata['asic_locallity']); ?></td>
                        </tr>
                        <tr>
                            <th style="width:350px;">Next Review Date</th>
                            <td><?php echo e($apidata['review_date']); ?></td>
                        </tr>
                    </table>
                </div>
            </div>


        </page>
        <div class="add-page"></div>
        <page>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-danger">Non – Financial Based Credit Score</h2>
                    <p style="text-align: justify;" class="mt-2">The score is a statistically based score indicating an entity's credit worthiness. The score
                        ultimately ranks entities based on their riskiness and is designed to assist you in making more
                        informed and consistent credit decisions.</p>
                    <p style="text-align: justify;">The score is based between 0 and 850 index points with a higher score considered lower risk while
                        lower scores are deemed to be riskier entities. It should be used in partnership with your
                        internal credit procedures and policies.
                    </p>
                    <p style="text-align: justify;">The Entity has acceptable creditworthiness. Extend terms within consideration. Entity has a <?php echo e($current_credit_percentage); ?>% chance of failure within the next 12 months.

                </div>
            </div>
            <div class="row">
                <div class="container">
                    <div class="col-md-12">

                        <?php
                            $creditScorePercentage = number_format(($apidata['current_credit_score']/850)*100,2);

                        ?>
                        <div class="progress" style=" height:50px; background:linear-gradient(90deg, rgba(255,0,31,1) 0%, rgba(255,226,0,1) 49%, rgba(18,255,0,1) 100%);">
                            <div class="progress-bar" role="progressbar" style=" text-align:right;width: <?php echo e($creditScorePercentage); ?>%; background:rgba(0,0,0,0.2);font-size:30px" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                <?php echo e($apidata['current_credit_score']); ?>

                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <p>0</p>
                                <p class="text-danger">Higher Risk</p>
                            </div>
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <p class="text-right">850</p>
                                <p class="text-right">Lower Risk</p>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="col-md-12">
                        <h2 class="mt-1">Historical Credit Scores</h2>

                        <div class="row" style="width: 100%" id="credit-chart">
                        </div>

                    </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-danger">Recommendations</h2>
                    <table class="table cardview">
                        <thead class="thead-light">
                        <tr>
                            <th width="20%">Range Risk</th>
                            <th width="20%">level</th>
                            <th width="60%">Recommendation</th>
                        </tr>
                        </thead>
                        <tbody style="text-align: justify;">
                        <tr>
                            <td>0</td>
                            <td>Critical</td>
                            <td>ACN deregistered or ABN cancelled.</td>
                        </tr>
                        <tr>
                            <td>1-125</td>
                            <td>Critical</td>
                            <td>Entity has a critical status and significant adverse information present.
                                Trading eligibility must be considered.</td>
                        </tr>
                        <tr>
                            <td>126-250</td>
                            <td>Very High</td>
                            <td>Entity has multiple pieces of adverse information present. COD trading highly
                                recommended.</td>
                        </tr>
                        <tr>
                            <td>251-450</td>
                            <td>High</td>
                            <td>Entity has a below average creditworthiness score and some adverse information
                                may be present. Trade with caution, monitor closely and consider your payment
                                terms.</td>
                        </tr>
                        <tr>
                            <td>451-550</td>
                            <td>Moderate</td>
                            <td>Entity has moderate creditworthiness with or without adverse information.
                                Monitor ongoing payment behaviour.</td>
                        </tr>
                        <tr>
                            <td>551-850</td>
                            <td>Low</td>
                            <td>Entity has acceptable creditworthiness. Extend terms within consideration.</td>
                        </tr>
                        </tbody>
                    </table>

                    <p>Please note that the score and recommendation should be used in partnership with your
                        company's internal credit procedures and policies. The score should not be used as the sole reason in making a decision about the entity.</p>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mt-3 text-danger">Financial Summary </h2>
                    <table class=" mt-5 table table-bordered">
                        <tbody>

                        <tr class="alert alert-danger">

                            <th width='300px' style="text-align:left">Financial Year</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <td colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><strong><?php echo e($details['financial_year']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">

                            <th width='300px' style="text-align:left">Rounding</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><strong><?php echo e(config('reports_config.rounding')[$details['rounding']]); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Base Currency</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><strong><?php echo e($details['base_currency']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Quality</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><strong><?php echo e(config('reports_config.quality')[$details['quality']]); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Reporting Period - Months</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><strong><?php echo e($details['report_period_months']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Month</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td colspan="<?php echo e(($key>0)?2:1); ?>" style="text-align:right"><strong><?php echo e($details['financial_month']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr>
                            <th colspan="<?php echo e(count($details->toArray())+1); ?>"><h5>Income Statement</h5></th>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Sales</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['sales'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['sales']-$report_details[$key-1]['sales'])); ?></strong></td>
                                    <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Gross Profit</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['gross_profit'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['gross_profit']-$report_details[$key-1]['gross_profit'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Other Income</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['other_income'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['other_income']-$report_details[$key-1]['other_income'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">EBIT</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['ebit'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['ebit']-$report_details[$key-1]['ebit'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">EBITDA</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['ebitda'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['ebitda']-$report_details[$key-1]['ebitda'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Profit Before Tax</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['profit_before_tax'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['profit_before_tax']-$report_details[$key-1]['profit_before_tax'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Profit After Tax</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['profit_after_tax'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['profit_after_tax']-$report_details[$key-1]['profit_after_tax'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr>
                            <th colspan="<?php echo e(count($details->toArray())+1); ?>"><h5>Balance Sheet</h5></th>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Current Assets</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['total_current_assets'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['total_current_assets']-$report_details[$key-1]['total_current_assets'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Non-Current Assets</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['total_non_current_assets'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['total_non_current_assets']-$report_details[$key-1]['total_non_current_assets'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Assets</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['total_assets'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['total_assets']-$report_details[$key-1]['total_assets'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Current Liabilities</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['total_current_liabilities'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['total_current_liabilities']-$report_details[$key-1]['total_current_liabilities'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Non-Current Liabilities</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['total_non_current_liabilities'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['total_non_current_liabilities']-$report_details[$key-1]['total_non_current_liabilities'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Liabilities</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['total_liabilities'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['total_liabilities']-$report_details[$key-1]['total_liabilities'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Share Capital</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['share_capital'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['share_capital']-$report_details[$key-1]['share_capital'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Equity</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <td style="text-align:right"><strong><?php echo e(number_format($details['total_equity'])); ?></strong></td>
                                <?php if($key>0): ?>
                                    <td style="text-align:right"><strong><?php echo e(Helpers::checkValue($details['total_equity']-$report_details[$key-1]['total_equity'])); ?></strong></td>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <?php
                $currentKeyRatio = last($report_details->toArray())['key_ratio_calculation'];

                ?>

                <?php if($report->report_type==1): ?>
                    <div class="col-md-12">
                        <h3 class="text-danger">Financial Analysis</h3>
                        <?php $__currentLoopData = $report->executiveSummery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $summery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <p><?php echo $summery['summery']; ?></p>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <?php elseif($report->report_type==2): ?>
                    <div class="col-md-12">
                        <h3 class="text-danger">Key Performance Indicator Analysis</h3>
                        <table class="table table-bordered">
                            <tbody style="text-align: justify;">
                            <tr>
                                <td style="width: 30%"><b> Gross Profit Margin</b></td>
                                <td><?php echo e(Helpers::getGrossProfitMargin($currentKeyRatio['gross_profit_margin'])); ?></td>
                            </tr>
                            <tr>
                                <td><b> Net Profit Margin </b></td>
                                <td><?php echo e(Helpers::getNetProfitMargin($currentKeyRatio['net_profit_margin'])); ?></td>
                            </tr>

                            <tr>
                                <td><b> Return on Assets</b></td>
                                <td><?php echo e(Helpers::getReturnOnAssets($currentKeyRatio['return_on_assets'])); ?></td>
                            </tr>
                            <tr>
                                <td><b> Current Ratio</b> </td>
                                <td><?php echo e(Helpers::getCurrentRatio($currentKeyRatio['current_ratio'])); ?></td>
                            </tr>
                            <tr>
                                <td><b> Quick Ratio</b> </td>
                                <td><?php echo e(Helpers::getQuickRatio($currentKeyRatio['quick_ratio'])); ?></td>
                            </tr>

                            <tr>
                                <td><b> Gearing</b></td>
                                <td><?php echo e(Helpers::getGearing($currentKeyRatio['gearing'])); ?></td>
                            </tr>
                            <tr>
                                <td><b> Interest Coverage </b></td>
                                <td><?php echo e(Helpers::getInterestCoverage($currentKeyRatio['interest_coverage'])); ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php endif; ?>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-danger">Financial Trend Graphs</h3>
                </div>
            </div>
            <div class="row">
                <div class="col" id="revenue_chart"></div>
                <div class="col" id="gp_np_margin"></div>
            </div>
            <div class="row">
                <div class="col" id="gp_np"></div>
                <div class="col" id="ebitda"></div>
            </div>
            <div  class="row">
                <div class="col" id="ratio"></div>
                <div class="col" id="capital"></div>
            </div>
            <div  class="row">
                <div class="col" id="equity"></div>
                <div class="col" id="interest_cover"></div>
            </div>
        </page>
        <div class="add-page"></div>
        <page>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-danger">Financial Performance</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class=" mt-5 table table-bordered">
                        <tbody>

                        <tr class="alert alert-danger">

                            <th width='300px' style="text-align:left;">Financial Year</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <td  style="text-align:right;min-width:287px"><strong><?php echo e($details['financial_year']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">

                            <th width='300px' style="text-align:left">Rounding</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right"><strong><?php echo e(config('reports_config.rounding')[$details['rounding']]); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Base Currency</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right"><strong><?php echo e($details['base_currency']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Quality</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right"><strong><?php echo e(config('reports_config.quality')[$details['quality']]); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Reporting Period - Months</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right"><strong><?php echo e($details['report_period_months']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Confidenality Record</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right"><strong><?php echo e($details->confidentiality['name']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Month</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right"><strong><?php echo e($details['financial_month']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr>
                            <th colspan="<?php echo e(count($details->toArray())+1); ?>"><h5>Income Statement</h5></th>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Sales</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['sales'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Cost of Sales</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['cost_of_sales'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Gross Profit</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['gross_profit'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Other Income</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['other_income'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Depreciation</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['depreciation'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Amortisation</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['amortisation'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Impairment</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['impairment'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Interest Expense</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['interest_expenses_gross'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Non - Recurring Gains/ (Losses)</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['non_recurring_gain_loses'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Other Gains / (Losses)</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['other_gain_loses'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Other Expenses</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['other_expenses'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">EBIT</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['ebit'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">EBITDA</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['ebitda'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Normalised EBITDA</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['normalized_ebitda'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Profit Before Tax</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['profit_before_tax'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Profit Before Tax (After Abnormals)</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['profit_before_tax_after_abnormals'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Tax Benefit/ (Expense)</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['tax_benefit_expenses'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Profit After Tax</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['profit_after_tax'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Distribution or Dividends</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['distribution_ordividends'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Other Post Tax Items - Gains/ (Losses)</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['other_post_tax_items_gains_losses'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Profit After Tax & Distribution</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right"><strong><?php echo e(number_format($details['profit_after_tax_distribution'])); ?></strong></td>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </page>
        <div class="add-page">

        </div>
        <page>
            <div class="row">
                <div class="col-md-12">
                    <table class="mt-5 table table-bordered">
                        <tbody>

                        <tr class="alert alert-danger">

                            <th width='300px' style="text-align:left">Financial Year</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <td  style="text-align:right;min-width:287px"><strong><?php echo e($details['financial_year']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">

                            <th width='300px' style="text-align:left">Rounding</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(config('reports_config.rounding')[$details['rounding']]); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Base Currency</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e($details['base_currency']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Quality</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(config('reports_config.quality')[$details['quality']]); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Reporting Period - Months</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e($details['report_period_months']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Confidenality Record</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e($details->confidentiality['name']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="alert alert-danger">
                            <th width='300px' style="text-align:left">Month</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e($details['financial_month']); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr>
                            <th colspan="<?php echo e(count($details->toArray())+1); ?>"><h5>Balance Sheet</h5></th>
                        </tr>
                        <tr>
                            <th class="pt-1 pb-1" colspan="<?php echo e(count($details->toArray())+1); ?>"><p class="text-warning"><strong>Assets</strong></p></th>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Cash</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['cash'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Trade Debtors</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['trade_debtors'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Inventories</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['total_inventories'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Loans to Related Parties</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['loan_to_related_parties_1'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Other Current Assets</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['other_current_assets'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Current Assets</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['total_current_assets'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Fixed Assets</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['fixed_assets'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Net Intangibles</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['net_intangibles'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Loans to Related Parties</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['loan_to_related_parties_2'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Other Non-Current Assets</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['other_non_current_assets'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Non-Current Assets</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['total_non_current_assets'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Assets</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['total_assets'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr>
                            <th class="pt-1 pb-1" colspan="<?php echo e(count($details->toArray())+1); ?>"><p class="text-warning"><strong>Liabilities</strong></p></th>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Trade Creditors</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['trade_creditors'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Interest Bearing Debt</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['interest_bearing_debt_1'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Loan from Related Parties</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['loan_from_related_parties_1'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Other Current Liabilities</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['other_current_liabilities'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Current Liabilities</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['total_current_liabilities'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Interest Bearing Debt</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['interest_bearing_debt_2'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Loan from Related Parties</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['loan_from_related_parties_2'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Other Non-Current Liabilities</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td  style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['other_non_current_liabilities'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>

                        <tr class="">
                            <th width='300px' style="text-align:left">Total Non-Current Liabilities</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;min-width:287px;"><strong><?php echo e(number_format($details['total_non_current_liabilities'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Liabilities</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['total_liabilities'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr>
                            <th class="pt-0 pb-0" colspan="<?php echo e(count($details->toArray())+1); ?>"><p class="text-warning"><strong>Equity</strong></p></th>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Share Capital</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['share_capital'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Preference Shares</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['prefence_shares'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Treasury Shares</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['threasury_shares'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Equity Ownerships</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['equity_ownerships'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Reserves</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['total_reserves'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Retained Earnings</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['retained_earning'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Minority Interest</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['minorty_interest'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        <tr class="">
                            <th width='300px' style="text-align:left">Total Equity</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td style="text-align:right;min-width:287px"><strong><?php echo e(number_format($details['total_equity'])); ?></strong></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </page>
        <div class="add-page"></div>
        <page>
            <div class="row">
                <?php


                ?>
                <div class="col-md-12">
                    <h3 class="text-danger">Key Ratios</h3>
                    <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th class="text-center">Denomination</th>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <th class="text-center"><?php echo e($details['financial_year']); ?></th>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                               <?php
                               $lastIndex = (count($report_details->toArray()))-1;

                               ?>
                                <th class="text-center">FY<?php echo e($report_details[$lastIndex]['financial_year']); ?> vs FY<?php echo e($report_details[$lastIndex-1]['financial_year']); ?></th>
                                <?php endif; ?>
                        </tr>
                        <tr>
                            <th class="pt-1 pb-1" colspan="<?php echo e(count($details->toArray())+6); ?>"><p class="text-warning"><strong>Profitability</strong></p></th>
                        </tr>
                        <tr>
                            <td>Gross Profit Margin</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['gross_profit_margin'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['gross_profit_margin']-$report_details[$lastIndex-1]['keyRatioCalculation']['gross_profit_margin'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>EBIDTA</td>
                            <td class="text-center">Thousands</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['ebitda'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['ebitda']-$report_details[$lastIndex-1]['keyRatioCalculation']['ebitda'],false)); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Net Profit Margin</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['net_profit_margin'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['net_profit_margin']-$report_details[$lastIndex-1]['keyRatioCalculation']['net_profit_margin'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Return on Assets</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['return_on_assets'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['return_on_assets']-$report_details[$lastIndex-1]['keyRatioCalculation']['return_on_assets'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Return on Equity</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['return_on_equity'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['return_on_equity']-$report_details[$lastIndex-1]['keyRatioCalculation']['return_on_equity'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <th class="pt-1 pb-1" colspan="<?php echo e(count($details->toArray())+6); ?>"><p class="text-warning"><strong>Financial Leverage</strong></p></th>
                        </tr>
                        <tr>
                            <td>Net Tangible Worth</td>
                            <td class="text-center">Thousands</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['net_tangible_worth'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['net_tangible_worth']-$report_details[$lastIndex-1]['keyRatioCalculation']['net_tangible_worth'],false)); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Gearing</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['gearing'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['gearing']-$report_details[$lastIndex-1]['keyRatioCalculation']['gearing'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Debt to Equity</td>
                            <td class="text-center">X</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['debt_to_equity'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['debt_to_equity']-$report_details[$lastIndex-1]['keyRatioCalculation']['debt_to_equity'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Interest Coverage</td>
                            <td class="text-center">X</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['interest_coverage'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['interest_coverage']-$report_details[$lastIndex-1]['keyRatioCalculation']['interest_coverage'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Repayment Capability</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['repayment_capability'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['repayment_capability']-$report_details[$lastIndex-1]['keyRatioCalculation']['repayment_capability'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <th class="pt-1 pb-1" colspan="<?php echo e(count($details->toArray())+6); ?>"><p class="text-warning"><strong>Liquidity</strong></p></th>
                        </tr>
                        <tr>
                            <td>Working Capital</td>
                            <td class="text-center">Thousands</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['working_capital'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['working_capital']-$report_details[$lastIndex-1]['keyRatioCalculation']['working_capital'],false)); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Working Capital to Sales</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['working_capital_to_sales'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['working_capital_to_sales']-$report_details[$lastIndex-1]['keyRatioCalculation']['working_capital_to_sales'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Current Ratio</td>
                            <td class="text-center">X</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['current_ratio'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['current_ratio']-$report_details[$lastIndex-1]['keyRatioCalculation']['current_ratio'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Quick Ratio</td>
                            <td class="text-center">X</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['quick_ratio'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['quick_ratio']-$report_details[$lastIndex-1]['keyRatioCalculation']['quick_ratio'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <th class="pt-1 pb-1" colspan="<?php echo e(count($details->toArray())+6); ?>"><p class="text-warning"><strong>Asset Turnover</strong></p></th>
                        </tr>
                        <tr>
                            <td>Creditor Days</td>
                            <td class="text-center">Days</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['creditor_days'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['creditor_days']-$report_details[$lastIndex-1]['keyRatioCalculation']['creditor_days'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Inventory Days</td>
                            <td class="text-center">Days</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['inventory_days'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['inventory_days']-$report_details[$lastIndex-1]['keyRatioCalculation']['inventory_days'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Debtor Days</td>
                            <td class="text-center">Days</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['debtor_days'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['debtor_days']-$report_details[$lastIndex-1]['keyRatioCalculation']['debtor_days'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Cash Conversion Cycle</td>
                            <td class="text-center">Days</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['cash_conversion_cycle'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['cash_conversion_cycle']-$report_details[$lastIndex-1]['keyRatioCalculation']['cash_conversion_cycle'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <th class="pt-1 pb-1" colspan="<?php echo e(count($details->toArray())+6); ?>"><p class="text-warning"><strong>Other Ratios</strong></p></th>
                        </tr>
                        <tr>
                            <td>Sales (Annualised)</td>
                            <td class="text-center">Thousands</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['sales_annualised'])); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['sales_annualised']-$report_details[$lastIndex-1]['keyRatioCalculation']['sales_annualised'],false)); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Sales Growth</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $keyRatio = $details['keyRatioCalculation']; ?>
                            <?php if($key==0): ?>
                            <td style="padding:7px;text-align:center;">N/A</td>
                            <?php else: ?>
                            <td style="padding:7px;text-align:center;"><?php echo e(number_format($keyRatio['sales_growth'],2)); ?></td>
                            <?php endif; ?>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['sales_growth']-$report_details[$lastIndex-1]['keyRatioCalculation']['sales_growth'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Related Party Loans Receivable</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['related_party_loans_receivable'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['related_party_loans_receivable']-$report_details[$lastIndex-1]['keyRatioCalculation']['related_party_loans_receivable'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Related Party Loans Payable</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['related_party_loans_payable'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['related_party_loans_payable']-$report_details[$lastIndex-1]['keyRatioCalculation']['related_party_loans_payable'])); ?></td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <td>Related Party Loans Dependency</td>
                            <td class="text-center">%</td>
                            <?php $__currentLoopData = $report_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php $keyRatio = $details['keyRatioCalculation']; ?>

                                <td class="text-center"><?php echo e(number_format($keyRatio['related_party_loans_dependency'],2)); ?></td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if(count($report_details->toArray())>1): ?>
                                <?php
                                    $lastIndex = (count($report_details->toArray()))-1;

                                ?>
                                <td class="text-center"><?php echo e(Helpers::checkRatioValue($report_details[$lastIndex]['keyRatioCalculation']['related_party_loans_dependency']-$report_details[$lastIndex-1]['keyRatioCalculation']['related_party_loans_dependency'])); ?></td>
                            <?php endif; ?>
                        </tr>
                    </table>
                </div>
            </div>
        </page>
        <div class="add-page"></div>
        <page>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-danger">Glossary of Financial Calculations</h3>
                    <table class="table table-bordered table-collapse">
                        <tr class="bg-primary">
                            <th width="37%">Profitability Indicators</th>
                            <th>Calculation</th>
                        </tr>
                        <tr>
                            <td>Gross Profit Margin</td>
                            <td>Gross Profit / Sales shown as a percentage</td>
                        </tr>
                        <tr>
                            <td>EBITDA</td>
                            <td>Earnings Before Interest Depreciation Tax and Amortisation</td>
                        </tr>
                        <!-- <tr>
                            <td>Normalised EBITDA</td>
                            <td>Earnings Before Interest Depreciation Tax Amortisation & Extraordinary Items</td>
                        </tr> -->
                        <!-- <tr>
                            <td>EBIT</td>
                            <td>Earnings Before Interest and Tax</td>
                        </tr> -->
                        <tr>
                            <td>Net Profit Margin</td>
                            <td>Net Profit Before Tax / Sales shown as a percentage</td>
                        </tr>
                        <!-- <tr>
                            <td>Profitability</td>
                            <td>Annualised Profit after Tax and Dividends / Total Assets shown as a percentage</td>
                        </tr> -->
                        <!-- <tr>
                            <td>Reinvestment</td>
                            <td>Retained Earnings / Total Assets shown as a percentage</td>
                        </tr> -->
                        <tr>
                            <td>Return on Assets</td>
                            <td>Annualised Profit before Interest and Tax / Total Assets shown as a percentage</td>
                        </tr>
                        <tr>
                            <td>Return on Equity</td>
                            <td>Annualised Profit after Tax / Shareholders Equity shown as a percentage</td>
                        </tr>
                        <tr class="bg-primary">
                            <th>Financial Leverage</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Net Tangible Worth</td>
                            <td>Total Net Assets - Intangibles</td>
                        </tr>
                        <tr>
                            <td>Gearing</td>
                            <td>Total Liabilities / Total Assets shown as a percentage</td>
                        </tr>
                        <tr>
                            <td>Debt to Equity</td>
                            <td>Total Debt/Shareholders Equity</td>
                        </tr>
                        <tr>
                            <td>Interest Coverage</td>
                            <td>Profit before Tax and Interest Expense / Interest Expense</td>
                        </tr>
                        <tr>
                            <td>Repayment Capability</td>
                            <td>Annualised Profit before Tax / Total Liabilities shown as a percentage</td>
                        </tr>

                        <tr class="bg-primary">
                            <th>Liquidity</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Working Capital</td>
                            <td>Current Assets - Current Liabilities</td>
                        </tr>
                        <tr>
                            <td>Working Capital to Sales</td>
                            <td>Working Capital / Annualised Sales shown as a percentage</td>
                        </tr>
                        <!-- <tr>
                            <td>Cash Flow Coverage</td>
                            <td>Annualised Operating Cash Flow / Current Liabilities shown as a percentage</td>
                        </tr>   -->
                        <!-- <tr>
                            <td>Cash Ratio</td>
                            <td>Cash/Current Liabilities shown as a percentage</td>
                        </tr>     -->
                        <tr>
                            <td>Current Ratio</td>
                            <td>Current Assets / Current Liabilities</td>
                        </tr>
                        <tr>
                            <td>Quick Ratio</td>
                            <td>(Current Assets - Inventories) / Current Liabilities</td>
                        </tr>

                        <tr class="bg-primary">
                            <th>Asset Turnover</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Creditor Days</td>
                            <td>(Trade Creditors / Annualised Cost of Goods Sold) x 365 days</td>
                        </tr>
                        <tr>
                            <td>Inventory Days</td>
                            <td>(Inventories / Annualised Cost of Goods Sold) x 365 Days</td>
                        </tr>
                        <tr>
                            <td>Debtor Days</td>
                            <td>(Trade Debtors / Annualised Sales) x 365 days</td>
                        </tr>
                        <tr>
                            <td>Cash Conversion Cycle</td>
                            <td>Debtor Days + Inventory Days - Creditor Days</td>
                        </tr>
                        <tr class="bg-primary">
                            <th>Other Indicators</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Sales (Annualised)</td>
                            <td>Annualised Sales Revenue</td>
                        </tr>
                        <tr>
                            <td>Sales Growth</td>
                            <td>Percentage Change in Sales</td>
                        </tr>
                        <tr>
                            <td>Related Party Loans Receivable</td>
                            <td>Current Plus Non-Current Loans Owing by Related Parties as a % of Total Assets</td>
                        </tr>
                        <tr>
                            <td>Related Party Loans Payable</td>
                            <td>Current Plus Non-Current Loans Owing to Related Parties as a % of Total Liabilities</td>
                        </tr>
                        <tr>
                            <td>Related Party Loans Dependency</td>
                            <td>Related Party Loans Payable / Working Capital shown as a percentage</td>
                        </tr>

                    </table>
                </div>
            </div>

        </page>
        <page>
            <div class="row">
                <div class="col-md-12">

                    <h3 class="text-danger">Disclaimer</h3>
                    <p>
                    CreditorWatch is committed to ensuring that the information provided is accurate and comprehensive however due to data being received from sources not controlled by CreditorWatch we cannot guarantee that it is complete, verified or free of errors. The information should therefore be used in conjunction with your own investigations and you should not rely solely on this information when making credit or financial decisions. To the extent permitted by law, CreditorWatch will not be held responsible for any errors or omissions therein concerning the information sourced and published in its publications, websites, API or emails.
                    </p>
                </div>
            </div>
        </page>

    </div>

    <?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
    <script type="text/javascript">
        $(document).ready(function(){
            var reportToken = '<?php echo e($report['report_token']); ?>';
            var rounding = '<?php echo e($report_details[0]['rounding']); ?>';

            if(rounding=='MI'){
                var title = "Value $'Mn";
            }else if(rounding=="TH"){
                var title = "Value $'000";
            }else{
                var title = "Value $";
            }
            // Get Credit Score History Chart

            $.ajax({
                method:"GET",
                url: base_url+"/admin/chart/credit-score-history/"+reportToken,
                dataType:"JSON",
                success:function(res) {

                    var options = {
                        exporting:false,
                        xAxis :
                    {
                        categories: res.date
                    },
                        credits: {
                            enabled: false
                        },

                    title: {
                        text: ''
                    },
                    plotOptions: {
                        series: {
                            allowPointSelect: true
                        }
                    }
                ,
                    series: [{

                        name: 'Credit Score',
                        data: res.value
                    }]
                }

                    var chart = Highcharts.chart('credit-chart', options);
                    $('.highcharts-credits').hide();

                    var data = {
                        options: JSON.stringify(options),
                        filename: 'test.png',
                        type: 'image/png',
                        async: true
                    };

                    // Get Sanpshot of Loaded Chart
                    saveReportImage(data,reportToken,'credit_score');


                }
            });


            // Load Revenue Chart
            $.ajax({
                method:"GET",
                url:base_url+"/admin/chart/revenue-data/"+reportToken,
                dataType:"JSON",
                success:function(res) {


                    var option = {
                        exporting:false,
                        chart: {
                            type: 'column'
                        },
                        credits: {
                            enabled: false
                        },

                        title: {
                            text: 'Revenue'
                        },
                        yAxis: {
                            labels: {

                                formatter: function () {
                                    if(rounding=='MI'){

                                        return this.value;
                                    }else{
                                        return this.value;
                                    }
                                },
                                "format": "{value}"
                            },
                            title: {
                                style: {
                                fontSize:'16px'
                                },
                                text: title
                            }
                        },
                        xAxis: {
                            categories: res.year
                        },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: 'Revenue',
                            data: res.value
                        }]

                    }
                    Highcharts.chart('revenue_chart', option);
                    $('.highcharts-credits').hide();

                    var data = {
                        options: JSON.stringify(option),
                        filename: 'test.png',
                        type: 'image/png',
                        async: true
                    };

                    // Get Sanpshot of Loaded Chart
                   saveReportImage(data,reportToken,'revenue');
                }
            });

            //GP NP margin chart

            $.ajax({
                method:"GET",
                url: base_url+"/admin/chart/gpnp-margin-data/"+reportToken,
                dataType:"JSON",
                success:function(res){
                    var options = {
                        exporting:false,
                        chart: {
                            type: 'line'
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Gross Profit & Net Profit Margin'
                        },

                        xAxis: {
                            categories: res.year
                        },
                        yAxis: {
                            title: {
                                style: {
                                fontSize:'16px'
                                },
                                text: 'Percentage'
                            },
                            labels: {

                                "format": "{value}"
                                },
                        },
                        plotOptions: {
                            line: {
                                dataLabels: {
                                    enabled: true
                                },
                                enableMouseTracking: false
                            }
                        },
                        series: [{
                            name: 'Gross Profit Margin',
                            data: res.gp
                        }, {
                            name: 'Net Profit Margin',
                            data: res.np
                        }]
                    };

                    Highcharts.chart('gp_np_margin',options);
                    $('.highcharts-credits').hide();

                    var data = {
                        options: JSON.stringify(options),
                        filename: 'test.png',
                        type: 'image/png',
                        async: true
                    };

                    // Get Sanpshot of Loaded Chart
                    saveReportImage(data,reportToken,'gpnp_margin');

                }
            });



        // Get Gross and Net Profit data

            $.ajax({
                method:"GET",
                url: base_url+"/admin/chart/grossnet-profit-data/"+reportToken,
                dataType:"JSON",
                success:function(res){
                    var option = {
                        exporting:false,
                        chart: {
                            type: 'column'
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Gross Profit & Net Profit'
                        },
                        xAxis: {
                            categories: res.year
                        },
                        yAxis: {
                            labels: {
                                formatter: function () {
                                    if(rounding=='MI'){

                                        return this.value;
                                    }else{
                                        return this.value;
                                    }
                                },


                            "format": "{value}"

                            },
                            credits: {
                                enabled: false
                            },
                            title: {
                                style: {
                                fontSize:'16px'
                                },
                                text: title
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: 'Gross profit',
                            data: res.gp
                        }, {
                            name: 'Net Profit',
                            data: res.np
                        }]
                    };

                    Highcharts.chart('gp_np',option);
                    $('.highcharts-credits').hide();

                    var data = {
                        options: JSON.stringify(option),
                        filename: 'test.png',
                        type: 'image/png',
                        async: true
                    };

                    // Get Sanpshot of Loaded Chart
                    saveReportImage(data,reportToken,'gross_net_profit');
                }


            });

            // Get Ebitda and EBIT
            $.ajax({
                method:"GET",
                url: base_url+"/admin/chart/ebitda-data/"+reportToken,
                dataType:"JSON",
                success:function(res){
                   var option = {
                    exporting:false,
                        chart: {
                            type: 'column'
                        },
                       credits: {
                           enabled: false
                       },
                        title: {
                            text: 'Normalised EBITDA and EBIT'
                        },
                        xAxis: {
                            categories: res.year
                        },
                        yAxis: {
                            labels: {
                                formatter: function () {
                                    if(rounding=='MI'){

                                        return this.value;
                                    }else{
                                        return this.value;
                                    }
                                },
                                "format": "{value}"
                            },
                            title: {
                                style: {
                                fontSize:'16px'
                                },
                                text: title
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: 'Normalised EBITDA',
                            data: res.normalized_ebit
                        }, {
                            name: 'EBIT',
                            data: res.ebit
                        }]
                    };

                    Highcharts.chart('ebitda',option);
                    $('.highcharts-credits').hide();

                    var data = {
                        options: JSON.stringify(option),
                        filename: 'test.png',
                        type: 'image/png',
                        async: true
                    };

                    // Get Sanpshot of Loaded Chart
                    saveReportImage(data,reportToken,'ebit');
                }


            });

            // Liquidity Ratios

            $.ajax({
                method:"GET",
                url: base_url+"/admin/chart/ratio-data/"+reportToken,
                dataType:"JSON",
                success:function(res){
                    var option = {
                        exporting:false,
                        chart: {
                            type: 'column'
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Liquidity Ratios'
                        },
                        xAxis: {
                            categories: res.year
                        },
                        yAxis: {
                            title: {
                                style: {
                                fontSize:'16px'
                                },
                                text: 'Times'
                            }
                        },
                        labels: {

                                "format": "{value}"
                            },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: 'Current Ratio',
                            data: res.current_ratio
                        }, {
                            name: 'Quick Ratio',
                            data: res.quick_ratio
                        }, {
                            name: 'Cash Ratio',
                            data: res.cash_ratio
                        }]
                    };

                    Highcharts.chart('ratio',option);
                    $('.highcharts-credits').hide();

                    var data = {
                        options: JSON.stringify(option),
                        filename: 'test.png',
                        type: 'image/png',
                        async: true
                    };

                    // Get Sanpshot of Loaded Chart
                    saveReportImage(data,reportToken,'ratio');
                }


            });

            // Get Working Capital

            $.ajax({
                method:"GET",
                url: base_url+"/admin/chart/working-capital-data/"+reportToken,
                dataType:"JSON",
                success:function(res){
                    var option = {
                        exporting:false,
                        chart: {
                            type: 'column'
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Working Capital'
                        },

                        xAxis: {
                            categories: res.year
                        },
                        yAxis: {
                            labels: {
                                formatter: function () {
                                    if(rounding=='MI'){

                                        return this.value;
                                    }else{
                                        return this.value;
                                    }
                                },
                                "format": "{value}"


                            },
                            title: {
                                style: {
                                fontSize:'16px'
                                },
                                text: title
                            },
                            minRange: 5
                        },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: 'Current Assets',
                            data: res.current_assets
                        }, {
                            name: 'Current Liabilities',
                            data: res.current_liabilities
                        }, {
                            name: 'Working Capital',
                            data: res.working_capital
                        }]
                    };

                    Highcharts.chart('capital',option);
                    $('.highcharts-credits').hide();

                    var data = {
                        options: JSON.stringify(option),
                        filename: 'test.png',
                        type: 'image/png',
                        async: true
                    };

                    // Get Sanpshot of Loaded Chart
                    saveReportImage(data,reportToken,'capital');
                }


            });


// Get Equity Chart data

            $.ajax({
                method:"GET",
                url: base_url+"/admin/chart/working-equity-data/"+reportToken,
                dataType:"JSON",
                success:function(res){
                    var option = {
                        exporting:false,

                        title: {
                            text: 'Equity'
                        },
                        xAxis: {
                            categories: res.year
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            labels: {
                                formatter: function () {
                                    if(rounding=='MI'){

                                        return this.value;
                                    }else{
                                        return this.value;
                                    }
                                },
                               "format": "{value}"

                            },
                            title: {
                                style: {
                                fontSize:'16px'
                                },
                                text: title
                            }
                        },
                        labels: {

                        },
                        series: [{
                            type: 'column',
                            name: 'Total Assets',
                            data: res.total_assets
                        }, {
                            type: 'column',
                            name: 'Total Debt',
                            data: res.total_liabilities
                        }, {
                            type: 'spline',
                            name: 'Equity',
                            data: res.equity,
                            marker: {
                                lineWidth: 2,
                                lineColor: Highcharts.getOptions().colors[3],
                                fillColor: 'white'
                            }
                        }]
                    };

                    Highcharts.chart('equity',option);
                    $('.highcharts-credits').hide();

                    var data = {
                        options: JSON.stringify(option),
                        filename: 'test.png',
                        type: 'image/png',
                        async: true
                    };

                    // Get Sanpshot of Loaded Chart
                    saveReportImage(data,reportToken,'equity');
                }


            });



//Get Interest Cover Data

            $.ajax({
                method:"GET",
                url: base_url+"/admin/chart/interest-cover-data/"+reportToken,
                dataType:"JSON",
                success:function(res){


                    var option = {
                        exporting:false,
                        chart: {
                            zoomType: 'xy'
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Debt to Equity and Interest Cover',
                            align: 'center'
                        },

                        xAxis: [{
                            categories: res.year,
                            crosshair: true
                        }],
                        yAxis: [{ // Primary yAxis
                            labels: {
                                text: 'Value',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                rotation: 270,
                                text: 'Times',
                                style: {
                                    fontSize:'16px',
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            opposite: true

                        }, { // Secondary yAxis
                            gridLineWidth: 0,
                            title: {
                                text: 'Times',
                                style: {
                                    fontSize:'16px',
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            labels: {
                                format: '{value}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }

                        }],
                        tooltip: {
                            shared: true
                        },
                        legend: {
                            floating: false,
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom',
                            x: 0,
                            y: 0

                        },
                        series: [{
                            name: 'Debt/Equity',
                            type: 'column',
                            yAxis: 1,
                            data: res.debt,
                            tooltip: {
                                valueSuffix: ' '
                            }

                        },  {
                            name: 'Interest Cover',
                            type: 'spline',
                            data: res.interest,
                            tooltip: {
                                valueSuffix: ''
                            }
                        }],
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        floating: false,
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom',
                                        x: 0,
                                        y: 0
                                    },
                                    yAxis: [{
                                        labels: {
                                            align: 'right',
                                            x: 0,
                                            y: -6
                                        },
                                        showLastLabel: false
                                    }, {
                                        labels: {
                                            align: 'left',
                                            x: 0,
                                            y: -6
                                        },
                                        showLastLabel: false
                                    }, {
                                        visible: false
                                    }]
                                }
                            }]
                        }
                    };

                    Highcharts.chart('interest_cover',option);
                    $('.highcharts-credits').hide();
                    var data = {
                        options: JSON.stringify(option),
                        filename: 'test.png',
                        type: 'image/png',
                        async: true
                    };

                    // Get Sanpshot of Loaded Chart
                    saveReportImage(data,reportToken,'interest_cover');


                }


            });

        });


        // Get Sanpshot of Loaded Chart
        function saveReportImage(chartData,reportToken,type){
            var chartCount = '<?php echo e($report['chart_count']); ?>';

            var exportUrl = 'https://export.highcharts.com/';

            // Save Chart Image
            $.post(exportUrl, chartData, function(data) {
                var imageUrl = exportUrl + data;

                $.ajax({
                    method:'GET',
                    url: base_url+'/admin/chart/save-chart-images',
                    data:{type: type,report_token:reportToken, img:imageUrl},
                    success:(function(res){

                    })
                });

            });
        }
    </script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.report-master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp_new\htdocs\riskd\resources\views/admin/reports/report-view.blade.php ENDPATH**/ ?>