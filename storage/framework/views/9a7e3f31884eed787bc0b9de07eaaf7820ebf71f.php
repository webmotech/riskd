<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <p class="text-center text-danger"><i class="fas fa-cog fa-spin fa-3x"></i></p>
                    <h1 class="text-danger text-center">PDF generation error found. Please contact system administrator</h1>
                <p class="text-center"><a class="btn btn-danger" href="<?php echo e(url('admin/reports/list/approved-report')); ?>">Back to Report List</a></p>
                </div>
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
    <script type="text/javascript">

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/report-error.blade.php ENDPATH**/ ?>