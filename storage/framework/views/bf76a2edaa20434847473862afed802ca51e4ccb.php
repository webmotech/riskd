<div class="modal fade company-select-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="text-center" id="exampleModalLongTitle">Select Company</h5>

        </div>
        <form data-parsley-validate="" id="frmselectCompany">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="">Company</label>
                            <select required="" id="select-company" class="form-control">

                            </select>
                        </div>
                    </div>

                </div>
 </div>
            <div class="modal-footer">

                <button type="button" data-dismiss="modal" class="btn btn-warning">Back</button>
                <button type="submit" class="btn btn-primary">Continue</button>
            </div>
        </form>

    </div>
</div>
</div>
<?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/reports/component/company-select-modal.blade.php ENDPATH**/ ?>