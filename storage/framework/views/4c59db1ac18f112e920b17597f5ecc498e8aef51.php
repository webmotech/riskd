<?php $__env->startSection('content'); ?>
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="card-body">

                      <div class="">
                          <div class="col-lg-12 col-md-12">
                              <h3 class="card-title">Registered Company</h3>
                              <p class="text-right">
                                  <a href="<?php echo e(url('admin/company/create')); ?>" class="btn btn-primary text-right"><i class="fa fa-plus"></i>Create Company</a>

                              </p>
                              <table id="company-table" class="table table-bordered">
                                  <thead>
                                  <tr>
                                      <th>ID</th>
                                      <th>Entity Name</th>
                                      <th>ABN</th>
                                      <th>ACN</th>
                                      <th>Action</th>

                                  </tr>
                                  </thead>
                                  <tfoot>
                                  <tr>
                                      <th>ID</th>
                                      <th>Entity Name</th>
                                      <th>ABN</th>
                                      <th>ACN</th>
                                      <th>Action</th>

                                  </tr>
                                  </tfoot>
                              </table>
                          </div>

                      </div>
                  </div>
              </div>
          </div>

      </div>
  </div>

    <?php echo $__env->make('admin.users.form.edit', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('admin.users.form.reset-password', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        refreshTable();
        // Create User
        $('#frmcreateuser').submit(function(e){


            if ($(this).parsley().isValid() ) {
                e.preventDefault();
                $.ajax({
                    method:'POST',
                    url:base_url+'/admin/users',
                    data:$('#frmcreateuser').serialize(),
                    success:function(res){
                        if(res.success){
                            // $(".bd-example-modal-lg").modal('toggle')
                            successNotify(res.msg);
                            setTimeout(function(){
                                location.reload();
                            },1000);
                        }else{
                            failedNotify(res.msg);
                        }

                    }
                });
            }

        });

    } );

// Load Company Data in Table
function refreshTable(){
    var user = '<?php echo e(json_encode(Auth::user()->user_role_id)); ?>';
    $('#company-table').DataTable({
        "ajax": "/company/get-all-companies",
        "order": [[ 0, "desc" ]],
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "abn" },
            { "data": "acn" },
            // {
            //     data: null,
            //     className: "center",
            //     defaultContent: '<a href="" class="editor_edit btn btn-primary"><i class="fa fa-edit"></i>Edit</a><a href="" class="editor_delete btn btn-danger"><i class="fa fa-trash-alt"></i>Delete</a>'
            // },
            {
                data: null,
                render: function (data, type, full, meta) {
                    if(user==1){
                        return '<a href="" class="editor_edit btn btn-primary"><i class="fa fa-edit"></i>Edit</a><a href="" class="editor_delete btn btn-danger"><i class="fa fa-trash-alt"></i>Delete</a>';
                    }else{
                        return '<a href="" class="editor_edit btn btn-primary"><i class="fa fa-edit"></i>Edit</a>';
                    }

                }
            }


        ],

    });
}




    // Edit record
    $('#company-table').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#company-table').DataTable().row($(this).parents('tr')).data();

            location.href = base_url+'/admin/company/'+data.company_token+'/edit';
    } );

    // Remove Record

    $('#company-table').on('click', 'a.editor_delete', function (e) {
        e.preventDefault();
        var data = $('#company-table').DataTable().row($(this).parents('tr')).data();
        $.confirm({
            title: 'Delete!',
            content: 'Are you sure you want to delete?',
            buttons: {
                Delete: function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                       type:'DELETE',
                       url: base_url+'/admin/company/'+data.company_token,
                       success:function(res){
                           if(res.success){
                               successNotify(res.msg);

                               setTimeout(function(){
                                   location.reload();
                               },1000);
                           }else{
                               failedNotify(res.msg);

                           }
                       }
                    });
                },
                cancel: function () {

                }

            }
        });
    } );






</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/company/index.blade.php ENDPATH**/ ?>