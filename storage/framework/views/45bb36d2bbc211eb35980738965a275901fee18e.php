<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <div class="">
                        <div class="col-lg-12 col-md-12">
                            <h3 class="card-title">Registered Users</h3>
                            <p class="text-right">
                                <button type="button" class="btn btn-primary text-right" onclick="loadUserModal()"><i class="fa fa-plus"></i>Create User</button>

                            </p>
                           <table id="user-table" class="table table-bordered">
                               <thead>
                               <tr>
                                   <th>ID</th>
                                   <th>First Name</th>
                                   <th>Last Name</th>
                                   <th>Email</th>
                                   <th>Username</th>
                                   <th>User Role</th>
                                   <th>Action</th>

                               </tr>
                               </thead>
                               <tfoot>
                               <tr>
                                   <th>ID</th>
                                   <th>First Name</th>
                                   <th>Last Name</th>
                                   <th>Email</th>
                                   <th>Username</th>
                                   <th>User Role</th>
                                   <th>Action</th>

                               </tr>
                               </tfoot>
                           </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php echo $__env->make('admin.users.form.create', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('admin.users.form.edit', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('admin.users.form.reset-password', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        refreshTable();
        $('#designation_area').hide();
        // Create User
        $('#frmcreateuser').submit(function(e){


            if ($(this).parsley().isValid() ) {
                e.preventDefault();
                $.ajax({
                    method:'POST',
                    url:base_url+'/admin/users',
                    data:$('#frmcreateuser').serialize(),
                    success:function(res){
                        if(res.success){
                            // $(".bd-example-modal-lg").modal('toggle')
                            successNotify(res.msg);
                            setTimeout(function(){
                                location.reload();
                            },1000);
                        }else{
                            failedNotify(res.msg);
                        }

                    }
                });
            }

        });

    } );

// Load User Data in Table
function refreshTable(){
    $('#user-table').DataTable({
        "ajax": "/users/get-all-users",
        "columns": [
            { "data": "id" },
            { "data": "fname" },
            { "data": "lname" },
            { "data": "email" },
            { "data": "username" },
            { "data": "user_role" },
            {
                data: null,
                className: "center",
                defaultContent: '<a href="" class="editor_edit btn btn-primary"><i class="fa fa-edit"></i>Edit</a>' +
                    '<a href="" class="reset_password btn btn-warning"><i class="fa fa-user-secret"></i>Reset Password</a>' +
                    '<a href="" class="remove_user btn btn-danger"><i class="fa fa-trash"></i>Delete</a>'
            }
        ],

    });
}

// Reset Password
    $('#frmresetpassword').submit(function(e){
        e.preventDefault();
        if ($(this).parsley().isValid() ) {
            var id = $('#hdnuserid').val();
            $.ajax({
                type : 'POST',
                url : base_url+'/admin/authenticate/change-password/'+id,
                data : $('#frmresetpassword').serialize(),
                success : function(res){

                    if(res.success){
                        successNotify(res.msg);
                        $('#user-password-reset').modal('toggle');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        failedNotify(res.msg);
                        $('#user-password-reset').modal('toggle');
                    }
                }

            })
        }
    });
    // Edit User

    $('#frmedituser').submit(function(e){
        e.preventDefault();
        if ($(this).parsley().isValid() ) {
            var id = $('#edithdnid').val();
            $.ajax({
                type : 'PUT',
                url : base_url+'/admin/users/'+id,
                data : $('#frmedituser').serialize(),
                success : function(res){

                    if(res.success){
                        successNotify(res.msg);
                        $('#user-edit').modal('toggle');
                        setTimeout(function(){
                            location.href=base_url+'/admin/users';
                        },1000);
                    }else{
                        failedNotify(res.msg);
                        $('#user-edit').modal('toggle');
                    }
                }

            })
        }

    });

    // Edit record
    $('#user-table').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#user-table').DataTable().row($(this).parents('tr')).data();

        // Load Designations
        loadDesignationsforEdit(data.designation_id);

        // Load USerRole
        console.log(data);
        loadUserRoleForEditModal(data.user_role_id);
        $('#editfname').val(data.fname);
        $('#editlname').val(data.lname);
        $('#editemail').val(data.email);
        $('#editaddress').val(data.address);
        $('#edittelephone').val(data.telephone);
        $('#edithdnid').val(data.id);
        $('#editusername').val(data.username);


        $('#user-edit').modal({backdrop: 'static', keyboard: false});

        var userRole = data.user_role_id;
        if((userRole==3) || userRole==4){
            $('#editdesignation_area').show();
        }else{
            $('#editdesignation_area').hide();
        }


        // $('#user-edit').modal('toggle');
    } );

    // Remove User
    $('#user-table').on('click', 'a.remove_user', function (e) {
        e.preventDefault();
        var data = $('#user-table').DataTable().row($(this).parents('tr')).data();

        $.confirm({
            title: 'Delete!',
            content: 'Are you sure you want to delete?',
            buttons: {
                Delete: function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type:'DELETE',
                        url: base_url+'/admin/users/'+data.id,
                        success:function(res){
                            if(res.success){
                                successNotify(res.msg);

                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }else{
                                failedNotify(res.msg);

                            }
                        }
                    });
                },
                cancel: function () {

                }

            }
        });
    } );
    // Reset Password modal

    $('#user-table').on('click', 'a.reset_password', function (e) {
        e.preventDefault();
        var data = $('#user-table').DataTable().row($(this).parents('tr')).data();

        $('#resetusername').val(data.username);
        $('#hdnuserid').val(data.id);
       $('#user-password-reset').modal({backdrop: 'static', keyboard: false});
        // $('#user-edit').modal('toggle');
    } );

    // Load Designation

    function loadDesignations(){
        $.ajax({
            type:'GET',
            url: base_url+'/designations',
            success:function(res){
                var html = '';

                for(var x=0; x<res.length; x++){
                    html+='<option value="'+res[x].id+'">'+res[x].name+'</option>';
                }
                $('#designation').html(html);

            }
        });
    }

    // Load Designation for Edit Modal
    function loadDesignationsforEdit(id){
        $.ajax({
            type:'GET',
            url: base_url+'/designations',
            success:function(res){
                var html = '';

                for(var x=0; x<res.length; x++){
                    if(res[x].id==id){

                        html += '<option selected value="'+res[x].id+'">'+res[x].name+'</option>';
                    }else{

                        html+='<option value="'+res[x].id+'">'+res[x].name+'</option>';
                    }
                }
                $('#editdesignation').html(html);

            }
        });
    }
    // load User Create Modal
    function loadUserModal(){
        $.ajax({
            url:base_url+'/user-roles',
            type:'GET',
            success:function(res){

                var html = '';
                html+='<option value="">Select User Type</option>';
                for(var x = 0; x<res.length; x++){
                    html += '<option value="'+res[x].id+'">'+res[x].name+'</option>';
                }

                $('#user-create').modal({backdrop: 'static', keyboard: false});
                loadDesignations();
                $('#user_role').html(html);
            }
        });

    }

    // Manage Designation with User Role
$('#user_role').change(function(){
   var userRole = $('#user_role').val();
  if((userRole==3) || userRole==4){
      $('#designation_area').show();
  }else{
      $('#designation_area').hide();
  }
});

    $('#edituser_role').change(function(){
        var userRole = $('#edituser_role').val();
        if((userRole==3) || userRole==4){
            $('#editdesignation_area').show();
        }else{
            $('#editdesignation_area').hide();
        }
    });
    // Load User Roles For Edit Modal
    function loadUserRoleForEditModal(id){

        $.ajax({
            url:base_url+'/user-roles',
            type:'GET',
            success:function(res){

                var html = '';

                for(var x = 0; x<res.length; x++){
                    if(res[x].id==id){

                        html += '<option selected value="'+res[x].id+'">'+res[x].name+'</option>';
                    }else{

                        html += '<option value="'+res[x].id+'">'+res[x].name+'</option>';
                    }
                }

                $('#edituser_role').html(html);
            }
        });
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp_new\htdocs\riskd\resources\views/admin/users/index.blade.php ENDPATH**/ ?>