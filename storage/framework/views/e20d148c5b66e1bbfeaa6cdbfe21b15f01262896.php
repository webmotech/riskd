<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Risk-D</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="favicon.ico" type="image/x-icon" />


    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/fontawesome-free/css/all.min.cs')); ?>">




    <?php echo $__env->yieldContent('custom-css'); ?>
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="auth-wrapper">
    <?php echo $__env->yieldContent('content'); ?>
</div>


</div>






<?php echo $__env->yieldContent('custom-js'); ?>
</body>
</html>
<?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/layout/print-master.blade.php ENDPATH**/ ?>