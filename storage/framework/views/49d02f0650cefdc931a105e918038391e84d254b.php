<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Risk-D</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="favicon.ico" type="image/x-icon" />

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/bootstrap/dist/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/fontawesome-free/css/all.min.cs')); ?>s">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/icon-kit/dist/css/iconkit.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/ionicons/dist/css/ionicons.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/perfect-scrollbar/css/perfect-scrollbar.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/jvectormap/jquery-jvectormap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/weather-icons/css/weather-icons.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/c3/c3.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/owl.carousel/dist/assets/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/owl.carousel/dist/assets/owl.theme.default.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/dist/css/theme.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/af-2.3.4/b-1.6.1/datatables.min.css"/>
    <script src="<?php echo e(asset('theme/src/js/vendor/modernizr-2.8.3.min.js')); ?>"></script>


    <link rel="stylesheet" href="<?php echo e(asset('assets/css/animate.css')); ?>"/>

    <link rel="stylesheet" href="<?php echo e(asset('assets/css/jquery-confirm.min.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/parsley.css')); ?>"/>
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="auth-wrapper">
    <?php echo $__env->yieldContent('content'); ?>
</div>


</div>





<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script src="<?php echo e(asset('theme/plugins/popper.js/dist/umd/popper.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/screenfull/dist/screenfull.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/datatables.net-responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')); ?>"></script>

<script src="<?php echo e(asset('theme/plugins/moment/moment.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/d3/dist/d3.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/plugins/c3/c3.min.js')); ?>"></script>
<script src="<?php echo e(asset('theme/js/tables.js')); ?>"></script>

<script src="<?php echo e(asset('theme/js/charts.js')); ?>"></script>
<script src="<?php echo e(asset('theme/dist/js/theme.min.js')); ?>"></script>

<script src="<?php echo e(asset('assets/js/bootstrap-notify.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/notification.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/jquery-confirm.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/bootstrap-datepicker.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/parsley.min.js')); ?>"></script>


<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/af-2.3.4/b-1.6.1/datatables.min.js"></script>

<script type="text/javascript">
    var base_url = '<?php echo e(url('')); ?>';

</script>
<?php echo $__env->yieldContent('custom-js'); ?>
</body>
</html>
<?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/layout/login-master.blade.php ENDPATH**/ ?>