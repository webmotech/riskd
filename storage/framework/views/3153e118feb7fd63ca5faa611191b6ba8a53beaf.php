<?php $__env->startSection('custom-css'); ?>
    <style type="text/css">
        .previous .table td, tr .table th{
            padding: 0.7em !important;
            width: 100%;
        }
        #input-area select {
            height: 33px;
        }




    </style>
    <?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class=" bg-primary text-light p-1">
                    <h1 class="text-center"> New Report Creation</h1>

                </div>
                <form id="frmreportcreate" data-parsley-validate="">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="hdncompanyToken" id="hdncompanyToken"/>
                <div class="card-body">
                    <p class="text-right"><span id="validate-year" class="alert alert-danger" style="display: none">You Can add Maximum 3 years</span> &nbsp;<button type="button" onclick="createYear()" class="btn btn-success"><i class="fa fa-plus"></i>Add Year</button><button type="button" onclick="changeCompany()" class="btn btn-warning"><i class="fa fa-plus"></i>Change Company</button></p>
                    <div class="row">
                        <div class="col-md-6 "></div>
                        <div class="col-md-6 ">
                                <div class=" form-group">
                                    <select required="" name="report_type" id="report_type" class="form-control">
                                        <option value="">Select Report Type</option>
                                        <?php $__currentLoopData = config('reports_config.report_type'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$reportType): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($key); ?>"><?php echo e($reportType); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>

                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-7 col-xl-7 table-responsive text-nowrap col-sm-7 col-lg-7 previous">

                           <table class="table table-bordered" id="tableleft">
                               <tr class=" border-0">
                                   <td class="p-1 text-light border-0" colspan="<?php echo e($no_of_years+1); ?>">
                                       <strong>&nbsp;</strong>
                                   </td>

                               </tr>
                                <tr class="">
                                   <td class="">ABN</td>
                                    <?php if($no_of_years > 0): ?>
                                        <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                   <td><?php echo e($report['abn']); ?></td>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                </tr>
                               <tr class="">
                                   <td class="">ACN</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e($report['acn']); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Company</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e($report->company->name); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Rounding</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(config('reports_config.rounding')[$previousData['rounding']]); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Base Currency</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e($previousData['base_currency']); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Quality</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(config('reports_config.quality')[$previousData['quality']]); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Reporting Period (months)</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e($previousData['report_period_months']); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Scope</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e($previousData['scope']); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Confidentiality Record</td>
                                   <?php if($no_of_years > 0): ?>

                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e($previousData->confidentiality['name']); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Financial Year</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e($previousData['financial_year']); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Month</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e($previousData['financial_month']); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                       <strong>Income Statement</strong>
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class="">Sales</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['sales'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Cost of sales</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['cost_of_sales'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Gross profit</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['gross_profit'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Other income</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['other_income'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Depreciation</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['depreciation'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Amortisation</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['amortisation'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Impairment</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['impairment'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Interest expense gross</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['interest_expenses_gross'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Operating lease expense</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['operating_lease_expenses'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Finance lease hire purchase charges</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['finance_lease_hire_expenses_charges'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Non-recurring gains (losses)</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['non_recurring_gain_loses'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Other gains (losses)</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['other_gain_loses'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Other expenses</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['other_expenses'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">EBIT</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['ebit'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">EBITDA</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['ebitda'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Normalised EBITDA</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['normalized_ebitda'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Profit before tax</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['profit_before_tax'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Profit before tax after abnormals</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['profit_before_tax_after_abnormals'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Tax benefit (expense)</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['tax_benefit_expenses'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Profit after tax</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['profit_after_tax'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Distribution or dividends</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['distribution_ordividends'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Other post tax items - gains/ (losses)</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['other_post_tax_items_gains_losses'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Profit after tax distribution</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['profit_after_tax_distribution'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                       <strong>Balance Sheet Assets</strong>
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                      Assets
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class="">Cash</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['cash'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Trade debtors</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['trade_debtors'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Total inventories</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['total_inventories'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Loans to related parties</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['loan_to_related_parties_1'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Other current assets</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['other_current_assets'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Total current assets</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['total_current_assets'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Fixed assets</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['fixed_assets'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Net intangibles</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['net_intangibles'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Loan to related parties</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['loan_to_related_parties_2'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Other non-current assets</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['other_non_current_assets'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Total non-curent assets</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['total_non_current_assets'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Total assets</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['total_assets'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                       <strong>Liabilities</strong>
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class="">Trade creditors</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['trade_creditors'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Interest bearing debt</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['interest_bearing_debt_1'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Loan from related parties</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['loan_from_related_parties_1'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Other current liabilities</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['other_current_liabilities'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Total current liabilities</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['total_current_liabilities'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Interest bearing debt</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['interest_bearing_debt_2'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Loans from related parties</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['loan_from_related_parties_2'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Other non-current liabilities</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['other_non_current_liabilities'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Total non-current liabilities</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['total_non_current_liabilities'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Total liabilities</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['total_liabilities'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                       <strong>Equity</strong>
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class="">Share capital</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['share_capital'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Preference shares</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['prefence_shares'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Treasury shares</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['threasury_shares'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Equity ownerships</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['equity_ownerships'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Total reserves</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['total_reserves'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Retained earnings</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['retained_earning'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Minorty interest</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['minorty_interest'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Total equity</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['total_equity'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Balance</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['balance'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>

                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="<?php echo e($no_of_years+1); ?>">
                                       <strong>Additional Information</strong>
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class="">Operating cash flow</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['operating_cash_flow'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Contingent liabilities</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['contingent_liabilities'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Other commitments</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['other_commitmentes'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                               <tr class="">
                                   <td class="">Operating lease outstandings</td>
                                   <?php if($no_of_years > 0): ?>
                                       <?php $__currentLoopData = $previous; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $previousData): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <td><?php echo e(number_format($previousData['operating_lease_outstanding'])); ?></td>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                               </tr>
                            </table>

                        </div>

                        <div class="col-md-5 col-xl-5 col-sm-5 col-lg-5 pl-1">


                           <div class="row" id="input-area">




                           </div>


                        </div>

                    </div>

                    <div class="row">
                        <hr>
                        <div class="col-md-12" id="summery-area">
                            


                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="text-right">
                                <a href="<?php echo e(url()->previous()); ?>" class="btn btn-warning">Back</a>
                                <button type="button" id="savebtn" class="btn btn-success">Save and Send for Approval</button>
                            </p>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-js'); ?>
  <script type="text/javascript" src="<?php echo e(asset('assets/modules/js/reports/create-report.js')); ?>"></script>
  <script type="text/javascript" src="<?php echo e(asset('assets/modules/js/reports/report-calculation.js')); ?>"></script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\riskd\resources\views/admin/reports/create.blade.php ENDPATH**/ ?>