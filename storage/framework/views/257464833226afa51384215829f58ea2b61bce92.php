<div class="nav-item active">
    <a href="<?php echo e(url('admin/dashboard')); ?>"><i class="ik ik-bar-chart-2"></i><span>Dashboard</span></a>
</div>


<div class="nav-item has-sub">
    <a href="javascript:void(0)"><i class="fa fa-user"></i><span>Users</span> </a>
    <div class="submenu-content">
        <a href="<?php echo e(url('admin/users')); ?>" class="menu-item"><i class="fa fa-user"></i>View List</a>

    </div>
</div>
<div class="nav-item has-sub">
    <a href="javascript:void(0)"><i class="fa fa-building"></i><span>Company</span> </a>
    <div class="submenu-content">
        <a href="<?php echo e(url('admin/company')); ?>" class="menu-item"><i class="fa fa-building"></i>View List</a>
        <a href="<?php echo e(url('admin/company/create')); ?>" class="menu-item"><i class="fa fa-plus"></i>Add New Company</a>

    </div>
</div>
<div class="nav-item has-sub">
    <a href="javascript:void(0)"><i class="fa fa-building"></i><span>Reports</span> </a>
    <div class="submenu-content">

        <a href="#" class="menu-item create-report"><i class="fa fa-plus"></i>Create New Report</a>
        <a href="<?php echo e(url('admin/reports/list/pending')); ?>" class="menu-item"><i class="fa fa-plus"></i>Pending Reports</a>
        <a href="<?php echo e(url('admin/reports/list/reject')); ?>" class="menu-item"><i class="fa fa-plus"></i>Rejected Reports</a>
        <a href="<?php echo e(url('admin/reports/list/pending-for-approval')); ?>" class="menu-item"><i class="fa fa-plus"></i>Pending Approval Reports</a>
        <a href="<?php echo e(url('admin/reports/list/approved-report')); ?>" class="menu-item"><i class="fa fa-plus"></i>Approved Reports</a>

    </div>
</div>
<div class="nav-item active">
    <a href="<?php echo e(url('admin/settings')); ?>"><i class="ik ik-settings"></i><span>Settings</span></a>
</div>
<?php /**PATH D:\xampp_new\htdocs\riskd\resources\views/admin/layout/menu/admin-menu.blade.php ENDPATH**/ ?>