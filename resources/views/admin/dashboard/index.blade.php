@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-12">
                            <h1 class=" text-center">Welcome to Risk D</h1>
                            <p class="text-center alert alert-success">Version 1.1</p>
                            <div id="visitfromworld" style="width:100%; height:350px"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    @endsection

@section('custom-js')

    @endsection
