@extends('admin.layout.master')

@section('content')

    <form id="frmcreatecompany" data-parsley-validate="">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h3>Create New Company</h3>
                        <hr>
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Entity Details</h3>
                                <span id="abnvalidate"></span>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3 sel2">
                                            <label>Country</label>
                                            <select required="" class="form-control country" id="ddlcountry" name="ddlcountry">
                                               <option value="">--Select Option--</option>
                                                @foreach($country as $countryData)
                                                    <option value="{{$countryData['country_code']}}">{{$countryData['name']}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Entity Name</label>
                                            <input type="text" required="" class="form-control" id="entity_name"
                                                   name="entity_name" placeholder="Entity Name">
                                        </div>

                                        <div class="col-md-3 sel2">
                                            <label>Entity Type</label>
                                            <select class="form-control entity-type" required="" id="ddlentitytype" name="ddlentitytype">
                                                <option value="">--Select option--</option>
                                                @foreach($entity_type as $entity)

                                                    <option value="{{$entity['id']}}">{{$entity['name']}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">ABN</label>
                                            <input type="text" class="form-control" id="abn" name="abn"
                                                   placeholder="ABN">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-3">
                                            <label for="">ACN</label>
                                            <input type="text" class="form-control" id="acn"
                                                   name="acn" placeholder="ACN">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">RBN</label>
                                            <input type="text" class="form-control" id="rbn"
                                                   name="rbn" placeholder="RBN">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Equity</label>
                                            <input type="text" class="form-control" id="equity"
                                                   name="equity" placeholder="Equity">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Date Established</label>
                                            <input type="text" value="{{date('Y-m-d')}}" required="" class="form-control" id="date_established"
                                                   name="date_established" placeholder="Date Established">
                                        </div>

                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label for="">Confidentiality</label>
                                            <select class="form-control" id="confidentiality" name="confidentiality">
                                                <option value="">--Select Option--</option>
                                                @foreach($confidentiality as $confident)
                                                    <option value="{{$confident['id']}}">{{$confident['name']}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Portfolio Analysis Status</label>
                                            <select class="form-control" id="portfolio_status" name="portfolio_status">
                                                <option>--Select Option--</option>
                                                @foreach(config('portfolio_status.status') as $status)
                                                    <option value="{{$status['value']}}">{{$status['name']}}</option>
                                                    @endforeach
                                            </select>
                                        </div>


                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Entity Address</h3>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label for="">Unit Number</label>
                                            <input type="text" class="form-control" id="unit_number"
                                                   name="unit_number" placeholder="Unit Number">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Street Number</label>
                                            <input type="text" class="form-control" id="street_number"
                                                   name="street_number" placeholder="Street Number">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Street Name</label>
                                            <input type="text" class="form-control" id="street_name"
                                                   name="street_name" placeholder="Street Name">
                                        </div>

                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label for="">Suburb</label>
                                            <input type="text" class="form-control" id="suburb"
                                                   name="suburb" placeholder="Subarb">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">State</label>
                                            <input type="text" class="form-control" id="state"
                                                   name="state" placeholder="State">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Post Code</label>
                                            <input type="text" class="form-control" id="postal_code"
                                                   name="postal_code" placeholder="Post Code">
                                        </div>

                                    </div>

                                </div>



                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Contact Telephone 1</h3>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <label for="">Contact Type</label>
                                            <select id="contact_type" name="contact_type" class="form-control">
                                           <option value="">--Select--</option>
                                                @foreach(config('contact_type.contact') as $key=>$contact)
                                                    <option value="{{$key}}">{{$contact}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Number</label>
                                            <input type="text" class="form-control" id="contact_type_number"
                                                   name="contact_type_number" placeholder="Numbers">
                                        </div>


                                    </div>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Contact Telephone 2</h3>
                                <div class="form-group">
                                    <div class="row">
                                        {{csrf_field()}}
                                        <div class="col-md-6">
                                            <label for="">Contact Type</label>
                                            <select id="business_type" name="business_type" class="form-control">
                                                <option value="">--Select--</option>
                                                @foreach(config('contact_type.contact') as $key=>$contact)
                                                    <option value="{{$key}}">{{$contact}}</option>
                                                @endforeach
                                            </select>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Number</label>
                                            <input type="text" class="form-control" id="business_type_number"
                                                   name="business_type_number" placeholder="Numbers">
                                        </div>


                                    </div>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">ANZIC Classification</h3>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-3 sel2">
                                            <label for="">Division</label>
                                            <select name="anzic_division" id="anzic_division" class="form-control anzic-division">
                                                <option value="">--Select Option--</option>
                                                @foreach($division as $divisionData)
                                                    <option value="{{$divisionData['id']}}">{{$divisionData['name']}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Sub Division</label>
                                            <select name="anzic_subdivision" id="anzic_subdivision" class="form-control anzic-sub-division">

                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Group</label>
                                            <select name="anzic_group" id="anzic_group" class="form-control anzic-group">

                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Class</label>
                                            <select name="anzic_class" id="anzic_class" class="form-control anzic-class">

                                            </select>
                                        </div>


                                    </div>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Primary Classification</h3>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-3 sel2">
                                            <label for="">Division</label>
                                            <select name="primary_division" id="primary_division" class="form-control primary-division">
                                               <option value="">--Select Option--</option>
                                                @foreach($division as $divisionData)
                                                    <option value="{{$divisionData['id']}}">{{$divisionData['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Sub Division</label>
                                            <select name="primary_subdivision" id="primary_subdivision" class="form-control primary-sub-division">

                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Group</label>
                                            <select name="primary_group" id="primary_group" class="form-control primary-group">

                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Class</label>
                                            <select name="primary_class" id="primary_class" class="form-control primary-class">

                                            </select>
                                        </div>


                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">
                                   <div class="col-md-12">
                                       <p class="text-right">
                                           <button type="submit" data-style="expand-right" class="company-button btn btn-primary">Create New Company</button>
                                       </p>
                                   </div>
                                    </div>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>


@endsection

@section('custom-js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#abnvalidate').hide();
        $('.country').select2();
        $('.anzic-division').select2();
        $('.anzic-sub-division').select2();
        $('.anzic-group').select2();
        $('.anzic-class').select2();
        $('.primary-division').select2();
        $('.primary-sub-division').select2();
        $('.primary-group').select2();
        $('.primary-class').select2();
        $('.entity-type').select2();
        $('#date_established').Zebra_DatePicker();

        // Create New Company
       $('#frmcreatecompany').submit(function (e) {
        e.preventDefault();

        var abn = $('#abn').val();
        var acn = $('#acn').val();
        var rbn = $('#rbn').val();

        if((abn=='') && (acn=='') && rbn=='' ){
           $('#abnvalidate').html('<p class="alert alert-danger">Please fill atleast one field from ABN, ACN or RBN</p>');
            $('#abnvalidate').show();
            $("html, body").animate({ scrollTop: 0 }, "slow");
            setTimeout(function () {
                $('#abnvalidate').hide();
            },4000);
            return;
        }

           if ($(this).parsley().isValid() ) {
                $.ajax({
                   type:'POST',
                   url : base_url+'/admin/company',
                   data : $('#frmcreatecompany').serialize(),
                   success:function(res){

                       if(res.success){
                           successNotify(res.msg);

                           setTimeout(function(){
                               location.href = base_url+'/admin/company';
                           },1000);
                       }else{

                           failedNotify(res.msg);

                       }
                   },
                    error:function(res){
                        failedNotify(res.msg);
                    }
                });
           }

       });
    });

    // Load Anzic Group
    $('.anzic-sub-division').on('select2:select', function (e) {
        $('#anzic_class').html('');
        var id = e.params.data.id;
        $.ajax({
           type : 'get',
           url : base_url+'/get-company-group-by-sub-divisions/'+id,
           success:function(res){

                var data = res;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

               $('#anzic_group').html(html);


           }

        });
    });

    // Load Anzic Sub Division
    $('.anzic-division').on('select2:select', function (e) {
        $('#anzic_group').html('');
        $('#anzic_class').html('');
        var id = e.params.data.id;
        $.ajax({
            type : 'get',
            url : base_url+'/get-sub-divisions-by-division/'+id,
            success:function(res){

                var data = res.data;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

                $('#anzic_subdivision').html(html);

            }

        });
    });

    // Load Anzic Class
    $('.anzic-group').on('select2:select', function (e) {
        var id = e.params.data.id;
        $.ajax({
            type : 'get',
            url : base_url+'/get-class-by-group/'+id,
            success:function(res){

                var data = res;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

                $('#anzic_class').html(html);

            }

        });
    });


    // ..............

    // Load Primary Group
    $('.primary-sub-division').on('select2:select', function (e) {
        $('#primary_class').html('');
        var id = e.params.data.id;
        $.ajax({
            type : 'get',
            url : base_url+'/get-company-group-by-sub-divisions/'+id,
            success:function(res){

                var data = res;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

                $('#primary_group').html(html);


            }

        });
    });

    // Load Primary Sub Division
    $('.primary-division').on('select2:select', function (e) {
        $('#primary_group').html('');
        $('#primary_class').html('');
        var id = e.params.data.id;
        $.ajax({
            type : 'get',
            url : base_url+'/get-sub-divisions-by-division/'+id,
            success:function(res){

                var data = res.data;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

                $('#primary_subdivision').html(html);

            }

        });
    });

    // Load Primary Class
    $('.primary-group').on('select2:select', function (e) {
        var id = e.params.data.id;
        $.ajax({
            type : 'get',
            url : base_url+'/get-class-by-group/'+id,
            success:function(res){

                var data = res;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

                $('#primary_class').html(html);

            }

        });
    });
</script>
@endsection
