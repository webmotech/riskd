@extends('admin.layout.master')

@section('content')

    <form id="frmeditcompany" data-parsley-validate="">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h3>Modify Company</h3>
                        <hr>
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Entity Details</h3>
                                <span id="abneditvalidate"></span>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3 sel2">
                                            <label for="">Country</label>
                                            <select class="form-control country" id="ddlcountry" name="ddlcountry">

                                                @foreach($country as $countryData)
                                                    @if($company['country_code']==$countryData['country_code'])
                                                        <option selected value="{{$countryData['country_code']}}">{{$countryData['name']}}</option>
                                                    @else
                                                    <option value="{{$countryData['country_code']}}">{{$countryData['name']}}</option>
                                                    @endif
                                                        @endforeach
                                            </select>
                                        </div>
                                        <input type="hidden" name="hdntoken" id="hdntoken" value="{{$company['company_token']}}">
                                        <div class="col-md-3">
                                            <label for="">Entity Name</label>
                                            <input type="text" required="" value="{{$company['name']}}" class="form-control" id="entity_name"
                                                   name="entity_name" placeholder="Entity Name">
                                        </div>

                                        <div class="col-md-3 sel2">
                                            <label for="">Entity Type</label>
                                            <select class="form-control entity-type" required="" id="ddlentitytype" name="ddlentitytype">
                                                <option value="">--Select option--</option>
                                                @foreach($entity_type as $entity)
                                                    @if($company['entity_type_id']==$entity['id'])
                                                        <option selected value="{{$entity['id']}}">{{$entity['name']}}</option>
                                                        @else
                                                    <option value="{{$entity['id']}}">{{$entity['name']}}</option>
                                                        @endif
                                                        @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">ABN</label>
                                            <input type="text" value="{{$company['abn']}}" class="form-control" id="abn" name="abn"
                                                   placeholder="ABN">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-3">
                                            <label for="">ACN</label>
                                            <input type="text" value="{{$company['acn']}}" class="form-control" id="acn"
                                                   name="acn" placeholder="ACN">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">RBN</label>
                                            <input type="text"  value="{{$company['rbn']}}" class="form-control" id="rbn"
                                                   name="rbn" placeholder="RBN">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Equity</label>
                                            <input type="text"  class="form-control" value="{{$company['equity']}}" id="equity"
                                                   name="equity" placeholder="Equity">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="">Date Established</label>
                                            <input type="text" required="" value="{{$company['established_date']}}" class="form-control" id="date_established"
                                                   name="date_established" placeholder="Date Established">
                                        </div>

                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label for="">Confidentiality</label>
                                            <select class="form-control" id="confidentiality" name="confidentiality">
                                                <option value="">--Select Option--</option>
                                                @foreach($confidentiality as $confident)
                                                    @if($company['confidentialiity_id'] == $confident['id'])
                                                        <option selected value="{{$confident['id']}}">{{$confident['name']}}</option>
                                                        @else
                                                    <option value="{{$confident['id']}}">{{$confident['name']}}</option>
                                                        @endif
                                                        @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Portfolio Analysis Status</label>
                                            <select class="form-control" id="portfolio_status" name="portfolio_status">
                                                <option>--Select Option--</option>
                                                @foreach(config('portfolio_status.status') as $status)
                                                    @if($company['portfolio_analysis_status']==$status['value'])
                                                        <option selected value="{{$status['value']}}">{{$status['name']}}</option>
                                                        @else
                                                    <option value="{{$status['value']}}">{{$status['name']}}</option>
                                                    @endif
                                                    @endforeach
                                            </select>
                                        </div>


                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Entity Address</h3>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label for="">Unit Number</label>
                                            <input type="text" value="{{$company['address_unit_number']}}" class="form-control" id="unit_number"
                                                   name="unit_number" placeholder="Unit Number">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Street Number</label>
                                            <input type="text" class="form-control" value="{{$company['address_street_number']}}" id="street_number"
                                                   name="street_number" placeholder="Street Number">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Street Name</label>
                                            <input type="text" class="form-control" value="{{$company['address_street_name']}}" id="street_name"
                                                   name="street_name" placeholder="Street Name">
                                        </div>

                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <label for="">Suburb</label>
                                            <input type="text" value="{{$company['address_suburb']}}" class="form-control" id="suburb"
                                                   name="suburb" placeholder="Subarb">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">State</label>
                                            <input type="text" value="{{$company['address_state']}}" class="form-control" id="state"
                                                   name="state" placeholder="State">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="">Post Code</label>
                                            <input type="text" value="{{$company['address_postal_code']}}" class="form-control" id="postal_code"
                                                   name="postal_code" placeholder="Postal Code">
                                        </div>

                                    </div>

                                </div>



                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Contact Telephone 1</h3>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <label for="">Contact Type</label>
                                            <select id="contact_type" name="contact_type" class="form-control">
                                           <option value="">--Select--</option>
                                                @foreach(config('contact_type.contact') as $key=>$contact)
                                                    @if($company['contact_telephone_type']==$key)
                                                        <option selected value="{{$key}}">{{$contact}}</option>
                                                        @else
                                                    <option value="{{$key}}">{{$contact}}</option>
                                                    @endif
                                                    @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Number</label>
                                            <input type="text" value="{{$company['contact_telephone_number']}}" class="form-control" id="contact_type_number"
                                                   name="contact_type_number" placeholder="Numbers">
                                        </div>


                                    </div>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Business Telephone 2</h3>
                                <div class="form-group">
                                    <div class="row">
                                        {{csrf_field()}}
                                        <div class="col-md-6">
                                            <label for="">Contact Type</label>
                                            <select id="business_type" name="business_type" class="form-control">
                                                <option value="">--Select--</option>
                                                @foreach(config('contact_type.contact') as $key=>$contact)
                                                    @if($company['business_telephone_type']==$key)
                                                        <option selected value="{{$key}}">{{$contact}}</option>
                                                    @else
                                                    <option value="{{$key}}">{{$contact}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">Number</label>
                                            <input type="text" value="{{$company['business_telephone_number']}}" class="form-control" id="business_type_number"
                                                   name="business_type_number" placeholder="Numbers">
                                        </div>


                                    </div>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">ANZIC Classification</h3>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-3 sel2">
                                            <label for="">Division</label>
                                            <select name="anzic_division" id="anzic_division" class="form-control anzic-division">
                                                <option value="">--Select Option--</option>
                                                @foreach($division as $divisionData)
                                                    @if($company['anzic_classification_division_id']==$divisionData['id'])

                                                        <option selected value="{{$divisionData['id']}}">{{$divisionData['name']}}</option>
                                                    @else
                                                    <option value="{{$divisionData['id']}}">{{$divisionData['name']}}</option>
                                                        @endif
                                                        @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Sub Division</label>
                                            <select name="anzic_subdivision" id="anzic_subdivision" class="form-control anzic-sub-division">
                                                <option value="">--Select Option--</option>
                                                @foreach($sub_division as $sub)
                                                    @if($company['anzic_classification_sub_division_id'] == $sub['id'])
                                                        <option selected value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                        @else
                                                    <option value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                        @endif
                                                        @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Group</label>
                                            <select name="anzic_group" id="anzic_group" class="form-control anzic-group">
                                                <option value="">--Select Option--</option>
                                                @foreach($group as $sub)
                                                    @if($company['anzic_classification_group_id'] == $sub['id'])
                                                        <option selected value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                    @else
                                                        <option value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Class</label>
                                            <select name="anzic_class" id="anzic_class" class="form-control anzic-class">
                                                <option value="">--Select Option--</option>
                                                @foreach($class as $sub)
                                                    @if($company['anzic_classification_class_id'] == $sub['id'])
                                                        <option selected value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                    @else
                                                        <option value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>


                                    </div>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Primary Classification</h3>
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-3 sel2">
                                            <label for="">Division</label>
                                            <select name="primary_division" id="primary_division" class="form-control primary-division">
                                               <option value="">--Select Option--</option>
                                                @foreach($division as $divisionData)
                                                    @if($company['primary_classification_division_id']==$divisionData['id'])

                                                        <option selected value="{{$divisionData['id']}}">{{$divisionData['name']}}</option>
                                                    @else
                                                    <option value="{{$divisionData['id']}}">{{$divisionData['name']}}</option>
                                                    @endif
                                                        @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Sub Division</label>
                                            <select name="primary_subdivision" id="primary_subdivision" class="form-control primary-sub-division">
                                                <option value="">--Select Option--</option>
                                                @foreach($sub_division as $sub)
                                                    @if($company['primary_classification_sub_division_id'] == $sub['id'])
                                                        <option selected value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                    @else
                                                        <option value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Group</label>
                                            <select name="primary_group" id="primary_group" class="form-control primary-group">
                                                <option value="">--Select Option--</option>
                                                @foreach($group as $sub)
                                                    @if($company['primary_classification_group_id'] == $sub['id'])
                                                        <option selected value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                    @else
                                                        <option value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 sel2">
                                            <label for="">Class</label>
                                            <select name="primary_class" id="primary_class" class="form-control primary-class">
                                                <option value="">--Select Option--</option>
                                                @foreach($class as $sub)
                                                    @if($company['primary_classification_class_id'] == $sub['id'])
                                                        <option selected value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                    @else
                                                        <option value="{{$sub['id']}}">{{$sub['name']}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>


                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">
                                   <div class="col-md-12">
                                       <p class="text-right">
                                           <button type="submit" data-style="expand-right" class="company-button btn btn-primary">Update Company</button>
                                       </p>
                                   </div>
                                    </div>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>


@endsection

@section('custom-js')
<script type="text/javascript">
    $(document).ready(function(){
        $('.country').select2();
        $('.anzic-division').select2();
        $('.anzic-sub-division').select2();
        $('.anzic-group').select2();
        $('.anzic-class').select2();
        $('.primary-division').select2();
        $('.primary-sub-division').select2();
        $('.primary-group').select2();
        $('.primary-class').select2();
        $('.entity-type').select2();
        $('#date_established').Zebra_DatePicker();

        // Create New Company
       $('#frmeditcompany').submit(function (e) {
        e.preventDefault();

           var abn = $('#abn').val();
           var acn = $('#acn').val();
           var rbn = $('#rbn').val();

           if((abn=='') && (acn=='') && rbn=='' ){
               $('#abneditvalidate').html('<p class="alert alert-danger">Please fill atleast one field from ABN, ACN or RBN</p>');
               $('#abneditvalidate').show();
               $("html, body").animate({ scrollTop: 0 }, "slow");
               setTimeout(function () {
                   $('#abneditvalidate').hide();
               },4000);
               return;
           }
           if ($(this).parsley().isValid() ) {
               var token = $('#hdntoken').val();
                $.ajax({
                   type:'PUT',
                   url : base_url+'/admin/company/'+token,
                   data : $('#frmeditcompany').serialize(),
                   success:function(res){
                       if(res.success){
                           successNotify(res.msg);

                           setTimeout(function(){
                               location.reload();
                           },1000);
                       }else{
                           failedNotify(res.msg);

                       }
                   }
                });
           }

       });
    });

    // Load Anzic Group
    $('.anzic-sub-division').on('select2:select', function (e) {
        $('#anzic_class').html('');
        var id = e.params.data.id;
        $.ajax({
           type : 'GET',
           url : base_url+'/get-company-group-by-sub-divisions/'+id,
           success:function(res){

                var data = res;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

               $('#anzic_group').html(html);


           }

        });
    });

    // Load Anzic Sub Division
    $('.anzic-division').on('select2:select', function (e) {
        $('#anzic_group').html('');
        $('#anzic_class').html('');
        var id = e.params.data.id;
        $.ajax({
            type : 'GET',
            url : base_url+'/get-sub-divisions-by-division/'+id,
            success:function(res){

                var data = res.data;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

                $('#anzic_subdivision').html(html);

            }

        });
    });

    // Load Anzic Class
    $('.anzic-group').on('select2:select', function (e) {
        var id = e.params.data.id;
        $.ajax({
            type : 'GET',
            url : base_url+'/get-class-by-group/'+id,
            success:function(res){

                var data = res;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

                $('#anzic_class').html(html);

            }

        });
    });


    // ..............

    // Load Primary Group
    $('.primary-sub-division').on('select2:select', function (e) {
        $('#primary_class').html('');
        var id = e.params.data.id;
        $.ajax({
            type : 'GET',
            url : base_url+'/get-company-group-by-sub-divisions/'+id,
            success:function(res){

                var data = res;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

                $('#primary_group').html(html);


            }

        });
    });

    // Load Primary Sub Division
    $('.primary-division').on('select2:select', function (e) {
        $('#primary_group').html('');
        $('#primary_class').html('');
        var id = e.params.data.id;
        $.ajax({
            type : 'get',
            url : base_url+'/get-sub-divisions-by-division/'+id,
            success:function(res){

                var data = res.data;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

                $('#primary_subdivision').html(html);

            }

        });
    });

    // Load Primary Class
    $('.primary-group').on('select2:select', function (e) {
        var id = e.params.data.id;
        $.ajax({
            type : 'get',
            url : base_url+'/get-class-by-group/'+id,
            success:function(res){

                var data = res;
                var html = '';
                html+= '<option value="">--Select option--</option>';
                for(var x=0; x<data.length; x++){
                    html+='<option value="'+data[x].id+'">'+data[x].name+'</option>';
                }

                $('#primary_class').html(html);

            }

        });
    });
</script>
@endsection
