@extends('admin.layout.master')

@section('content')

    <form id="frmsettings" data-parsley-validate="">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h3>Application Settings</h3>
                        <hr>
                        <div class="row align-items-center">
                            <div class="col-lg-12 col-md-12">
                                <h3 class="card-title">Manage CreditorWatch API Settings</h3>
                                <div class="form-group">
                                    <div class="row">
                                        {{csrf_field()}}
                                        <div class="col-md-6">
                                            <label for="">API Username</label>
                                            <input type="text" value="{{(!empty($data))?$data['value']['username']:''}}" required="" class="form-control" id="api_username"
                                                   name="api_username" placeholder="API Username">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="">API Password</label>
                                            <input required="" value="{{(!empty($data))?$data['value']['password']:''}}" type="password" class="form-control" id="api_password"
                                                   name="api_password" placeholder="API Password">
                                        </div>


                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">

                                       <div class="col-md-12">
                                           <p class="text-right"><button class="btn btn-primary" type="submit">Save</button> </p>
                                       </div>

                                    </div>

                                </div>



                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </form>


@endsection

@section('custom-js')
    <script type="text/javascript">
        $(document).ready(function(){


            // Create New Company
            $('#frmsettings').submit(function (e) {
                e.preventDefault();
                if ($(this).parsley().isValid() ) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type:'POST',
                        url : base_url+'/admin/settings',
                        data : $('#frmsettings').serialize(),
                        success:function(res){
                            if(res.success){
                                successNotify(res.msg);

                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }else{
                                failedNotify(res.msg);

                            }
                        }
                    });
                }

            });
        });








    </script>
@endsection
