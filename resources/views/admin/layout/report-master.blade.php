<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Risk-D</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="favicon.ico" type="image/x-icon" />

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('theme/plugins/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/fontawesome-free/css/all.min.cs')}}s">
    <link rel="stylesheet" href="{{asset('theme/plugins/icon-kit/dist/css/iconkit.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/ionicons/dist/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/jvectormap/jquery-jvectormap.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/weather-icons/css/weather-icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/c3/c3.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/owl.carousel/dist/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/owl.carousel/dist/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/dist/css/theme.min.css')}}">
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/modules/css/report.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/af-2.3.4/b-1.6.1/datatables.min.css"/>
    <script src="{{asset('theme/src/js/vendor/modernizr-2.8.3.min.js')}}"></script>

{{--    custom css--}}
    <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}"/>
{{--    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker.js')}}"/>--}}
    <link rel="stylesheet" href="{{asset('assets/css/jquery-confirm.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/parsley.css')}}"/>
    @yield('custom-css')
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="auth-wrapper">
    @yield('content')
</div>


</div>





<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script src="{{asset('theme/plugins/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('theme/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('theme/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('theme/plugins/screenfull/dist/screenfull.js')}}"></script>
<script src="{{asset('theme/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('theme/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('theme/plugins/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('theme/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

<script src="{{asset('theme/plugins/moment/moment.js')}}"></script>
<script src="{{asset('theme/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{asset('theme/plugins/d3/dist/d3.min.js')}}"></script>
<script src="{{asset('theme/plugins/c3/c3.min.js')}}"></script>
<script src="{{asset('theme/js/tables.js')}}"></script>

<script src="{{asset('theme/js/charts.js')}}"></script>
<script src="{{asset('theme/dist/js/theme.min.js')}}"></script>
{{--custom js--}}
<script src="{{asset('assets/js/bootstrap-notify.js')}}"></script>
<script src="{{asset('assets/js/notification.js')}}"></script>
<script src="{{asset('assets/js/jquery-confirm.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('assets/js/parsley.min.js')}}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>


<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/af-2.3.4/b-1.6.1/datatables.min.js"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script type="text/javascript">
    var base_url = '{{url('')}}';

</script>
@yield('custom-js')
</body>
</html>
