@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <p class="text-center text-danger"><i class="fas fa-cog fa-spin fa-3x"></i></p>
                    <h1 class="text-danger text-center">PDF generation error found. Please contact system administrator</h1>
                <p class="text-center"><a class="btn btn-danger" href="{{url('admin/reports/list/approved-report')}}">Back to Report List</a></p>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('custom-js')
    <script type="text/javascript">

    </script>
@endsection
