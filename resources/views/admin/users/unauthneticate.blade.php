@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <p class="text-center text-danger"><i class="fas fa-cog fa-spin fa-3x"></i></p>
                    <h1 class="text-danger text-center">Permission denied. Please contact your system administrator.</h1>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('custom-js')
    <script type="text/javascript">

    </script>
@endsection
