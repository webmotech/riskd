@extends('admin.layout.login-master')

@section('content')
    <div class="container-fluid h-100">
        <div class="row flex-row h-100 bg-white">
            <div class="col-xl-8 col-lg-6 col-md-5 p-0 d-md-block d-lg-block d-sm-none d-none">
                <div class="lavalite-bg" style="background-image: url('{{asset('assets/img/back.jpg')}}')">
                    <div class="lavalite-overlay"></div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-7 my-auto p-0">
                <div class="authentication-form mx-auto">
                    <div class="logo-centered">
                        <a href="../index.html"><img class="img-fluid" src="{{asset('assets/img/logo.png')}}" alt=""></a>
                    </div>
                    <h3>Sign in to Risk-D</h3>
                    @if (session('status'))
                        <div class="alert alert-danger">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form data-parsley-validate="" action="{{url('admin/authenticate')}}" method="POST">
                        {{csrf_field()}}

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Username" name="username" id="username" required="" value="">
                            <i class="ik ik-user"></i>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="" >
                            <i class="ik ik-lock"></i>
                        </div>
                        <div class="row">
                            <div class="col text-left">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="item_checkbox" name="item_checkbox" value="option1">

                                </label>
                            </div>
                            <div class="col text-right">

                            </div>
                        </div>
                        <div class="sign-btn text-center">
                            <button type="submit" class="btn btn-success">Sign in</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
