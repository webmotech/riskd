<div class="modal fade" id="user-edit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modify User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form data-parsley-validate="" id="frmedituser">
                <div class="modal-body">
                    {{csrf_field()}}
                    <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="fname">First Name</label>
                                    <input type="text" required="" class="form-control" id="editfname" name="editfname" placeholder="First Name">
                                </div>
                                <input type="hidden" id="edithdnid"/>
                                <div class="col-md-6">
                                    <label for="lname">Last Name</label>
                                    <input type="text" required="" class="form-control" id="editlname" name="editlname" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="emailW">Username</label>
                                <input type="text" class="form-control" data-parsley-minlength="4" id="editusername" name="editusername" placeholder="Username">
                            </div>
                            <div class="col-md-4">
                                <label for="email">Email</label>
                                <input type="email" data-parsley-type="email" class="form-control" id="editemail" name="editemail" placeholder="Email">
                            </div>
                            <div class="col-md-4">
                                <label for="user_role">User Type</label>
                                <select name="edituser_role" id="edituser_role" class="form-control">
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">

                            <div class="col-md-4">
                                <label for="address">Address</label>
                                <textarea class="form-control" id="editaddress" name="editaddress"></textarea>
                            </div>
                            <div class="col-md-4">
                                <label for="telephone">Telephone</label>
                                <input type="text" name="edittelephone" id="edittelephone" class="form-control"/>

                            </div>

                        </div>
                    </div>




                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Register</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


