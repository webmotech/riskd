<div class="modal fade" id="reset-user-password" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Reset Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form data-parsley-validate="" id="frmresetuserpassword">
                <div class="modal-body">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Current Password</label>

                                <input type="password" class="form-control" id="current_password" name="current_password" />
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="password">New Password</label>
                                <input type="password" data-parsley-pattern-message="Your password must contain at least (1) lowercase, (1) uppercase letter, (1) number and (1) special character" data-parsley-pattern="(?=.*[!@#$%^&*()\-_=+{};:,<.>ยง~])(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z]).*"  class="form-control" required="" id="new_password" name="new_password" placeholder="Password">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="confirm">Confirm Password</label>
                                <input type="password" data-parsley-equalto="#new_password" required="" class="form-control" id="confirm" name="confirm" placeholder="Confirm Password">
                            </div>

                        </div>
                    </div>





                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Reset Password</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


