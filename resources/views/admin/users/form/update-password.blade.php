@extends('admin.layout.login-master')

@section('content')
    <div class="container-fluid h-100">
        <div class="row flex-row h-100 bg-white">
            <div class="col-xl-8 col-lg-6 col-md-5 p-0 d-md-block d-lg-block d-sm-none d-none">
                <div class="lavalite-bg" style="background-image: url('{{asset('assets/img/back.jpg')}}')">
                    <div class="lavalite-overlay"></div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-7 my-auto p-0">
                <div class="authentication-form mx-auto">
                    <div class="logo-centered">
                        <a href=""><img class="img-fluid" src="{{asset('assets/img/logo.png')}}" alt=""></a>
                    </div>
                    <h3>Reset Password</h3>

                    <form data-parsley-validate="" action="{{url('admin/authenticate/update-password')}}" method="POST">
                        {{csrf_field()}}

                        <div class="form-group">
                            <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Current Password" required="" >
                            <i class="ik ik-lock"></i>
                        </div>
                        <div class="form-group">
                            <input type="password" data-parsley-pattern-message="Your password must contain at least (1) lowercase, (1) uppercase letter, (1) number and (1) special character" data-parsley-pattern="(?=.*[!@#$%^&*()\-_=+{};:,<.>ยง~])(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z]).*" class="form-control" id="new_password" name="new_password" placeholder="New Password" required="" >
                            <i class="ik ik-lock"></i>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" data-parsley-equalto="#new_password" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required="" >
                            <i class="ik ik-lock"></i>
                        </div>

                        <div class="sign-btn text-center">
                            <button type="submit" class="btn btn-success">Sign In</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @endsection
