@extends('admin.layout.master')

@section('custom-css')
    <style type="text/css">
        .previous .table td, tr .table th{
            padding: 0.7em !important;
            width: 100%;
        }
        #input-area select {
            height: 33px;
        }




    </style>
    @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class=" bg-primary text-light p-1">
                    <h1 class="text-center"> New Report Creation</h1>

                </div>
                <form id="frmreportcreate" data-parsley-validate="">
                    {{csrf_field()}}
                    <input type="hidden" name="hdncompanyToken" id="hdncompanyToken"/>
                <div class="card-body">
                    <p class="text-right"><span id="validate-year" class="alert alert-danger" style="display: none">You Can add Maximum 3 years</span> &nbsp;<button type="button" onclick="createYear()" class="btn btn-success"><i class="fa fa-plus"></i>Add Year</button><button type="button" onclick="changeCompany()" class="btn btn-warning"><i class="fa fa-plus"></i>Change Company</button></p>
                    <div class="row">
                        <div class="col-md-6 "></div>
                        <div class="col-md-6 ">
                                <div class=" form-group">
                                    <select required="" name="report_type" id="report_type" class="form-control">
                                        <option value="">Select Report Type</option>
                                        @foreach(config('reports_config.report_type') as $key=>$reportType)
                                            <option value="{{$key}}">{{$reportType}}</option>
                                            @endforeach
                                    </select>
                                </div>

                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-7 col-xl-7 table-responsive text-nowrap col-sm-7 col-lg-7 previous">

                           <table class="table table-bordered" id="tableleft">
                               <tr class=" border-0">
                                   <td class="p-1 text-light border-0" colspan="{{$no_of_years+1}}">
                                       <strong>&nbsp;</strong>
                                   </td>

                               </tr>
                                <tr class="">
                                   <td class="">ABN</td>
                                    @if($no_of_years > 0)
                                        @foreach($previous as $previousData)
                                   <td>{{$report['abn']}}</td>
                                        @endforeach
                                        @endif
                                </tr>
                               <tr class="">
                                   <td class="">ACN</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{$report['acn']}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Company</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{$report->company->name}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Rounding</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{config('reports_config.rounding')[$previousData['rounding']]}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Base Currency</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{$previousData['base_currency']}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Quality</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{config('reports_config.quality')[$previousData['quality']]}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Reporting Period (months)</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{$previousData['report_period_months']}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Scope</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{$previousData['scope']}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Confidentiality Record</td>
                                   @if($no_of_years > 0)

                                       @foreach($previous as $previousData)
                                           <td>{{$previousData->confidentiality['name']}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Financial Year</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{$previousData['financial_year']}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Month</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{$previousData['financial_month']}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="{{$no_of_years+1}}">
                                       <strong>Income Statement</strong>
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class="">Sales</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['sales'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Cost of sales</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['cost_of_sales'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Gross profit</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['gross_profit'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Other income</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['other_income'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Depreciation</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['depreciation'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Amortisation</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['amortisation'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Impairment</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['impairment'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Interest expense gross</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['interest_expenses_gross'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Operating lease expense</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['operating_lease_expenses'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Finance lease hire purchase charges</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['finance_lease_hire_expenses_charges'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Non-recurring gains (losses)</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['non_recurring_gain_loses'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Other gains (losses)</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['other_gain_loses'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Other expenses</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['other_expenses'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">EBIT</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['ebit'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">EBITDA</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['ebitda'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Normalised EBITDA</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['normalized_ebitda'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Profit before tax</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['profit_before_tax'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Profit before tax after abnormals</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['profit_before_tax_after_abnormals'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Tax benefit (expense)</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['tax_benefit_expenses'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Profit after tax</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['profit_after_tax'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Distribution or dividends</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['distribution_ordividends'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Other post tax items - gains/ (losses)</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['other_post_tax_items_gains_losses'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Profit after tax distribution</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['profit_after_tax_distribution'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="{{$no_of_years+1}}">
                                       <strong>Balance Sheet Assets</strong>
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="{{$no_of_years+1}}">
                                      Assets
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class="">Cash</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['cash'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Trade debtors</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['trade_debtors'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Total inventories</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['total_inventories'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Loans to related parties</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['loan_to_related_parties_1'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Other current assets</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['other_current_assets'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Total current assets</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['total_current_assets'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Fixed assets</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['fixed_assets'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Net intangibles</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['net_intangibles'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Loan to related parties</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['loan_to_related_parties_2'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Other non-current assets</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['other_non_current_assets'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Total non-curent assets</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['total_non_current_assets'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Total assets</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['total_assets'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="{{$no_of_years+1}}">
                                       <strong>Liabilities</strong>
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class="">Trade creditors</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['trade_creditors'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Interest bearing debt</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['interest_bearing_debt_1'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Loan from related parties</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['loan_from_related_parties_1'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Other current liabilities</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['other_current_liabilities'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Total current liabilities</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['total_current_liabilities'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Interest bearing debt</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['interest_bearing_debt_2'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Loans from related parties</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['loan_from_related_parties_2'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Other non-current liabilities</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['other_non_current_liabilities'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Total non-current liabilities</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['total_non_current_liabilities'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Total liabilities</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['total_liabilities'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="{{$no_of_years+1}}">
                                       <strong>Equity</strong>
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class="">Share capital</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['share_capital'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Preference shares</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['prefence_shares'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Treasury shares</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['threasury_shares'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Equity ownerships</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['equity_ownerships'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Total reserves</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['total_reserves'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Retained earnings</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['retained_earning'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Minorty interest</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['minorty_interest'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Total equity</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['total_equity'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Balance</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['balance'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>

                               <tr class="">
                                   <td class=" bg-primary text-light" colspan="{{$no_of_years+1}}">
                                       <strong>Additional Information</strong>
                                   </td>

                               </tr>
                               <tr class="">
                                   <td class="">Operating cash flow</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['operating_cash_flow'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Contingent liabilities</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['contingent_liabilities'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Other commitments</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['other_commitmentes'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                               <tr class="">
                                   <td class="">Operating lease outstandings</td>
                                   @if($no_of_years > 0)
                                       @foreach($previous as $previousData)
                                           <td>{{number_format($previousData['operating_lease_outstanding'])}}</td>
                                       @endforeach
                                   @endif
                               </tr>
                            </table>

                        </div>

                        <div class="col-md-5 col-xl-5 col-sm-5 col-lg-5 pl-1">


                           <div class="row" id="input-area">




                           </div>


                        </div>

                    </div>

                    <div class="row">
                        <hr>
                        <div class="col-md-12" id="summery-area">
                            {{--<p class="text-right"><span class="alert alert-danger" id="validate-summery" style="display: none">You can maximum 3 executive summery</span><button type="button" onclick="addSummery()" class="btn btn-primary">Add Executive Summery</button></p>--}}


                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="text-right">
                                <a href="{{ url()->previous() }}" class="btn btn-warning">Back</a>
                                <button type="button" id="savebtn" class="btn btn-success">Save and Send for Approval</button>
                            </p>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endsection

@section('custom-js')
  <script type="text/javascript" src="{{asset('assets/modules/js/reports/create-report.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/modules/js/reports/report-calculation.js')}}"></script>
    @endsection
