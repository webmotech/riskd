@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="p-1">
                    <h1 class="text-center"> Rejected Report List</h1>

                </div>

                <div class="card-body">


                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="">
                                        <div class="col-lg-12 col-md-12">
                                            <h3 class="card-title">Rejected Report List</h3>
                                            <p class="text-right">


                                            </p>
                                            <table id="reject-report-table" class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Company</th>
                                                    <th>Report Type</th>
                                                    <th>Status</th>
                                                    <th style="width:100px;">Rejected by</th>
                                                    <th>Created at</th>
                                                    <th>Action</th>


                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Company</th>
                                                    <th>Report Type</th>
                                                    <th>Status</th>
                                                    <th>Rejected by</th>
                                                    <th>Created at</th>
                                                    <th>Action</th>


                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>



                    </div>

                </div>

            </div>
        </div>
    </div>
    @endsection

@section('custom-js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#reject-report-table').DataTable({

            ajax: {
                "url": "/admin/reports/list/get-by-status",
                "async": true,
                "data": {status: [2]},
            },
            "order": [[ 0, "desc" ]],
            "datatype": "json",
            "columns": [
                { "data": "id" },
                { "data": "company" },
                { "data": "report_type" },
                { "data": "report_status" },
                { "data": "approve_reject_by" },
                { "data": "created_at" },
                {
                    data:null,
                    render: function( data, type, full, meta) {

                            return '<a href="#" class="editor_clone btn btn-warning"><i class="fa fa-clone"></i>Clone</a>';

                        }
                }

            ],

        });
    });


</script>
    @endsection
