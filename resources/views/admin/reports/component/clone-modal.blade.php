<div class="modal fade clone-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="text-center" id="exampleModalLongTitle">Select Report Type</h5>

            </div>
            <form data-parsley-validate="" id="frmclonereport">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">Report Type</label>
                                <input type="hidden" name="report_token" id="report_token"/>
                                <select required="" id="select-report-type" class="form-control">
                                    <option value="">Select Report Type</option>
                                    @foreach(config('reports_config.report_type') as $key=>$report)
                                        <option value="{{$key}}">{{$report}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary">Continue</button>
                    <button  data-dismiss="modal" class="btn btn-warning">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
