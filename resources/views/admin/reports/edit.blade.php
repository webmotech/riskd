@extends('admin.layout.master')
@section('custom-css')
    <style type="text/css">
        .previous .table td, tr .table th{
            padding: 0.7em !important;
            width: 100%;
        }
        #input-area select {
            height: 33px;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class=" bg-primary text-light p-1">
                    <h1 class="text-center"> New Report Creation</h1>

                </div>
                <form id="frmreportEdit" data-parsley-validate="">
                    {{csrf_field()}}
                <input type="hidden" name="hdncompanyToken" value="{{$report_data['report']->company->company_token}}" id="hdncompanyToken"/>
                    <input type="hidden" name="hdnreportToken" value="{{$report_data['report']['report_token']}}" id="hdnreportToken"/>
                    <div class="card-body">
                        <p class="text-right"><span id="validate-year" class="alert alert-danger" style="display: none">You Can add Maximum 3 years</span> &nbsp;<button type="button" onclick="createYear()" class="btn btn-success"><i class="fa fa-plus"></i>Add Year</button></p>
                        <div class="row">
                            <div class="col-md-5 p-0"></div>
                            <div class="col-md-7 p-0">
                                <div class=" form-group">
                                    <select required="" name="report_type" id="report_type" class="form-control">
                                        <option value="">Select Report Type</option>
                                        @foreach(config('reports_config.report_type') as $key=>$reportType)
                                            @if($report_data['report']['report_type']==$key)
                                                <option selected value="{{$key}}">{{$reportType}}</option>
                                                @else
                                            <option value="{{$key}}">{{$reportType}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7 table-responsive text-nowrap col-sm-7 col-lg-7 previous">

                                <table class="table table-bordered">
                                    <tr class="p-0 border-0">
                                        <td class="p-1 text-light border-0" colspan="{{$no_of_years+1}}">
                                            <strong>&nbsp;</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">ABN</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{$report['abn']}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">ACN</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{$report['acn']}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Company</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{$report->company->name}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Rounding</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{config('reports_config.rounding')[$previousData['rounding']]}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Base Currency</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{$previousData['base_currency']}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Quality</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{config('reports_config.quality')[$previousData['quality']]}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Reporting Period (months)</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{$previousData['report_period_months']}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Scope</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{$previousData['scope']}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Confidentiality Record</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{$previousData->confidentiality['name']}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Financial Year</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{$previousData['financial_year']}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Month</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{$previousData['financial_month']}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="{{$no_of_years+1}}">
                                            <strong>Income Statement</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Sales</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['sales'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Cost of sales</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['cost_of_sales'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Gross profit</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['gross_profit'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other income</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['other_income'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Depreciation</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['depreciation'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Amortisation</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['amortisation'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Impairment</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['impairment'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Interest expense gross</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['interest_expenses_gross'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Operating lease expense</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['operating_lease_expenses'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Finance lease hire purchase charges</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['finance_lease_hire_expenses_charges'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Non-recurring gains (losses)</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['non_recurring_gain_loses'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other gains (losses)</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['other_gain_loses'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other expenses</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['other_expenses'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">EBIT</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['ebit'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">EBITDA</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['ebitda'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Normalised EBITDA</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['normalized_ebitda'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Profit before tax</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['profit_before_tax'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Profit before tax after abnormals</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['profit_before_tax_after_abnormals'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Tax benefit (expense)</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['tax_benefit_expenses'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Profit after tax</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['profit_after_tax'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Distribution or dividends</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['distribution_ordividends'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other post tax items - gains/ (losses)</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['other_post_tax_items_gains_losses'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Profit after tax distribution</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['profit_after_tax_distribution'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="{{$no_of_years+1}}">
                                            <strong>Balance Sheet Assets</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="{{$no_of_years+1}}">
                                            Assets
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Cash</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['cash'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Trade debtors</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['trade_debtors'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total inventories</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['total_inventories'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Loans to related parties</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['loan_to_related_parties_1'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other current assets</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['other_current_assets'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total current assets</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['total_current_assets'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Fixed assets</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['fixed_assets'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Net intangibles</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['net_intangibles'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Loan to related parties</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['loan_to_related_parties_2'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other non-current assets</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['other_non_current_assets'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total non-curent assets</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['total_non_current_assets'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total assets</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['total_assets'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="{{$no_of_years+1}}">
                                            <strong>Liabilities</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Trade creditors</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['trade_creditors'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Interest bearing debt</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['interest_bearing_debt_1'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Loan from related parties</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['loan_from_related_parties_1'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other current liabilities</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['other_current_liabilities'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total current liabilities</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['total_current_liabilities'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Interest bearing debt</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['interest_bearing_debt_2'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Loans from related parties</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['loan_from_related_parties_2'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other non-current liabilities</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['other_non_current_liabilities'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total non-current liabilities</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['total_non_current_liabilities'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total liabilities</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['total_liabilities'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="{{$no_of_years+1}}">
                                            <strong>Equity</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Share capital</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['share_capital'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Preference shares</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['prefence_shares'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Treasury shares</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['threasury_shares'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Equity ownerships</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['equity_ownerships'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total reserves</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['total_reserves'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Retained earnings</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['retained_earning'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Minorty interest</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['minorty_interest'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Total equity</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['total_equity'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Balance</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['balance'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>

                                    <tr class="p-0">
                                        <td class="p-2 bg-primary text-light" colspan="{{$no_of_years+1}}">
                                            <strong>Additional Information</strong>
                                        </td>

                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Operating cash flow</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['operating_cash_flow'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Contingent liabilities</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['contingent_liabilities'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Other commitments</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['other_commitmentes'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-2">Operating lease outstandings</td>
                                        @if($no_of_years > 0)
                                            @foreach($previous as $previousData)
                                                <td>{{number_format($previousData['operating_lease_outstanding'])}}</td>
                                            @endforeach
                                        @endif
                                    </tr>

                                </table>

                            </div>

                            <div class="col-md-5 col-sm-5 col-lg-5 pl-1">


                                <div class="row" id="input-area">
                                    @php
                                        $tabIndex = 1;
                                        $subIndex = 0.1;
                                    @endphp

                                    @foreach($report_data['details'] as $key => $details)
                                    <div class="col p-0" id="coloumn_{{$key}}">
                                        <table class="table table-bordered">
                                            <tr>
                                        <td class="p-0" style="padding: 0.2em !important;"><button onclick="removeMe({{$key}})" type="button" class="btn btn-danger btn-block"><i class="fa fa-trash"></i></i>Remove</button> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{$report_data['report']['abn']}}" tabindex="{{($tabIndex++)+$subIndex}}" readonly class="form-control" name="data[{{$key}}][abn]" id="abn_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{$report_data['report']['acn']}}"  tabindex="{{($tabIndex++)+$subIndex}}" readonly class="form-control" name="data[{{$key}}][acn]" id="acn_{{$key}}"/> </td>
                                            </tr>
                                            <tr>

                                                <td class="p-0"><select required="" tabindex="{{($tabIndex++)+$subIndex}}" readonly class="form-control" name="data[{{$key}}][company]" id="company_{{$key}}">
                                                        @foreach($company as $companyData)
                                                            @if($report_data['report']->company->id==$companyData['id'])
                                                                <option selected value="{{$companyData['id']}}">{{$companyData['name']}}</option>
                                                                @else
                                                            <option value="{{$companyData['id']}}">{{$companyData['name']}}</option>
                                                            @endif
                                                            @endforeach
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select required="" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][rounding]" id="rounding_{{$key}}">
                                                        @foreach(config('reports_config.rounding') as $roundingKey => $rounding)
                                                            @if($details['rounding']==$roundingKey)
                                                                <option selected value="{{$roundingKey}}">{{$rounding}}</option>
                                                                @else
                                                            <option value="{{$roundingKey}}">{{$rounding}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][base_currency]" id="base_currency_{{$key}}">
                                                        @foreach(config('reports_config.base_currency') as $currencyKey => $currency)
                                                            @if($details['base_currency']==$currencyKey)
                                                                <option selected value="{{$currencyKey}}">{{$currency}}</option>
                                                                @else
                                                            <option value="{{$currencyKey}}">{{$currency}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][quality]" id="quality_{{$key}}">
                                                        @foreach(config('reports_config.quality') as $qualityKey => $quality)
                                                            @if($details['quality']==$qualityKey)
                                                                <option selected value="{{$qualityKey}}">{{$quality}}</option>
                                                                @else
                                                            <option value="{{$qualityKey}}">{{$quality}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select required="" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][reporting_period]" id="reporting_period_{{$key}}">

                                                        @for($reportingPeriodIndex = 1; $reportingPeriodIndex<=24; $reportingPeriodIndex++)
                                                            @if($details['report_period_months']==$reportingPeriodIndex)
                                                                <option selected value="{{$reportingPeriodIndex}}">{{$reportingPeriodIndex}}</option>
                                                            @else
                                                                <option value="{{$reportingPeriodIndex}}">{{$reportingPeriodIndex}}</option>
                                                            @endif
                                                        @endfor
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][scope]" id="scope_{{$key}}">
                                                        @foreach(config('reports_config.scope') as $scopeKey => $scope)
                                                            @if($details['scope']==$scopeKey)
                                                                <option selected value="{{$scopeKey}}">{{$scope}}</option>
                                                                @else
                                                            <option value="{{$scopeKey}}">{{$scope}}</option>
                                                            @endif
                                                            @endforeach
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][confidentiality_record]" id="confidentiality_record_{{$key}}">

                                                        @foreach($confidentiality as $confident)
                                                            @if($details['confidentiality_id']==$confident['id'])
                                                                <option selected value="{{$confident['id']}}">{{$confident['name']}}</option>
                                                            @else
                                                            <option value="{{$confident['id']}}">{{$confident['name']}}</option>
                                                            @endif
                                                                @endforeach
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select required=""  tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][financial_year]" id="financial_year_{{$key}}">
                                                        @for($yearIndex = 2000; $yearIndex<=2050; $yearIndex++)
                                                            @if($details['financial_year']==$yearIndex)
                                                                <option selected value="{{$yearIndex}}">FY{{$yearIndex}}</option>
                                                                @else
                                                            <option value="{{$yearIndex}}">FY{{$yearIndex}}</option>
                                                            @endif
                                                            @endfor
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><select required="" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][month]" id="month_{{$key}}">
                                                        @foreach(config('reports_config.months') as $monthKey => $month)
                                                            @if($details['financial_month']==$monthKey)
                                                                <option selected value="{{$monthKey}}">{{$month}}</option>
                                                            @else
                                                            <option value="{{$monthKey}}">{{$month}}</option>
                                                            @endif
                                                                @endforeach
                                                    </select> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Income Statement</td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" onchange="calculateGrossProfit({{$key}})" class="form-control" value="{{round($details['sales'])}}" name="data[{{$key}}][sales]" id="sales_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['cost_of_sales'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" value="0" onchange="calculateGrossProfit({{$key}})" class="form-control" name="data[{{$key}}][cost_of_sales]" id="cost_of_sales_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['gross_profit'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" readonly class="form-control" name="data[{{$key}}][gross_profit]" onblur="blurGrossProfit({{$key}})" id="gross_profit_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['other_income'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][other_income]" onchange="changeOtherIncome({{$key}})" id="other_income_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['depreciation'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][depreciation]" onchange="changeDepreciation({{$key}})" id="depreciation_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['amortisation'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][amortisation]" onchange="changeAmortisation({{$key}})" id="amortisation_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['impairment'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][impairment]" onchange="changeImpairment({{$key}})" id="impairment_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['interest_expenses_gross'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" onchange="changeInterestExpense({{$key}})" name="data[{{$key}}][interest_expense_gross]" id="interest_expense_gross_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['operating_lease_expenses'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][operating_lease_expense]" onchange="changeOperatingLeaseExpense({{$key}})" id="operating_lease_expense_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['finance_lease_hire_expenses_charges'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][finance_lease_hire_purchase_charges]" onchange="changeFinanceLeaseHirePurchaseCharges({{$key}})" id="finance_lease_hire_purchase_charges_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['non_recurring_gain_loses'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][non_recurring_gains_losses]" onchange="changeNonRecurrentGainLosses({{$key}})" id="non_recurring_gains_losses_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['other_gain_loses'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][other_gains_losses]" onchange="changeOtherGainLosses({{$key}})" id="other_gains_losses_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['other_expenses'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][other_expenses]" onchange="changeOtherExpense({{$key}})" id="other_expenses_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['ebit'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" readonly class="form-control" name="data[{{$key}}][ebit]" id="ebit_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['ebitda'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" readonly class="form-control" name="data[{{$key}}][ebitda]" id="ebitda_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['normalized_ebitda'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" readonly class="form-control" name="data[{{$key}}][normalized_ebitda]" id="normalized_ebitda_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['profit_before_tax'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" readonly class="form-control" name="data[{{$key}}][profit_before_tax]" id="profit_before_tax_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['profit_before_tax_after_abnormals'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" readonly class="form-control" name="data[{{$key}}][profit_before_tax_after_abnormals]" onblur="blurProfitBeforeTaxAfterAbnormal({{$key}})" id="profit_before_tax_after_abnormals_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['tax_benefit_expenses'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][tax_benefit_expense]" onchange="changeTaxBenefitExpense({{$key}})"  id="tax_benefit_expense_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['profit_after_tax'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][profit_after_tax]" readonly onblur="changeProfitAfterTax({{$key}})" id="profit_after_tax_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['distribution_ordividends'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][distribution_or_dividends]" onchange="changeDistributionOrDividends({{$key}})" id="distribution_or_dividends_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['other_post_tax_items_gains_losses'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][other_post_tax_items_gains_losses]" onchange="changeOtherPostTaxItemsGainsLosses({{$key}})" id="other_post_tax_items_gains_losses_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['profit_after_tax_distribution'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][profit_after_tax_distributions]" readonly id="profit_after_tax_distributions_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Balance Sheet Assets</td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Assets</td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['cash'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][cash]" onchange="changeCash({{$key}})" id="cash_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['trade_debtors'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][trade_debtors]" onchange="changeTradeDebtors({{$key}})" id="trade_debtors_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['total_inventories'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][total_inventories]" onchange="changetotalInventories({{$key}})" id="total_inventories_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['loan_to_related_parties_1'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][loans_to_related_parties]" onchange="changeLoanToRelatedParties1({{$key}})" id="loans_to_related_parties_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['other_current_assets'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][other_current_assets]" onchange="changeOtherCurrentAssets({{$key}})" id="other_current_assets_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['total_current_assets'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][total_current_assets]" readonly onblur="blurTotalCurrentAssets({{$key}})" id="total_current_assets_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['fixed_assets'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][fixed_assets]" onchange="changeFixedAssets({{$key}})" id="fixed_assets_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['net_intangibles'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][net_intangibles]" onchange="changeNetIntangibles({{$key}})" id="net_intangibles_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['loan_to_related_parties_2'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][loan_to_related_parties2]" onchange="changeLoanToRelatedParties2({{$key}})" id="loan_to_related_parties2_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['other_non_current_assets'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][other_non_current_assets]" onchange="changeOtherNonCurrentAssets({{$key}})" id="other_non_current_assets_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['total_non_current_assets'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][total_non_current_assets]" readonly onblur="blurTotalNonCurrentAssets({{$key}})" id="total_non_current_assets_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['total_assets'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][total_assets]" readonly onblur="blurTotalAssets({{$key}})" id="total_assets_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Liabilities</td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['trade_creditors'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][total_creditors]" onchange="changeTradeCreditors({{$key}})" id="total_creditors_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['interest_bearing_debt_1'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][interes_bearing_debt]" onchange="changeInterestBearingDebt1({{$key}})" id="interes_bearing_debt_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['loan_from_related_parties_1'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][loan_from_related_parties]" onchange="changeLoanFromRelatedParties1({{$key}})" id="loan_from_related_parties_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['other_current_liabilities'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][other_current_liabilities]" onchange="changeOtherCurrentLiabilities({{$key}})" id="other_current_liabilities_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['total_current_liabilities'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][total_current_liabilities]" readonly onblur="blurTotalCurrentLiabilities({{$key}})" id="total_current_liabilities_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['interest_bearing_debt_2'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][interest_bearing_debt2]" onchange="changeInterestBearingDebt2({{$key}})" id="interest_bearing_debt2_{{$key}}"/> </td>
                                            </tr>

                                            <td class="p-0"><input type="text" value="{{round($details['loan_from_related_parties_2'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][loan_from_related_parties_2]" onchange="changeLoanFromRelatedParties2({{$key}})" id="loan_from_related_parties_2_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['other_non_current_liabilities'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][other_non_current_liabilities]" onchange="changeOtherNonCurrentLiabilities({{$key}})" id="other_non_current_liabilities_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['total_non_current_liabilities'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][total_non_current_liabilities]" readonly onblur="blurTotalNonCurrentLiabilities({{$key}})" id="total_non_current_liabilities_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['total_liabilities'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][total_liabilities]" readonly onblur="blurTotalLiabilities({{$key}})" id="total_liabilities_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Equity</td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['share_capital'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][share_capital]" onchange="changeShareCapital({{$key}})" id="share_capital_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['prefence_shares'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][prefence_shares]" onchange="changePrefenceShares({{$key}})" id="prefence_shares_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['threasury_shares'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][treasury_shares]" onchange="changeTreasuryShares({{$key}})" id="treasury_shares_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['equity_ownerships'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][equity_ownerships]" readonly onblur="blurEquityOwnerships({{$key}})" id="equity_ownerships_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['total_reserves'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][total_reserves]" onchange="changeTotalReserves({{$key}})" id="total_reserves_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['retained_earning'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][retained_earnings]" onchange="changeRetainedEarning({{$key}})" id="retained_earnings_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['minorty_interest'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][minorty_interest]" onchange="changeMinortyInterest({{$key}})" id="minorty_interest_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['total_equity'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][total_equity]" readonly onblur="blurTotalEquity({{$key}})" id="total_equity_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['balance'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][balance]" readonly id="balance_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-2 bg-primary text-light">Additional Information</td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['operating_cash_flow'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][operating_cash_flow]" id="operating_cash_flow_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['contingent_liabilities'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][contingent_liabilities]" id="contingent_liabilities_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['other_commitmentes'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][other_commiments]" id="other_commiments_{{$key}}"/> </td>
                                            </tr>
                                            <tr>
                                                <td class="p-0"><input type="text" value="{{round($details['operating_lease_outstanding'])}}" data-parsley-type="number" tabindex="{{($tabIndex++)+$subIndex}}" class="form-control" name="data[{{$key}}][operating_lease_outstandings]" id="operating_lease_outstandings_{{$key}}"/> </td>
                                            </tr>
                                        </table>
                                    </div>

                                        @php
                                            $tabIndex = 1;
                                            $subIndex += 0.1;
                                        @endphp
                                @endforeach
                                <input type="hidden" id="nextcoloumn" value="{{$key}}"/>
                                <input type="hidden" id="hdnyears" value="{{$subIndex}}"/>


                                </div>


                            </div>

                        </div>

                        <div class="row">
                            <hr>
                            <div class="col-md-12" id="summery-area">
                                {{--<p class="text-right"><span class="alert alert-danger" id="validate-summery" style="display: none">You can maximum 3 executive summery</span><button type="button" onclick="addSummery()" class="btn btn-primary">Add Executive Summery</button></p>--}}



                            @foreach ($report_data['report']->executiveSummery as $key=>$summery)
                                    <div class="form-group">
                                        <label>Financial Analysis</label>
                                        <textarea name="exective_summery[{{$key}}][summery]" required id="executive_summery{{$key}}" class="form-control">{{$summery->summery}}</textarea>
                                       </div>
                            @endforeach
                                <input type="hidden" id="summeryindex" value="{{$key}}"/>
                                <input type="hidden" id="summeryCount" value="{{count($report_data['report']->executiveSummery)}}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p class="text-right">
                                    <a href="{{ url()->previous() }}" class="btn btn-warning">Back</a>
                                    @if($report_data['report']['report_status']==0)
                                        <button type="button" id="savebtn" class="btn btn-success">Save</button>
                                        @else
                                         <button type="button" id="savebtn" class="btn btn-success">Save and Send for Approval</button>
                                        @endif

                                </p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection

@section('custom-js')
    <script type="text/javascript" src="{{asset('assets/modules/js/reports/edit-report.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/modules/js/reports/report-calculation.js')}}"></script>
@endsection
