@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="p-1">
                    <h1 class="text-center"> Approved Reports</h1>

                </div>

                <div class="card-body">


                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="">
                                        <div class="col-lg-12 col-md-12">
                                            <h3 class="card-title">Approved Reports</h3>
                                            <p class="text-right">


                                            </p>
                                            <table id="pending-report-table" class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th style="width:20px;">ID</th>
                                                    <th>Company</th>
                                                    <th style="width:100px;">Type</th>
                                                    <th>Status</th>
                                                    <th style="width:100px;">Created by</th>
                                                    <th style="width:100px;">Created at</th>
                                                    <th style="width:100px;">Approved by</th>
                                                    <th style="width:100px;">Approved at</th>
                                                    <th>Action</th>


                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Company</th>
                                                    <th>Type</th>
                                                    <th>Status</th>
                                                    <th>Created by</th>
                                                    <th>Created at</th>
                                                    <th>Approved by</th>
                                                    <th>Approved at</th>
                                                    <th>Action</th>


                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>



                    </div>

                </div>

            </div>
        </div>
    </div>
    @include('admin.reports.component.report-generating')
    @endsection


@section('custom-js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#pending-report-table').DataTable({

            ajax: {
                "url": "/admin/reports/list/get-by-status",
                "async": true,
                "data": {status: [1,4]},
            },
            "datatype": "json",
            "order": [[ 0, "desc" ]],
            "columns": [
                { "data": "id" },
                { "data": "company" },
                { "data": "report_type" },
                { "data": "report_status" },
                { "data": "created_by" },
                { "data": "created_at" },
                { "data": "approve_reject_by" },
                { "data": "approve_reject_at" },
                {
                    data:null,
                    render: function( data, type, full, meta) {
                        if(data.status==1){
                            return '<a href="" class="report_generate btn btn-warning"><i class="fa fa-building"></i>Generate</a><a href="#" class="editor_clone btn btn-warning"><i class="fa fa-clone"></i>Clone</a>';
                        }else if(data.status==4){
                            return '<a target="_blank" href="/admin/reports/view/report/'+data.report_token+'" class="view_report btn btn-primary"><i class="fa fa-file"></i>View</a><a href="#" class="editor_clone btn btn-warning"><i class="fa fa-clone"></i>Clone</a>';
                        }



                        }
                }

            ],

        });
    });

    // Edit Report
    $('#pending-report-table').on('click', 'a.report_generate', function (e) {
        e.preventDefault();
        var data = $('#pending-report-table').DataTable().row($(this).parents('tr')).data();
        $('#report-generate-modal').modal({backdrop: 'static', keyboard: false});
        $('#loader-img').show();
        $('.warning-msg').show();
        $('#error-msg').hide();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'GET',
            url: base_url+'/admin/reports/generate-report/'+data.report_token,
            success:function(res){
                if(res.success){
                    $('#loader-img').hide();
                    $('.warning-msg').hide();
                    $('#success-msg').html(res.msg);
                    $('#success-msg').fadeIn();
                    $('#viewbtn').html('<a class="btn btn-primary" target="_blank" href="'+res.data.report_url+'">View Report</a>')
                    $('#close-btn').show();
                    setTimeout(function(){

                    },1000);
                }else{
                    $('#loader-img').hide();
                    $('.warning-msg').hide();
                    $('#error-msg').html(res.msg);
                    $('#error-msg').fadeIn();
                    setTimeout(function () {
                        $('#report-generate-modal').modal('toggle');
                    },3000);


                }
            },
            error:function (res) {
                console.log(res);
                failedNotify(res.msg);
            }
        });
    });


</script>
    @endsection
