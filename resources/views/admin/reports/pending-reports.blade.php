@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="p-1">
                    <h1 class="text-center"> Pending Report List</h1>

                </div>

                <div class="card-body">


                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">

                                    <div class="">
                                        <div class="col-lg-12 col-md-12">
                                            <h3 class="card-title">Pending Report List</h3>
                                            <p class="text-right">


                                            </p>
                                            <table id="pending-report-table" class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Company</th>
                                                    <th>Report Type</th>
                                                    <th>Status</th>
                                                    <th>Created by</th>
                                                    <th>Created at</th>
                                                    <th>Action</th>


                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Company</th>
                                                    <th>Report Type</th>
                                                    <th>Status</th>
                                                    <th>Created by</th>
                                                    <th>Created at</th>
                                                    <th>Action</th>


                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>



                    </div>

                </div>

            </div>
        </div>
    </div>
    @endsection

@section('custom-js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#pending-report-table').DataTable({

            ajax: {
                "url": "/admin/reports/list/get-by-status",
                "async": true,
                "data": {status: [0,3]},
            },
            "datatype": "json",
            "order": [[ 0, "desc" ]],
            "columns": [
                { "data": "id" },
                { "data": "company" },
                { "data": "report_type" },
                { "data": "report_status" },
                { "data": "created_by" },
                { "data": "created_at" },
                {
                    data:null,
                    render: function( data, type, full, meta) {
                        if(data.status==0){
                            return '<a href="#" class="editor_cancel btn btn-danger"><i class="fa fa-times-circle"></i>Cancel</a>';
                        }else if(data.status==3){
                        return '<a href="" class="editor_edit btn btn-primary"><i class="fa fa-edit"></i>Edit</a><a href="" class="editor_clone btn btn-warning"><i class="fa fa-clone"></i>Clone</a>';
                        }
                        }
                }

            ],

        });
    });

    // Edit Report
    $('#pending-report-table').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var data = $('#pending-report-table').DataTable().row($(this).parents('tr')).data();
        location.href = base_url+'/admin/reports/'+data.report_token+'/edit';
    });


    // Report Cancellation
    $('#pending-report-table').on('click', 'a.editor_cancel', function (e) {
        e.preventDefault();
        var data = $('#pending-report-table').DataTable().row($(this).parents('tr')).data();
        $.confirm({
            title: 'Confirmation',
            content: 'Are you sure?',
            buttons: {
                Confirm: function () {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type:'GET',
                        url: base_url+'/admin/report/cancel-report/'+data.report_token,
                        success:function(res){
                            if(res.success){
                                successNotify(res.msg);

                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }else{
                                failedNotify(res.msg);

                            }
                        }
                    });
                },
                cancel: function () {

                }

            }
        });
    } );
</script>
    @endsection
