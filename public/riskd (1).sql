-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2020 at 01:04 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `riskd`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_credit_score_history`
--

CREATE TABLE `api_credit_score_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `api_custom_profile_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `score` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `api_credit_score_history`
--

INSERT INTO `api_credit_score_history` (`id`, `api_custom_profile_id`, `date`, `score`, `created_at`, `updated_at`) VALUES
(14, 4, '2020-02-23', 840.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(15, 4, '2020-02-15', 837.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(16, 4, '2020-01-15', 837.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(17, 4, '2019-12-15', 837.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(18, 4, '2019-11-15', 838.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(19, 4, '2019-10-15', 837.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(20, 4, '2019-09-15', 836.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(21, 4, '2019-08-15', 835.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(22, 4, '2019-07-15', 834.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(23, 4, '2019-06-15', 836.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(24, 4, '2019-05-15', 835.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(25, 4, '2019-04-15', 836.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(26, 4, '2019-03-15', 830.00, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(27, 5, '2020-02-23', 840.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(28, 5, '2020-02-15', 837.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(29, 5, '2020-01-15', 837.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(30, 5, '2019-12-15', 837.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(31, 5, '2019-11-15', 838.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(32, 5, '2019-10-15', 837.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(33, 5, '2019-09-15', 836.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(34, 5, '2019-08-15', 835.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(35, 5, '2019-07-15', 834.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(36, 5, '2019-06-15', 836.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(37, 5, '2019-05-15', 835.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(38, 5, '2019-04-15', 836.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(39, 5, '2019-03-15', 830.00, '2020-02-23 04:25:52', '2020-02-23 04:25:52'),
(40, 6, '2020-02-25', 840.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(41, 6, '2020-02-15', 837.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(42, 6, '2020-01-15', 837.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(43, 6, '2019-12-15', 837.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(44, 6, '2019-11-15', 838.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(45, 6, '2019-10-15', 837.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(46, 6, '2019-09-15', 836.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(47, 6, '2019-08-15', 835.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(48, 6, '2019-07-15', 834.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(49, 6, '2019-06-15', 836.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(50, 6, '2019-05-15', 835.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(51, 6, '2019-04-15', 836.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03'),
(52, 6, '2019-03-15', 830.00, '2020-02-25 08:47:03', '2020-02-25 08:47:03');

-- --------------------------------------------------------

--
-- Table structure for table `api_custom_profile`
--

CREATE TABLE `api_custom_profile` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `report_id` int(11) NOT NULL,
  `main_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_status_effective_from` datetime NOT NULL,
  `entity_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gst` date NOT NULL,
  `locality` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `record_last_updated` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registered_date` date NOT NULL,
  `review_date` date NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subclass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asic_locallity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_credit_score` double NOT NULL,
  `history_credit_score` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `api_custom_profile`
--

INSERT INTO `api_custom_profile` (`id`, `report_id`, `main_name`, `abn`, `entity_status`, `entity_status_effective_from`, `entity_type`, `gst`, `locality`, `record_last_updated`, `name`, `acn`, `type`, `status`, `registered_date`, `review_date`, `class`, `subclass`, `asic_locallity`, `current_credit_score`, `history_credit_score`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 23, 'G.P. EMBELTON & COMPANY PROPRIETARY LIMITED', '11004251861', 'Active', '2000-07-01 00:00:00', 'Australian Private Company', '2000-07-01', '3058 VIC', '2019-12-05 00:00:00', 'G.P. EMBELTON & COMPANY PROPRIETARY LIMITED', '004251861', 'Australian Proprietary Company', 'Registered', '1947-04-30', '2020-04-30', 'Limited By Shares', 'Proprietary Company', 'COBURG VIC 3058', 840, 0, 1, 1, '2020-02-23 02:42:03', '2020-02-23 02:42:03'),
(6, 36, 'G.P. EMBELTON & COMPANY PROPRIETARY LIMITED', '11004251861', 'Active', '2000-07-01 00:00:00', 'Australian Private Company', '2000-07-01', '3058 VIC', '2019-12-05 00:00:00', 'G.P. EMBELTON & COMPANY PROPRIETARY LIMITED', '004251861', 'Australian Proprietary Company', 'Registered', '1947-04-30', '2020-04-30', 'Limited By Shares', 'Proprietary Company', 'COBURG VIC 3058', 840, 0, 1, 1, '2020-02-25 08:47:02', '2020-02-25 08:47:02');

-- --------------------------------------------------------

--
-- Table structure for table `application_settings`
--

CREATE TABLE `application_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `application_settings`
--

INSERT INTO `application_settings` (`id`, `code`, `value`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'CREDTIWATCH', '{\"username\":\"csapiuser\",\"password\":\"JICSAPI@\"}', 1, 1, '2020-02-21 21:06:12', '2020-02-21 21:06:12');

-- --------------------------------------------------------

--
-- Table structure for table `company_class`
--

CREATE TABLE `company_class` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_group_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_class`
--

INSERT INTO `company_class` (`id`, `company_group_id`, `name`, `created_at`, `updated_at`) VALUES
(111, 11, 'Nursery Production (Under Cover)', NULL, NULL),
(112, 11, 'Nursery Production (Outdoors)', NULL, NULL),
(113, 11, 'Turf Growing', NULL, NULL),
(114, 11, 'Floriculture Production (Under Cover)', NULL, NULL),
(115, 11, 'Floriculture Production (Outdoors)', NULL, NULL),
(121, 12, 'Mushroom Growing', NULL, NULL),
(122, 12, 'Vegetable Growing (Under Cover)', NULL, NULL),
(123, 12, 'Vegetable Growing (Outdoors)', NULL, NULL),
(131, 13, 'Grape Growing', NULL, NULL),
(132, 13, 'Kiwifruit Growing', NULL, NULL),
(133, 13, 'Berry Fruit Growing', NULL, NULL),
(134, 13, 'Apple and Pear Growing', NULL, NULL),
(135, 13, 'Stone Fruit Growing', NULL, NULL),
(136, 13, 'Citrus Fruit Growing', NULL, NULL),
(137, 13, 'Olive Growing', NULL, NULL),
(139, 13, 'Other Fruit and Tree Nut Growing', NULL, NULL),
(141, 14, 'Sheep Farming (Specialised)', NULL, NULL),
(142, 14, 'Beef Cattle Farming (Specialised)', NULL, NULL),
(143, 14, 'Beef Cattle Feedlots (Specialised)', NULL, NULL),
(144, 14, 'Sheep-Beef Cattle Farming', NULL, NULL),
(145, 14, 'Grain-Sheep or Grain-Beef Cattle Farming', NULL, NULL),
(146, 14, 'Rice Growing', NULL, NULL),
(149, 14, 'Other Grain Growing', NULL, NULL),
(151, 15, 'Sugar Cane Growing', NULL, NULL),
(152, 15, 'Cotton Growing', NULL, NULL),
(159, 15, 'Other Crop Growing n.e.c.', NULL, NULL),
(160, 16, 'Dairy Cattle Farming', NULL, NULL),
(171, 17, 'Poultry Farming (Meat)', NULL, NULL),
(172, 17, 'Poultry Farming (Eggs)', NULL, NULL),
(180, 18, 'Deer Farming', NULL, NULL),
(191, 19, 'Horse Farming', NULL, NULL),
(192, 19, 'Pig Farming', NULL, NULL),
(193, 19, 'Beekeeping', NULL, NULL),
(199, 19, 'Other Livestock Farming n.e.c.', NULL, NULL),
(201, 20, 'Offshore Longline and Rack Aquaculture', NULL, NULL),
(202, 20, 'Offshore Caged Aquaculture', NULL, NULL),
(203, 20, 'Onshore Aquaculture', NULL, NULL),
(301, 30, 'Forestry', NULL, NULL),
(302, 30, 'Logging', NULL, NULL),
(411, 41, 'Rock Lobster and Crab Potting', NULL, NULL),
(412, 41, 'Prawn Fishing', NULL, NULL),
(413, 41, 'Line Fishing', NULL, NULL),
(414, 41, 'Fish Trawling, Seining and Netting', NULL, NULL),
(419, 41, 'Other Fishing', NULL, NULL),
(420, 42, 'Hunting and Trapping', NULL, NULL),
(510, 51, 'Forestry Support Services', NULL, NULL),
(521, 52, 'Cotton Ginning', NULL, NULL),
(522, 52, 'Shearing Services', NULL, NULL),
(529, 52, 'Other Agriculture and Fishing Support Services', NULL, NULL),
(600, 60, 'Coal Mining', NULL, NULL),
(700, 70, 'Oil and Gas Extraction', NULL, NULL),
(801, 80, 'Iron Ore Mining', NULL, NULL),
(802, 80, 'Bauxite Mining', NULL, NULL),
(803, 80, 'Copper Ore Mining', NULL, NULL),
(804, 80, 'Gold Ore Mining', NULL, NULL),
(805, 80, 'Mineral Sand Mining', NULL, NULL),
(806, 80, 'Nickel Ore Mining', NULL, NULL),
(807, 80, 'Silver-Lead-Zinc Ore Mining', NULL, NULL),
(809, 80, 'Other Metal Ore Mining', NULL, NULL),
(911, 91, 'Gravel and Sand Quarrying', NULL, NULL),
(919, 91, 'Other Construction Material Mining', NULL, NULL),
(990, 99, 'Other Non-Metallic Mineral Mining and Quarrying', NULL, NULL),
(1011, 101, 'Petroleum Exploration', NULL, NULL),
(1012, 101, 'Mineral Exploration', NULL, NULL),
(1090, 109, 'Other Mining Support Services', NULL, NULL),
(1111, 111, 'Meat Processing', NULL, NULL),
(1112, 111, 'Poultry Processing', NULL, NULL),
(1113, 111, 'Cured Meat and Smallgoods Manufacturing', NULL, NULL),
(1120, 112, 'Seafood Processing', NULL, NULL),
(1131, 113, 'Milk and Cream Processing', NULL, NULL),
(1132, 113, 'Ice Cream Manufacturing', NULL, NULL),
(1133, 113, 'Cheese and Other Dairy Product Manufacturing', NULL, NULL),
(1140, 114, 'Fruit and Vegetable Processing', NULL, NULL),
(1150, 115, 'Oil and Fat Manufacturing', NULL, NULL),
(1161, 116, 'Grain Mill Product Manufacturing', NULL, NULL),
(1162, 116, 'Cereal, Pasta and Baking Mix Manufacturing', NULL, NULL),
(1171, 117, 'Bread Manufacturing (Factory based)', NULL, NULL),
(1172, 117, 'Cake and Pastry Manufacturing (Factory based)', NULL, NULL),
(1173, 117, 'Biscuit Manufacturing (Factory based)', NULL, NULL),
(1174, 117, 'Bakery Product Manufacturing (Non-factory based)', NULL, NULL),
(1181, 118, 'Sugar Manufacturing', NULL, NULL),
(1182, 118, 'Confectionery Manufacturing', NULL, NULL),
(1191, 119, 'Potato, Corn and Other Crisp Manufacturing', NULL, NULL),
(1192, 119, 'Prepared Animal and Bird Feed Manufacturing', NULL, NULL),
(1199, 119, 'Other Food Product Manufacturing n.e.c.', NULL, NULL),
(1211, 121, 'Soft Drink, Cordial and Syrup Manufacturing', NULL, NULL),
(1212, 121, 'Beer Manufacturing', NULL, NULL),
(1213, 121, 'Spirit Manufacturing', NULL, NULL),
(1214, 121, 'Wine and Other Alcoholic Beverage Manufacturing', NULL, NULL),
(1220, 122, 'Cigarette and Tobacco Product Manufacturing', NULL, NULL),
(1311, 131, 'Wool Scouring', NULL, NULL),
(1312, 131, 'Natural Textile Manufacturing', NULL, NULL),
(1313, 131, 'Synthetic Textile Manufacturing', NULL, NULL),
(1320, 132, 'Leather Tanning, Fur Dressing and Leather Product Manufacturing', NULL, NULL),
(1331, 133, 'Textile Floor Covering Manufacturing', NULL, NULL),
(1332, 133, 'Rope, Cordage and Twine Manufacturing', NULL, NULL),
(1333, 133, 'Cut and Sewn Textile Product Manufacturing', NULL, NULL),
(1334, 133, 'Textile Finishing and Other Textile Product Manufacturing', NULL, NULL),
(1340, 134, 'Knitted Product Manufacturing', NULL, NULL),
(1351, 135, 'Clothing Manufacturing', NULL, NULL),
(1352, 135, 'Footwear Manufacturing', NULL, NULL),
(1411, 141, 'Log Sawmilling', NULL, NULL),
(1412, 141, 'Wood Chipping', NULL, NULL),
(1413, 141, 'Timber Resawing and Dressing', NULL, NULL),
(1491, 149, 'Prefabricated Wooden Building Manufacturing', NULL, NULL),
(1492, 149, 'Wooden Structural Fitting and Component Manufacturing', NULL, NULL),
(1493, 149, 'Veneer and Plywood Manufacturing', NULL, NULL),
(1494, 149, 'Reconstituted Wood Product Manufacturing', NULL, NULL),
(1499, 149, 'Other Wood Product Manufacturing n.e.c.', NULL, NULL),
(1510, 151, 'Pulp, Paper and Paperboard Manufacturing', NULL, NULL),
(1521, 152, 'Corrugated Paperboard and Paperboard Container Manufacturing', NULL, NULL),
(1522, 152, 'Paper Bag Manufacturing', NULL, NULL),
(1523, 152, 'Paper Stationery Manufacturing', NULL, NULL),
(1524, 152, 'Sanitary Paper Product Manufacturing', NULL, NULL),
(1529, 152, 'Other Converted Paper Product Manufacturing', NULL, NULL),
(1611, 161, 'Printing', NULL, NULL),
(1612, 161, 'Printing Support Services', NULL, NULL),
(1620, 162, 'Reproduction of Recorded Media', NULL, NULL),
(1701, 170, 'Petroleum Refining and Petroleum Fuel Manufacturing', NULL, NULL),
(1709, 170, 'Other Petroleum and Coal Product Manufacturing', NULL, NULL),
(1811, 181, 'Industrial Gas Manufacturing', NULL, NULL),
(1812, 181, 'Basic Organic Chemical Manufacturing', NULL, NULL),
(1813, 181, 'Basic Inorganic Chemical Manufacturing', NULL, NULL),
(1821, 182, 'Synthetic Resin and Synthetic Rubber Manufacturing', NULL, NULL),
(1829, 182, 'Other Basic Polymer Manufacturing', NULL, NULL),
(1831, 183, 'Fertiliser Manufacturing', NULL, NULL),
(1832, 183, 'Pesticide Manufacturing', NULL, NULL),
(1841, 184, 'Human Pharmaceutical and Medicinal Product Manufacturing', NULL, NULL),
(1842, 184, 'Veterinary Pharmaceutical and Medicinal Product Manufacturing', NULL, NULL),
(1851, 185, 'Cleaning Compound Manufacturing', NULL, NULL),
(1852, 185, 'Cosmetic and Toiletry Preparation Manufacturing', NULL, NULL),
(1891, 189, 'Photographic Chemical Product Manufacturing', NULL, NULL),
(1892, 189, 'Explosive Manufacturing', NULL, NULL),
(1899, 189, 'Other Basic Chemical Product Manufacturing n.e.c.', NULL, NULL),
(1911, 191, 'Polymer Film and Sheet Packaging Material Manufacturing', NULL, NULL),
(1912, 191, 'Rigid and Semi-Rigid Polymer Product Manufacturing', NULL, NULL),
(1913, 191, 'Polymer Foam Product Manufacturing', NULL, NULL),
(1914, 191, 'Tyre Manufacturing', NULL, NULL),
(1915, 191, 'Adhesive Manufacturing', NULL, NULL),
(1916, 191, 'Paint and Coatings Manufacturing', NULL, NULL),
(1919, 191, 'Other Polymer Product Manufacturing', NULL, NULL),
(1920, 192, 'Natural Rubber Product Manufacturing', NULL, NULL),
(2010, 201, 'Glass and Glass Product Manufacturing', NULL, NULL),
(2021, 202, 'Clay Brick Manufacturing', NULL, NULL),
(2029, 202, 'Other Ceramic Product Manufacturing', NULL, NULL),
(2031, 203, 'Cement and Lime Manufacturing', NULL, NULL),
(2032, 203, 'Plaster Product Manufacturing', NULL, NULL),
(2033, 203, 'Ready-Mixed Concrete Manufacturing', NULL, NULL),
(2034, 203, 'Concrete Product Manufacturing', NULL, NULL),
(2090, 209, 'Other Non-Metallic Mineral Product Manufacturing', NULL, NULL),
(2110, 211, 'Iron Smelting and Steel Manufacturing', NULL, NULL),
(2121, 212, 'Iron and Steel Casting', NULL, NULL),
(2122, 212, 'Steel Pipe and Tube Manufacturing', NULL, NULL),
(2131, 213, 'Alumina Production', NULL, NULL),
(2132, 213, 'Aluminium Smelting', NULL, NULL),
(2133, 213, 'Copper, Silver, Lead and Zinc Smelting and Refining', NULL, NULL),
(2139, 213, 'Other Basic Non-Ferrous Metal Manufacturing', NULL, NULL),
(2141, 214, 'Non-Ferrous Metal Casting', NULL, NULL),
(2142, 214, 'Aluminium Rolling, Drawing, Extruding', NULL, NULL),
(2149, 214, 'Other Basic Non-Ferrous Metal Product Manufacturing', NULL, NULL),
(2210, 221, 'Iron and Steel Forging', NULL, NULL),
(2221, 222, 'Structural Steel Fabricating', NULL, NULL),
(2222, 222, 'Prefabricated Metal Building Manufacturing', NULL, NULL),
(2223, 222, 'Architectural Aluminium Product Manufacturing', NULL, NULL),
(2224, 222, 'Metal Roof and Guttering Manufacturing (except Aluminium)', NULL, NULL),
(2229, 222, 'Other Structural Metal Product Manufacturing', NULL, NULL),
(2231, 223, 'Boiler, Tank and Other Heavy Gauge Metal Container Manufacturing', NULL, NULL),
(2239, 223, 'Other Metal Container Manufacturing', NULL, NULL),
(2240, 224, 'Sheet Metal Product Manufacturing (except Metal Structural and Container', NULL, NULL),
(2291, 229, 'Spring and Wire Product Manufacturing', NULL, NULL),
(2292, 229, 'Nut, Bolt, Screw and Rivet Manufacturing', NULL, NULL),
(2293, 229, 'Metal Coating and Finishing', NULL, NULL),
(2299, 229, 'Other Fabricated Metal Product Manufacturing n.e.c.', NULL, NULL),
(2311, 231, 'Motor Vehicle Manufacturing', NULL, NULL),
(2312, 231, 'Motor Vehicle Body and Trailer Manufacturing', NULL, NULL),
(2313, 231, 'Automotive Electrical Component Manufacturing', NULL, NULL),
(2319, 231, 'Other Motor Vehicle Parts Manufacturing', NULL, NULL),
(2391, 239, 'Shipbuilding and Repair Services', NULL, NULL),
(2392, 239, 'Boatbuilding and Repair Services', NULL, NULL),
(2393, 239, 'Railway Rolling Stock Manufacturing and Repair Services', NULL, NULL),
(2394, 239, 'Aircraft Manufacturing and Repair Services', NULL, NULL),
(2399, 239, 'Other Transport Equipment Manufacturing n.e.c.', NULL, NULL),
(2411, 241, 'Photographic, Optical and Ophthalmic Equipment Manufacturing', NULL, NULL),
(2412, 241, 'Medical and Surgical Equipment Manufacturing', NULL, NULL),
(2419, 241, 'Other Professional and Scientific Equipment Manufacturing', NULL, NULL),
(2421, 242, 'Computer and Electronic Office Equipment Manufacturing', NULL, NULL),
(2422, 242, 'Communication Equipment Manufacturing', NULL, NULL),
(2429, 242, 'Other Electronic Equipment Manufacturing', NULL, NULL),
(2431, 243, 'Electric Cable and Wire Manufacturing', NULL, NULL),
(2432, 243, 'Electric Lighting Equipment Manufacturing', NULL, NULL),
(2439, 243, 'Other Electrical Equipment Manufacturing', NULL, NULL),
(2441, 244, 'Whiteware Appliance Manufacturing', NULL, NULL),
(2449, 244, 'Other Domestic Appliance Manufacturing', NULL, NULL),
(2451, 245, 'Pump and Compressor Manufacturing', NULL, NULL),
(2452, 245, 'Fixed Space Heating, Cooling and Ventilation Equipment Manufacturing', NULL, NULL),
(2461, 246, 'Agricultural Machinery and Equipment Manufacturing', NULL, NULL),
(2462, 246, 'Mining and Construction Machinery Manufacturing', NULL, NULL),
(2463, 246, 'Machine Tool and Parts Manufacturing', NULL, NULL),
(2469, 246, 'Other Specialised Machinery and Equipment Manufacturing', NULL, NULL),
(2491, 249, 'Lifting and Material Handling Equipment Manufacturing', NULL, NULL),
(2499, 249, 'Other Machinery and Equipment Manufacturing n.e.c.', NULL, NULL),
(2511, 251, 'Wooden Furniture and Upholstered Seat Manufacturing', NULL, NULL),
(2512, 251, 'Metal Furniture Manufacturing', NULL, NULL),
(2513, 251, 'Mattress Manufacturing', NULL, NULL),
(2519, 251, 'Other Furniture Manufacturing', NULL, NULL),
(2591, 259, 'Jewellery and Silverware Manufacturing', NULL, NULL),
(2592, 259, 'Toy, Sporting and Recreational Product Manufacturing', NULL, NULL),
(2599, 259, 'Other Manufacturing n.e.c.', NULL, NULL),
(2611, 261, 'Fossil Fuel Electricity Generation', NULL, NULL),
(2612, 261, 'Hydro-Electricity Generation', NULL, NULL),
(2619, 261, 'Other Electricity Generation', NULL, NULL),
(2620, 262, 'Electricity Transmission', NULL, NULL),
(2630, 263, 'Electricity Distribution', NULL, NULL),
(2640, 264, 'On Selling Electricity and Electricity Market Operation', NULL, NULL),
(2700, 270, 'Gas Supply', NULL, NULL),
(2811, 281, 'Water Supply', NULL, NULL),
(2812, 281, 'Sewerage and Drainage Services', NULL, NULL),
(2911, 291, 'Solid Waste Collection Services', NULL, NULL),
(2919, 291, 'Other Waste Collection Services', NULL, NULL),
(2921, 292, 'Waste Treatment and Disposal Services', NULL, NULL),
(2922, 292, 'Waste Remediation and Materials Recovery Services', NULL, NULL),
(3011, 301, 'House Construction', NULL, NULL),
(3019, 301, 'Other Residential Building Construction', NULL, NULL),
(3020, 302, 'Non-Residential Building Construction', NULL, NULL),
(3101, 310, 'Road and Bridge Construction', NULL, NULL),
(3109, 310, 'Other Heavy and Civil Engineering Construction', NULL, NULL),
(3211, 321, 'Land Development and Subdivision', NULL, NULL),
(3212, 321, 'Site Preparation Services', NULL, NULL),
(3221, 322, 'Concreting Services', NULL, NULL),
(3222, 322, 'Bricklaying Services', NULL, NULL),
(3223, 322, 'Roofing Services', NULL, NULL),
(3224, 322, 'Structural Steel Erection Services', NULL, NULL),
(3231, 323, 'Plumbing Services', NULL, NULL),
(3232, 323, 'Electrical Services', NULL, NULL),
(3233, 323, 'Air Conditioning and Heating Services', NULL, NULL),
(3234, 323, 'Fire and Security Alarm Installation Services', NULL, NULL),
(3239, 323, 'Other Building Installation Services', NULL, NULL),
(3241, 324, 'Plastering and Ceiling Services', NULL, NULL),
(3242, 324, 'Carpentry Services', NULL, NULL),
(3243, 324, 'Tiling and Carpeting Services', NULL, NULL),
(3244, 324, 'Painting and Decorating Services', NULL, NULL),
(3245, 324, 'Glazing Services', NULL, NULL),
(3291, 329, 'Landscape Construction Services', NULL, NULL),
(3292, 329, 'Hire of Construction Machinery with Operator', NULL, NULL),
(3299, 329, 'Other Construction Services n.e.c.', NULL, NULL),
(3311, 331, 'Wool Wholesaling', NULL, NULL),
(3312, 331, 'Cereal Grain Wholesaling', NULL, NULL),
(3319, 331, 'Other Agricultural Product Wholesaling', NULL, NULL),
(3321, 332, 'Petroleum Product Wholesaling', NULL, NULL),
(3322, 332, 'Metal and Mineral Wholesaling', NULL, NULL),
(3323, 332, 'Industrial and Agricultural Chemical Product Wholesaling', NULL, NULL),
(3331, 333, 'Timber Wholesaling', NULL, NULL),
(3332, 333, 'Plumbing Goods Wholesaling', NULL, NULL),
(3339, 333, 'Other Hardware Goods Wholesaling', NULL, NULL),
(3411, 341, 'Agricultural and Construction Machinery Wholesaling', NULL, NULL),
(3419, 341, 'Other Specialised Industrial Machinery and Equipment Wholesaling', NULL, NULL),
(3491, 349, 'Professional and Scientific Goods Wholesaling', NULL, NULL),
(3492, 349, 'Computer and Computer Peripheral Wholesaling', NULL, NULL),
(3493, 349, 'Telecommunication Goods Wholesaling', NULL, NULL),
(3494, 349, 'Other Electrical and Electronic Goods Wholesaling', NULL, NULL),
(3499, 349, 'Other Machinery and Equipment Wholesaling n.e.c.', NULL, NULL),
(3501, 350, 'Car Wholesaling', NULL, NULL),
(3502, 350, 'Commercial Vehicle Wholesaling', NULL, NULL),
(3503, 350, 'Trailer and Other Motor Vehicle Wholesaling', NULL, NULL),
(3504, 350, 'Motor Vehicle New Parts Wholesaling', NULL, NULL),
(3505, 350, 'Motor Vehicle Dismantling and Used Parts Wholesaling', NULL, NULL),
(3601, 360, 'General Line Grocery Wholesaling', NULL, NULL),
(3602, 360, 'Meat, Poultry and Smallgoods Wholesaling', NULL, NULL),
(3603, 360, 'Dairy Produce Wholesaling', NULL, NULL),
(3604, 360, 'Fish and Seafood Wholesaling', NULL, NULL),
(3605, 360, 'Fruit and Vegetable Wholesaling', NULL, NULL),
(3606, 360, 'Liquor and Tobacco Product Wholesaling', NULL, NULL),
(3609, 360, 'Other Grocery Wholesaling', NULL, NULL),
(3711, 371, 'Textile Product Wholesaling', NULL, NULL),
(3712, 371, 'Clothing and Footwear Wholesaling', NULL, NULL),
(3720, 372, 'Pharmaceutical and Toiletry Goods Wholesaling', NULL, NULL),
(3731, 373, 'Furniture and Floor Covering Wholesaling', NULL, NULL),
(3732, 373, 'Jewellery and Watch Wholesaling', NULL, NULL),
(3733, 373, 'Kitchen and Diningware Wholesaling', NULL, NULL),
(3734, 373, 'Toy and Sporting Goods Wholesaling', NULL, NULL),
(3735, 373, 'Book and Magazine Wholesaling', NULL, NULL),
(3736, 373, 'Paper Product Wholesaling', NULL, NULL),
(3739, 373, 'Other Goods Wholesaling n.e.c.', NULL, NULL),
(3800, 380, 'Commission-Based Wholesaling', NULL, NULL),
(3911, 391, 'Car Retailing', NULL, NULL),
(3912, 391, 'Motor Cycle Retailing', NULL, NULL),
(3913, 391, 'Trailer and Other Motor Vehicle Retailing', NULL, NULL),
(3921, 392, 'Motor Vehicle Parts Retailing', NULL, NULL),
(3922, 392, 'Tyre Retailing', NULL, NULL),
(4000, 400, 'Fuel Retailing', NULL, NULL),
(4110, 411, 'Supermarket and Grocery Stores', NULL, NULL),
(4121, 412, 'Fresh Meat, Fish and Poultry Retailing', NULL, NULL),
(4122, 412, 'Fruit and Vegetable Retailing', NULL, NULL),
(4123, 412, 'Liquor Retailing', NULL, NULL),
(4129, 412, 'Other Specialised Food Retailing', NULL, NULL),
(4211, 421, 'Furniture Retailing', NULL, NULL),
(4212, 421, 'Floor Coverings Retailing', NULL, NULL),
(4213, 421, 'Houseware Retailing', NULL, NULL),
(4214, 421, 'Manchester and Other Textile Goods Retailing', NULL, NULL),
(4221, 422, 'Electrical, Electronic and Gas Appliance Retailing', NULL, NULL),
(4222, 422, 'Computer and Computer Peripheral Retailing', NULL, NULL),
(4229, 422, 'Other Electrical and Electronic Goods Retailing', NULL, NULL),
(4231, 423, 'Hardware and Building Supplies Retailing', NULL, NULL),
(4232, 423, 'Garden Supplies Retailing', NULL, NULL),
(4241, 424, 'Sport and Camping Equipment Retailing', NULL, NULL),
(4242, 424, 'Entertainment Media Retailing', NULL, NULL),
(4243, 424, 'Toy and Game Retailing', NULL, NULL),
(4244, 424, 'Newspaper and Book Retailing', NULL, NULL),
(4245, 424, 'Marine Equipment Retailing', NULL, NULL),
(4251, 425, 'Clothing Retailing', NULL, NULL),
(4252, 425, 'Footwear Retailing', NULL, NULL),
(4253, 425, 'Watch and Jewellery Retailing', NULL, NULL),
(4259, 425, 'Other Personal Accessory Retailing', NULL, NULL),
(4260, 426, 'Department Stores', NULL, NULL),
(4271, 427, 'Pharmaceutical, Cosmetic and Toiletry Goods Retailing', NULL, NULL),
(4272, 427, 'Stationery Goods Retailing', NULL, NULL),
(4273, 427, 'Antique and Used Goods Retailing', NULL, NULL),
(4274, 427, 'Flower Retailing', NULL, NULL),
(4279, 427, 'Other Store-Based Retailing n.e.c.', NULL, NULL),
(4310, 431, 'Non-Store Retailing', NULL, NULL),
(4400, 440, 'Accommodation', NULL, NULL),
(4511, 451, 'Cafes and Restaurants', NULL, NULL),
(4512, 451, 'Takeaway Food Services', NULL, NULL),
(4513, 451, 'Catering Services', NULL, NULL),
(4520, 452, 'Pubs, Taverns and Bars', NULL, NULL),
(4530, 453, 'Clubs (Hospitality)', NULL, NULL),
(4610, 461, 'Road Freight Transport', NULL, NULL),
(4621, 462, 'Interurban and Rural Bus Transport', NULL, NULL),
(4622, 462, 'Urban Bus Transport (Including Tramway)', NULL, NULL),
(4623, 462, 'Taxi and Other Road Transport', NULL, NULL),
(4710, 471, 'Rail Freight Transport', NULL, NULL),
(4720, 472, 'Rail Passenger Transport', NULL, NULL),
(4810, 481, 'Water Freight Transport', NULL, NULL),
(4820, 482, 'Water Passenger Transport', NULL, NULL),
(4900, 490, 'Air and Space Transport', NULL, NULL),
(5010, 501, 'Scenic and Sightseeing Transport', NULL, NULL),
(5021, 502, 'Pipeline Transport', NULL, NULL),
(5029, 502, 'Other Transport n.e.c.', NULL, NULL),
(5101, 510, 'Postal Services', NULL, NULL),
(5102, 510, 'Courier Pick-up and Delivery Services', NULL, NULL),
(5211, 521, 'Stevedoring Services', NULL, NULL),
(5212, 521, 'Port and Water Transport Terminal Operations', NULL, NULL),
(5219, 521, 'Other Water Transport Support Services', NULL, NULL),
(5220, 522, 'Airport Operations and Other Air Transport Support Services', NULL, NULL),
(5291, 529, 'Customs Agency Services', NULL, NULL),
(5292, 529, 'Freight Forwarding Services', NULL, NULL),
(5299, 529, 'Other Transport Support Services n.e.c.', NULL, NULL),
(5301, 530, 'Grain Storage Services', NULL, NULL),
(5309, 530, 'Other Warehousing and Storage Services', NULL, NULL),
(5411, 541, 'Newspaper Publishing', NULL, NULL),
(5412, 541, 'Magazine and Other Periodical Publishing', NULL, NULL),
(5413, 541, 'Book Publishing', NULL, NULL),
(5414, 541, 'Directory and Mailing List Publishing', NULL, NULL),
(5419, 541, 'Other Publishing (except Software, Music and Internet)', NULL, NULL),
(5420, 542, 'Software Publishing', NULL, NULL),
(5511, 551, 'Motion Picture and Video Production', NULL, NULL),
(5512, 551, 'Motion Picture and Video Distribution', NULL, NULL),
(5513, 551, 'Motion Picture Exhibition', NULL, NULL),
(5514, 551, 'Post-production Services and Other Motion Picture and Video Activities', NULL, NULL),
(5521, 552, 'Music Publishing', NULL, NULL),
(5522, 552, 'Music and Other Sound Recording Activities', NULL, NULL),
(5610, 561, 'Radio Broadcasting', NULL, NULL),
(5621, 562, 'Free-to-Air Television Broadcasting', NULL, NULL),
(5622, 562, 'Cable and Other Subscription Broadcasting', NULL, NULL),
(5700, 570, 'Internet Publishing and Broadcasting', NULL, NULL),
(5801, 580, 'Wired Telecommunications Network Operation', NULL, NULL),
(5802, 580, 'Other Telecommunications Network Operation', NULL, NULL),
(5809, 580, 'Other Telecommunications Services', NULL, NULL),
(5910, 591, 'Internet Service Providers and Web Search Portals', NULL, NULL),
(5921, 592, 'Data Processing and Web Hosting Services', NULL, NULL),
(5922, 592, 'Electronic Information Storage Services', NULL, NULL),
(6010, 601, 'Libraries and Archives', NULL, NULL),
(6020, 602, 'Other Information Services', NULL, NULL),
(6210, 621, 'Central Banking', NULL, NULL),
(6221, 622, 'Banking', NULL, NULL),
(6222, 622, 'Building Society Operation', NULL, NULL),
(6223, 622, 'Credit Union Operation', NULL, NULL),
(6229, 622, 'Other Depository Financial Intermediation', NULL, NULL),
(6230, 623, 'Non-Depository Financing', NULL, NULL),
(6240, 624, 'Financial Asset Investing', NULL, NULL),
(6310, 631, 'Life Insurance', NULL, NULL),
(6321, 632, 'Health Insurance', NULL, NULL),
(6322, 632, 'General Insurance', NULL, NULL),
(6330, 633, 'Superannuation Funds', NULL, NULL),
(6411, 641, 'Financial Asset Broking Services', NULL, NULL),
(6419, 641, 'Other Auxiliary Finance and Investment Services', NULL, NULL),
(6420, 642, 'Auxiliary Insurance Services', NULL, NULL),
(6611, 661, 'Passenger Car Rental and Hiring', NULL, NULL),
(6619, 661, 'Other Motor Vehicle and Transport Equipment Rental and Hiring', NULL, NULL),
(6620, 662, 'Farm Animal and Bloodstock Leasing', NULL, NULL),
(6631, 663, 'Heavy Machinery and Scaffolding Rental and Hiring', NULL, NULL),
(6632, 663, 'Video and Other Electronic Media Rental and Hiring', NULL, NULL),
(6639, 663, 'Other Goods and Equipment Rental and Hiring n.e.c.', NULL, NULL),
(6640, 664, 'Non-Financial Intangible Assets (Except Copyrights) Leasing', NULL, NULL),
(6711, 671, 'Residential Property Operators', NULL, NULL),
(6712, 671, 'Non-Residential Property Operators', NULL, NULL),
(6720, 672, 'Real Estate Services', NULL, NULL),
(6910, 691, 'Scientific Research Services', NULL, NULL),
(6921, 692, 'Architectural Services', NULL, NULL),
(6922, 692, 'Surveying and Mapping Services', NULL, NULL),
(6923, 692, 'Engineering Design and Engineering Consulting Services', NULL, NULL),
(6924, 692, 'Other Specialised Design Services', NULL, NULL),
(6925, 692, 'Scientific Testing and Analysis Services', NULL, NULL),
(6931, 693, 'Legal Services', NULL, NULL),
(6932, 693, 'Accounting Services', NULL, NULL),
(6940, 694, 'Advertising Services', NULL, NULL),
(6950, 695, 'Market Research and Statistical Services', NULL, NULL),
(6962, 696, 'Management Advice and Related Consulting Services', NULL, NULL),
(6970, 697, 'Veterinary Services', NULL, NULL),
(6991, 699, 'Professional Photographic Services', NULL, NULL),
(6999, 699, 'Other Professional, Scientific and Technical Services n.e.c.', NULL, NULL),
(7000, 700, 'Computer System Design and Related Services', NULL, NULL),
(7211, 721, 'Employment Placement and Recruitment Services', NULL, NULL),
(7212, 721, 'Labour Supply Services', NULL, NULL),
(7220, 722, 'Travel Agency and Tour Arrangement Services', NULL, NULL),
(7291, 729, 'Office Administrative Services', NULL, NULL),
(7292, 729, 'Document Preparation Services', NULL, NULL),
(7293, 729, 'Credit Reporting and Debt Collection Services', NULL, NULL),
(7294, 729, 'Call Centre Operation', NULL, NULL),
(7299, 729, 'Other Administrative Services n.e.c.', NULL, NULL),
(7311, 731, 'Building and Other Industrial Cleaning Services', NULL, NULL),
(7312, 731, 'Building Pest Control Services', NULL, NULL),
(7313, 731, 'Gardening Services', NULL, NULL),
(7320, 732, 'Packaging Services', NULL, NULL),
(7510, 751, 'Central Government Administration', NULL, NULL),
(7520, 752, 'State Government Administration', NULL, NULL),
(7530, 753, 'Local Government Administration', NULL, NULL),
(7540, 754, 'Justice', NULL, NULL),
(7551, 755, 'Domestic Government Representation', NULL, NULL),
(7552, 755, 'Foreign Government Representation', NULL, NULL),
(7600, 760, 'Defence', NULL, NULL),
(7711, 771, 'Police Services', NULL, NULL),
(7712, 771, 'Investigation and Security Services', NULL, NULL),
(7713, 771, 'Fire Protection and Other Emergency Services', NULL, NULL),
(7714, 771, 'Correctional and Detention Services', NULL, NULL),
(7719, 771, 'Other Public Order and Safety Services', NULL, NULL),
(7720, 772, 'Regulatory Services', NULL, NULL),
(8010, 801, 'Preschool Education', NULL, NULL),
(8021, 802, 'Primary Education', NULL, NULL),
(8022, 802, 'Secondary Education', NULL, NULL),
(8023, 802, 'Combined Primary and Secondary Education', NULL, NULL),
(8024, 802, 'Special School Education', NULL, NULL),
(8101, 810, 'Technical and Vocational Education and Training', NULL, NULL),
(8102, 810, 'Higher Education', NULL, NULL),
(8211, 821, 'Sports and Physical Recreation Instruction', NULL, NULL),
(8212, 821, 'Arts Education', NULL, NULL),
(8219, 821, 'Adult, Community and Other Education n.e.c.', NULL, NULL),
(8220, 822, 'Educational Support Services', NULL, NULL),
(8401, 840, 'Hospitals (Except Psychiatric Hospitals)', NULL, NULL),
(8402, 840, 'Psychiatric Hospitals', NULL, NULL),
(8511, 851, 'General Practice Medical Services', NULL, NULL),
(8512, 851, 'Specialist Medical Services', NULL, NULL),
(8520, 852, 'Pathology and Diagnostic Imaging Services', NULL, NULL),
(8531, 853, 'Dental Services', NULL, NULL),
(8532, 853, 'Optometry and Optical Dispensing', NULL, NULL),
(8533, 853, 'Physiotherapy Services', NULL, NULL),
(8534, 853, 'Chiropractic and Osteopathic Services', NULL, NULL),
(8539, 853, 'Other Allied Health Services', NULL, NULL),
(8591, 859, 'Ambulance Services', NULL, NULL),
(8599, 859, 'Other Health Care Services n.e.c.', NULL, NULL),
(8601, 860, 'Aged Care Residential Services', NULL, NULL),
(8609, 860, 'Other Residential Care Services', NULL, NULL),
(8710, 871, 'Child Care Services', NULL, NULL),
(8790, 879, 'Other Social Assistance Services', NULL, NULL),
(8910, 891, 'Museum Operation', NULL, NULL),
(8921, 892, 'Zoological and Botanical Gardens Operation', NULL, NULL),
(8922, 892, 'Nature Reserves and Conservation Parks Operation', NULL, NULL),
(9001, 900, 'Performing Arts Operation', NULL, NULL),
(9002, 900, 'Creative Artists, Musicians, Writers and Performers', NULL, NULL),
(9003, 900, 'Performing Arts Venue Operation', NULL, NULL),
(9111, 911, 'Health and Fitness Centres and Gymnasia Operation', NULL, NULL),
(9112, 911, 'Sports and Physical Recreation Clubs and Sports Professionals', NULL, NULL),
(9113, 911, 'Sports and Physical Recreation Venues, Grounds and Facilities Operation', NULL, NULL),
(9114, 911, 'Sports and Physical Recreation Administrative Service', NULL, NULL),
(9121, 912, 'Horse and Dog Racing Administration and Track Operation', NULL, NULL),
(9129, 912, 'Other Horse and Dog Racing Activities', NULL, NULL),
(9131, 913, 'Amusement Parks and Centres Operation', NULL, NULL),
(9139, 913, 'Amusement and Other Recreational Activities n.e.c.', NULL, NULL),
(9201, 920, 'Casino Operation', NULL, NULL),
(9202, 920, 'Lottery Operation', NULL, NULL),
(9209, 920, 'Other Gambling Activities', NULL, NULL),
(9411, 941, 'Automotive Electrical Services', NULL, NULL),
(9412, 941, 'Automotive Body, Paint and Interior Repair', NULL, NULL),
(9419, 941, 'Other Automotive Repair and Maintenance', NULL, NULL),
(9421, 942, 'Domestic Appliance Repair and Maintenance', NULL, NULL),
(9422, 942, 'Electronic (except Domestic Appliance) and Precision Equipment Repair', NULL, NULL),
(9429, 942, 'Other Machinery and Equipment Repair and Maintenance', NULL, NULL),
(9491, 949, 'Clothing and Footwear Repair', NULL, NULL),
(9499, 949, 'Other Repair and Maintenance n.e.c.', NULL, NULL),
(9511, 951, 'Hairdressing and Beauty Services', NULL, NULL),
(9512, 951, 'Diet and Weight Reduction Centre Operation', NULL, NULL),
(9520, 952, 'Funeral, Crematorium and Cemetery Services', NULL, NULL),
(9531, 953, 'Laundry and Dry-Cleaning Services', NULL, NULL),
(9532, 953, 'Photographic Film Processing', NULL, NULL),
(9533, 953, 'Parking Services', NULL, NULL),
(9534, 953, 'Brothel Keeping and Prostitution Services', NULL, NULL),
(9539, 953, 'Other Personal Services n.e.c.', NULL, NULL),
(9540, 954, 'Religious Services', NULL, NULL),
(9551, 955, 'Business and Professional Association Services', NULL, NULL),
(9552, 955, 'Labour Association Services', NULL, NULL),
(9559, 955, 'Other Interest Group Services n.e.c.', NULL, NULL),
(9601, 960, 'Private Households Employing Staff', NULL, NULL),
(9602, 960, 'Undifferentiated Goods-Producing Activities of Private Households for Own Use', NULL, NULL),
(9603, 960, 'Undifferentiated Service-Producing Activities of Private Households for Own Use', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_group`
--

CREATE TABLE `company_group` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sub_divisions_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_group`
--

INSERT INTO `company_group` (`id`, `sub_divisions_id`, `name`, `created_at`, `updated_at`) VALUES
(11, 1, 'Nursery and Floriculture Production', NULL, NULL),
(12, 1, 'Mushroom and Vegetable Growing', NULL, NULL),
(13, 1, 'Fruit and Tree Nut Growing', NULL, NULL),
(14, 1, 'Sheep, Beef Cattle and Grain Farming', NULL, NULL),
(15, 1, 'Other Crop Growing', NULL, NULL),
(16, 1, 'Dairy Cattle Farming', NULL, NULL),
(17, 1, 'Poultry Farming', NULL, NULL),
(18, 1, 'Deer Farming', NULL, NULL),
(19, 1, 'Other Livestock Farming', NULL, NULL),
(20, 2, 'Aquaculture', NULL, NULL),
(30, 3, 'Forestry and Logging', NULL, NULL),
(41, 4, 'Fishing', NULL, NULL),
(42, 4, 'Hunting and Trapping', NULL, NULL),
(51, 5, 'Forestry Support Services', NULL, NULL),
(52, 5, 'Agriculture and Fishing Support Services', NULL, NULL),
(60, 6, 'Coal Mining', NULL, NULL),
(70, 7, 'Oil and Gas Extraction', NULL, NULL),
(80, 8, 'Metal Ore Mining', NULL, NULL),
(91, 9, 'Construction Material Mining', NULL, NULL),
(99, 9, 'Other Non-Metallic Mineral Mining and Quarrying', NULL, NULL),
(101, 10, 'Exploration', NULL, NULL),
(109, 10, 'Other Mining Support Services', NULL, NULL),
(111, 11, 'Meat and Meat Product Manufacturing', NULL, NULL),
(112, 11, 'Seafood Processing', NULL, NULL),
(113, 11, 'Dairy Product Manufacturing', NULL, NULL),
(114, 11, 'Fruit and Vegetable Processing', NULL, NULL),
(115, 11, 'Oil and Fat Manufacturing', NULL, NULL),
(116, 11, 'Grain Mill and Cereal Product Manufacturing', NULL, NULL),
(117, 11, 'Bakery Product Manufacturing', NULL, NULL),
(118, 11, 'Sugar and Confectionery Manufacturing', NULL, NULL),
(119, 11, 'Other Food Product Manufacturing', NULL, NULL),
(121, 12, 'Beverage Manufacturing', NULL, NULL),
(122, 12, 'Cigarette and Tobacco Product Manufacturing', NULL, NULL),
(131, 13, 'Textile Manufacturing', NULL, NULL),
(132, 13, 'Leather Tanning, Fur Dressing and Leather Product Manufacturing', NULL, NULL),
(133, 13, 'Textile Product Manufacturing', NULL, NULL),
(134, 13, 'Knitted Product Manufacturing', NULL, NULL),
(135, 13, 'Clothing and Footwear Manufacturing', NULL, NULL),
(141, 14, 'Log Sawmilling and Timber Dressing', NULL, NULL),
(149, 14, 'Other Wood Product Manufacturing', NULL, NULL),
(151, 15, 'Pulp, Paper and Paperboard Manufacturing', NULL, NULL),
(152, 15, 'Converted Paper Product Manufacturing', NULL, NULL),
(161, 16, 'Printing and Printing Support Services', NULL, NULL),
(162, 16, 'Reproduction of Recorded Media', NULL, NULL),
(170, 17, 'Petroleum and Coal Product Manufacturing', NULL, NULL),
(181, 18, 'Basic Chemical Manufacturing', NULL, NULL),
(182, 18, 'Basic Polymer Manufacturing', NULL, NULL),
(183, 18, 'Fertiliser and Pesticide Manufacturing', NULL, NULL),
(184, 18, 'Pharmaceutical and Medicinal Product Manufacturing', NULL, NULL),
(185, 18, 'Cleaning Compound and Toiletry Preparation Manufacturing', NULL, NULL),
(189, 18, 'Other Basic Chemical Product Manufacturing', NULL, NULL),
(191, 19, 'Polymer Product Manufacturing', NULL, NULL),
(192, 19, 'Natural Rubber Product Manufacturing', NULL, NULL),
(201, 20, 'Glass and Glass Product Manufacturing', NULL, NULL),
(202, 20, 'Ceramic Product Manufacturing', NULL, NULL),
(203, 20, 'Cement, Lime, Plaster and Concrete Product Manufacturing', NULL, NULL),
(209, 20, 'Other Non-Metallic Mineral Product Manufacturing', NULL, NULL),
(211, 21, 'Basic Ferrous Metal Manufacturing', NULL, NULL),
(212, 21, 'Basic Ferrous Metal Product Manufacturing', NULL, NULL),
(213, 21, 'Basic Non-Ferrous Metal Manufacturing', NULL, NULL),
(214, 21, 'Basic Non-Ferrous Metal Product Manufacturing', NULL, NULL),
(221, 22, 'Iron and Steel Forging', NULL, NULL),
(222, 22, 'Structural Metal Product Manufacturing', NULL, NULL),
(223, 22, 'Metal Container Manufacturing', NULL, NULL),
(224, 22, 'Sheet Metal Product Manufacturing (except Metal Structural and Container Products)', NULL, NULL),
(229, 22, 'Other Fabricated Metal Product Manufacturing', NULL, NULL),
(231, 23, 'Motor Vehicle and Motor Vehicle Part Manufacturing', NULL, NULL),
(239, 23, 'Other Transport Equipment Manufacturing', NULL, NULL),
(241, 24, 'Professional and Scientific Equipment Manufacturing', NULL, NULL),
(242, 24, 'Computer and Electronic Equipment Manufacturing', NULL, NULL),
(243, 24, 'Electrical Equipment Manufacturing', NULL, NULL),
(244, 24, 'Domestic Appliance Manufacturing', NULL, NULL),
(245, 24, 'Pump, Compressor, Heating and Ventilation Equipment Manufacturing', NULL, NULL),
(246, 24, 'Specialised Machinery and Equipment Manufacturing', NULL, NULL),
(249, 24, 'Other Machinery and Equipment Manufacturing', NULL, NULL),
(251, 25, 'Furniture Manufacturing', NULL, NULL),
(259, 25, 'Other Manufacturing', NULL, NULL),
(261, 26, 'Electricity Generation', NULL, NULL),
(262, 26, 'Electricity Transmission', NULL, NULL),
(263, 26, 'Electricity Distribution', NULL, NULL),
(264, 26, 'On Selling Electricity and Electricity Market Operation', NULL, NULL),
(270, 27, 'Gas Supply', NULL, NULL),
(281, 28, 'Water Supply, Sewerage and Drainage Services', NULL, NULL),
(291, 29, 'Waste Collection Services', NULL, NULL),
(292, 29, 'Waste Treatment, Disposal and Remediation Services', NULL, NULL),
(301, 30, 'Residential Building Construction', NULL, NULL),
(302, 30, 'Non-Residential Building Construction', NULL, NULL),
(310, 31, 'Heavy and Civil Engineering Construction', NULL, NULL),
(321, 32, 'Land Development and Site Preparation Services', NULL, NULL),
(322, 32, 'Building Structure Services', NULL, NULL),
(323, 32, 'Building Installation Services', NULL, NULL),
(324, 32, 'Building Completion Services', NULL, NULL),
(329, 32, 'Other Construction Services', NULL, NULL),
(331, 33, 'Agricultural Product Wholesaling', NULL, NULL),
(332, 33, 'Mineral, Metal and Chemical Wholesaling', NULL, NULL),
(333, 33, 'Timber and Hardware Goods Wholesaling', NULL, NULL),
(341, 34, 'Specialised Industrial Machinery and Equipment Wholesaling', NULL, NULL),
(349, 34, 'Other Machinery and Equipment Wholesaling', NULL, NULL),
(350, 35, 'Motor Vehicle and Motor Vehicle Parts Wholesaling', NULL, NULL),
(360, 36, 'Grocery, Liquor and Tobacco Product Wholesaling', NULL, NULL),
(371, 37, 'Textile, Clothing and Footwear Wholesaling', NULL, NULL),
(372, 37, 'Pharmaceutical and Toiletry Goods Wholesaling', NULL, NULL),
(373, 37, 'Furniture, Floor Covering and Other Goods Wholesaling', NULL, NULL),
(380, 38, 'Commission-Based Wholesaling', NULL, NULL),
(391, 39, 'Motor Vehicle Retailing', NULL, NULL),
(392, 39, 'Motor Vehicle Parts and Tyre Retailing', NULL, NULL),
(400, 40, 'Fuel Retailing', NULL, NULL),
(411, 41, 'Supermarket and Grocery Stores', NULL, NULL),
(412, 41, 'Specialised Food Retailing', NULL, NULL),
(421, 42, 'Furniture, Floor Coverings, Houseware and Textile Goods Retailing', NULL, NULL),
(422, 42, 'Electrical and Electronic Goods Retailing', NULL, NULL),
(423, 42, 'Hardware, Building and Garden Supplies Retailing', NULL, NULL),
(424, 42, 'Recreational Goods Retailing', NULL, NULL),
(425, 42, 'Clothing, Footwear and Personal Accessory Retailing', NULL, NULL),
(426, 42, 'Department Stores', NULL, NULL),
(427, 42, 'Pharmaceutical and Other Store-Based Retailing', NULL, NULL),
(431, 43, 'Non-Store Retailing', NULL, NULL),
(432, 43, 'Retail Commission-Based Buying and/or Selling', NULL, NULL),
(440, 44, 'Accommodation', NULL, NULL),
(451, 45, 'Cafes, Restaurants and Takeaway Food Services', NULL, NULL),
(452, 45, 'Pubs, Taverns and Bars', NULL, NULL),
(453, 45, 'Clubs (Hospitality)', NULL, NULL),
(461, 46, 'Road Freight Transport', NULL, NULL),
(462, 46, 'Road Passenger Transport', NULL, NULL),
(471, 47, 'Rail Freight Transport', NULL, NULL),
(472, 47, 'Rail Passenger Transport', NULL, NULL),
(481, 48, 'Water Freight Transport', NULL, NULL),
(482, 48, 'Water Passenger Transport', NULL, NULL),
(490, 49, 'Air and Space Transport', NULL, NULL),
(501, 50, 'Scenic and Sightseeing Transport', NULL, NULL),
(502, 50, 'Pipeline and Other Transport', NULL, NULL),
(510, 51, 'Postal and Courier Pick-up and Delivery Services', NULL, NULL),
(521, 52, 'Water Transport Support Services', NULL, NULL),
(522, 52, 'Airport Operations and Other Air Transport Support Services', NULL, NULL),
(529, 52, 'Other Transport Support Services', NULL, NULL),
(530, 53, 'Warehousing and Storage Services', NULL, NULL),
(541, 54, 'Newspaper, Periodical, Book and Directory Publishing', NULL, NULL),
(542, 54, 'Software Publishing', NULL, NULL),
(551, 55, 'Motion Picture and Video Activities', NULL, NULL),
(552, 55, 'Sound Recording and Music Publishing', NULL, NULL),
(561, 56, 'Radio Broadcasting', NULL, NULL),
(562, 56, 'Television Broadcasting', NULL, NULL),
(570, 57, 'Internet Publishing and Broadcasting', NULL, NULL),
(580, 58, 'Telecommunications Services', NULL, NULL),
(591, 59, 'Internet Service Providers and Web Search Portals', NULL, NULL),
(592, 59, 'Data Processing, Web Hosting and Electronic Information Storage Services', NULL, NULL),
(601, 60, 'Libraries and Archives', NULL, NULL),
(602, 60, 'Other Information Services', NULL, NULL),
(621, 62, 'Central Banking', NULL, NULL),
(622, 62, 'Depository Financial Intermediation', NULL, NULL),
(623, 62, 'Non-Depository Financing', NULL, NULL),
(624, 62, 'Financial Asset Investing', NULL, NULL),
(631, 63, 'Life Insurance', NULL, NULL),
(632, 63, 'Health and General Insurance', NULL, NULL),
(633, 63, 'Superannuation Funds', NULL, NULL),
(641, 64, 'Auxiliary Finance and Investment Services', NULL, NULL),
(642, 64, 'Auxiliary Insurance Services', NULL, NULL),
(661, 66, 'Motor Vehicle and Transport Equipment Rental and Hiring', NULL, NULL),
(662, 66, 'Farm Animal and Bloodstock Leasing', NULL, NULL),
(663, 66, 'Other Goods and Equipment Rental and Hiring', NULL, NULL),
(664, 66, 'Non-Financial Intangible Assets (Except Copyrights) Leasing', NULL, NULL),
(671, 67, 'Property Operators', NULL, NULL),
(672, 67, 'Real Estate Services', NULL, NULL),
(691, 69, 'Scientific Research Services', NULL, NULL),
(692, 69, 'Architectural, Engineering and Technical Services', NULL, NULL),
(693, 69, 'Legal and Accounting Services', NULL, NULL),
(694, 69, 'Advertising Services', NULL, NULL),
(695, 69, 'Market Research and Statistical Services', NULL, NULL),
(696, 69, 'Management and Related Consulting Services', NULL, NULL),
(697, 69, 'Veterinary Services', NULL, NULL),
(699, 69, 'Other Professional, Scientific and Technical Services', NULL, NULL),
(700, 70, 'Computer System Design and Related Services', NULL, NULL),
(721, 72, 'Employment Services', NULL, NULL),
(722, 72, 'Travel Agency and Tour Arrangement Services', NULL, NULL),
(729, 72, 'Other Administrative Services', NULL, NULL),
(731, 73, 'Building Cleaning, Pest Control and Gardening Services', NULL, NULL),
(732, 73, 'Packaging Services', NULL, NULL),
(751, 75, 'Central Government Administration', NULL, NULL),
(752, 75, 'State Government Administration', NULL, NULL),
(753, 75, 'Local Government Administration', NULL, NULL),
(754, 75, 'Justice', NULL, NULL),
(755, 75, 'Government Representation', NULL, NULL),
(760, 76, 'Defence', NULL, NULL),
(771, 77, 'Public Order and Safety Services', NULL, NULL),
(772, 77, 'Regulatory Services', NULL, NULL),
(801, 80, 'Preschool Education', NULL, NULL),
(802, 80, 'School Education', NULL, NULL),
(810, 81, 'Tertiary Education', NULL, NULL),
(821, 82, 'Adult, Community and Other Education', NULL, NULL),
(822, 82, 'Educational Support Services', NULL, NULL),
(840, 84, 'Hospitals', NULL, NULL),
(851, 85, 'Medical Services', NULL, NULL),
(852, 85, 'Pathology and Diagnostic Imaging Services', NULL, NULL),
(853, 85, 'Allied Health Services', NULL, NULL),
(859, 85, 'Other Health Care Services', NULL, NULL),
(860, 86, 'Residential Care Services', NULL, NULL),
(871, 87, 'Child Care Services', NULL, NULL),
(879, 87, 'Other Social Assistance Services', NULL, NULL),
(891, 89, 'Museum Operation', NULL, NULL),
(892, 89, 'Parks and Gardens Operations', NULL, NULL),
(900, 90, 'Creative and Performing Arts Activities', NULL, NULL),
(911, 91, 'Sports and Physical Recreation Activities', NULL, NULL),
(912, 91, 'Horse and Dog Racing Activities', NULL, NULL),
(913, 91, 'Amusement and Other Recreation Activities', NULL, NULL),
(920, 92, 'Gambling Activities', NULL, NULL),
(941, 94, 'Automotive Repair and Maintenance', NULL, NULL),
(942, 94, 'Machinery and Equipment Repair and Maintenance', NULL, NULL),
(949, 94, 'Other Repair and Maintenance', NULL, NULL),
(951, 95, 'Personal Care Services', NULL, NULL),
(952, 95, 'Funeral, Crematorium and Cemetery Services', NULL, NULL),
(953, 95, 'Other Personal Services', NULL, NULL),
(954, 95, 'Religious Services', NULL, NULL),
(955, 95, 'Civic, Professional and Other Interest Group Services', NULL, NULL),
(960, 96, 'Private Households Employing Staff and Undifferentiated Goods- and Service-Producing Activities of Households for Own Use', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_type_id` int(11) NOT NULL,
  `abn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rbn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `established_date` date NOT NULL,
  `confidentialiity_id` int(11) DEFAULT NULL,
  `portfolio_analysis_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_unit_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_street_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_street_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_street_suburb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_suburb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_telephone_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_telephone_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anzic_classification_division_id` int(11) DEFAULT NULL,
  `anzic_classification_sub_division_id` int(11) DEFAULT NULL,
  `anzic_classification_group_id` int(11) DEFAULT NULL,
  `anzic_classification_class_id` int(11) DEFAULT NULL,
  `primary_classification_division_id` int(11) DEFAULT NULL,
  `primary_classification_sub_division_id` int(11) DEFAULT NULL,
  `primary_classification_group_id` int(11) DEFAULT NULL,
  `primary_classification_class_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `name`, `entity_type_id`, `abn`, `acn`, `rbn`, `equity`, `established_date`, `confidentialiity_id`, `portfolio_analysis_status`, `address_unit_number`, `address_street_number`, `address_street_name`, `address_street_suburb`, `address_suburb`, `address_state`, `country_code`, `address_postal_code`, `contact_telephone_type`, `contact_telephone_number`, `business_telephone_type`, `business_telephone_number`, `anzic_classification_division_id`, `anzic_classification_sub_division_id`, `anzic_classification_group_id`, `anzic_classification_class_id`, `primary_classification_division_id`, `primary_classification_sub_division_id`, `primary_classification_group_id`, `primary_classification_class_id`, `created_by`, `updated_by`, `deleted_at`, `created_at`, `updated_at`, `company_token`) VALUES
(1, 'dsdsds', 1, '123', NULL, NULL, NULL, '2020-02-20', NULL, '--Select Option--', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, '2020-02-08 01:45:48', '2020-02-08 01:45:48', '29cd34294ec67f9eae76385b14b83afe'),
(2, 'ABC', 1, '11004251861', NULL, '343434', NULL, '2020-02-21', NULL, '--Select Option--', NULL, NULL, NULL, NULL, NULL, NULL, 'AD', NULL, '0', '0770155500', '0', '0770155500', 1, 2, 20, 201, NULL, NULL, NULL, NULL, 3, 1, NULL, '2020-02-08 01:46:51', '2020-02-21 22:46:05', '32fde3117a2c2a0390889fdddca9fd71'),
(3, 'XYZ', 1, NULL, NULL, '121212', NULL, '2020-02-19', NULL, '--Select Option--', NULL, NULL, NULL, NULL, NULL, NULL, 'AD', NULL, '0', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, '2020-02-08 02:04:59', '2020-02-08 02:06:52', '512452c798615cbd798b49a03867987c'),
(4, 'XYZ', 1, '343434', NULL, NULL, NULL, '2020-02-20', NULL, '--Select Option--', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 13, 133, 1333, 3, 3, NULL, '2020-02-08 02:08:40', '2020-02-08 02:08:40', '407be60520eeeb8c3a3b6f8f50f50451');

-- --------------------------------------------------------

--
-- Table structure for table `confidentiality`
--

CREATE TABLE `confidentiality` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `confidentiality`
--

INSERT INTO `confidentiality` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Public', NULL, NULL),
(2, 'Asic', NULL, NULL),
(3, 'Published', NULL, NULL),
(4, 'Confidential', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_code`, `name`, `created_at`, `updated_at`) VALUES
('AD', 'Andorra', NULL, NULL),
('AE', 'United Arab Emirates', NULL, NULL),
('AF', 'Afghanistan', NULL, NULL),
('AG', 'Antigua And Barbuda', NULL, NULL),
('AI', 'Anguilla', NULL, NULL),
('AL', 'Albania', NULL, NULL),
('AM', 'Armenia', NULL, NULL),
('AN', 'Netherlands Antilles', NULL, NULL),
('AO', 'Angola', NULL, NULL),
('AR', 'Argentina', NULL, NULL),
('AS', 'American Samoa', NULL, NULL),
('AT', 'Austria', NULL, NULL),
('AU', 'Australia', NULL, NULL),
('AW', 'Aruba', NULL, NULL),
('AZ', 'Azerbaijan', NULL, NULL),
('BA', 'Bosnia And Herzegovina', NULL, NULL),
('BB', 'Barbados', NULL, NULL),
('BD', 'Bangladesh', NULL, NULL),
('BE', 'Belgium', NULL, NULL),
('BF', 'Burkina Faso', NULL, NULL),
('BG', 'Bulgaria', NULL, NULL),
('BH', 'Bahrain', NULL, NULL),
('BI', 'Burundi', NULL, NULL),
('BJ', 'Benin', NULL, NULL),
('BL', 'Saint Barthelemy', NULL, NULL),
('BM', 'Bermuda', NULL, NULL),
('BN', 'Brunei Darussalam', NULL, NULL),
('BO', 'Bolivia', NULL, NULL),
('BQ', 'BONAIRE', NULL, NULL),
('BR', 'Brazil', NULL, NULL),
('BS', 'Bahamas', NULL, NULL),
('BT', 'Bhutan', NULL, NULL),
('BW', 'Botswana', NULL, NULL),
('BY', 'Belarus', NULL, NULL),
('BZ', 'Belize', NULL, NULL),
('CA', 'Canada', NULL, NULL),
('CD', 'CONGO DR', NULL, NULL),
('CF', 'Central African Republic', NULL, NULL),
('CG', 'Republic of the Congo', NULL, NULL),
('CH', 'Switzerland', NULL, NULL),
('CI', 'Cote dIvoire', NULL, NULL),
('CK', 'Cook Islands', NULL, NULL),
('CL', 'Chile', NULL, NULL),
('CM', 'Cameroon', NULL, NULL),
('CN', 'China', NULL, NULL),
('CO', 'Colombia', NULL, NULL),
('CR', 'Costa Rica', NULL, NULL),
('CS', 'Serbia & Montenegro', NULL, NULL),
('CU', 'Cuba', NULL, NULL),
('CV', 'Cape Verde', NULL, NULL),
('CW', 'CURACAO', NULL, NULL),
('CY', 'Cyprus', NULL, NULL),
('CZ', 'Czech Republic', NULL, NULL),
('DE', 'Germany', NULL, NULL),
('DJ', 'Djibouti', NULL, NULL),
('DK', 'Denmark', NULL, NULL),
('DM', 'Dominica', NULL, NULL),
('DO', 'Dominican Republic', NULL, NULL),
('DZ', 'Algeria', NULL, NULL),
('EC', 'Ecuador', NULL, NULL),
('EE', 'Estonia', NULL, NULL),
('EG', 'Egypt', NULL, NULL),
('ER', 'Eritrea', NULL, NULL),
('ES', 'Spain', NULL, NULL),
('ET', 'Ethiopia', NULL, NULL),
('FI', 'Finland', NULL, NULL),
('FJ', 'Fiji', NULL, NULL),
('FM', 'Micronesia', NULL, NULL),
('FO', 'Faroe Islands', NULL, NULL),
('FR', 'France', NULL, NULL),
('GA', 'Gabon', NULL, NULL),
('GB', 'United Kingdom', NULL, NULL),
('GD', 'Grenada', NULL, NULL),
('GE', 'Georgia', NULL, NULL),
('GF', 'French Guiana', NULL, NULL),
('GH', 'Ghana', NULL, NULL),
('GI', 'Gibraltar', NULL, NULL),
('GL', 'Greenland', NULL, NULL),
('GM', 'Gambia', NULL, NULL),
('GN', 'Guinea', NULL, NULL),
('GP', 'Guadeloupe', NULL, NULL),
('GQ', 'Equatorial Guinea', NULL, NULL),
('GR', 'Greece', NULL, NULL),
('GT', 'Guatemala', NULL, NULL),
('GU', 'Guam', NULL, NULL),
('GW', 'Guinea-Bissau', NULL, NULL),
('GY', 'Guyana', NULL, NULL),
('HK', 'Hong Kong', NULL, NULL),
('HN', 'Honduras', NULL, NULL),
('HR', 'Croatia', NULL, NULL),
('HT', 'Haiti', NULL, NULL),
('HU', 'Hungary', NULL, NULL),
('ID', 'Indonesia', NULL, NULL),
('IE', 'Ireland', NULL, NULL),
('IL', 'Israel', NULL, NULL),
('IN', 'India', NULL, NULL),
('IQ', 'Iraq', NULL, NULL),
('IR', 'Iran', NULL, NULL),
('IS', 'Iceland', NULL, NULL),
('IT', 'Italy', NULL, NULL),
('JE', 'Jersey', NULL, NULL),
('JM', 'Jamaica', NULL, NULL),
('JO', 'Jordan', NULL, NULL),
('JP', 'Japan', NULL, NULL),
('KE', 'Kenya', NULL, NULL),
('KG', 'Kyrgyzstan', NULL, NULL),
('KH', 'Cambodia', NULL, NULL),
('KM', 'COMOROS', NULL, NULL),
('KN', 'Saint Kitts And Nevis', NULL, NULL),
('KP', 'Korea', NULL, NULL),
('KR', 'South Korea', NULL, NULL),
('KS', 'Comoros', NULL, NULL),
('KW', 'Kuwait', NULL, NULL),
('KY', 'Cayman Islands', NULL, NULL),
('KZ', 'Kazakhstan', NULL, NULL),
('LA', 'Laos', NULL, NULL),
('LB', 'Lebanon', NULL, NULL),
('LC', 'Saint Lucia', NULL, NULL),
('LI', 'Liechtenstein', NULL, NULL),
('LK', 'Sri Lanka', NULL, NULL),
('LR', 'Liberia', NULL, NULL),
('LS', 'Lesotho', NULL, NULL),
('LT', 'Lithuania', NULL, NULL),
('LU', 'Luxembourg', NULL, NULL),
('LV', 'Latvia', NULL, NULL),
('LY', 'Libyan Arab Jamahiriya', NULL, NULL),
('MA', 'Morocco', NULL, NULL),
('MC', 'Monaco', NULL, NULL),
('MD', 'Moldova', NULL, NULL),
('ME', 'Montenegro', NULL, NULL),
('MF', 'Sint Maarten', NULL, NULL),
('MG', 'Madagascar', NULL, NULL),
('MK', 'Macedonia', NULL, NULL),
('ML', 'Mali', NULL, NULL),
('MM', 'Myanmar', NULL, NULL),
('MN', 'Mongolia', NULL, NULL),
('MO', 'Macau', NULL, NULL),
('MP', 'Northern Mariana Islands', NULL, NULL),
('MQ', 'Martinique', NULL, NULL),
('MR', 'Mauritania', NULL, NULL),
('MT', 'Malta', NULL, NULL),
('MU', 'Mauritius', NULL, NULL),
('MV', 'Maldives', NULL, NULL),
('MW', 'Malawi', NULL, NULL),
('MX', 'Mexico', NULL, NULL),
('MY', 'Malaysia', NULL, NULL),
('MZ', 'Mozambique', NULL, NULL),
('NA', 'Namibia', NULL, NULL),
('NC', 'New Caledonia', NULL, NULL),
('NE', 'NIGER', NULL, NULL),
('NF', 'Norfolk Island', NULL, NULL),
('NG', 'Nigeria', NULL, NULL),
('NI', 'Nicaragua', NULL, NULL),
('NL', 'Netherlands', NULL, NULL),
('NO', 'Norway', NULL, NULL),
('NP', 'Nepal', NULL, NULL),
('NU', 'NIUE', NULL, NULL),
('NY', 'Northern Cyprus', NULL, NULL),
('NZ', 'New Zealand', NULL, NULL),
('OM', 'Oman', NULL, NULL),
('PA', 'Panama', NULL, NULL),
('PE', 'Peru', NULL, NULL),
('PF', 'French Polynesia', NULL, NULL),
('PG', 'Papua New Guinea', NULL, NULL),
('PH', 'Philippines', NULL, NULL),
('PK', 'Pakistan', NULL, NULL),
('PL', 'Poland', NULL, NULL),
('PR', 'Puerto Rico', NULL, NULL),
('PS', 'Palestinian Territory', NULL, NULL),
('PT', 'Portugal', NULL, NULL),
('PW', 'Palau', NULL, NULL),
('PY', 'Paraguay', NULL, NULL),
('QA', 'Qatar', NULL, NULL),
('RE', 'Reunion', NULL, NULL),
('RO', 'Romania', NULL, NULL),
('RS', 'Serbia', NULL, NULL),
('RU', 'Russia', NULL, NULL),
('RW', 'Rwanda', NULL, NULL),
('SA', 'Saudi Arabia', NULL, NULL),
('SB', 'SOLOMON ISLANDS', NULL, NULL),
('SC', 'Seychelles', NULL, NULL),
('SD', 'Sudan', NULL, NULL),
('SE', 'Sweden', NULL, NULL),
('SG', 'Singapore', NULL, NULL),
('SI', 'Slovenia', NULL, NULL),
('SJ', 'Svalbard And Jan Mayen', NULL, NULL),
('SK', 'Slovakia', NULL, NULL),
('SL', 'Sierra Leone', NULL, NULL),
('SM', 'San Marino', NULL, NULL),
('SN', 'Senegal', NULL, NULL),
('SR', 'Suriname', NULL, NULL),
('SS', 'South Sudan', NULL, NULL),
('ST', 'Sao Tome And Principe', NULL, NULL),
('SV', 'El Salvador', NULL, NULL),
('SX', 'Sint Maarten', NULL, NULL),
('SY', 'Syrian Arab Republic', NULL, NULL),
('SZ', 'Swaziland', NULL, NULL),
('TC', 'Turks And Caicos Islands', NULL, NULL),
('TD', 'Chad', NULL, NULL),
('TG', 'Togo', NULL, NULL),
('TH', 'Thailand', NULL, NULL),
('TJ', 'Tajikistan', NULL, NULL),
('TM', 'Turkmenistan', NULL, NULL),
('TN', 'Tunisia', NULL, NULL),
('TO', 'Tonga', NULL, NULL),
('TR', 'Turkey', NULL, NULL),
('TT', 'Trinidad And Tobago', NULL, NULL),
('TW', 'Taiwan', NULL, NULL),
('TZ', 'Tanzania', NULL, NULL),
('UA', 'Ukraine', NULL, NULL),
('UG', 'Uganda', NULL, NULL),
('US', 'United States', NULL, NULL),
('UY', 'Uruguay', NULL, NULL),
('UZ', 'Uzbekistan', NULL, NULL),
('VC', 'Saint Vincent And The Grenadines', NULL, NULL),
('VE', 'Venezuela', NULL, NULL),
('VG', 'British Virgin Islands', NULL, NULL),
('VI', 'US Virgin Islands', NULL, NULL),
('VN', 'Vietnam', NULL, NULL),
('VU', 'Vanuatu', NULL, NULL),
('WS', 'Samoa', NULL, NULL),
('WX', 'UK - Wales', NULL, NULL),
('WY', 'UK - Scotland', NULL, NULL),
('XB', 'UK - Northern Ireland', NULL, NULL),
('YE', 'Yemen', NULL, NULL),
('ZA', 'South Africa', NULL, NULL),
('ZM', 'Zambia', NULL, NULL),
('ZW', 'Zimbabwe', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `creditorwatch_api_data`
--

CREATE TABLE `creditorwatch_api_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `report_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `abn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_status_effective_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `record_last_updated` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subclass` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asic_locality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_credit_score` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `history_credit_score` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Junier Analyst', NULL, NULL),
(2, 'Senier Analyst', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Agriculture, Forestry and Fishing', NULL, NULL),
(2, 'Mining', NULL, NULL),
(3, 'Manufacturing', NULL, NULL),
(4, 'Electricity, Gas, Water and Waste Services', NULL, NULL),
(5, 'Construction', NULL, NULL),
(6, 'Wholesale Trade', NULL, NULL),
(7, 'Retail Trade', NULL, NULL),
(8, 'Accommodation and Food Services', NULL, NULL),
(9, 'Transport, Postal and Warehousing', NULL, NULL),
(10, 'Information Media and Telecommunications', NULL, NULL),
(11, 'Financial and Insurance Services', NULL, NULL),
(12, 'Rental, Hiring and Real Estate Services', NULL, NULL),
(13, 'Professional, Scientific and Technical Services', NULL, NULL),
(14, 'Administrative and Support Services', NULL, NULL),
(15, 'Public Administration and Safety', NULL, NULL),
(16, 'Education and Training', NULL, NULL),
(17, 'Health Care and Social Assistance', NULL, NULL),
(18, 'Arts and Recreation Services', NULL, NULL),
(19, 'Other Services', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `entity_type`
--

CREATE TABLE `entity_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `entity_type`
--

INSERT INTO `entity_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Aggregated', NULL, NULL),
(2, 'Association', NULL, NULL),
(3, 'Bank', NULL, NULL),
(4, 'Financial Institution', NULL, NULL),
(5, 'Government', NULL, NULL),
(6, 'Partnership-Individual', NULL, NULL),
(7, 'Partnership-Corporation', NULL, NULL),
(8, 'Private Company', NULL, NULL),
(9, 'Public Company-Listed', NULL, NULL),
(10, 'Public Company-Unlisted', NULL, NULL),
(11, 'Sole Trader', NULL, NULL),
(12, 'Discretionary Trust', NULL, NULL),
(13, 'Unit Trust', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `key_ratio_calculation`
--

CREATE TABLE `key_ratio_calculation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `report_details_id` int(11) NOT NULL,
  `gross_profit_margin` decimal(10,4) DEFAULT NULL,
  `ebitda` decimal(10,4) DEFAULT NULL,
  `normalised_ebitda` decimal(10,4) DEFAULT NULL,
  `ebit` decimal(10,4) DEFAULT NULL,
  `net_profit_margin` decimal(10,4) DEFAULT NULL,
  `profitability` decimal(10,4) DEFAULT NULL,
  `return_on_investment` decimal(10,4) DEFAULT NULL,
  `return_on_assets` decimal(10,4) DEFAULT NULL,
  `return_on_equity` decimal(10,4) DEFAULT NULL,
  `working_capital` decimal(10,4) DEFAULT NULL,
  `working_capital_to_sales` decimal(10,4) DEFAULT NULL,
  `cash_flow_coverage` decimal(10,4) DEFAULT NULL,
  `cash_ratio` decimal(10,4) DEFAULT NULL,
  `current_ratio` decimal(10,4) DEFAULT NULL,
  `quick_ratio` decimal(10,4) DEFAULT NULL,
  `capital_adequacy` decimal(10,4) DEFAULT NULL,
  `net_tangible_worth` decimal(10,4) DEFAULT NULL,
  `net_asset_backing` decimal(10,4) DEFAULT NULL,
  `gearing` decimal(10,4) DEFAULT NULL,
  `debt_to_equity` decimal(10,4) DEFAULT NULL,
  `interest_coverage` decimal(10,4) DEFAULT NULL,
  `repayment_capability` decimal(10,4) DEFAULT NULL,
  `financial_leverage` decimal(10,4) DEFAULT NULL,
  `short_ratio` decimal(10,4) DEFAULT NULL,
  `operating_leverage` decimal(10,4) DEFAULT NULL,
  `creditor_exposure` decimal(10,4) DEFAULT NULL,
  `creditor_days` decimal(10,4) DEFAULT NULL,
  `inventory_days` decimal(10,4) DEFAULT NULL,
  `debtor_days` decimal(10,4) DEFAULT NULL,
  `cash_conversion_cycle` decimal(10,4) DEFAULT NULL,
  `sales_annualised` decimal(10,4) DEFAULT NULL,
  `activity` decimal(10,4) DEFAULT NULL,
  `sales_growth` decimal(10,4) DEFAULT NULL,
  `related_party_loans_receivable` decimal(10,4) DEFAULT NULL,
  `related_party_loans_payable` decimal(10,4) DEFAULT NULL,
  `related_party_loans_dependency` decimal(10,4) DEFAULT NULL,
  `quick_asset_composition` decimal(10,4) DEFAULT NULL,
  `current_asset_composition` decimal(10,4) DEFAULT NULL,
  `current_liability_composition` decimal(10,4) DEFAULT NULL,
  `zscore_risk_measure` decimal(10,4) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `key_ratio_calculation`
--

INSERT INTO `key_ratio_calculation` (`id`, `report_details_id`, `gross_profit_margin`, `ebitda`, `normalised_ebitda`, `ebit`, `net_profit_margin`, `profitability`, `return_on_investment`, `return_on_assets`, `return_on_equity`, `working_capital`, `working_capital_to_sales`, `cash_flow_coverage`, `cash_ratio`, `current_ratio`, `quick_ratio`, `capital_adequacy`, `net_tangible_worth`, `net_asset_backing`, `gearing`, `debt_to_equity`, `interest_coverage`, `repayment_capability`, `financial_leverage`, `short_ratio`, `operating_leverage`, `creditor_exposure`, `creditor_days`, `inventory_days`, `debtor_days`, `cash_conversion_cycle`, `sales_annualised`, `activity`, `sales_growth`, `related_party_loans_receivable`, `related_party_loans_payable`, `related_party_loans_dependency`, `quick_asset_composition`, `current_asset_composition`, `current_liability_composition`, `zscore_risk_measure`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(53, 56, '91.6667', '58280.0000', '5.0000', '100400.0000', '66.8889', '383.0904', '52.4781', '292.7114', '141.7801', '-12000.0000', '-6.6667', '7.7253', '0.0644', '0.4850', '0.4163', '-0.0200', '4400.0000', '2.4444', '81.3411', '0.2346', '5.0200', '431.5412', '0.3844', '88.3929', NULL, '4.3732', '36.5000', '38.9333', '0.4056', '2.8389', '180000.0000', '5.2478', NULL, '23.3236', '70.6093', '-164.1667', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-21 02:51:00', '2020-02-21 02:51:00'),
(54, 57, '98.8235', '118408.0000', '65406.0000', '122618.0000', '75.1871', '627.9278', '10.3093', '632.0515', '432.7267', '-32200.0000', '-18.9412', '5.1282', '0.0513', '0.1744', '0.1667', '-0.2382', '-38000.0000', '-22.3529', '269.5876', '1.3433', '23.5804', '244.3939', '0.3403', '79.4045', NULL, '10.3093', '365.0000', '54.7500', '1.0735', '-309.1765', '170000.0000', '8.7629', '-5.5556', '12.8866', '61.1855', '-99.3789', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-21 02:51:00', '2020-02-21 02:51:00'),
(55, 58, '80.0000', '87800.0000', '81800.0000', '122000.0000', '83.3333', '492.0635', '11.9048', '484.1270', '297.6744', '-13800.0000', '-9.2000', '10.3448', '0.1034', '0.5241', '0.4552', '-0.1320', '-12800.0000', '-8.5333', '142.8571', '0.6512', '40.6667', '347.2222', '0.3189', '82.1429', NULL, '11.9048', '36.5000', '24.3333', '0.4867', '-11.6800', '150000.0000', '5.9524', '-11.7647', '27.7778', '63.8889', '-166.6667', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-21 02:51:00', '2020-02-21 02:51:00'),
(59, 62, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-25 05:27:44', '2020-02-25 05:27:44'),
(64, 70, '91.6667', '58280.0000', '5.0000', '100400.0000', '66.8889', '383.0904', '52.4781', '292.7114', '141.7801', '-12000.0000', '-6.6667', '7.7253', '0.0644', '0.4850', '0.4163', '-0.0200', '4400.0000', '2.4444', '81.3411', '0.2346', '5.0200', '431.5412', '0.3844', '88.3929', NULL, '4.3732', '36.5000', '38.9333', '0.4056', '2.8389', '180000.0000', '5.2478', NULL, '23.3236', '70.6093', '-164.1667', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-25 08:27:08', '2020-02-25 08:27:08'),
(65, 71, '98.8235', '118408.0000', '65406.0000', '122618.0000', '75.1871', '627.9278', '10.3093', '632.0515', '432.7267', '-32200.0000', '-18.9412', '5.1282', '0.0513', '0.1744', '0.1667', '-0.2382', '-38000.0000', '-22.3529', '269.5876', '1.3433', '23.5804', '244.3939', '0.3403', '79.4045', NULL, '10.3093', '365.0000', '54.7500', '1.0735', '-309.1765', '170000.0000', '8.7629', '-5.5556', '12.8866', '61.1855', '-99.3789', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-25 08:27:08', '2020-02-25 08:27:08'),
(66, 72, '80.0000', '87800.0000', '81800.0000', '122000.0000', '83.3333', '492.0635', '11.9048', '484.1270', '297.6744', '-13800.0000', '-9.2000', '10.3448', '0.1034', '0.5241', '0.4552', '-0.1320', '-12800.0000', '-8.5333', '142.8571', '0.6512', '40.6667', '347.2222', '0.3189', '82.1429', NULL, '11.9048', '36.5000', '24.3333', '0.4867', '-11.6800', '150000.0000', '5.9524', '-11.7647', '27.7778', '63.8889', '-166.6667', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-25 08:27:08', '2020-02-25 08:27:08'),
(67, 73, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-25 08:27:54', '2020-02-25 08:27:54'),
(68, 74, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-25 08:38:29', '2020-02-25 08:38:29'),
(71, 77, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-25 08:42:37', '2020-02-25 08:42:37'),
(73, 79, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-25 08:44:16', '2020-02-25 08:44:16'),
(74, 80, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-25 08:44:25', '2020-02-25 08:44:25'),
(75, 81, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-25 14:23:40', '2020-02-25 14:23:40'),
(76, 82, '91.6667', '58280.0000', '5.0000', '100400.0000', '66.8889', '383.0904', '52.4781', '292.7114', '141.7801', '-12000.0000', '-6.6667', '7.7253', '0.0644', '0.4850', '0.4163', '-0.0200', '4400.0000', '2.4444', '81.3411', '0.2346', '5.0200', '431.5412', '0.3844', '88.3929', NULL, '4.3732', '36.5000', '38.9333', '0.4056', '2.8389', '180000.0000', '5.2478', NULL, '23.3236', '70.6093', '-164.1667', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-26 16:10:54', '2020-02-26 16:10:54'),
(77, 83, '98.8235', '118408.0000', '65406.0000', '122618.0000', '75.1871', '627.9278', '10.3093', '632.0515', '432.7267', '-32200.0000', '-18.9412', '5.1282', '0.0513', '0.1744', '0.1667', '-0.2382', '-38000.0000', '-22.3529', '269.5876', '1.3433', '23.5804', '244.3939', '0.3403', '79.4045', NULL, '10.3093', '365.0000', '54.7500', '1.0735', '-309.1765', '170000.0000', '8.7629', '-5.5556', '12.8866', '61.1855', '-99.3789', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-26 16:10:54', '2020-02-26 16:10:54'),
(78, 84, '80.0000', '87800.0000', '81800.0000', '122000.0000', '83.3333', '492.0635', '11.9048', '484.1270', '297.6744', '-13800.0000', '-9.2000', '10.3448', '0.1034', '0.5241', '0.4552', '-0.1320', '-12800.0000', '-8.5333', '142.8571', '0.6512', '40.6667', '347.2222', '0.3189', '82.1429', NULL, '11.9048', '36.5000', '24.3333', '0.4867', '-11.6800', '150000.0000', '5.9524', '-11.7647', '27.7778', '63.8889', '-166.6667', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-26 16:10:54', '2020-02-26 16:10:54'),
(79, 85, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', NULL, NULL, NULL, '0.0000', 1, 1, '2020-02-26 16:11:27', '2020-02-26 16:11:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_22_044219_create_user_roles_table', 1),
(5, '2020_01_22_044243_create_company_info_table', 1),
(6, '2020_01_22_044530_create_designations_table', 1),
(7, '2020_01_22_044556_create_confidentiality_table', 1),
(8, '2020_01_22_044611_create_entity_type_table', 1),
(9, '2020_01_22_044653_create_divisions_table', 1),
(10, '2020_01_22_044712_create_sub_divisions_table', 1),
(11, '2020_01_22_044746_create_company_class_table', 1),
(12, '2020_01_22_044810_create_company_group_table', 1),
(13, '2020_01_22_044828_create_application_settings_table', 1),
(14, '2020_01_22_044847_create_reports_table', 1),
(15, '2020_01_22_044906_create_reports_details_table', 1),
(16, '2020_01_22_044927_create_api_custom_profile_table', 1),
(17, '2020_01_22_044952_create_api_credit_score_history_table', 1),
(18, '2020_01_22_082911_create_report_executive_summery_table', 1),
(19, '2020_02_02_161356_create_country_table', 1),
(20, '2020_02_05_084935_add_coloumn_as_nullable_for_company_info_table', 1),
(21, '2020_02_07_043756_add_nullable_for_user_table_coloumn', 2),
(22, '2020_02_07_045440_change_user_role_value_in_user_roles_table', 3),
(27, '2020_02_07_075413_change_email_coloumn_unique_property_in_users_table', 4),
(28, '2020_02_08_070420_remove_required_coloumn_from_company_info_table', 5),
(29, '2020_02_18_173329_create_key_ratio_calculation_table', 6),
(30, '2020_02_19_050647_add_new_coloumn_to_reports_table', 7),
(32, '2020_02_19_051434_change_coloumn_as_nullable_to_reports_details_table', 8),
(34, '2020_02_19_093042_change_coloumn_attrib_to_reports_table', 9),
(37, '2020_02_19_094438_change_coloumn_as_nullable_to_key_ratio_calculation_table', 10),
(38, '2020_02_19_164555_create_report_status_table', 11),
(39, '2020_02_19_164935_add_data_to_report_status_table', 12),
(40, '2020_02_22_183059_create_creditorwatch_api_data_table', 13),
(41, '2020_02_23_022308_remove_coloumn_api_credit_score_history_to_table', 14),
(43, '2020_02_24_122053_add_a_coloumn_to_reports_table', 15),
(44, '2020_02_25_093306_remove_coloumn_from_report_executive_summery_table', 16),
(45, '2020_02_25_121752_rename_coloumn_in_reports_table', 17),
(48, '2020_02_26_125017_create_system_route_table', 18),
(49, '2020_02_26_125104_create_user_routes_table', 18),
(50, '2020_02_26_125139_add_data_to_system_route_table', 18),
(52, '2020_02_26_125156_add_data_to_user_routes_table', 19);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `report_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `report_type` int(11) NOT NULL,
  `report_status` int(11) NOT NULL DEFAULT 0,
  `approved_rejected_date` datetime DEFAULT NULL,
  `clone_report_id` int(11) NOT NULL DEFAULT 0,
  `approved_by` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `generated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `chart_count` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `report_token`, `abn`, `acn`, `company_id`, `report_type`, `report_status`, `approved_rejected_date`, `clone_report_id`, `approved_by`, `created_by`, `updated_by`, `generated_at`, `created_at`, `updated_at`, `chart_count`) VALUES
(23, 'e801a41d865b3944deadba161aaf2cce', '3asas43434', NULL, 2, 2, 4, NULL, 0, NULL, 1, 1, '2020-02-23 09:55:52', '2020-02-19 07:45:38', '2020-02-26 16:09:14', 285),
(26, '401c20a26a22e08ca865c43c498be2f4', '11004251861', NULL, 2, 1, 2, NULL, 0, NULL, 1, 1, NULL, '2020-02-25 04:10:03', '2020-02-25 05:28:04', 0),
(35, '64ecdb3b343e57b495e492ad0b39e5e0', '11004251861', NULL, 2, 1, 1, NULL, 26, NULL, 1, 1, NULL, '2020-02-25 08:24:40', '2020-02-26 09:04:01', 0),
(36, 'ac4ddbafc3eaaa7cbdb3c91cebda59b5', '11004251861', NULL, 2, 2, 4, NULL, 23, NULL, 1, 1, '2020-02-25 14:17:03', '2020-02-25 08:26:11', '2020-02-26 09:15:17', 20),
(37, 'b92ab62641b0729015c6c048d3120d14', '11004251861', NULL, 2, 2, 3, NULL, 35, NULL, 1, 1, NULL, '2020-02-25 08:27:54', '2020-02-25 08:27:54', 0),
(38, 'f8e76b9cfc07e0bd7d87db116d2d224c', '11004251861', NULL, 2, 2, 3, NULL, 26, NULL, 1, 1, NULL, '2020-02-25 08:38:29', '2020-02-25 08:38:29', 0),
(39, '998807f729cf0383a03b5a3d1b878c02', '11004251861', NULL, 2, 2, 0, NULL, 37, NULL, 1, 1, NULL, '2020-02-25 08:40:40', '2020-02-25 08:44:25', 0),
(40, '3b6c97e8172fc34307ef1df28017ffa0', '11004251861', NULL, 2, 2, 0, NULL, 39, NULL, 1, 1, NULL, '2020-02-25 08:42:25', '2020-02-25 08:42:37', 0),
(41, 'c3688a4106231d5597d74dc29da8538c', '11004251861', NULL, 2, 2, 3, NULL, 37, NULL, 1, 1, NULL, '2020-02-25 14:23:40', '2020-02-25 14:23:40', 0),
(42, '913fc67d355cf0a5e0254a18e842b4fb', '11004251861', NULL, 2, 1, 3, NULL, 23, NULL, 1, 1, NULL, '2020-02-26 16:10:54', '2020-02-26 16:10:54', 0),
(43, '08e7b5770ed82b441310cd150c672ace', '11004251861', NULL, 2, 1, 3, NULL, 26, NULL, 1, 1, NULL, '2020-02-26 16:11:27', '2020-02-26 16:11:27', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reports_details`
--

CREATE TABLE `reports_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `report_id` int(11) NOT NULL,
  `rounding` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report_period_months` int(11) DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confidentiality_id` int(11) DEFAULT NULL,
  `financial_year` int(11) DEFAULT NULL,
  `financial_month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales` decimal(10,2) DEFAULT NULL,
  `cost_of_sales` decimal(10,4) DEFAULT NULL,
  `gross_profit` decimal(10,4) DEFAULT NULL,
  `other_income` decimal(10,4) DEFAULT NULL,
  `depreciation` decimal(10,4) DEFAULT NULL,
  `amortisation` decimal(10,4) DEFAULT NULL,
  `impairment` decimal(10,4) DEFAULT NULL,
  `interest_expenses_gross` decimal(10,4) DEFAULT NULL,
  `operating_lease_expenses` decimal(10,4) DEFAULT NULL,
  `finance_lease_hire_expenses_charges` decimal(10,4) DEFAULT NULL,
  `non_recurring_gain_loses` decimal(10,4) DEFAULT NULL,
  `other_gain_loses` decimal(10,4) DEFAULT NULL,
  `other_expenses` decimal(10,4) DEFAULT NULL,
  `ebit` decimal(10,4) DEFAULT NULL,
  `ebitda` decimal(10,4) DEFAULT NULL,
  `normalized_ebitda` decimal(10,4) DEFAULT NULL,
  `profit_before_tax` decimal(10,4) DEFAULT NULL,
  `profit_before_tax_after_abnormals` decimal(10,4) DEFAULT NULL,
  `tax_benefit_expenses` decimal(10,4) DEFAULT NULL,
  `profit_after_tax` decimal(10,4) DEFAULT NULL,
  `distribution_ordividends` decimal(10,4) DEFAULT NULL,
  `other_post_tax_items_gains_losses` decimal(10,4) DEFAULT NULL,
  `profit_after_tax_distribution` decimal(10,4) DEFAULT NULL,
  `cash` decimal(10,4) DEFAULT NULL,
  `trade_debtors` decimal(10,4) DEFAULT NULL,
  `total_inventories` decimal(10,4) DEFAULT NULL,
  `loan_to_related_parties_1` decimal(10,4) DEFAULT NULL,
  `other_current_assets` decimal(10,4) DEFAULT NULL,
  `total_current_assets` decimal(10,4) DEFAULT NULL,
  `fixed_assets` decimal(10,4) DEFAULT NULL,
  `net_intangibles` decimal(10,4) DEFAULT NULL,
  `loan_to_related_parties_2` decimal(10,4) DEFAULT NULL,
  `other_non_current_assets` decimal(10,4) DEFAULT NULL,
  `total_non_current_assets` decimal(10,4) DEFAULT NULL,
  `total_assets` decimal(10,4) DEFAULT NULL,
  `trade_creditors` decimal(10,4) DEFAULT NULL,
  `interest_bearing_debt_1` decimal(10,4) DEFAULT NULL,
  `loan_from_related_parties_1` decimal(10,4) DEFAULT NULL,
  `other_current_liabilities` decimal(10,4) DEFAULT NULL,
  `total_current_liabilities` decimal(10,4) DEFAULT NULL,
  `interest_bearing_debt_2` decimal(10,4) DEFAULT NULL,
  `loan_from_related_parties_2` decimal(10,4) DEFAULT NULL,
  `other_non_current_liabilities` decimal(10,4) DEFAULT NULL,
  `total_non_current_liabilities` decimal(10,4) DEFAULT NULL,
  `total_liabilities` decimal(10,4) DEFAULT NULL,
  `share_capital` decimal(10,4) DEFAULT NULL,
  `prefence_shares` decimal(10,4) DEFAULT NULL,
  `threasury_shares` decimal(10,4) DEFAULT NULL,
  `equity_ownerships` decimal(10,4) DEFAULT NULL,
  `total_reserves` decimal(10,4) DEFAULT NULL,
  `retained_earning` decimal(10,4) DEFAULT NULL,
  `minorty_interest` decimal(10,4) DEFAULT NULL,
  `total_equity` decimal(10,4) DEFAULT NULL,
  `balance` decimal(10,4) DEFAULT NULL,
  `operating_cash_flow` decimal(10,4) DEFAULT NULL,
  `contingent_liabilities` decimal(10,4) DEFAULT NULL,
  `other_commitmentes` decimal(10,4) DEFAULT NULL,
  `operating_lease_outstanding` decimal(10,4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reports_details`
--

INSERT INTO `reports_details` (`id`, `report_id`, `rounding`, `base_currency`, `quality`, `report_period_months`, `scope`, `confidentiality_id`, `financial_year`, `financial_month`, `sales`, `cost_of_sales`, `gross_profit`, `other_income`, `depreciation`, `amortisation`, `impairment`, `interest_expenses_gross`, `operating_lease_expenses`, `finance_lease_hire_expenses_charges`, `non_recurring_gain_loses`, `other_gain_loses`, `other_expenses`, `ebit`, `ebitda`, `normalized_ebitda`, `profit_before_tax`, `profit_before_tax_after_abnormals`, `tax_benefit_expenses`, `profit_after_tax`, `distribution_ordividends`, `other_post_tax_items_gains_losses`, `profit_after_tax_distribution`, `cash`, `trade_debtors`, `total_inventories`, `loan_to_related_parties_1`, `other_current_assets`, `total_current_assets`, `fixed_assets`, `net_intangibles`, `loan_to_related_parties_2`, `other_non_current_assets`, `total_non_current_assets`, `total_assets`, `trade_creditors`, `interest_bearing_debt_1`, `loan_from_related_parties_1`, `other_current_liabilities`, `total_current_liabilities`, `interest_bearing_debt_2`, `loan_from_related_parties_2`, `other_non_current_liabilities`, `total_non_current_liabilities`, `total_liabilities`, `share_capital`, `prefence_shares`, `threasury_shares`, `equity_ownerships`, `total_reserves`, `retained_earning`, `minorty_interest`, `total_equity`, `balance`, `operating_cash_flow`, `contingent_liabilities`, `other_commitmentes`, `operating_lease_outstanding`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(56, 23, 'TH', 'AUD', 'Management', 12, 'Consolidated', 1, 2017, 'January', '180000.00', '15000.0000', '165000.0000', '1520.0000', '41.0000', '1000.0000', '2000.0000', '20000.0000', '2000.0000', '1500.0000', '2500.0000', '20000.0000', '1000.0000', '100400.0000', '58280.0000', '5.0000', '119900.0000', '120400.0000', '15000.0000', '135400.0000', '2000.0000', '2000.0000', '131400.0000', '1500.0000', '200.0000', '1600.0000', '5000.0000', '3000.0000', '11300.0000', '15000.0000', '2000.0000', '3000.0000', '3000.0000', '23000.0000', '34300.0000', '1500.0000', '1800.0000', '18000.0000', '2000.0000', '23300.0000', '900.0000', '1700.0000', '2000.0000', '4600.0000', '27900.0000', '18000.0000', '25000.0000', '18000.0000', '61000.0000', '1500.0000', '18000.0000', '15000.0000', '95500.0000', '-89100.0000', '1800.0000', '2000.0000', '3000.0000', '15000.0000', 1, 1, '2020-02-21 02:51:00', '2020-02-21 02:51:00'),
(57, 23, 'TH', 'AUD', 'Management', 12, 'Consolidated', 1, 2018, 'January', '170000.00', '2000.0000', '168000.0000', '25230.0000', '2210.0000', '2000.0000', '51002.0000', '5200.0000', '5000.0000', '2000.0000', '2000.0000', '2000.0000', '2000.0000', '122618.0000', '118408.0000', '65406.0000', '176820.0000', '127818.0000', '2000.0000', '129818.0000', '3000.0000', '5000.0000', '121818.0000', '2000.0000', '500.0000', '300.0000', '2000.0000', '2000.0000', '6800.0000', '2000.0000', '5100.0000', '500.0000', '5000.0000', '12600.0000', '19400.0000', '2000.0000', '2000.0000', '30000.0000', '5000.0000', '39000.0000', '6300.0000', '2000.0000', '5000.0000', '13300.0000', '52300.0000', '2000.0000', '2000.0000', '2000.0000', '6000.0000', '2000.0000', '2000.0000', '20000.0000', '30000.0000', '-62900.0000', '2000.0000', '15000.0000', '20000.0000', '2000.0000', 1, 1, '2020-02-21 02:51:00', '2020-02-21 02:51:00'),
(58, 23, 'TH', 'AUD', 'Management', 12, 'Consolidated', 1, 2019, 'January', '150000.00', '30000.0000', '120000.0000', '52200.0000', '2000.0000', '32200.0000', '3000.0000', '3000.0000', '7000.0000', '3000.0000', '3000.0000', '3000.0000', '3000.0000', '122000.0000', '87800.0000', '81800.0000', '125000.0000', '125000.0000', '3000.0000', '128000.0000', '2000.0000', '2000.0000', '124000.0000', '3000.0000', '200.0000', '2000.0000', '5000.0000', '5000.0000', '15200.0000', '3000.0000', '2000.0000', '2000.0000', '3000.0000', '10000.0000', '25200.0000', '3000.0000', '3000.0000', '20000.0000', '3000.0000', '29000.0000', '2000.0000', '3000.0000', '2000.0000', '7000.0000', '36000.0000', '3000.0000', '1000.0000', '3000.0000', '7000.0000', '3000.0000', '3000.0000', '30000.0000', '43000.0000', '-53800.0000', '3000.0000', '2000.0000', '1000.0000', '3000.0000', 1, 1, '2020-02-21 02:51:00', '2020-02-21 02:51:00'),
(62, 26, 'N', 'CNY', 'Autdited', 12, 'ASIC', 1, 2002, 'June', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 1, 1, '2020-02-25 05:27:44', '2020-02-25 05:27:44'),
(70, 36, 'TH', 'AUD', 'Management', 12, 'Consolidated', 1, 2017, 'January', '180000.00', '15000.0000', '165000.0000', '1520.0000', '41.0000', '1000.0000', '2000.0000', '20000.0000', '2000.0000', '1500.0000', '2500.0000', '20000.0000', '1000.0000', '100400.0000', '58280.0000', '5.0000', '119900.0000', '120400.0000', '15000.0000', '135400.0000', '2000.0000', '2000.0000', '131400.0000', '1500.0000', '200.0000', '1600.0000', '5000.0000', '3000.0000', '11300.0000', '15000.0000', '2000.0000', '3000.0000', '3000.0000', '23000.0000', '34300.0000', '1500.0000', '1800.0000', '18000.0000', '2000.0000', '23300.0000', '900.0000', '1700.0000', '2000.0000', '4600.0000', '27900.0000', '18000.0000', '25000.0000', '18000.0000', '61000.0000', '1500.0000', '18000.0000', '15000.0000', '95500.0000', '-89100.0000', '1800.0000', '2000.0000', '3000.0000', '15000.0000', 1, 1, '2020-02-25 08:27:08', '2020-02-25 08:27:08'),
(71, 36, 'TH', 'AUD', 'Management', 12, 'Consolidated', 1, 2018, 'January', '170000.00', '2000.0000', '168000.0000', '25230.0000', '2210.0000', '2000.0000', '51002.0000', '5200.0000', '5000.0000', '2000.0000', '2000.0000', '2000.0000', '2000.0000', '122618.0000', '118408.0000', '65406.0000', '176820.0000', '127818.0000', '2000.0000', '129818.0000', '3000.0000', '5000.0000', '121818.0000', '2000.0000', '500.0000', '300.0000', '2000.0000', '2000.0000', '6800.0000', '2000.0000', '5100.0000', '500.0000', '5000.0000', '12600.0000', '19400.0000', '2000.0000', '2000.0000', '30000.0000', '5000.0000', '39000.0000', '6300.0000', '2000.0000', '5000.0000', '13300.0000', '52300.0000', '2000.0000', '2000.0000', '2000.0000', '6000.0000', '2000.0000', '2000.0000', '20000.0000', '30000.0000', '-62900.0000', '2000.0000', '15000.0000', '20000.0000', '2000.0000', 1, 1, '2020-02-25 08:27:08', '2020-02-25 08:27:08'),
(72, 36, 'TH', 'AUD', 'Management', 12, 'Consolidated', 1, 2019, 'January', '150000.00', '30000.0000', '120000.0000', '52200.0000', '2000.0000', '32200.0000', '3000.0000', '3000.0000', '7000.0000', '3000.0000', '3000.0000', '3000.0000', '3000.0000', '122000.0000', '87800.0000', '81800.0000', '125000.0000', '125000.0000', '3000.0000', '128000.0000', '2000.0000', '2000.0000', '124000.0000', '3000.0000', '200.0000', '2000.0000', '5000.0000', '5000.0000', '15200.0000', '3000.0000', '2000.0000', '2000.0000', '3000.0000', '10000.0000', '25200.0000', '3000.0000', '3000.0000', '20000.0000', '3000.0000', '29000.0000', '2000.0000', '3000.0000', '2000.0000', '7000.0000', '36000.0000', '3000.0000', '1000.0000', '3000.0000', '7000.0000', '3000.0000', '3000.0000', '30000.0000', '43000.0000', '-53800.0000', '3000.0000', '2000.0000', '1000.0000', '3000.0000', 1, 1, '2020-02-25 08:27:08', '2020-02-25 08:27:08'),
(73, 37, 'N', 'CNY', 'Autdited', 12, 'ASIC', 1, 2002, 'June', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, NULL, '2020-02-25 08:27:54', '2020-02-25 08:27:54'),
(74, 38, 'N', 'CNY', 'Autdited', 12, 'ASIC', 1, 2002, 'June', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, NULL, '2020-02-25 08:38:29', '2020-02-25 08:38:29'),
(77, 40, 'N', 'CNY', 'Autdited', 12, 'ASIC', 1, 2002, 'June', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 1, 1, '2020-02-25 08:42:37', '2020-02-25 08:42:37'),
(79, 35, 'N', 'CNY', 'Autdited', 12, 'ASIC', 1, 2002, 'June', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 1, 1, '2020-02-25 08:44:16', '2020-02-25 08:44:16'),
(80, 39, 'N', 'CNY', 'Autdited', 12, 'ASIC', 1, 2002, 'June', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 1, 1, '2020-02-25 08:44:25', '2020-02-25 08:44:25'),
(81, 41, 'N', 'CNY', 'Autdited', 12, 'ASIC', 1, 2002, 'June', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, NULL, '2020-02-25 14:23:40', '2020-02-25 14:23:40'),
(82, 42, 'TH', 'AUD', 'Management', 12, 'Consolidated', 1, 2017, 'January', '180000.00', '15000.0000', '165000.0000', '1520.0000', '41.0000', '1000.0000', '2000.0000', '20000.0000', '2000.0000', '1500.0000', '2500.0000', '20000.0000', '1000.0000', '100400.0000', '58280.0000', '5.0000', '119900.0000', '120400.0000', '15000.0000', '135400.0000', '2000.0000', '2000.0000', '131400.0000', '1500.0000', '200.0000', '1600.0000', '5000.0000', '3000.0000', '11300.0000', '15000.0000', '2000.0000', '3000.0000', '3000.0000', '23000.0000', '34300.0000', '1500.0000', '1800.0000', '18000.0000', '2000.0000', '23300.0000', '900.0000', '1700.0000', '2000.0000', '4600.0000', '27900.0000', '18000.0000', '25000.0000', '18000.0000', '61000.0000', '1500.0000', '18000.0000', '15000.0000', '95500.0000', '-89100.0000', '1800.0000', '2000.0000', '3000.0000', '15000.0000', NULL, NULL, '2020-02-26 16:10:54', '2020-02-26 16:10:54'),
(83, 42, 'TH', 'AUD', 'Management', 12, 'Consolidated', 1, 2018, 'January', '170000.00', '2000.0000', '168000.0000', '25230.0000', '2210.0000', '2000.0000', '51002.0000', '5200.0000', '5000.0000', '2000.0000', '2000.0000', '2000.0000', '2000.0000', '122618.0000', '118408.0000', '65406.0000', '176820.0000', '127818.0000', '2000.0000', '129818.0000', '3000.0000', '5000.0000', '121818.0000', '2000.0000', '500.0000', '300.0000', '2000.0000', '2000.0000', '6800.0000', '2000.0000', '5100.0000', '500.0000', '5000.0000', '12600.0000', '19400.0000', '2000.0000', '2000.0000', '30000.0000', '5000.0000', '39000.0000', '6300.0000', '2000.0000', '5000.0000', '13300.0000', '52300.0000', '2000.0000', '2000.0000', '2000.0000', '6000.0000', '2000.0000', '2000.0000', '20000.0000', '30000.0000', '-62900.0000', '2000.0000', '15000.0000', '20000.0000', '2000.0000', NULL, NULL, '2020-02-26 16:10:54', '2020-02-26 16:10:54'),
(84, 42, 'TH', 'AUD', 'Management', 12, 'Consolidated', 1, 2019, 'January', '150000.00', '30000.0000', '120000.0000', '52200.0000', '2000.0000', '32200.0000', '3000.0000', '3000.0000', '7000.0000', '3000.0000', '3000.0000', '3000.0000', '3000.0000', '122000.0000', '87800.0000', '81800.0000', '125000.0000', '125000.0000', '3000.0000', '128000.0000', '2000.0000', '2000.0000', '124000.0000', '3000.0000', '200.0000', '2000.0000', '5000.0000', '5000.0000', '15200.0000', '3000.0000', '2000.0000', '2000.0000', '3000.0000', '10000.0000', '25200.0000', '3000.0000', '3000.0000', '20000.0000', '3000.0000', '29000.0000', '2000.0000', '3000.0000', '2000.0000', '7000.0000', '36000.0000', '3000.0000', '1000.0000', '3000.0000', '7000.0000', '3000.0000', '3000.0000', '30000.0000', '43000.0000', '-53800.0000', '3000.0000', '2000.0000', '1000.0000', '3000.0000', NULL, NULL, '2020-02-26 16:10:54', '2020-02-26 16:10:54'),
(85, 43, 'N', 'CNY', 'Autdited', 12, 'ASIC', 1, 2002, 'June', '0.00', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, NULL, '2020-02-26 16:11:27', '2020-02-26 16:11:27');

-- --------------------------------------------------------

--
-- Table structure for table `report_executive_summery`
--

CREATE TABLE `report_executive_summery` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `report_id` int(11) NOT NULL,
  `summery` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `report_executive_summery`
--

INSERT INTO `report_executive_summery` (`id`, `report_id`, `summery`, `created_at`, `updated_at`) VALUES
(7, 23, 'dsds asdad ad adsasdad', '2020-02-25 04:45:21', '2020-02-25 04:45:21'),
(8, 23, 'sdsds asdd ad a', '2020-02-25 04:45:21', '2020-02-25 04:45:21'),
(9, 23, 'sddsds asdaaad asd ada ad', '2020-02-25 04:45:21', '2020-02-25 04:45:21'),
(10, 26, 'erw', '2020-02-25 05:27:44', '2020-02-25 05:27:44'),
(12, 35, 'dfdfdfd', '2020-02-25 08:44:15', '2020-02-25 08:44:15');

-- --------------------------------------------------------

--
-- Table structure for table `report_status`
--

CREATE TABLE `report_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `report_status`
--

INSERT INTO `report_status` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'Pending For Approval', 0, NULL, NULL),
(2, 'Approved', 1, NULL, NULL),
(3, 'Rejected', 2, NULL, NULL),
(4, 'Editable', 3, NULL, NULL),
(5, 'Generated', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_divisions`
--

CREATE TABLE `sub_divisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `divisions_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_divisions`
--

INSERT INTO `sub_divisions` (`id`, `divisions_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Agriculture', NULL, NULL),
(2, 1, 'Aquaculture', NULL, NULL),
(3, 1, 'Forestry and Logging', NULL, NULL),
(4, 1, 'Fishing, Hunting and Trapping', NULL, NULL),
(5, 1, 'Agriculture, Forestry and Fishing Support Services', NULL, NULL),
(6, 2, 'Coal Mining', NULL, NULL),
(7, 2, 'Oil and Gas Extraction', NULL, NULL),
(8, 2, 'Metal Ore Mining', NULL, NULL),
(9, 2, 'Non-Metallic Mineral Mining and Quarrying', NULL, NULL),
(10, 2, 'Exploration and Other Mining Support Services', NULL, NULL),
(11, 3, 'Food Product Manufacturing', NULL, NULL),
(12, 3, 'Beverage and Tobacco Product Manufacturing', NULL, NULL),
(13, 3, 'Textile, Leather, Clothing and Footwear Manufacturing', NULL, NULL),
(14, 3, 'Wood Product Manufacturing', NULL, NULL),
(15, 3, 'Pulp, Paper and Converted Paper Product Manufacturing', NULL, NULL),
(16, 3, 'Printing (including the Reproduction of Recorded Media)', NULL, NULL),
(17, 3, 'Petroleum and Coal Product Manufacturing', NULL, NULL),
(18, 3, 'Basic Chemical and Chemical Product Manufacturing', NULL, NULL),
(19, 3, 'Polymer Product and Rubber Product Manufacturing', NULL, NULL),
(20, 3, 'Non-Metallic Mineral Product Manufacturing', NULL, NULL),
(21, 3, 'Primary Metal and Metal Product Manufacturing', NULL, NULL),
(22, 3, 'Fabricated Metal Product Manufacturing', NULL, NULL),
(23, 3, 'Transport Equipment Manufacturing', NULL, NULL),
(24, 3, 'Machinery and Equipment Manufacturing', NULL, NULL),
(25, 3, 'Furniture and Other Manufacturing', NULL, NULL),
(26, 4, 'Electricity Supply', NULL, NULL),
(27, 4, 'Gas Supply', NULL, NULL),
(28, 4, 'Water Supply, Sewerage and Drainage Services', NULL, NULL),
(29, 4, 'Waste Collection, Treatment and Disposal Services', NULL, NULL),
(30, 5, 'Building Construction', NULL, NULL),
(31, 5, 'Heavy and Civil Engineering Construction', NULL, NULL),
(32, 5, 'Construction Services', NULL, NULL),
(33, 6, 'Basic Material Wholesaling', NULL, NULL),
(34, 6, 'Machinery and Equipment Wholesaling', NULL, NULL),
(35, 6, 'Motor Vehicle and Motor Vehicle Parts Wholesaling', NULL, NULL),
(36, 6, 'Grocery, Liquor and Tobacco Product Wholesaling', NULL, NULL),
(37, 6, 'Other Goods Wholesaling', NULL, NULL),
(38, 6, 'Commission-Based Wholesaling', NULL, NULL),
(39, 7, 'Motor Vehicle and Motor Vehicle Parts Retailing', NULL, NULL),
(40, 7, 'Fuel Retailing', NULL, NULL),
(41, 7, 'Food Retailing', NULL, NULL),
(42, 7, 'Other Store-Based Retailing', NULL, NULL),
(43, 7, 'Non-Store Retailing and Retail Commission-Based Buying and/or Selling', NULL, NULL),
(44, 8, 'Accommodation', NULL, NULL),
(45, 8, 'Food and Beverage Services', NULL, NULL),
(46, 9, 'Road Transport', NULL, NULL),
(47, 9, 'Rail Transport', NULL, NULL),
(48, 9, 'Water Transport', NULL, NULL),
(49, 9, 'Air and Space Transport', NULL, NULL),
(50, 9, 'Other Transport', NULL, NULL),
(51, 9, 'Postal and Courier Pick-up and Delivery Services', NULL, NULL),
(52, 9, 'Transport Support Services', NULL, NULL),
(53, 9, 'Warehousing and Storage Services', NULL, NULL),
(54, 10, 'Publishing (except Internet and Music Publishing)', NULL, NULL),
(55, 10, 'Motion Picture and Sound Recording Activities', NULL, NULL),
(56, 10, 'Broadcasting (except Internet)', NULL, NULL),
(57, 10, 'Internet Publishing and Broadcasting', NULL, NULL),
(58, 10, 'Telecommunications Services', NULL, NULL),
(59, 10, 'Internet Service Providers, Web Search Portals and Data Processing Services', NULL, NULL),
(60, 10, 'Library and Other Information Services', NULL, NULL),
(62, 11, 'Finance', NULL, NULL),
(63, 11, 'Insurance and Superannuation Funds', NULL, NULL),
(64, 11, 'Auxiliary Finance and Insurance Services', NULL, NULL),
(66, 12, 'Rental and Hiring Services (except Real Estate)', NULL, NULL),
(67, 12, 'Property Operators and Real Estate Services', NULL, NULL),
(69, 13, 'Professional, Scientific and Technical Services (Except Computer System Design and Related Services)', NULL, NULL),
(70, 13, 'Computer System Design and Related Services', NULL, NULL),
(72, 14, 'Administrative Services', NULL, NULL),
(73, 14, 'Building Cleaning, Pest Control and Other Support Services', NULL, NULL),
(75, 15, 'Public Administration', NULL, NULL),
(76, 15, 'Defence', NULL, NULL),
(77, 15, 'Public Order, Safety and Regulatory Services', NULL, NULL),
(80, 16, 'Preschool and School Education', NULL, NULL),
(81, 16, 'Tertiary Education', NULL, NULL),
(82, 16, 'Adult, Community and Other Education', NULL, NULL),
(84, 17, 'Hospitals', NULL, NULL),
(85, 17, 'Medical and Other Health Care Services', NULL, NULL),
(86, 17, 'Residential Care Services', NULL, NULL),
(87, 17, 'Social Assistance Services', NULL, NULL),
(89, 18, 'Heritage Activities', NULL, NULL),
(90, 18, 'Creative and Performing Arts Activities', NULL, NULL),
(91, 18, 'Sports and Recreation Activities', NULL, NULL),
(92, 18, 'Gambling Activities', NULL, NULL),
(94, 19, 'Repair and Maintenance', NULL, NULL),
(95, 19, 'Personal and Other Services', NULL, NULL),
(96, 19, 'Private Households Employing Staff and Undifferentiated Goods- and Service-Producing Activities of Households for Own Use', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `system_route`
--

CREATE TABLE `system_route` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `route_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_route`
--

INSERT INTO `system_route` (`id`, `route_name`, `created_at`, `updated_at`) VALUES
(1, 'generate-report', NULL, NULL),
(2, 'approve-report', NULL, NULL),
(3, 'users.create', NULL, NULL),
(4, 'users.store', NULL, NULL),
(5, 'users.destroy', NULL, NULL),
(6, 'users.edit', NULL, NULL),
(7, 'users.update', NULL, NULL),
(8, 'company.edit', NULL, NULL),
(9, 'company.update', NULL, NULL),
(10, 'view-report', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_role_id` int(11) NOT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reset_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_reset_password` int(11) NOT NULL DEFAULT 0,
  `backup_password` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_first_login` int(11) NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `user_role_id`, `designation_id`, `address`, `telephone`, `password`, `username`, `reset_token`, `is_reset_password`, `backup_password`, `is_first_login`, `email_verified_at`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'User', 'lkeranga@gmail.com', 1, 1, 'test', '123456789', '$2y$10$bHvJWParTeDb.ThY3/glluz/MWLDK2vSpNohNUuUcLmoGPFE06aIK', 'admin', NULL, 1, '$2y$10$E.hidB5ypYVQJKdWQOJOtO00uLF0Gc7TBc6RdE5NDLkJ220mIatsm', 0, NULL, NULL, NULL, NULL, '2020-02-06 23:42:22'),
(2, 'manager', 'Shaman', 'lkeranga@gmail.com', 2, 2, NULL, '0770155500', '$2y$10$bHvJWParTeDb.ThY3/glluz/MWLDK2vSpNohNUuUcLmoGPFE06aIK', 'manager', NULL, 1, '$2y$10$E.hidB5ypYVQJKdWQOJOtO00uLF0Gc7TBc6RdE5NDLkJ220mIatsm', 0, NULL, NULL, NULL, '2020-02-06 23:06:39', '2020-02-07 02:40:48'),
(3, 'analyst', 'Shaman', 'lkeranga@gmail.com', 3, 1, 'Dehiwala Road', '0770155500', '$2y$10$bHvJWParTeDb.ThY3/glluz/MWLDK2vSpNohNUuUcLmoGPFE06aIK', 'analyst', NULL, 1, '$2y$10$E.hidB5ypYVQJKdWQOJOtO00uLF0Gc7TBc6RdE5NDLkJ220mIatsm', 0, NULL, 'KFkGXsiGcYvguJN4YVuWzTDeonbrKArpBMHB0EAZVLj6ncsBY5HkHAFCvGj0', NULL, '2020-02-07 02:41:34', '2020-02-07 02:41:56');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_code`, `name`, `description`, `template`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 'Administrator', NULL, 'admin.layout.menu.admin-menu', NULL, NULL),
(2, 'MANAGER', 'Manager', NULL, 'admin.layout.menu.manager-menu', NULL, NULL),
(3, 'JUNIOUR_ANALYST', 'Junior Analyst', NULL, 'admin.layout.menu.juniour-analyst-menu', NULL, '2020-02-06 23:27:16'),
(4, 'SENIOR_ANALYST', 'Senior Analyst', NULL, 'admin.layout.menu.senior-analyst-menu', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_routes`
--

CREATE TABLE `user_routes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `route_id` int(11) NOT NULL,
  `user_role_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_routes`
--

INSERT INTO `user_routes` (`id`, `route_id`, `user_role_code`, `created_at`, `updated_at`) VALUES
(1, 3, 'MANAGER', NULL, NULL),
(2, 3, 'ADMIN', NULL, NULL),
(3, 4, 'MANAGER', NULL, NULL),
(4, 4, 'ADMIN', NULL, NULL),
(5, 6, 'MANAGER', NULL, NULL),
(6, 6, 'ADMIN', NULL, NULL),
(7, 7, 'MANAGER', NULL, NULL),
(8, 7, 'ADMIN', NULL, NULL),
(9, 5, 'ADMIN', NULL, NULL),
(10, 8, 'MANAGER', NULL, NULL),
(11, 8, 'ADMIN', NULL, NULL),
(12, 9, 'MANAGER', NULL, NULL),
(13, 9, 'ADMIN', NULL, NULL),
(14, 1, 'MANAGER', NULL, NULL),
(15, 1, 'ADMIN', NULL, NULL),
(16, 10, 'MANAGER', NULL, NULL),
(17, 10, 'ADMIN', NULL, NULL),
(18, 2, 'ADMIN', NULL, NULL),
(19, 2, 'MANAGER', NULL, NULL),
(20, 2, 'SENIOR_ANALYST', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_credit_score_history`
--
ALTER TABLE `api_credit_score_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_custom_profile`
--
ALTER TABLE `api_custom_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `application_settings`
--
ALTER TABLE `application_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_class`
--
ALTER TABLE `company_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_group`
--
ALTER TABLE `company_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `confidentiality`
--
ALTER TABLE `confidentiality`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_code`);

--
-- Indexes for table `creditorwatch_api_data`
--
ALTER TABLE `creditorwatch_api_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entity_type`
--
ALTER TABLE `entity_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `key_ratio_calculation`
--
ALTER TABLE `key_ratio_calculation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports_details`
--
ALTER TABLE `reports_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_executive_summery`
--
ALTER TABLE `report_executive_summery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_status`
--
ALTER TABLE `report_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_divisions`
--
ALTER TABLE `sub_divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_route`
--
ALTER TABLE `system_route`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_routes`
--
ALTER TABLE `user_routes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_credit_score_history`
--
ALTER TABLE `api_credit_score_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `api_custom_profile`
--
ALTER TABLE `api_custom_profile`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `application_settings`
--
ALTER TABLE `application_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company_class`
--
ALTER TABLE `company_class`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9604;

--
-- AUTO_INCREMENT for table `company_group`
--
ALTER TABLE `company_group`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=961;

--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `confidentiality`
--
ALTER TABLE `confidentiality`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `creditorwatch_api_data`
--
ALTER TABLE `creditorwatch_api_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `entity_type`
--
ALTER TABLE `entity_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `key_ratio_calculation`
--
ALTER TABLE `key_ratio_calculation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `reports_details`
--
ALTER TABLE `reports_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `report_executive_summery`
--
ALTER TABLE `report_executive_summery`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `report_status`
--
ALTER TABLE `report_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sub_divisions`
--
ALTER TABLE `sub_divisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `system_route`
--
ALTER TABLE `system_route`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_routes`
--
ALTER TABLE `user_routes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
