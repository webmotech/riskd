$('.create-report').click(function(e){
    e.preventDefault();
    $.removeCookie('cptoken');
    $.ajax({
        method : 'GET',
        url : base_url+'/company/get-all-companies',
        success : function(res){
            var data = res.data;
            var html = '';
            html+='<option value="">Select Company</option>';
            for (var x=0; x<data.length; x++){

                html+='<option value="'+data[x].company_token+'">'+data[x].name+'</option>'

            }
            console.log(html);
            $('#select-company').html(html);

            $('.company-select-modal').modal({backdrop: 'static', keyboard: false});
        }
    })
});

$('#frmselectCompany').submit(function(e){
    e.preventDefault();
    if ($(this).parsley().isValid() ) {
        var company = $('#select-company').val();

        var urlPath = window.location.pathname;

        if(urlPath=="/admin/reports/create"){
            $.confirm({
                title: 'Warning!',
                content: 'IF YOU PROCEED ALL DATA WILL BE LOST',
                buttons: {
                    Confirm: function () {
                        $.cookie('cptoken',company,{expires: 7, path: '/'});
                        $('.company-select-modal').modal('toggle');

                        location.href = base_url+ '/admin/reports/create';
                    },
                    cancel: function () {

                    }

                }
            });
        }else{
            $.cookie('cptoken',company,{expires: 7, path: '/'});
            $('.company-select-modal').modal('toggle');

            location.href = base_url+ '/admin/reports/create';
        }







    }
});
