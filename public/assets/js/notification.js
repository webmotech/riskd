function successNotify(msg,position='right',allowDismiss=true,delay=4000){
    $.notify({
        // options
        message: msg
    },{
        // settings
      type: 'success',
      placement: {
            from: "top",
            align: position
      },
      offset: 10,
      spacing: 10,
      z_index: 20000,
      allow_dismiss:allowDismiss,
      delay :delay,


    });
}

function failedNotify(msg){
    $.notify({
        // options
        message: msg
    },{
        // settings
      type: 'danger',
      placement: {
            from: "top",
            align: "right"
      },
      offset: 20,
        spacing: 10,
      z_index: 20000,

    });
}

function notify(msg,type){
    $.notify({
        // options
        message: msg
    },{
        // settings
      type: type,
      placement: {
            from: "top",
            align: "right"
      },
      offset: 20,
        spacing: 10,
      z_index: 20000,

    });
}
