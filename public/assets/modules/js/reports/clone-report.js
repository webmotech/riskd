$("#pending-report-table").on("click", "a.editor_clone", function (e) {
    e.preventDefault();
    var data = $("#pending-report-table")
        .DataTable()
        .row($(this).parents("tr"))
        .data();
    cloneReport(data);
});

$("#reject-report-table").on("click", "a.editor_clone", function (e) {
    e.preventDefault();
    var data = $("#reject-report-table")
        .DataTable()
        .row($(this).parents("tr"))
        .data();

    cloneReport(data);
});

/**
 * Clone a new Report using existing report
 * @param data
 */
function cloneReport(data) {
    $("#report_token").val(data.report_token);
    $(".clone-modal").modal("toggle");
}

$("#frmclonereport").submit(function (e) {
    e.preventDefault();
    if ($(this).parsley().isValid()) {
        var reportType = $("#select-report-type").val();
        var reportToken = $("#report_token").val();
        $(".clone-modal").modal("toggle");
        $("#process-clone-modal").modal({
            backdrop: "static",
            keyboard: false,
        });

        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            type: "POST",
            url: base_url + "/admin/report/clone-report",
            data: { type: reportType, rptoken: reportToken },
            success: function (res) {
                if (res.success) {
                    // $(".bd-example-modal-lg").modal('toggle')

                    $("#clone-success-msg").html(
                        "Report clone process completed. You will be redirected to a new report shortly."
                    );
                    $("#clone-success-msg").show();
                    $("#loader-img").hide();
                    $(".warning-msg").hide();
                    // $('#clone-viewbtn').html('<a href="'+res.data.report_url+'" class="btn btn-primary">Open New Report</a>');
                    setTimeout(function () {
                        location.href = res.data.report_url;
                    }, 5000);
                } else {
                    $("#error-msg").show();
                    $("#error-msg").html(res.msg);
                    setTimeout(function () {
                        $("#process-clone-modal").modal("toggle");
                    }, 2000);
                }
            },
        });
    }
});
