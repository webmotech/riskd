$(document).ready(function () {
    addSummery();

    if (typeof $.cookie("cptoken") == "undefined") {
        $(".create-report").click();
    }

    $("#hdncompanyToken").val($.cookie("cptoken"));
    //Load single data set when ready the page
    //     $(window).keydown(function(event){
    //         if(event.keyCode == 13) {
    //             event.preventDefault();
    //
    //             return false;
    //         }
    //     });

    createYear();

    $(".form-control").keyup(function () {
        var id = $(this).attr("id");
        var val = parseInt($("#" + id).val());
        if (val < 0) {
            $("#" + id).attr("style", "color:#ff0000");
        } else if (val > 0) {
            $("#" + id).attr("style", "color:#000000");
        }
    });

    $(document).on("keydown", ".form-control", function (event) {
        var keycode = event.keyCode ? event.keyCode : event.which;
        if (keycode == "13") {
            var currentTabIndex = parseFloat($(this).attr("tabindex"));
            var nextTabIndex = currentTabIndex + 1;
            $("input[tabindex='" + nextTabIndex + "']").focus();
            $("input[tabindex='" + nextTabIndex + "']").select();
        }
    });

    $(".form-control").change(function () {
        var id = $(this).attr("id");
        var val = parseInt($("#" + id).val());
        if (val < 0) {
            $("#" + id).attr("style", "color:#ff0000");
        } else if (val > 0) {
            $("#" + id).attr("style", "color:#000000");
        }
    });
});

$(document).on("blur", ".form-control", function (event) {
    $('[class^="form-control"]').each(function () {
        var id = this.id;
        var val = parseInt($("#" + id).val());
        if (val < 0) {
            $("#" + id).attr("style", "color:#ff0000");
        } else if (val > 0) {
            $("#" + id).attr("style", "color:#000000");
        }
    });
});



// Auto Calculation
$(document).on("keyup", ".form-control", function (event) {
    $('[class^="form-control"]').each(function () {
        var id = this.id;
        var val = parseInt($("#" + id).val());
        var index = id.substr(id.length - 1);

        if (val < 0) {

            $("#" + id).attr("style", "color:#ff0000");
        } else if (val >= 0) {
            $("#" + id).attr("style", "color:#000000");
        }

        calculateGrossProfit(index);
        changeInterestExpense(index);
        // changeTaxBenefitExpense(index);
        // changeProfitAfterTax(index);
        changeDepreciation(index);
        // changeAmortisation(index);
        // changeImpairment(index);
        // changeNonRecurrentGainLosses(index);
        // blurGrossProfit(index);
        // changeOtherIncome(index);
        // changeOperatingLeaseExpense(index);
        // changeFinanceLeaseHirePurchaseCharges(index);
        // changeOtherGainLosses(index);
        // changeOtherExpense(index);
        // blurProfitBeforeTaxAfterAbnormal(index);
        // changeDistributionOrDividends(index);
        // changeOtherPostTaxItemsGainsLosses(index);
        changeCash(index);
        // changeTradeDebtors(index);
        // changetotalInventories(index);
        // changeLoanToRelatedParties1(index);
        // changeOtherCurrentAssets(index);
        // changeFixedAssets(index);
        // changeNetIntangibles(index);
        // changeLoanToRelatedParties2(index);
        // changeOtherNonCurrentAssets(index);
        // blurTotalCurrentAssets(index);
        // blurTotalNonCurrentAssets(index);
        // changeTradeCreditors(index);
        // changeInterestBearingDebt1(index);
        // changeLoanFromRelatedParties1(index);
        // changeOtherCurrentLiabilities(index);
        // changeInterestBearingDebt2(index);
        // changeLoanFromRelatedParties2(index);
        // changeOtherNonCurrentLiabilities(index);
        // blurTotalCurrentLiabilities(index);
        // blurTotalNonCurrentLiabilities(index);
        // changeShareCapital(index);
        // changePrefenceShares(index);
        // changeTreasuryShares(index);
        // blurEquityOwnerships(index);
        // changeTotalReserves(index);
        // changeRetainedEarning(index);
        // changeMinortyInterest(index);
        // blurTotalAssets(index);
        // blurTotalLiabilities(index);
        // blurTotalEquity(index);
        calculateEbit(index);
        calculateEbitda(index);
        // calculateNormalizedEbitda(index);
        // calculateProfitBeforeTax(index);
        // profitBeforeTaxAfterAbnormal(index);
        calculateProfitAfterTax(index);
        calculateProfitAfterTaxDestribution(index);
        calculateTotalCurrentAssets(index);
        calculateTotalNonCurrentAssets(index);
        calculateTotalAssets(index);
        calculateTotalCurrentLiabilities(index);
        calculateTotalNonCurrentLiabilities(index);
        calculateTotalLiabilities(index);
        calculateEquityOwnerships(index);
        calculateTotalEquity(index);
        calculateBalance(index);
    });
});

$(".form-control").keydown(function (event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        alert("form-control");
        return false;
    }
});

$("#report_type").change(function () {
    var reportType = $("#report_type").val();
    if (reportType == 1) {
        $("#summery-area").show();
        $(".executive-text").attr("required", true);
    } else if (reportType == 2) {
        $("#summery-area").hide();
        $(".executive-text").attr("required", false);
    }
});
var summeryIndex = 0;
function addSummery() {
    var colCount = 0;
    $('[id^="executive_summery"]').each(function () {
        colCount++;
    });
    if (colCount > 2) {
        $("#validate-summery").fadeIn("fast");

        setTimeout(function () {
            $("#validate-summery").fadeOut("slow");
        }, 2000);
        return;
    }
    var html = "";
    html += '<div class="form-group">';
    html += '<label style="font-size: 12px;">Financial Analysis</label>';
    html +=
        '<textarea name="exective_summery[' +
        summeryIndex +
        '][summery]" required id="executive_summery' +
        summeryIndex +
        '" class="form-control executive-text"></textarea>';
    html += "</div>";
    $("#summery-area").append(html);
    $("#executive_summery" + summeryIndex).summernote({
        fontNames: ["Arial"],
        fontSize: "12px",
        tabsize: 1,
        toolbar: [
            ["style", ["style"]],
            ["font", ["bold", "underline", "clear"]],
            ["color", ["color"]],
            ["para", ["ul", "ol", "paragraph"]],
        ],
    });

    summeryIndex++;
}

function changeCompany() {
    $(".create-report").click();
}

/**
 * Get Company List for Modal and Load Modal
 */
// function getCompanyData(){
//
//     $.ajax({
//         method : 'GET',
//         url : base_url+'/company/get-all-companies',
//         success : function(res){
//             var data = res.data;
//             var html = '';
//             html+='<option value="">Select Company</option>';
//             for (var x=0; x<data.length; x++){
//
//                 html+='<option value="'+data[x].company_token+'">'+data[x].name+'</option>'
//
//             }
//             console.log(html);
//             $('#select-company').html(html);
//
//             $('.company-select-modal').modal({backdrop: 'static', keyboard: false});
//         }
//     })
// }

/**
 * Select a Company to Create A Report and Get data for Selected Company
 */
// $('#frmselectCompany').submit(function(e){
//     e.preventDefault();
//     if ($(this).parsley().isValid() ) {
//         var company = $('#select-company').val();
//         $('#hdncompanyToken').val(company);
//         $('.company-select-modal').modal('toggle');
//         getSelectedCompanyData();
//     }
// });

function getSelectedCompanyData(col = 0) {
    var companyToken = $("#hdncompanyToken").val();
    $.ajax({
        method: "GET",
        url: base_url + "/admin/company/" + companyToken,
        success: function (res) {
            $("#abn_" + col).val(res.abn);
            $("#acn_" + col).val(res.acn);
            var companyHtml = "";
            companyHtml +=
                '<option value="' + res.id + '">' + res.name + "</option>";
            $("#company_" + col).html(companyHtml);

            // Load Base currency
            getBaseCurrency(col);

            //Load Rounding
            getRounding(col);

            // Load Quality
            getQuality(col);

            // Load Scope
            getScope(col);

            // Load Months
            getMonths(col);

            // Load Confidentiality
            getConfidentiality(col);

            //Load Reporting Period
            getReportingPeriod(col);

            // Load Financial Years
            getFinancialYears(col);
        },
    });
}

$("#savebtn").click(function () {
    $("#frmreportcreate").submit();
});

//Submit Report
$("#frmreportcreate").submit(function (e) {
    e.preventDefault();

    if ($(this).parsley().isValid()) {
        $.ajax({
            method: "POST",
            url: base_url + "/admin/reports",
            data: $("#frmreportcreate").serialize(),
            success: function (res) {
                if (res.success) {
                    // $(".bd-example-modal-lg").modal('toggle')
                    successNotify(res.msg);
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else {
                    failedNotify(res.msg);
                }
            },
        });
    }
    setTimeout(function () {
        // $('.parsley-required').fadeOut('slow');
    }, 2000);
});

/**
 * Create New data set for an additional year
 */
var index = 0;

var subIndex = 0.1;
function createYear() {
    var colCount = 0;
    $('[id^="coloumn_"]').each(function () {
        colCount++;
    });
    if (colCount > 2) {
        $("#validate-year").fadeIn("fast");

        setTimeout(function () {
            $("#validate-year").fadeOut("slow");
        }, 2000);
        return;
    }
    var tabIndex = 1;
    //Declare variable
    var dataset = "";

    dataset += '<div class="col p-0" id="coloumn_' + index + '">';
    dataset += '<table class="table table-bordered">';
    dataset += "<tr>";
    dataset +=
        '<td style="padding: 0.2em !important;"><button onclick="removeMe(' +
        index +
        ')" type="button" class="btn btn-danger btn-block"><i class="fa fa-trash"></i></i>Remove</button> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" readonly class="form-control" name="data[' +
        index +
        '][abn]" id="abn_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0"  tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" readonly class="form-control" name="data[' +
        index +
        '][acn]" id="acn_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><select required="" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" readonly class="form-control" name="data[' +
        index +
        '][company]" id="company_' +
        index +
        '"></select> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><select required="" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][rounding]" id="rounding_' +
        index +
        '"></select> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><select required="" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][base_currency]" id="base_currency_' +
        index +
        '"></select> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><select required="" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][quality]" id="quality_' +
        index +
        '"></select> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><select required="" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][reporting_period]" id="reporting_period_' +
        index +
        '"></select> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><select required="" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][scope]" id="scope_' +
        index +
        '"></select> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><select required="" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][confidentiality_record]" id="confidentiality_record_' +
        index +
        '"></select> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><select required=""  tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][financial_year]" id="financial_year_' +
        index +
        '"></select> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><select required="" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][month]" id="month_' +
        index +
        '"></select> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset += '<td class="p-2 bg-primary text-light">Income Statement</td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" value="0" onchange="calculateGrossProfit(' +
        index +
        ')" class="form-control" name="data[' +
        index +
        '][sales]" id="sales_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" value="0" onchange="calculateGrossProfit(' +
        index +
        ')" class="form-control" name="data[' +
        index +
        '][cost_of_sales]" id="cost_of_sales_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" readonly class="form-control" name="data[' +
        index +
        '][gross_profit]" onblur="blurGrossProfit(' +
        index +
        ')" id="gross_profit_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][other_income]" onchange="changeOtherIncome(' +
        index +
        ')" id="other_income_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][depreciation]" onchange="changeDepreciation(' +
        index +
        ')" id="depreciation_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][amortisation]" onchange="changeAmortisation(' +
        index +
        ')" id="amortisation_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][impairment]" onchange="changeImpairment(' +
        index +
        ')" id="impairment_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" onchange="changeInterestExpense(' +
        index +
        ')" name="data[' +
        index +
        '][interest_expense_gross]" id="interest_expense_gross_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][operating_lease_expense]" onchange="changeOperatingLeaseExpense(' +
        index +
        ')" id="operating_lease_expense_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][finance_lease_hire_purchase_charges]" onchange="changeFinanceLeaseHirePurchaseCharges(' +
        index +
        ')" id="finance_lease_hire_purchase_charges_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][non_recurring_gains_losses]" onchange="changeNonRecurrentGainLosses(' +
        index +
        ')" id="non_recurring_gains_losses_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][other_gains_losses]" onchange="changeOtherGainLosses(' +
        index +
        ')" id="other_gains_losses_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][other_expenses]" onchange="changeOtherExpense(' +
        index +
        ')" id="other_expenses_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" readonly class="form-control" name="data[' +
        index +
        '][ebit]" id="ebit_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" readonly class="form-control" name="data[' +
        index +
        '][ebitda]" id="ebitda_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" readonly class="form-control" name="data[' +
        index +
        '][normalized_ebitda]" id="normalized_ebitda_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" readonly class="form-control" name="data[' +
        index +
        '][profit_before_tax]" id="profit_before_tax_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" readonly class="form-control" name="data[' +
        index +
        '][profit_before_tax_after_abnormals]" onblur="blurProfitBeforeTaxAfterAbnormal(' +
        index +
        ')" id="profit_before_tax_after_abnormals_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][tax_benefit_expense]" onchange="changeTaxBenefitExpense(' +
        index +
        ')"  id="tax_benefit_expense_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][profit_after_tax]" readonly onblur="changeProfitAfterTax(' +
        index +
        ')" id="profit_after_tax_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][distribution_or_dividends]" onchange="changeDistributionOrDividends(' +
        index +
        ')" id="distribution_or_dividends_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][other_post_tax_items_gains_losses]" onchange="changeOtherPostTaxItemsGainsLosses(' +
        index +
        ')" id="other_post_tax_items_gains_losses_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][profit_after_tax_distributions]" readonly id="profit_after_tax_distributions_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-2 bg-primary text-light">Balance Sheet Assets</td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset += '<td class="p-2 bg-primary text-light">Assets</td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][cash]" onchange="changeCash(' +
        index +
        ')" id="cash_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][trade_debtors]" onchange="changeTradeDebtors(' +
        index +
        ')" id="trade_debtors_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][total_inventories]" onchange="changetotalInventories(' +
        index +
        ')" id="total_inventories_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][loans_to_related_parties]" onchange="changeLoanToRelatedParties1(' +
        index +
        ')" id="loans_to_related_parties_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][other_current_assets]" onchange="changeOtherCurrentAssets(' +
        index +
        ')" id="other_current_assets_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][total_current_assets]" readonly onblur="blurTotalCurrentAssets(' +
        index +
        ')" id="total_current_assets_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][fixed_assets]" onchange="changeFixedAssets(' +
        index +
        ')" id="fixed_assets_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][net_intangibles]" onchange="changeNetIntangibles(' +
        index +
        ')" id="net_intangibles_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][loan_to_related_parties2]" onchange="changeLoanToRelatedParties2(' +
        index +
        ')" id="loan_to_related_parties2_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][other_non_current_assets]" onchange="changeOtherNonCurrentAssets(' +
        index +
        ')" id="other_non_current_assets_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][total_non_current_assets]" readonly onblur="blurTotalNonCurrentAssets(' +
        index +
        ')" id="total_non_current_assets_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][total_assets]" readonly onblur="blurTotalAssets(' +
        index +
        ')" id="total_assets_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset += '<td class="p-2 bg-primary text-light">Liabilities</td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][total_creditors]" onchange="changeTradeCreditors(' +
        index +
        ')" id="total_creditors_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][interes_bearing_debt]" onchange="changeInterestBearingDebt1(' +
        index +
        ')" id="interes_bearing_debt_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][loan_from_related_parties]" onchange="changeLoanFromRelatedParties1(' +
        index +
        ')" id="loan_from_related_parties_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][other_current_liabilities]" onchange="changeOtherCurrentLiabilities(' +
        index +
        ')" id="other_current_liabilities_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][total_current_liabilities]" readonly onblur="blurTotalCurrentLiabilities(' +
        index +
        ')" id="total_current_liabilities_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][interest_bearing_debt2]" onchange="changeInterestBearingDebt2(' +
        index +
        ')" id="interest_bearing_debt2_' +
        index +
        '"/> </td>';
    dataset += "</tr>";

    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][loan_from_related_parties_2]" onchange="changeLoanFromRelatedParties2(' +
        index +
        ')" id="loan_from_related_parties_2_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][other_non_current_liabilities]" onchange="changeOtherNonCurrentLiabilities(' +
        index +
        ')" id="other_non_current_liabilities_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][total_non_current_liabilities]" readonly onblur="blurTotalNonCurrentLiabilities(' +
        index +
        ')" id="total_non_current_liabilities_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][total_liabilities]" readonly onblur="blurTotalLiabilities(' +
        index +
        ')" id="total_liabilities_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset += '<td class="p-2 bg-primary text-light">Equity</td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][share_capital]" onchange="changeShareCapital(' +
        index +
        ')" id="share_capital_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][prefence_shares]" onchange="changePrefenceShares(' +
        index +
        ')" id="prefence_shares_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][treasury_shares]" onchange="changeTreasuryShares(' +
        index +
        ')" id="treasury_shares_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][equity_ownerships]" readonly onblur="blurEquityOwnerships(' +
        index +
        ')" id="equity_ownerships_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][total_reserves]" onchange="changeTotalReserves(' +
        index +
        ')" id="total_reserves_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][retained_earnings]" onchange="changeRetainedEarning(' +
        index +
        ')" id="retained_earnings_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][minorty_interest]" onchange="changeMinortyInterest(' +
        index +
        ')" id="minorty_interest_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][total_equity]" readonly onblur="blurTotalEquity(' +
        index +
        ')" id="total_equity_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][balance]" readonly id="balance_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-2 bg-primary text-light">Additional Information</td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][operating_cash_flow]" id="operating_cash_flow_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][contingent_liabilities]" id="contingent_liabilities_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value="0" data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][other_commiments]" id="other_commiments_' +
        index +
        '"/> </td>';
    dataset += "</tr>";
    dataset += "<tr>";
    dataset +=
        '<td class="p-0"><input type="text" value=0 data-parsley-type="number" tabindex="' +
        parseFloat(tabIndex++ + subIndex) +
        '" class="form-control" name="data[' +
        index +
        '][operating_lease_outstandings]" id="operating_lease_outstandings_' +
        index +
        '"/> </td>';
    dataset += "</tr>";

    dataset += "</table>";
    dataset += "</div>";

    $("#input-area").append(dataset);

    getSelectedCompanyData(index);
    index++;
    subIndex += 0.1;
}

// Load Base currency
function getBaseCurrency(col) {
    $.ajax({
        method: "GET",
        url: base_url + "/config/get-base-currency",
        success: function (data) {
            var html = "";
            html += '<option value="">Select Base Currency</option>';
            $.each(data, function (key, value) {
                html += '<option value="' + key + '">' + value + "</option>";
            });
            $("#base_currency_" + col).html(html);
        },
    });
}

//Load Rounding
function getRounding(col) {
    $.ajax({
        method: "GET",
        url: base_url + "/config/get-rounding",
        success: function (data) {
            var html = "";
            html += '<option value="">Select Rounding</option>';
            $.each(data, function (key, value) {
                html += '<option value="' + key + '">' + value + "</option>";
            });
            $("#rounding_" + col).html(html);
        },
    });
}

// Load Quality
function getQuality(col) {
    $.ajax({
        method: "GET",
        url: base_url + "/config/get-quality",
        success: function (data) {
            var html = "";
            html += '<option value="">Select Quality</option>';
            $.each(data, function (key, value) {
                html += '<option value="' + key + '">' + value + "</option>";
            });
            $("#quality_" + col).html(html);
        },
    });
}

// Load Scope
function getScope(col) {
    $.ajax({
        method: "GET",
        url: base_url + "/config/get-scope",
        success: function (data) {
            var html = "";
            html += '<option value="">Select Scope</option>';
            $.each(data, function (key, value) {
                html += '<option value="' + key + '">' + value + "</option>";
            });
            $("#scope_" + col).html(html);
        },
    });
}

// Load Months
function getMonths(col) {
    $.ajax({
        method: "GET",
        url: base_url + "/config/get-months",
        success: function (data) {
            var html = "";
            html += '<option value="">Select Month</option>';
            $.each(data, function (key, value) {
                html += '<option value="' + key + '">' + value + "</option>";
            });
            $("#month_" + col).html(html);
        },
    });
}

// Load Reportind Periods
function getReportingPeriod(col) {
    var max = 24;
    var html = "";
    html += '<option value="">Select Reporting Period</option>';
    for (var x = 1; x <= max; x++) {
        html += '<option value="' + x + '">' + x + "</option>";
    }
    $("#reporting_period_" + col).html(html);
}

function getFinancialYears(col) {
    var max = 2050;
    var html = "";
    html += '<option value="">Select Financial year</option>';
    for (var x = 2000; x <= max; x++) {
        html += '<option value="' + x + '">FY' + x + "</option>";
    }
    $("#financial_year_" + col).html(html);
}

// Load Confidentiality
function getConfidentiality(col) {
    $.ajax({
        method: "GET",
        url: base_url + "/config/get-confidentiality",
        success: function (data) {
            var html = "";
            html += '<option value="">Select Confidentiality</option>';
            for (var x = 0; x < data.length; x++) {
                html +=
                    '<option value="' +
                    data[x].id +
                    '">' +
                    data[x].name +
                    "</option>";
            }
            console.log("sss", html);
            $("#confidentiality_record_" + col).html(html);
        },
    });
}

/**
 * Remove Year Coloumn
 * @param index
 */
function removeMe(index) {
    $("#coloumn_" + index).remove();
    index--;
}
