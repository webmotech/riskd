/**
 * Calculate Gross Profit
 * @param index
 */
function calculateGrossProfit(index) {
    var sales = $("#sales_" + index).val();
    var costOfSales = $("#cost_of_sales_" + index).val();
    var GrossProfit = parseFloat(sales) - parseFloat(costOfSales);
    $("#gross_profit_" + index).val(GrossProfit);
}

/**
 * When change the interest Expenses will trigger this function
 * @param index
 */
function changeInterestExpense(index) {
    calculateEbit(index);
    calculateEbitda(index);
    calculateNormalizedEbitda(index);
    calculateProfitBeforeTax(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * * When change the Tax Benefit Expenses will trigger this function
 * @param index
 */
function changeTaxBenefitExpense(index) {
    calculateEbit(index);
    calculateEbitda(index);
    calculateNormalizedEbitda(index);
    calculateProfitAfterTax(index);
}

/**
 * When change the Profit After Tax, will trigger this function
 * @param index
 */
function changeProfitAfterTax(index) {
    calculateEbit(index);
    calculateEbitda(index);
    calculateNormalizedEbitda(index);
    calculateProfitAfterTaxDestribution(index);
}

/**
 * When change the Depreciation, will trigger this function
 * @param index
 */
function changeDepreciation(index) {
    calculateEbitda(index);
    calculateNormalizedEbitda(index);
    calculateProfitBeforeTax(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * When change the Amortisation, will trigger this function
 * @param index
 */
function changeAmortisation(index) {
    calculateEbitda(index);
    calculateNormalizedEbitda(index);
    calculateProfitBeforeTax(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * When change the Impairment, will trigger this function
 * @param index
 */
function changeImpairment(index) {
    calculateNormalizedEbitda(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * When change the Non ReCurrent Gain Losses, will trigger this function
 * @param index
 */
function changeNonRecurrentGainLosses(index) {
    calculateNormalizedEbitda(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * When Blur the Gross Profit, will trigger this function
 * @param index
 */
function blurGrossProfit(index) {
    calculateProfitBeforeTax(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * When change Other Income, will trigger this function
 * @param index
 */
function changeOtherIncome(index) {
    calculateProfitBeforeTax(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * When change Operating Lease Expenses, will trigger this function
 * @param index
 */
function changeOperatingLeaseExpense(index) {
    calculateProfitBeforeTax(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * When change Finance Lease Hire Purchase Charges, will trigger this function
 * @param index
 */
function changeFinanceLeaseHirePurchaseCharges(index) {
    calculateProfitBeforeTax(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * When change Other Gain Losses, will trigger this function
 * @param index
 */
function changeOtherGainLosses(index) {
    calculateProfitBeforeTax(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * When change Other Expense, will trigger this function
 * @param index
 */
function changeOtherExpense(index) {
    calculateProfitBeforeTax(index);
    profitBeforeTaxAfterAbnormal(index);
}

/**
 * When blur Profit Before Tax after abnormal, will trigger this function
 * @param index
 */
function blurProfitBeforeTaxAfterAbnormal(index) {
    calculateProfitAfterTax(index);
}

/**
 * When change Distribution or Dividends, will trigger this function
 * @param index
 */
function changeDistributionOrDividends(index) {
    calculateProfitAfterTaxDestribution(index);
}

/**
 * When change Other Post Tax Items Gain Losses, will trigger this function
 * @param index
 */
function changeOtherPostTaxItemsGainsLosses(index) {
    calculateProfitAfterTaxDestribution(index);
}

/**
 * When Change Cash, will trigger this function
 * @param index
 */
function changeCash(index) {
    calculateTotalCurrentAssets(index);
}

/**
 * When change Trade Debtors, will trigger this function
 * @param index
 */
function changeTradeDebtors(index) {
    calculateTotalCurrentAssets(index);
}

/**
 * When Change Total Inventories, will trigger this function
 * @param index
 */
function changetotalInventories(index) {
    calculateTotalCurrentAssets(index);
}

/**
 * When Change Loan to Related Parties 1, will trigger this function
 * @param index
 */
function changeLoanToRelatedParties1(index) {
    calculateTotalCurrentAssets(index);
}

/**
 * When Change Other Current Assets, will trigger this function
 * @param index
 */
function changeOtherCurrentAssets(index) {
    calculateTotalCurrentAssets(index);
}

/**
 * When change Fixed Assets will trigger this function
 * @param index
 */
function changeFixedAssets(index) {
    calculateTotalNonCurrentAssets(index);
}

/**
 * When Change Net Intangibles, will trigger this function
 * @param index
 */
function changeNetIntangibles(index) {
    calculateTotalNonCurrentAssets(index);
}

/**
 * When Change Loan To Related Parties 2, will trigger this function
 * @param index
 */
function changeLoanToRelatedParties2(index) {
    calculateTotalNonCurrentAssets(index);
}

/**
 * When Change Other Non Current Assets, will trigger this function
 * @param index
 */
function changeOtherNonCurrentAssets(index) {
    calculateTotalNonCurrentAssets(index);
}

/**
 * When blur total Current Asssets , will trigger this function
 * @param index
 */
function blurTotalCurrentAssets(index) {
    calculateTotalAssets(index);
}

/**
 * When blur Total Non Current Assets, will trigger this function
 * @param index
 */
function blurTotalNonCurrentAssets(index) {
    calculateTotalAssets(index);
}

/**
 * When change Trade Creditors, will trigger this function
 * @param index
 */
function changeTradeCreditors(index) {
    calculateTotalCurrentLiabilities(index);
}

/**
 * When Change Interest Bearing Debt 1, will trigger this function
 * @param index
 */
function changeInterestBearingDebt1(index) {
    calculateTotalCurrentLiabilities(index);
}

/**
 * When change Loan from related parties 1, will trigger this function
 * @param index
 */
function changeLoanFromRelatedParties1(index) {
    calculateTotalCurrentLiabilities(index);
}

/**
 * When change Other current liabilities, will trigger this function
 * @param index
 */
function changeOtherCurrentLiabilities(index) {
    calculateTotalCurrentLiabilities(index);
}

/**
 * When change Interest bearing Debt 2, will trigger this function
 * @param index
 */
function changeInterestBearingDebt2(index) {
    calculateTotalNonCurrentLiabilities(index);
}

/**
 * When change Loan From Related Parties 2, will trigger this function
 * @param index
 */
function changeLoanFromRelatedParties2(index) {
    calculateTotalNonCurrentLiabilities(index);
}

/**
 * When change Other Non Current Liabilities, will trigger this function
 * @param index
 */
function changeOtherNonCurrentLiabilities(index) {
    calculateTotalNonCurrentLiabilities(index);
}

/**
 * When Blur Total Current Liabilities, will trigger this function
 * @param index
 */
function blurTotalCurrentLiabilities(index) {
    calculateTotalLiabilities(index);
}

/**
 * When Blur Total Non Current Liabilities, will trigger this function
 * @param index
 */
function blurTotalNonCurrentLiabilities(index) {
    calculateTotalLiabilities(index);
}

/**
 * When Change Share capital, will trigger this function
 * @param index
 */
function changeShareCapital(index) {
    calculateEquityOwnerships(index);
}

/**
 * When change Prefence Shares , will trigger this function
 * @param index
 */
function changePrefenceShares(index) {
    calculateEquityOwnerships(index);
}

/**
 * When Change Threasury Shares , will trigger this function
 * @param index
 */
function changeTreasuryShares(index) {
    calculateEquityOwnerships(index);
}

/**
 * When Blur Equity Ownerships, will trigger this function
 * @param index
 */
function blurEquityOwnerships(index) {
    calculateTotalEquity(index);
}

/**
 * When Change Total Reserves, will trigger this function
 * @param index
 */
function changeTotalReserves(index) {
    calculateTotalEquity(index);
}

/**
 * When change Retained Earning, will trigger this function
 * @param index
 */
function changeRetainedEarning(index) {
    calculateTotalEquity(index);
}

/**
 * When Change minorty interest, will trigger this function
 * @param index
 */
function changeMinortyInterest(index) {
    calculateTotalEquity(index);
}

/**
 * When blur total Assets, will trigger this function
 * @param index
 */
function blurTotalAssets(index) {
    calculateBalance(index);
}

/**
 * When blur total liabilities, will trigger this function
 * @param index
 */
function blurTotalLiabilities(index) {
    calculateBalance(index);
}

/**
 * When Blur Total Equity, will trigger this function
 * @param index
 */
function blurTotalEquity(index) {
    calculateBalance(index);
}

//Calculation Function Below

/**
 * Calculate Ebit
 * @param index
 */
function calculateEbit(index) {
    var interestExpense = $("#interest_expense_gross_" + index).val();
    var taxBenefitExpense = $("#tax_benefit_expense_" + index).val();
    var profitAfterTax = $("#profit_after_tax_" + index).val();

    $("#ebit_" + index).val(
        parseFloat(profitAfterTax) -
            parseFloat(taxBenefitExpense) +
            parseFloat(interestExpense)
    );
}

/**
 * Calculate Ebitda
 * @param index
 */
function calculateEbitda(index) {
    var profitAfterTax = $("#profit_after_tax_" + index).val();
    var taxBenefitExpense = $("#tax_benefit_expense_" + index).val();
    var interestExpense = $("#interest_expense_gross_" + index).val();
    var depriciation = $("#depreciation_" + index).val();
    var amortisation = $("#amortisation_" + index).val();

    var ebitda =
        parseFloat(profitAfterTax) -
        parseFloat(taxBenefitExpense) +
        parseFloat(interestExpense) +
        parseFloat(depriciation) +
        parseFloat(amortisation);
    $("#ebitda_" + index).val(ebitda);
}

/**
 * Calculate Normalized Ebitda
 * @param index
 */
function calculateNormalizedEbitda(index) {
    var profitAfterTax = $("#profit_after_tax_" + index).val();
    var taxBenefitExpense = $("#tax_benefit_expense_" + index).val();
    var interestExpense = $("#interest_expense_gross_" + index).val();
    var depriciation = $("#depreciation_" + index).val();
    var amortisation = $("#amortisation_" + index).val();
    var impairment = $("#impairment_" + index).val();
    var nonRecurringGain = $("#non_recurring_gains_losses_" + index).val();

    var normalizedEbitda =
        parseFloat(profitAfterTax) -
        parseFloat(taxBenefitExpense) +
        parseFloat(interestExpense) +
        parseFloat(depriciation) +
        parseFloat(amortisation) +
        parseFloat(impairment) -
        parseFloat(nonRecurringGain);
    $("#normalized_ebitda_" + index).val(normalizedEbitda);
}

/**
 * Calculate Profit Before Tax Before Abnormal
 * @param index
 */
function calculateProfitBeforeTax(index) {
    var grossProfit = $("#gross_profit_" + index).val();
    var otherIncome = $("#other_income_" + index).val();
    var depriciation = $("#depreciation_" + index).val();
    var amortisation = $("#amortisation_" + index).val();
    var interestExpense = $("#interest_expense_gross_" + index).val();
    var operatingLeaseExpense = $("#operating_lease_expense_" + index).val();
    var financeLeaseHirePurchase = $(
        "#finance_lease_hire_purchase_charges_" + index
    ).val();
    var otherGainLosses = $("#other_gains_losses_" + index).val();
    var otherExpenses = $("#other_expenses_" + index).val();

    var profitBeforeTax =
        parseFloat(grossProfit) +
        parseFloat(otherIncome) -
        parseFloat(depriciation) -
        parseFloat(amortisation) -
        parseFloat(interestExpense) -
        parseFloat(operatingLeaseExpense) -
        parseFloat(financeLeaseHirePurchase) +
        parseFloat(otherGainLosses) -
        parseFloat(otherExpenses);
    $("#profit_before_tax_" + index).val(profitBeforeTax);
}

/**
 * Calculate Profit Before Tax After Abnormal
 * @param index
 */
function profitBeforeTaxAfterAbnormal(index) {
    var grossProfit = $("#gross_profit_" + index).val();
    var otherIncome = $("#other_income_" + index).val();
    var depriciation = $("#depreciation_" + index).val();
    var amortisation = $("#amortisation_" + index).val();
    var interestExpense = $("#interest_expense_gross_" + index).val();
    var operatingLeaseExpense = $("#operating_lease_expense_" + index).val();
    var financeLeaseHirePurchase = $(
        "#finance_lease_hire_purchase_charges_" + index
    ).val();
    var otherGainLosses = $("#other_gains_losses_" + index).val();
    var otherExpenses = $("#other_expenses_" + index).val();
    var impairment = $("#impairment_" + index).val();
    var nonRecurringGain = $("#non_recurring_gains_losses_" + index).val();

    var profitBeforeTaxAfterAbnormal =
        parseFloat(grossProfit) +
        parseFloat(otherIncome) -
        parseFloat(depriciation) -
        parseFloat(amortisation) -
        parseFloat(impairment) -
        parseFloat(interestExpense) -
        parseFloat(operatingLeaseExpense) -
        parseFloat(financeLeaseHirePurchase) -
        parseFloat(otherExpenses) +
        parseFloat(otherGainLosses) +
        parseFloat(nonRecurringGain);
    $("#profit_before_tax_after_abnormals_" + index).val(
        profitBeforeTaxAfterAbnormal
    );
}

/**
 * calculate Profit after tax
 * @param index
 */
function calculateProfitAfterTax(index) {
    var profitBeforeTaxAfterAbnormal = $(
        "#profit_before_tax_after_abnormals_" + index
    ).val();
    var taxBenefitExpense = $("#tax_benefit_expense_" + index).val();

    var profitAfterTax =
        parseFloat(profitBeforeTaxAfterAbnormal) +
        parseFloat(taxBenefitExpense);

    $("#profit_after_tax_" + index).val(profitAfterTax);
}

/**
 * calculate Profit after tax Destribution
 * @param index
 */
function calculateProfitAfterTaxDestribution(index) {
    var profitAfterTax = $("#profit_after_tax_" + index).val();
    var distributionDividends = $("#distribution_or_dividends_" + index).val();
    var otherPostTaxItemGain = $(
        "#other_post_tax_items_gains_losses_" + index
    ).val();

    var profitAfterTaxDestribution =
        (parseFloat(profitAfterTax) - parseFloat(distributionDividends)) + parseFloat(otherPostTaxItemGain);
    $("#profit_after_tax_distributions_" + index).val(
        profitAfterTaxDestribution
    );
}

/**
 * Calculate Total Current Assets
 * @param index
 */
function calculateTotalCurrentAssets(index) {
    var cash = $("#cash_" + index).val();
    var tradeDebtors = $("#trade_debtors_" + index).val();
    var totalInventories = $("#total_inventories_" + index).val();
    var loanToRelatedParties1 = $("#loans_to_related_parties_" + index).val();
    var otherCurrentAssets = $("#other_current_assets_" + index).val();

    var totalCurrentAssets =
        parseFloat(cash) +
        parseFloat(tradeDebtors) +
        parseFloat(totalInventories) +
        parseFloat(loanToRelatedParties1) +
        parseFloat(otherCurrentAssets);
    $("#total_current_assets_" + index).val(totalCurrentAssets);
}

/**
 * Calculate Total Non Current Assets
 * @param index
 */
function calculateTotalNonCurrentAssets(index) {
    var fixedAssets = $("#fixed_assets_" + index).val();
    var netIntangible = $("#net_intangibles_" + index).val();
    var loanToRelatedParties2 = $("#loan_to_related_parties2_" + index).val();
    var otherNonCurrentAssets = $("#other_non_current_assets_" + index).val();

    var TotalNonCurrentAssets =
        parseFloat(fixedAssets) +
        parseFloat(netIntangible) +
        parseFloat(loanToRelatedParties2) +
        parseFloat(otherNonCurrentAssets);
    $("#total_non_current_assets_" + index).val(TotalNonCurrentAssets);
}

/**
 * Calculate Total Assets
 * @param index
 */
function calculateTotalAssets(index) {
    var totalCurrentAssets = $("#total_current_assets_" + index).val();
    var totalNonCurrentAssets = $("#total_non_current_assets_" + index).val();

    var TotalAssets =
        parseFloat(totalCurrentAssets) + parseFloat(totalNonCurrentAssets);
    $("#total_assets_" + index).val(TotalAssets);
}

/**
 * Calculate Total current Liabilities
 * @param index
 */
function calculateTotalCurrentLiabilities(index) {
    var tradeCreditors = $("#total_creditors_" + index).val();
    var interestBearingDebt1 = $("#interes_bearing_debt_" + index).val();
    var loanFromRelatedParties1 = $(
        "#loan_from_related_parties_" + index
    ).val();
    var otherCurrentLiabilities = $(
        "#other_current_liabilities_" + index
    ).val();

    var totalCurrentLiabilities =
        parseFloat(tradeCreditors) +
        parseFloat(interestBearingDebt1) +
        parseFloat(loanFromRelatedParties1) +
        parseFloat(otherCurrentLiabilities);
    $("#total_current_liabilities_" + index).val(totalCurrentLiabilities);
}

/**
 * Calculate Total non Current Liabilities
 * @param index
 */
function calculateTotalNonCurrentLiabilities(index) {
    var interestBearingDebt2 = $("#interest_bearing_debt2_" + index).val();
    var loanFromRelatedParties2 = $(
        "#loan_from_related_parties_2_" + index
    ).val();
    var otherNonCurrentLiabilities = $(
        "#other_non_current_liabilities_" + index
    ).val();

    var totalNonCurrentLiabilities =
        parseFloat(interestBearingDebt2) +
        parseFloat(loanFromRelatedParties2) +
        parseFloat(otherNonCurrentLiabilities);
    $("#total_non_current_liabilities_" + index).val(
        totalNonCurrentLiabilities
    );
}

/**
 * Calculate Total Liabilities
 * @param index
 */
function calculateTotalLiabilities(index) {
    var totalCurrentLiabilities = $(
        "#total_current_liabilities_" + index
    ).val();
    var totalNonCurrentLiabilities = $(
        "#total_non_current_liabilities_" + index
    ).val();

    var totalLiabilities =
        parseFloat(totalCurrentLiabilities) +
        parseFloat(totalNonCurrentLiabilities);
    $("#total_liabilities_" + index).val(totalLiabilities);
}

/**
 * Calculate Equity Ownerships
 * @param index
 */
function calculateEquityOwnerships(index) {
    var shareCapital = $("#share_capital_" + index).val();
    var preferenceShares = $("#prefence_shares_" + index).val();
    var treasuryShares = $("#treasury_shares_" + index).val();

    var equityOwnership =
        parseFloat(shareCapital) +
        parseFloat(preferenceShares) +
        parseFloat(treasuryShares);
    $("#equity_ownerships_" + index).val(equityOwnership);
}

/**
 * calculate Total Equity
 * @param index
 */
function calculateTotalEquity(index) {
    var equityOwnerships = $("#equity_ownerships_" + index).val();
    var totalReserves = $("#total_reserves_" + index).val();
    var retainedEarning = $("#retained_earnings_" + index).val();
    var minortyInterest = $("#minorty_interest_" + index).val();

    var totalEquity =
        parseFloat(equityOwnerships) +
        parseFloat(totalReserves) +
        parseFloat(retainedEarning) +
        parseFloat(minortyInterest);
    $("#total_equity_" + index).val(totalEquity);
}

/**
 * Calculate Balance
 * @param index
 */
function calculateBalance(index) {
    var totalAssets = $("#total_assets_" + index).val();
    var totalLiabilities = $("#total_liabilities_" + index).val();
    var totalEquity = $("#total_equity_" + index).val();

    var balance =
        parseFloat(totalAssets) -
        (parseFloat(totalLiabilities) + parseFloat(totalEquity));
    $("#balance_" + index).val(balance);
}
